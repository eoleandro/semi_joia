<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 20/02/17
 * Time: 15:20
 */

namespace App\Action\Usuario;


use App\Form\LoginForm;
use App\Service\Authentication\Adapter;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;

class UsuarioLogoutAction
{

    private $router;
    private $authenticationService ;

    public function __construct(
                                RouterInterface $router,
                                \Zend\Authentication\AuthenticationService $authenticationService
    )
    {
        $this->router  = $router;
        $this->authenticationService = $authenticationService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        if($this->authenticationService->hasIdentity()){
            $this->authenticationService->clearIdentity();
        }

        $uri = $this->router->generateUri('usuario.auth');
        return new RedirectResponse($uri);
    }
}