<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 16/02/17
 * Time: 15:03
 */

namespace App\Action\Usuario;


use App\Entity\Grupo;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template;

class GrupoListPageAction
{
    private $template;

    private $entityManager;

    public function __construct( Template\TemplateRendererInterface $template = null, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $repository = $this->entityManager->getRepository(Grupo::class);
        $grupos = $repository->findAll();

        return new HtmlResponse($this->template->render('app::usuario/grupo/list', [
            'grupos' => $grupos
        ]));
    }
}
