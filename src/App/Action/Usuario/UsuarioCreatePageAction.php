<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 18/02/17
 * Time: 18:28
 */

namespace App\Action\Usuario;


use App\Entity\Grupo;
use App\Entity\Usuario;
use App\Filter\UploadImageFilter;
use App\Form\UsuarioForm;
use App\Service\ReferencesDatasService;
use App\Service\ResizeImageService;
use App\Service\UploadFileService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class UsuarioCreatePageAction
{
    private $template;

    private $entityManager;

    private $router;

    private $referencesDatasService;

    private $uploadFileService;

    private $resizeImageService;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager,
        ReferencesDatasService $referencesDatasService,
        UploadFileService $uploadFileService,
        ResizeImageService $resizeImageService)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->referencesDatasService = $referencesDatasService;
        $this->uploadFileService = $uploadFileService;
        $this->resizeImageService = $resizeImageService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $form = new UsuarioForm();
        $flash = $request->getAttribute('flash');
        $validatorUploadImage = new UploadImageFilter();
        $form->setHydrator(new ClassMethods());

        $usuario = new Usuario();
        $form->bind($usuario);
        $repository = $this->entityManager->getRepository(Usuario::class);
        $repositoryGrupo = $this->entityManager->getRepository(Grupo::class);
        $grupos = $repositoryGrupo->fetchPairs();
        $form->get('grupo')->setAttribute('options', ["" => "Selecione"] + $grupos);

        $form->get('ativo')->setOptions( array(1 => "Sim", 0 => "Não"));

        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            if(!empty($data['login'])){
                $UserLoginAlreadyExists = $repository->findByLogin( $data['login']);
                if(!empty($UserLoginAlreadyExists)){
                    $flash->addMessage('danger', "Este login não está disponível, escolha outro login!");
                    $uri = $this->router->generateUri('usuario.create');
                    return new RedirectResponse($uri);
                }
            }
            $data = $this->referencesDatasService->treatRefenceDataArrayEntity($data, Usuario::class);
            $form->setInputFilter( new \App\Filter\UsuarioFilter());

            $form->setData($data);

            if($form->isValid()){
                $entity = $form->getData();
                $this->entityManager->persist($entity);
                if(!empty($_FILES) && $validatorUploadImage->isValid("foto")){
                    $usuario->setFoto($this->uploadFileService->UploadFile("foto"));
                    $foto = $this->uploadFileService->getFileNameUploaded();
                    $this->resizeImageService->resizeImage($foto,$this->uploadFileService->getPathFileUpload(), "140_", 140,140);
                }
                $this->entityManager->flush();

                $uri = $this->router->generateUri('usuario.list');
                return new RedirectResponse($uri);
            }
        }

        return new HtmlResponse($this->template->render('app::usuario/usuario/create', [
            'form' => $form,
            'grupos' => $grupos,
            'flashMessages' => $flash->getMessages()
        ]));
    }
}