<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 18/02/17
 * Time: 19:11
 */

namespace App\Action\Usuario;

use App\Entity\Usuario;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template;

class UsuarioListPageAction
{
    private $template;

    private $entityManager;

    public function __construct( Template\TemplateRendererInterface $template = null, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $repository = $this->entityManager->getRepository(Usuario::class);

        $grupos = $repository->findAll();

        return new HtmlResponse($this->template->render('app::usuario/usuario/list', [
            'grupos' => $grupos
        ]));
    }
}