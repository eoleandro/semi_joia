<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 16/02/17
 * Time: 16:41
 */

namespace App\Action\Usuario;


use App\Entity\Grupo;
use App\Entity\PermissaoAcl;
use App\Entity\RecursoSistema;
use App\Service\RecursoSistemaService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;

class PermissaoAclGerenciarPageAction
{
    private $template;

    private $entityManager;

    private $router;

    private $recursoSistemaService;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager,
        RecursoSistemaService $recursoSistemaService
    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->recursoSistemaService = $recursoSistemaService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $repository = $this->entityManager->getRepository(Grupo::class);
        $infoGroup  = $repository->find($request->getAttribute('id'));

        if($infoGroup){
            $permissoes = $this->recursoSistemaService->carregarPermissoesGrupo($request->getAttribute('id'));
        }else{
            $permissoes = null;
        }

        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $idGrupo = $request->getAttribute('id');
            $recursoSistema = new RecursoSistema();
            $permissaoAcl   = new PermissaoAcl();
            $this->recursoSistemaService->gravarPermissoesGrupo( $idGrupo, $data, $recursoSistema, $permissaoAcl);

                $uri = $this->router->generateUri('product.list');
                return new RedirectResponse($uri);


        }
        return new HtmlResponse($this->template->render('app::usuario/permissoes-grupo/gerenciar', [
            'permissoes' => $permissoes,
            'infoGrupo'  => $infoGroup
        ]));


    }
}