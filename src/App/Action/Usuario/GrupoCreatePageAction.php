<?php
namespace App\Action\Usuario;

use App\Entity\Grupo;
use App\Filter\GrupoInputFilter;
use App\Form\GrupoForm;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class GrupoCreatePageAction
{
    private $template;

    private $entityManager;

    private $router;

    public function __construct( Template\TemplateRendererInterface $template = null, RouterInterface $router,  EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $form = new GrupoForm();
        $form->setHydrator(new ClassMethods());
        $product = new Grupo();
        $form->bind($product);
        $form->setInputFilter(new GrupoInputFilter());
        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->setData($data);
            if($form->isValid()){
                $entity = $form->getData();
                $this->entityManager->persist($entity);
                $this->entityManager->flush();

                $uri = $this->router->generateUri('grupo.list');
                return new RedirectResponse($uri);
            }
        }

        return new HtmlResponse($this->template->render('app::usuario/grupo/create', [
            'form' => $form
        ]));
    }
}