<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 16/02/17
 * Time: 16:43
 */

namespace App\Action\Usuario\Factory;


use App\Action\Usuario\PermissaoAclGerenciarPageAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use App\Service\RecursoSistemaService;

class PermissaoAclGerenciarPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        $recursoSistemaService = $container->get(RecursoSistemaService::class);

        return new PermissaoAclGerenciarPageAction( $template, $router, $entityManager, $recursoSistemaService);
    }
}