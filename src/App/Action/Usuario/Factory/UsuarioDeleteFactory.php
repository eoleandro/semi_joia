<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 18/02/17
 * Time: 19:14
 */

namespace App\Action\Usuario\Factory;

use App\Action\Usuario\UsuarioDeleteAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;

class UsuarioDeleteFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new UsuarioDeleteAction( $router, $entityManager);
    }
}