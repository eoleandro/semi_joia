<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 20/02/17
 * Time: 15:19
 */

namespace App\Action\Usuario\Factory;


use App\Action\Usuario\UsuarioLogoutAction;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Authentication\AuthenticationService;

class UsuarioLogoutFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $router   = $container->get(RouterInterface::class);
        $authAdapterService = $container->get(AuthenticationService::class);

        return new UsuarioLogoutAction( $router, $authAdapterService);
    }
}