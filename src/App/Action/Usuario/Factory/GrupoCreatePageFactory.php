<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 16/02/17
 * Time: 14:58
 */

namespace App\Action\Usuario\Factory;


use App\Action\Usuario\GrupoCreatePageAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class GrupoCreatePageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new GrupoCreatePageAction( $template, $router, $entityManager);
    }
}