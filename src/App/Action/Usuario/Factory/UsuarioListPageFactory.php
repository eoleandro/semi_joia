<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 18/02/17
 * Time: 19:11
 */

namespace App\Action\Usuario\Factory;

use App\Action\Usuario\UsuarioListPageAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class UsuarioListPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new UsuarioListPageAction( $template, $entityManager);
    }
}