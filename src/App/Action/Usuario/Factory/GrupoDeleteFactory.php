<?php

namespace App\Action\Usuario\Factory;


use App\Action\Usuario\GrupoDeleteAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;


class GrupoDeleteFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new GrupoDeleteAction( $router, $entityManager);
    }
}