<?php
namespace App\Action\Usuario\Factory;

use App\Action\Usuario\GrupoListPageAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class GrupoListPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new GrupoListPageAction( $template, $entityManager);
    }
}