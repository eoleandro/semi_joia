<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10/04/17
 * Time: 10:27
 */

namespace App\Action\Usuario\Factory;


use App\Action\Usuario\PerfilUsuarioPageAction;
use App\Service\ReferencesDatasService;
use App\Service\ResizeImageService;
use App\Service\UploadFileService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Authentication\AuthenticationService;

class PerfilUsuarioPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        $referencesDatasService = $container->get(ReferencesDatasService::class);
        $uploadFileService = $container->get(UploadFileService::class);
        $resizeImageService = $container->get(ResizeImageService::class);
        $authAdapterService = $container->get(AuthenticationService::class);
        return new PerfilUsuarioPageAction( $template, $router, $entityManager, $referencesDatasService,
            $uploadFileService, $resizeImageService, $authAdapterService);
    }
}