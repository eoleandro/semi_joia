<?php

namespace App\Action\Usuario\Factory;

use App\Action\Usuario\UsuarioUpdatePageAction;
use App\Service\ReferencesDatasService;
use App\Service\ResizeImageService;
use App\Service\UploadFileService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class UsuarioUpdatePageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        $referencesDatasService = $container->get(ReferencesDatasService::class);
        $uploadFileService = $container->get(UploadFileService::class);
        $resizeImageService = $container->get(ResizeImageService::class);
        return new UsuarioUpdatePageAction( $template, $router, $entityManager, $referencesDatasService,
            $uploadFileService, $resizeImageService);
    }
}