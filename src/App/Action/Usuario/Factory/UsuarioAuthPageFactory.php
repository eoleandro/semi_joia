<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 20/02/17
 * Time: 11:27
 */

namespace App\Action\Usuario\Factory;


use App\Action\Usuario\UsuarioAuthPageAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class UsuarioAuthPageFactory
{

    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        $authAdapterService = $container->get(AuthenticationService::class);

        return new UsuarioAuthPageAction( $template, $router, $entityManager, $authAdapterService);
    }
}