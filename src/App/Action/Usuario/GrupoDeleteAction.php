<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 16/02/17
 * Time: 15:05
 */

namespace App\Action\Usuario;


use App\Entity\Grupo;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;

class GrupoDeleteAction
{

    private $entityManager;

    private $router;

    public function __construct(  RouterInterface $router,  EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $repository = $this->entityManager->getRepository(Grupo::class);
        $product = $repository->find($request->getAttribute('id'));
        $this->entityManager->remove($product);
        $this->entityManager->flush();
        $uri = $this->router->generateUri('grupo.list');
        return new RedirectResponse($uri);

    }
}