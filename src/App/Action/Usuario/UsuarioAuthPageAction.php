<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 20/02/17
 * Time: 11:07
 */

namespace App\Action\Usuario;


use App\Form\LoginForm;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Doctrine\ORM\EntityManager;

class UsuarioAuthPageAction
{
    private $template;

    private $router;

    private $entityManager;

    private $authenticationService;


    public function __construct(Template\TemplateRendererInterface $template = null,
                                RouterInterface $router,
                                EntityManager $entityManager,
                                \Zend\Authentication\AuthenticationService $authenticationService
)
    {
        $this->template = $template;
        $this->router  = $router;
        $this->entityManager = $entityManager;
        $this->authenticationService = $authenticationService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $form = new LoginForm();
        $adapter = $this->authenticationService->getAdapter();
        $flash = $request->getAttribute('flash');


        if($request->getMethod() == "POST") {
            $data = $request->getParsedBody();

            $adapter->setLogin($data['login'])->setPassword($data['senha']);
            $result = $adapter->authenticate();
            if($result->isValid()){
                $dataUser = $result->getIdentity();
                $this->authenticationService->getStorage()->write( ['id' => $dataUser->getId(),
                    'nome' => $dataUser->getNome(), 'grupo' => $dataUser->getGrupo()->getId(),
                    'foto' => $dataUser->getFoto(), 'email' => $dataUser->getEmail() ] );

                $uri = $this->router->generateUri('home');
                return new RedirectResponse($uri);

            }else{

                $flash->addMessage('authentication_fail', $result->getMessages()[0]);
                $uri = $this->router->generateUri('usuario.auth');
                return new RedirectResponse($uri);
            }

        }


        return new HtmlResponse($this->template->render('app::usuario/usuario/login', [
            'layout' => 'layout/login',
            'form' => $form,
            'messages' => $flash->getMessage('authentication_fail')
        ]));
    }
}