<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01/04/17
 * Time: 02:14
 */

namespace App\Action\Evento;

use App\Form\Pesquisa\AgendaForm;
use Doctrine\ORM\EntityManager;
use App\Entity\Agenda;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;

class AgendaListPageAction
{
    private $template;

    private $entityManager;

    public function __construct( Template\TemplateRendererInterface $template = null, RouterInterface $router, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $repository = $this->entityManager->getRepository(Agenda::class);
        $form = new AgendaForm();
        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->populateValues($data);
            $agendamentos = $repository->getListaVisitas($data);
        }else{
            $agendamentos = $repository->getListaVisitas();
        }


        return new HtmlResponse($this->template->render('app::evento/agenda/list', [
            'agendamentos' => $agendamentos,
            'flashMessages' => $flash->getMessages(),
            'form' => $form
        ]));
    }
}