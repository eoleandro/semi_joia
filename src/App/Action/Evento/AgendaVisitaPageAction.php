<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15/03/17
 * Time: 22:11
 */

namespace App\Action\Evento;

use App\Entity\Agenda;
use App\Service\Email\EnviaEmailService;
use Doctrine\ORM\EntityManager;
use App\Entity\Usuario;
use App\Entity\Cliente;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Hydrator\ClassMethods;
use Zend\Expressive\Template;


class AgendaVisitaPageAction {

    private $entityManager;

    private $enviaEmailService;

    private $authenticationService;


    public function __construct(
                                Template\TemplateRendererInterface $template = null,
                                EntityManager $entityManager,
                                EnviaEmailService $enviaEmailService,
                                \Zend\Authentication\AuthenticationService $authenticationService)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->enviaEmailService = $enviaEmailService;
        $this->authenticationService = $authenticationService;

    }
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $data = $request->getParsedBody();
        $id = null;
        if($request->getMethod() == "POST"){
            $data['data_hora'] = (new \DateTime( $data['data']));
            $data['usuario'] = $this->entityManager->getReference(Usuario::class, 1);
            $fragmentoEvento = explode(" - ", $data['evento']);
            $idCliente = (int) trim($fragmentoEvento[0]);
            $dataCliente = $this->entityManager->getReference(Cliente::class, $idCliente);
            if($dataCliente){
                $data['cliente'] = $dataCliente;
            }

            $hydrator = new ClassMethods();
            if(empty($data['id'])){
                $agenda = new Agenda();
                $agenda->setStatus("M");
            }else{
                $agenda = $this->entityManager->getReference(Agenda::class, $data['id']);
            }
            $hydrator->hydrate($data, $agenda);
            $this->entityManager->persist($agenda);
            $this->entityManager->flush();
            $id = $agenda->getId();
            $infoUsuario = $this->authenticationService->getIdentity();
            if(!is_null($dataCliente) && !is_null($id) && $agenda->getStatus() == "M"){

                $html = $this->template->render('app::email/evento/agendamento-visita', ['layout' => 'layout/email',
                    'cliente' => $dataCliente,
                    'infoAgendamento' => $agenda,
                    'usuario' => $infoUsuario
                ]);
                if(!empty($dataCliente->getEmail() )){
                    $emailEnviado = $this->enviaEmailService->enviarEmail(['destinatario' => $dataCliente->getEmail(),
                        'mensagem' => $html, 'assunto' => 'Agendamento de visita - Semi joias' ]);
                }
                if(!empty($infoUsuario['email'] )){
                    $emailEnviado = $this->enviaEmailService->enviarEmail(['destinatario' => $infoUsuario['email'],
                        'mensagem' => $html, 'assunto' => 'Agendamento de visita - Semi joias' ]);
                }
            }
        }
        return new JsonResponse(['id'  => $id ]);


    }
}