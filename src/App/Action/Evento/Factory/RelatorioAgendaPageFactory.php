<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01/04/17
 * Time: 03:17
 */

namespace App\Action\Evento\Factory;

use App\Action\Evento\RelatorioAgendaPageAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class RelatorioAgendaPageFactory {

    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new RelatorioAgendaPageAction(
            $template,
            $router,
            $entityManager
        );
    }
}