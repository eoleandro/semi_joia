<?php
namespace App\Action\Evento\Factory;

use App\Action\Evento\AgendaDeleteAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class AgendaDeleteFactory {

    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new AgendaDeleteAction(
            $template,
            $router,
            $entityManager
        );
    }
}