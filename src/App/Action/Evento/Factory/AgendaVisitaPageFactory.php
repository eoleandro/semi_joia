<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15/03/17
 * Time: 22:11
 */

namespace App\Action\Evento\Factory;

use App\Action\Evento\AgendaVisitaPageAction;
use App\Service\Email\EnviaEmailService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class AgendaVisitaPageFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $template = $container->get(TemplateRendererInterface::class);
        $entityManager = $container->get(EntityManager::class);
        $enviaEmailService = $container->get(EnviaEmailService::class);
        $authenticationService = $container->get(\Zend\Authentication\AuthenticationService::class);

        return new AgendaVisitaPageAction($template,$entityManager, $enviaEmailService, $authenticationService);
    }
}