<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01/04/17
 * Time: 02:50
 */

namespace App\Action\Evento;

use Doctrine\ORM\EntityManager;
use App\Entity\Agenda;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;

class AgendaDeleteAction
{
    private $template;

    private $entityManager;

    private  $router;

    public function __construct( Template\TemplateRendererInterface $template = null, RouterInterface $router, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $repository = $this->entityManager->getRepository(Agenda::class);

        $idAgenda = $request->getAttribute('agenda');
        $infoAgenda = $repository->find($idAgenda);
        $uri = $this->router->generateUri('agenda.list');
        if(!$infoAgenda){
            $flash->addMessage('danger', "Estes dados de agendamento não são válidos!");

            return new RedirectResponse($uri);
        }

        try{
            $agenda = $this->entityManager->getReference(Agenda::class, $idAgenda);
            $this->entityManager->remove($agenda);
            $this->entityManager->flush();
            $flash->addMessage('success', "Registro excluído com sucesso!");
        }catch (\Exception $e){
            $flash->addMessage('danger', "Ocorreu algum erro ao excluir agendamento, tente novamente!");
        }
        return new RedirectResponse($uri);
    }
}