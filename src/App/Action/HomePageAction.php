<?php

namespace App\Action;

use App\Entity\Pedido;
use App\Service\AnnotationReaderService;
use App\Service\TesteService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Entity;
use App\Entity\Cliente;
use App\Entity\MovFinanceiro;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template;
use Zend\Expressive\Plates\PlatesRenderer;
use Zend\Expressive\Twig\TwigRenderer;
use Zend\Expressive\ZendView\ZendViewRenderer;
use App\Entity\Agenda;

class HomePageAction
{
    private $router;

    private $template;

    private $entityManager;

    public function __construct(
        Router\RouterInterface $router,
        Template\TemplateRendererInterface $template = null,
        EntityManager $entityManager)
    {
        $this->router   = $router;
        $this->template = $template;
        $this->entityManager = $entityManager;

    }
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $data = [];
        $repository = $this->entityManager->getRepository(Agenda::class);
        $repositoryPedido = $this->entityManager->getRepository(Pedido::class);
        $repositoryMovfin = $this->entityManager->getRepository(MovFinanceiro::class);
        $infoAgenda = $repository->getRegistroAgenda();
        $infoAgenda = $repository->getDataRegistroAgendaJSON($infoAgenda);

        $repositoryCliente = $this->entityManager->getRepository(Cliente::class);
        $infoSuggestionCliente = $repositoryCliente->getClienteJSON();

       // echo "<pre>";
       // var_dump( $infoAgenda); die;

       /* if ($this->router instanceof Router\AuraRouter) {
            $data['routerName'] = 'Aura.Router';
            $data['routerDocs'] = 'http://auraphp.com/packages/2.x/Router.html';
        } elseif ($this->router instanceof Router\FastRouteRouter) {
            $data['routerName'] = 'FastRoute';
            $data['routerDocs'] = 'https://github.com/nikic/FastRoute';
        } elseif ($this->router instanceof Router\ZendRouter) {
            $data['routerName'] = 'Zend Router';
            $data['routerDocs'] = 'http://framework.zend.com/manual/current/en/modules/zend.mvc.routing.html';
        }

        if ($this->template instanceof PlatesRenderer) {
            $data['templateName'] = 'Plates';
            $data['templateDocs'] = 'http://platesphp.com/';
        } elseif ($this->template instanceof TwigRenderer) {
            $data['templateName'] = 'Twig';
            $data['templateDocs'] = 'http://twig.sensiolabs.org/documentation';
        } elseif ($this->template instanceof ZendViewRenderer) {
            $data['templateName'] = 'Zend View';
            $data['templateDocs'] = 'http://framework.zend.com/manual/current/en/modules/zend.view.quick-start.html';
        }

        if (!$this->template) {
            return new JsonResponse([
                'welcome' => 'Congratulations! You have installed the zend-expressive skeleton application.',
                'docsUrl' => 'zend-expressive.readthedocs.org',
            ]);
        }

       */
        $infoPedidosEntregar = $repositoryPedido->getPedidosEntregar();
        $totalPedidosEntregar = !empty($infoPedidosEntregar)? count($infoPedidosEntregar) : 0;
        $infoTotaisContas = $repositoryMovfin->calculaTotaisContas( $repositoryMovfin->getContasPendentesQuitacao());
        $data['infoAgendaJSON']        = $infoAgenda;
        $data['infoSuggestionCliente'] = $infoSuggestionCliente;
        $data['totalVisitasRealizar']  = $repository->getTotalVisitasRealizar();
        $data['totalPedidosEntregar']  = $totalPedidosEntregar;
        $data['infoTotaisContas']  = $infoTotaisContas;

        return new HtmlResponse($this->template->render('app::home-page', $data));
    }
}
