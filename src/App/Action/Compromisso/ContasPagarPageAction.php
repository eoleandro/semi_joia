<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01/04/17
 * Time: 17:49
 */

namespace App\Action\Compromisso;

use App\Entity\FormaPgto;
use App\Entity\PlanoConta;
use App\Form\Financeiro\ContasQuitarForm;
use Doctrine\ORM\EntityManager;
use App\Entity\MovFinanceiro;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;

class ContasPagarPageAction
{
    private $template;

    private $entityManager;

    private $router;

    public function __construct( Template\TemplateRendererInterface $template = null, RouterInterface $router, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $tipoConta = $request->getAttribute('tipo');
        if(empty($tipoConta) || !in_array($tipoConta, ['pagar', 'receber'])){
            $flash->addMessage('danger', "O lançamento contábel não é válido!");
            $uri = $this->router->generateUri('movfinanceiro.list');
            return new RedirectResponse($uri);
        }
       // $classEntityRelationshipFinancial = "\\App\\Entity\\" .(($tipoConta == "pagar")? "MovFinRemessa" : "MovFinPedido");


        //$repository = $this->entityManager->getRepository($classEntityRelationshipFinancial);
        $repository = $this->entityManager->getRepository(MovFinanceiro::class);
        $tipoFinanceiro = $tipoConta == "pagar" ? "D" : "C";
        $form = new  ContasQuitarForm();
        $repoFormaPgto  = $this->entityManager->getRepository(FormaPgto::class);
        $repoPlanoConta = $this->entityManager->getRepository(PlanoConta::class);
        $form->get('formaPgto')->setAttribute('options', ["" => "Selecione"] + $repoFormaPgto->fetchPairs());
        $form->get('planoConta')->setAttribute('options', ["" => "Selecione"] + $repoPlanoConta->fetchPairs());
        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->populateValues($data);
            $data['tipo'] = [$tipoFinanceiro];
            $contas = $repository->getMovFinanceiroVencimento($tipoConta, $data);
        }else{
            $data['tipo'] = [$tipoFinanceiro];
            $data['dataVencimentoDe'] = date('d/m/Y');
            $data['dataVencimentoAte'] = date('d/m/Y');
            $contas = $repository->getMovFinanceiroVencimento($tipoConta, $data);
        }
        //echo "<pre>"; var_dump($contas); die;


        return new HtmlResponse($this->template->render('app::compromisso/financeiro/' . $tipoConta, [
            'contas' => $contas,
            'flashMessages' => $flash->getMessages(),
            'form' => $form
        ]));
    }
}