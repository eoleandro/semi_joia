<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 22/02/17
 * Time: 15:29
 */

namespace App\Action\Estoque;


use App\Entity\Fornecedor;
use App\Entity\Produto;
use App\Filter\ProdutoInputFilter;
use App\Form\Estoque\ProdutoForm;
use App\Service\ReferencesDatasService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class ProdutoUpdatePageAction
{
    private $template;

    private $entityManager;

    private $router;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager,
        ReferencesDatasService $referencesDatasService
    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->referencesDatasService = $referencesDatasService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $repository = $this->entityManager->getRepository(Produto::class);
        $flash = $request->getAttribute('flash');
        $produto = $repository->find($request->getAttribute('id'));


        $form = new ProdutoForm();
        $form->setHydrator(new ClassMethods());
        $form->bind($produto);
        $repository = $this->entityManager->getRepository(Fornecedor::class);
        $form->get('fornecedor')->setAttribute('options', ["" => "Selecione"] + $repository->fetchPairs());



        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $data  = $this->referencesDatasService->treatRefenceDataArrayEntity($data, Produto::class);
            $form->setData($data);
            $form->setInputFilter(new ProdutoInputFilter());
            if($form->isValid()){

                $this->entityManager->flush();
                $flash->addMessage('success', "Registro atualizado com sucesso!");
                $uri = $this->router->generateUri('produto.list');
                return new RedirectResponse($uri);
            }
        }

        $form->get('tipoPedra')->setValue( $produto->getTipoPedra());
        $form->get('corPedra')->setValue( $produto->getCorPedra());
        $form->get('dimensaoPedra')->setValue( $produto->getDimensaoPedra());
        $form->get('fornecedor')->setValue( $produto->getFornecedor()->getId());

        return new HtmlResponse($this->template->render('app::estoque/produto/update', [
            'form' => $form
        ]));
    }
}