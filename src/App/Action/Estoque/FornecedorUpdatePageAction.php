<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 21/02/17
 * Time: 09:10
 */

namespace App\Action\Estoque;


use App\Entity\ContatoFornecedor;
use App\Entity\EnderecoFornecedor;
use App\Entity\Fornecedor;
use App\Filter\FornecedorInputFilter;
use App\Form\Estoque\FornecedorForm;
use App\Service\EnderecoService;
use App\Service\Estoque\PersisteEntidadeRelacionadasService;
use App\Service\Form\PopulateValuesFormService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class FornecedorUpdatePageAction
{
    private $template;

    private $entityManager;

    private $router;

    private $enderecoService;

    private $populateValuesFormService;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,  EntityManager $entityManager,
        EnderecoService $enderecoService,
        PersisteEntidadeRelacionadasService $persistEntityRelatedService,
        PopulateValuesFormService $populateValuesFormService
    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->enderecoService = $enderecoService;
        $this->persistEntityRelatedService = $persistEntityRelatedService;
        $this->populateValuesFormService = $populateValuesFormService;


    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $id = $request->getAttribute('id');
        $repository = $this->entityManager->getRepository(Fornecedor::class);
        $fornecedorData = $repository->getInfoFornecedorForm($id);

        $form = new FornecedorForm();
        $hidrator = new ClassMethods();
        $form->setInputFilter(new FornecedorInputFilter());

        $form->get('UF')->setAttribute('options', ["" => "Selecione"] + $this->enderecoService->getListUf());

        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->setData($data);
            if($form->isValid()){

                $data = $form->getData();

                $fornecedor = $this->entityManager->getReference(Fornecedor::class, $id);
                $fornecedor = $hidrator->hydrate($data, $fornecedor);
                $this->persistEntityRelatedService->setEntityMain($fornecedor);
                $this->persistEntityRelatedService->updateDataEntitiesRelated($data, [
                    ContatoFornecedor::class, EnderecoFornecedor::class
                ], [$fornecedorData['idContatoFornecedor'], $fornecedorData['idEnderecoFornecedor']], $hidrator);
                $flash->addMessage('success', "Registro atualizado com sucesso! ");

                $uri = $this->router->generateUri('fornecedor.list');
                return new RedirectResponse($uri);
            }
        }
        $form = $this->populateValuesFormService->exportValuesKeyArrayToForm($fornecedorData,$form,array() );
        $form->get('ativo')->setValue($fornecedorData['ativo']);

        return new HtmlResponse($this->template->render('app::estoque/fornecedor/update', [
            'form' => $form,
            'id'   => $id,
            'flashMessages' => $flash->getMessages(),
        ]));
    }
}