<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31/03/17
 * Time: 23:23
 */

namespace App\Action\Estoque;


use App\Entity\RemessaFornecedor;
use App\Entity\MovFinRemessa;
use App\Entity\ItemRemessa;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template;

class RemessaShowPageAction
{
    private $template;

    private $entityManager;

    public function __construct( Template\TemplateRendererInterface $template = null, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $idRemessa = $request->getAttribute('remessa');
        $repository = $this->entityManager->getRepository(RemessaFornecedor::class);
        $produtos = $repository->findAll();

        $repoRemessa = $this->entityManager->getRepository(RemessaFornecedor::class);
        $repoParcelas = $this->entityManager->getRepository(MovFinRemessa::class);

        $infoParcelas = $repoParcelas->getParcelasRemessaFornecedor($idRemessa);
        $repositoryItensRemessa = $this->entityManager->getRepository(ItemRemessa::class);

        $infoRemessa = $repoRemessa->find($idRemessa);

        if(!$infoRemessa){
            $flash->addMessage('danger', "A remessa informada não é valido");
            $uri = $this->router->generateUri('remessa.list');
            return new RedirectResponse($uri);
        }
        $itensRemessa = $repositoryItensRemessa->getItensRemessaFornecedor($idRemessa);


        return new HtmlResponse($this->template->render('app::estoque/remessa/show', [
            'infoRemessa' => $infoRemessa,
            'infoParcelas' => $infoParcelas,
            'itensRemessa' => $itensRemessa

        ]));
    }
}