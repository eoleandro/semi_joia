<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 22/02/17
 * Time: 11:08
 */

namespace App\Action\Estoque;


use App\Entity\ContatoFornecedor;
use App\Entity\EnderecoFornecedor;
use App\Entity\Fornecedor;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;

class FornecedorDeleteAction
{

    private $entityManager;

    private $router;

    public function __construct(  RouterInterface $router,  EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $id = $request->getAttribute('id');
        $repository = $this->entityManager->getRepository(Fornecedor::class);
        $fornecedorData = $repository->getInfoFornecedor($id);

        $fornecedorData = array_merge( $fornecedorData[0][0], $fornecedorData[1][0], $fornecedorData[2]);
        if($fornecedorData){

            $contatoFornecedor = $this->entityManager->getReference(ContatoFornecedor::class,
                $fornecedorData['contato_fornecedor_id']);
            $this->entityManager->remove($contatoFornecedor);
            $enderecoFornecedor = $this->entityManager->getReference(EnderecoFornecedor::class,
                $fornecedorData['endereco_fonecedor_id']);
            $this->entityManager->remove($enderecoFornecedor);
            $fornecedor = $this->entityManager->getReference(Fornecedor::class,$id);
            $this->entityManager->remove($fornecedor);
            $this->entityManager->flush();
            $uri = $this->router->generateUri('fornecedor.list');
            return new RedirectResponse($uri);

        }

    }
}