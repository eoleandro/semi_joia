<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 23/02/17
 * Time: 15:24
 */

namespace App\Action\Estoque;

use App\Entity\Fornecedor;
use App\Entity\ItemRemessa;
use App\Entity\Produto;
use App\Entity\RemessaFornecedor;
use App\Entity\Usuario;
use App\Service\Email\EnviaEmailService;
use App\Service\Estoque\PersisteRemessa;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class RemessaItemsAddPageAction
{
    private $template;

    private $entityManager;

    private $router;

    private $authenticationService;

    private $persiteRemessa;

    private $enviaEmailService;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager,
        \Zend\Authentication\AuthenticationService $authenticationService,
       PersisteRemessa $persisteRemessa,
       EnviaEmailService $enviaEmailService
    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->authenticationService = $authenticationService;
        $this->persiteRemessa = $persisteRemessa;
        $this->enviaEmailService = $enviaEmailService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $idFornecedor = $request->getAttribute('fornecedor');
        $repository = $this->entityManager->getRepository(Produto::class);
        $repositoryItemRemessa = $this->entityManager->getRepository(ItemRemessa::class);
        $produtosFornecedor = $repository ->getProdutosFornecedor($idFornecedor, 1);
        $flash = $request->getAttribute('flash');
        if(empty($produtosFornecedor)){
            $flash->addMessage('danger', "Este fornecedor não possui produtos ativos!");
            $uri = $this->router->generateUri('remessa.define_provider');
            return new RedirectResponse($uri);
        }
        $produtosFornecedorJSON = json_encode($repository->getProdutosItemsRemessaJSON($produtosFornecedor));

        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $dataProdutoRemessaAtivo = $repositoryItemRemessa->getItensRemessaFornecedorAtivo($idFornecedor, $data['produto'],  "Ativa");
            if(!empty($dataProdutoRemessaAtivo)){
                foreach ($dataProdutoRemessaAtivo as $itemRemessa){
                    $flash->addMessage('danger', "O produto: " . $itemRemessa->getProduto()->getNome() . " já está na remessa: " . $itemRemessa->getRemessaFornecedor()->getId());
                }
                $uri = $this->router->generateUri('remessa.list');
                return new RedirectResponse($uri);
            }

            $hydrator = new ClassMethods();
            $remessaFornecedor = new RemessaFornecedor();
            $itemRemessaFornecedor = new ItemRemessa();
            $remessaFornecedor->setFornecedor($this->entityManager->getReference(Fornecedor::class, $idFornecedor));
            $hydrator->hydrate($data, $remessaFornecedor);
            $usuario = $this->entityManager->getReference(
                Usuario::class, $this->authenticationService->getIdentity()['id']);
            $itemRemessaFornecedor->setUsuario($usuario);

            $infoRemessa = $this->persiteRemessa->gravarDadosRemessa($remessaFornecedor, $itemRemessaFornecedor, $hydrator, $data);
            $html = $this->template->render('app::email/estoque/remessa-gravada', ['layout' => 'layout/email',
                'infoRemessa' => $infoRemessa,
                'itensRemessa' => $repositoryItemRemessa->getItensRemessaFornecedor($infoRemessa->getId())

            ]);
            $emailEnviado = false;
            if(!empty($infoRemessa->getFornecedor()->getEmail())){
                $emailEnviado = $this->enviaEmailService->enviarEmail(['destinatario' => $infoRemessa->getFornecedor()->getEmail(),
                    'mensagem' => $html, 'assunto' => 'Cadastramento de Remessa - Semi joias' ]);
            }
            $msgEnvioEmail = ($emailEnviado== true)? "Alerta de e-email enviado ao fornecedor!" : "Alerta de e-email não enviado ao fornecedor!";
            $flash->addMessage('success', "Remessa cadastrada com sucesso! ");
            $uri = $this->router->generateUri('remessa.list');
            return new RedirectResponse($uri);


        }
        //die;

        return new HtmlResponse($this->template->render('app::estoque/remessa/definir-items', [
            'produtosFornecedor' => $produtosFornecedor,
            'produtosFornecedorJSON'=> $produtosFornecedorJSON
        ]));
    }
}
