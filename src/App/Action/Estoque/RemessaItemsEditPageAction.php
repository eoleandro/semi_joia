<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 02/03/17
 * Time: 20:24
 */

namespace App\Action\Estoque;


use App\Entity\Fornecedor;
use App\Entity\ItemRemessa;
use App\Entity\MovFinRemessa;
use App\Entity\Produto;
use App\Entity\RemessaFornecedor;
use App\Entity\Usuario;
use App\Service\Email\EnviaEmailService;
use App\Service\Estoque\PersisteRemessa;
use App\Service\Financeiro\ParcelaRemessaService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class RemessaItemsEditPageAction
{
    private $template;

    private $entityManager;

    private $router;

    private $authenticationService;

    private $persiteRemessa;

    private $parcelaRemessaService;

    private $enviaEmailService;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager,
        \Zend\Authentication\AuthenticationService $authenticationService,
        PersisteRemessa $persisteRemessa,
        ParcelaRemessaService $parcelaRemessaService,
        EnviaEmailService $enviaEmailService

    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->authenticationService = $authenticationService;
        $this->persiteRemessa = $persisteRemessa;
        $this->parcelaRemessaService = $parcelaRemessaService;
        $this->enviaEmailService = $enviaEmailService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $idRemessaFornecedor = $request->getAttribute('remessa');
        $repository = $this->entityManager->getRepository(ItemRemessa::class);
        $produtosRemessa = $repository->getItensRemessaFornecedor($idRemessaFornecedor);
        $repositoryProduto = $this->entityManager->getRepository(Produto::class);
        $flash = $request->getAttribute('flash');

        if(empty($produtosRemessa)){
            $flash->addMessage('danger', "Esta remessa não possui produtos válidos!");
            $uri = $this->router->generateUri('remessa.list');
            return new RedirectResponse($uri);
        }
        if($produtosRemessa[0]->getRemessaFornecedor()->getStatus() == "Devolvida"){
            $flash->addMessage('danger', "Esta remessa foi devolvida e portanto não pode ser editada!");
            $uri = $this->router->generateUri('remessa.list');
            return new RedirectResponse($uri);
        }

        $infoRemessa = $produtosRemessa[0]->getRemessaFornecedor();
        $repoMovFinRemessa = $this->entityManager->getRepository(MovFinRemessa::class);
        $infoParcelas = $repoMovFinRemessa->getParcelasRemessaFornecedor($infoRemessa->getId());
        if(!empty($infoParcelas)){
            $flash->addMessage('danger', "Já existem pagamentos agendados para essa remessa, para editá-la você precisa"
            ." excluir todas as parcelas presentes na seção de lançamentos contábeis, que na descrição contem os dizeres: "
            ." <b>...referente a remessa de fornecedor: ".$infoRemessa->getId() ."</b>");
            $uri = $this->router->generateUri('remessa.list');
            return new RedirectResponse($uri);
        }

        $idFornecedor = $produtosRemessa[0]->getRemessaFornecedor()->getFornecedor()->getId();
        $produtosFornecedor = $repositoryProduto->getProdutosFornecedor($idFornecedor, 1);
        $produtosFornecedorJSON = json_encode($repositoryProduto->getProdutosItemsRemessaJSON($produtosFornecedor));


        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $hydrator = new ClassMethods();
            $remessaFornecedor = $this->entityManager->getReference(RemessaFornecedor::class, $idRemessaFornecedor);
            $itemRemessaFornecedor = new ItemRemessa();
            $remessaFornecedor->setFornecedor($this->entityManager->getReference(Fornecedor::class, $idFornecedor));
            // Valida alteracao de remessa
            $valorTotalCusto = $repository->getValorTotalItensRemessa(  $data);
            if(!empty($infoRemessa->getValorParcial()) && $valorTotalCusto < $infoRemessa->getValorParcial()){
                $flash->addMessage('danger', "Esta remessa já possui parcelas quitadas,
                 portanto o valor total de custo dos itens não pode ser menor do que: R$ ".number_format( $infoRemessa->getValorParcial(), 2, ',', '.') ."!");
                $uri = $this->router->generateUri('remessa.list');
                return new RedirectResponse($uri);
            }

            $hydrator->hydrate($data, $remessaFornecedor);
            $usuario = $this->entityManager->getReference(
                Usuario::class, $this->authenticationService->getIdentity()['id']);
            $itemRemessaFornecedor->setUsuario($usuario);

            $infoRemessa = $this->persiteRemessa->gravarDadosRemessa(
                $remessaFornecedor,
                $itemRemessaFornecedor,
                $hydrator,
                $data,
                $produtosRemessa);

            /*$repoMovFinRemessa = $this->entityManager->getRepository(MovFinRemessa::class);
            $infoParcelas = $repoMovFinRemessa->getParcelasRemessaFornecedor($remessaFornecedor->getId(),
                ['Quitada' => 'nao']);
            if(!empty($infoParcelas)){
                $this->parcelaRemessaService->removeParcelasDesconsiderar($infoParcelas);
            }*/
            $html = $this->template->render('app::email/estoque/remessa-gravada', ['layout' => 'layout/email',
                'infoRemessa' => $infoRemessa,
                'itensRemessa' => $repository->getItensRemessaFornecedor($infoRemessa->getId())

            ]);
            $emailEnviado = false;
            if(!empty($infoRemessa->getFornecedor()->getEmail())){
                $emailEnviado = $this->enviaEmailService->enviarEmail(['destinatario' => $infoRemessa->getFornecedor()->getEmail(),
                    'mensagem' => $html, 'assunto' => 'Cadastramento de Remessa - Semi joias' ]);
            }
            $msgEnvioEmail = ($emailEnviado== true)? "Alerta de e-email enviado ao fornecedor!" : "Alerta de e-email não enviado ao fornecedor!";
            $flash->addMessage('success', "Remessa alterada com sucesso! ");
            $uri = $this->router->generateUri('remessa.list');
            return new RedirectResponse($uri);

        }

        return new HtmlResponse($this->template->render('app::estoque/remessa/editar-items', [
            'produtosFornecedor' => $produtosFornecedor,
            'produtosFornecedorJSON'=> $produtosFornecedorJSON,
            'produtosRemessa'       => $produtosRemessa,
            'infoRemessa' => $infoRemessa
        ]));
    }
}
