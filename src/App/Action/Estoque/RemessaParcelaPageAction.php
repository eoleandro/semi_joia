<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10/03/17
 * Time: 12:36
 */

namespace App\Action\Estoque;

use App\Entity\ItemRemessa;
use App\Entity\Pedido;
use App\Entity\MovFinPedido;
use App\Entity\FormaPgto;
use App\Entity\RemessaFornecedor;
use App\Entity\MovFinRemessa;
use App\Service\Email\EnviaEmailService;
use App\Service\Financeiro\ParcelaRemessaService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;


class RemessaParcelaPageAction
{
    private $template;

    private $router;

    private $entityManager;

    private $parcelaRemessaService;

    private $enviaEmailService;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager,
        ParcelaRemessaService $parcelaRemessaService,
        EnviaEmailService $enviaEmailService

    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->parcelaRemessaService = $parcelaRemessaService;
        $this->enviaEmailService = $enviaEmailService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $idRemessa = $request->getAttribute('remessa');
        $repoRemessa = $this->entityManager->getRepository(RemessaFornecedor::class);
        $repoParcelas = $this->entityManager->getRepository(MovFinRemessa::class);
        $repoFormPag = $this->entityManager->getRepository(FormaPgto::class);
        $formasPgto = $repoFormPag->fetchPairs();
        $infoParcelas = $repoParcelas->getParcelasRemessaFornecedor($idRemessa);

        $infoRemessa = $repoRemessa->find($idRemessa);
        if(!$infoRemessa){
            $flash->addMessage('danger', "O pedido informado não é valido");
            $uri = $this->router->generateUri('pedido.list');
            return new RedirectResponse($uri);
        }

        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $remessaFornecedor = $this->entityManager->getReference(RemessaFornecedor::class, $idRemessa);
            //validar o total parcelado na requisicao com o total de itens vendidos
            $repoItensRemessa = $this->entityManager->getRepository(ItemRemessa::class);
            $itensRemessa = $repoItensRemessa->getItensRemessaFornecedor($infoRemessa->getId());
            $totalVendido = $repoItensRemessa->getValorTotalItensRemessaCadastrados( $itensRemessa );
            $totalParcelado = $repoParcelas->calculaTotalParcelasLancadas($data);
            if($totalVendido != $totalParcelado){
                $flash->addMessage('danger', "O valor total de itens vendidos e o valor total do parcelamento estão diferente, defina as parcelas novamente!");
                $uri = $this->router->generateUri('remessa.define_plots', ['remessa' => $infoRemessa->getId()]);
                return new RedirectResponse($uri);
            }

            $idMovFinRemessa = array_filter( $data['idMovRemessa'], function($v, $k) { return $v != "";}, ARRAY_FILTER_USE_BOTH);
            $parcelasDesconsiderar = $repoParcelas->getParcelasDesconsiderar($idRemessa, $idMovFinRemessa);

            $hydrator = new ClassMethods();
            $this->parcelaRemessaService->gravarDadosParcelas($remessaFornecedor,$hydrator, $data, $parcelasDesconsiderar);
            $infoParcelas = $repoParcelas->getParcelasRemessaFornecedor($idRemessa);
            // enviar email notificando o fornecedor sobre os pagamentos
            $html = $this->template->render('app::email/financeiro/acerto-remessa', ['layout' => 'layout/email',
                'infoRemessa' => $infoRemessa,
                'totalVendido' => $totalVendido,
                'produtosRemessa' => $itensRemessa,
                'infoParcelas' => $infoParcelas
            ]);
            $emailEnviado = false;
            if(!empty($infoRemessa->getFornecedor()->getEmail())){
                $emailEnviado = $this->enviaEmailService->enviarEmail(['destinatario' => $infoRemessa->getFornecedor()->getEmail(),
                    'mensagem' => $html, 'assunto' => 'Acerto de remessa - Semi joias' ]);
            }
            $msgEnvioEmail = ($emailEnviado== true)? "Alerta de e-email enviado ao fornecedor!" : "Alerta de e-email não enviado ao fornecedor!";

            if(in_array(date('d/m/Y'), $data['datasVencimento'])){
                $flash->addMessage('info', "Você cadastrou parcelas com vencimento hoje para a remessa com o código: ".
                    $infoRemessa->getId() . " faça a quitação das parcelas com vencimento na data de hoje!");
                $uri = $this->router->generateUri('parcela-remessa.pay_order_plots', ['remessa' => $infoRemessa->getId()]);
                return new RedirectResponse($uri);
            }

            $flash->addMessage('success', "Parcelas definidas com sucesso!".$msgEnvioEmail);
            $uri = $this->router->generateUri('remessa.list');
            return new RedirectResponse($uri);
        }



        return new HtmlResponse($this->template->render('app::estoque/remessa-parcela/definir-parcelas', [
            'flashMessages' => $flash->getMessages(),
            'infoRemessa' => $infoRemessa,
            'infoParcelas' => $infoParcelas,
            'formasPgto' =>$formasPgto,
            'formasPgtoJSON' => json_encode($formasPgto)
        ]));
    }
}