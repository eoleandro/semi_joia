<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31/03/17
 * Time: 23:25
 */

namespace App\Action\Estoque\Factory;

use App\Action\Estoque\RemessaShowPageAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class RemessaShowPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new RemessaShowPageAction( $template, $entityManager);
    }
}