<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 22/02/17
 * Time: 15:28
 */

namespace App\Action\Estoque\Factory;


use App\Action\Estoque\ProdutoUpdatePageAction;
use App\Service\ReferencesDatasService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class ProdutoUpdatePageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        $referencesDatasService = $container->get(ReferencesDatasService::class);
        return new ProdutoUpdatePageAction( $template, $router, $entityManager,$referencesDatasService);
    }
}