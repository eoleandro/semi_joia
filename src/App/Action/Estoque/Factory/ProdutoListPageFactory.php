<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 22/02/17
 * Time: 15:09
 */

namespace App\Action\Estoque\Factory;

use App\Action\Estoque\ProdutoListPageAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class ProdutoListPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new ProdutoListPageAction( $template, $entityManager);
    }
}