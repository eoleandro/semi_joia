<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 22/02/17
 * Time: 11:07
 */

namespace App\Action\Estoque\Factory;


use App\Action\Estoque\FornecedorDeleteAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;


class FornecedorDeleteFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new FornecedorDeleteAction( $router, $entityManager);
    }
}