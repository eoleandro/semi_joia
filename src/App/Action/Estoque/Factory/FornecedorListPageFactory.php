<?php


namespace App\Action\Estoque\Factory;

use App\Action\Estoque\FornecedorListPageAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class FornecedorListPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new FornecedorListPageAction( $template, $entityManager);
    }
}