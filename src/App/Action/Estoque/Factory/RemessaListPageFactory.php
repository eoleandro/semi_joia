<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 03/03/17
 * Time: 00:32
 */

namespace App\Action\Estoque\Factory;


use App\Action\Estoque\RemessaListPageAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class RemessaListPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new RemessaListPageAction( $template, $entityManager);
    }
}