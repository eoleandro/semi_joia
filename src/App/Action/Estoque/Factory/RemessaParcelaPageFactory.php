<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10/03/17
 * Time: 12:59
 */

namespace App\Action\Estoque\Factory;

use App\Action\Estoque\RemessaParcelaPageAction;
use App\Service\Email\EnviaEmailService;
use App\Service\Financeiro\ParcelaRemessaService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;


class RemessaParcelaPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        $parcelaRemessaService = $container->get(ParcelaRemessaService::class);
        $enviaEmailService   = $container->get(EnviaEmailService::class);

        return new RemessaParcelaPageAction( $template, $router, $entityManager, $parcelaRemessaService, $enviaEmailService);
    }
}