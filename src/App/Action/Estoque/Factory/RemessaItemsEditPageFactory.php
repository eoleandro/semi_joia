<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 02/03/17
 * Time: 20:24
 */

namespace App\Action\Estoque\Factory;

use App\Action\Estoque\RemessaItemsEditPageAction;
use App\Service\Email\EnviaEmailService;
use App\Service\Estoque\PersisteRemessa;
use App\Service\Financeiro\ParcelaRemessaService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Authentication\AuthenticationService;

class RemessaItemsEditPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        //$referencesDatasService = $container->get(ReferencesDatasService::class);
        $authAdapterService = $container->get(AuthenticationService::class);
        $persisteRemessa = $container->get(PersisteRemessa::class);
        $parcelaRessaService = $container->get(ParcelaRemessaService::class);
        $enviaEmailService   = $container->get(EnviaEmailService::class);

        return new RemessaItemsEditPageAction( $template, $router, $entityManager,$authAdapterService, $persisteRemessa,
        $parcelaRessaService, $enviaEmailService);
    }
}