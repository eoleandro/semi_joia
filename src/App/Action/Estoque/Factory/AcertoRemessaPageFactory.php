<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10/03/17
 * Time: 09:06
 */

namespace App\Action\Estoque\Factory;

use App\Action\Estoque\AcertoRemessaPageAction;
use App\Service\Financeiro\ParcelaRemessaService;
use App\Service\Estoque\PersisteRemessa;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Authentication\AuthenticationService;

class AcertoRemessaPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        $persisteRemessaService = $container->get(PersisteRemessa::class);
        $parcelaRemessaService  = $container->get(ParcelaRemessaService::class);

        return new AcertoRemessaPageAction( $template, $router, $entityManager, $persisteRemessaService, $parcelaRemessaService);
    }
}