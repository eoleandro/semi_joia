<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 23/02/17
 * Time: 15:24
 */

namespace App\Action\Estoque\Factory;



use App\Action\Estoque\RemessaItemsAddPageAction;
use App\Service\Email\EnviaEmailService;
use App\Service\Estoque\PersisteRemessa;
use App\Service\ReferencesDatasService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Authentication\AuthenticationService;

class RemessaItemsAddPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        //$referencesDatasService = $container->get(ReferencesDatasService::class);
        $authAdapterService = $container->get(AuthenticationService::class);
        $persisteRemessa = $container->get(PersisteRemessa::class);
        $enviaEmailService   = $container->get(EnviaEmailService::class);

        return new RemessaItemsAddPageAction( $template, $router, $entityManager,$authAdapterService, $persisteRemessa,
            $enviaEmailService);
    }
}