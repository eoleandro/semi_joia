<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 22/02/17
 * Time: 11:56
 */

namespace App\Action\Estoque\Factory;


use App\Action\Estoque\ProdutoCreatePageAction;
use App\Service\ReferencesDatasService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class ProdutoCreatePageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        $referencesDatasService = $container->get(ReferencesDatasService::class);
        return new ProdutoCreatePageAction( $template, $router, $entityManager,$referencesDatasService);
    }
}