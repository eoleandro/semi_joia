<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 21/02/17
 * Time: 09:12
 */

namespace App\Action\Estoque\Factory;

use App\Action\Estoque\FornecedorCreatePageAction;
use App\Service\EnderecoService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class FornecedorCreatePageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        $enderecoService = $container->get(EnderecoService::class);
        return new FornecedorCreatePageAction( $template, $router, $entityManager, $enderecoService);
    }
}