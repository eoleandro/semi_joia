<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 21/02/17
 * Time: 09:12
 */

namespace App\Action\Estoque\Factory;

use App\Action\Estoque\FornecedorUpdatePageAction;
use App\Service\EnderecoService;
use App\Service\Form\PopulateValuesFormService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use App\Service\Estoque\PersisteEntidadeRelacionadasService;

class FornecedorUpdatePageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        $enderecoService = $container->get(EnderecoService::class);
        $persisteEntidadeRelacionadasService = $container->get(PersisteEntidadeRelacionadasService::class);
        $populateValuesFormService = $container->get(PopulateValuesFormService::class);

        return new FornecedorUpdatePageAction(
            $template,
            $router,
            $entityManager,
            $enderecoService,
            $persisteEntidadeRelacionadasService,
            $populateValuesFormService
        );
    }
}