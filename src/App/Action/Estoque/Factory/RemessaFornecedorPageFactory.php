<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 23/02/17
 * Time: 14:06
 */

namespace App\Action\Estoque\Factory;


use App\Action\Estoque\RemessaFornecedorPageAction;
use App\Service\EnderecoService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class RemessaFornecedorPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new RemessaFornecedorPageAction( $template, $router, $entityManager );
    }
}