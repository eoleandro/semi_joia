<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 23/02/17
 * Time: 14:05
 */

namespace App\Action\Estoque;

use App\Entity\Fornecedor;
use App\Entity\Produto;
use App\Filter\Estoque\RemessaFornecedorInputFilter;
use App\Filter\ProdutoInputFilter;
use App\Form\Estoque\ProdutoForm;
use App\Form\Estoque\RemessaFornecedorForm;
use App\Service\ReferencesDatasService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;


class RemessaFornecedorPageAction
{
    private $template;


    private $router;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager
    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $form = new RemessaFornecedorForm();
        $form->setHydrator(new ClassMethods());
        $repository = $this->entityManager->getRepository(Fornecedor::class);
        $form->get('fornecedor')->setAttribute('options', ["" => "Selecione"] + $repository->getFetchPairsFornecedorAtivo() );
        $flash = $request->getAttribute('flash');

        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->setData($data);
            $form->setInputFilter(new RemessaFornecedorInputFilter());
            if($form->isValid()){

                $data = $form->getData();
                $fornecedor = $repository->find($data['fornecedor']);
                if($fornecedor){
                    $uri = $this->router->generateUri('remessa.define_itens', ['fornecedor' => $fornecedor->getId()]);
                }else{
                    $flash->addMessage('danger', "Fornecedor Inválido!");
                    $uri = $this->router->generateUri('remessa.define_provider');
                }

                return new RedirectResponse($uri);
            }
        }

        return new HtmlResponse($this->template->render('app::estoque/remessa/definir-fornecedor', [
            'form' => $form,
            'flashMessages' => $flash->getMessage('danger')
        ]));
    }
}
