<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 22/02/17
 * Time: 11:56
 */

namespace App\Action\Estoque;


use App\Entity\Fornecedor;
use App\Entity\Produto;
use App\Filter\ProdutoInputFilter;
use App\Form\Estoque\ProdutoForm;
use App\Service\ReferencesDatasService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class ProdutoCreatePageAction
{
    private $template;

    private $entityManager;

    private $router;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager,
        ReferencesDatasService $referencesDatasService
    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->referencesDatasService = $referencesDatasService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $form = new ProdutoForm();
        $form->setHydrator(new ClassMethods());
        $produto = new Produto();
        $repository = $this->entityManager->getRepository(Fornecedor::class);
        $form->get('fornecedor')->setAttribute('options', ["" => "Selecione"] + $repository->fetchPairs());
        $form->get('ativo')->setOptions( array(1 => "Sim", 0 => "Não"));
        $form->get('garantia')->setOptions( array(1 => "Sim", 0 => "Não"));
        $form->bind($produto);
        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $data  = $this->referencesDatasService->treatRefenceDataArrayEntity($data, Produto::class);
            $form->setData($data);
            $form->setInputFilter(new ProdutoInputFilter());
            if($form->isValid()){

                $entity = $form->getData();

                $this->entityManager->persist($entity);
                $this->entityManager->flush();
                $flash->addMessage('success', "Registro cadastrado com sucesso!");
                $uri = $this->router->generateUri('produto.list');
                return new RedirectResponse($uri);
            }
        }

        return new HtmlResponse($this->template->render('app::estoque/produto/create', [
            'form' => $form
        ]));
    }
}
