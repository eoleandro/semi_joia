<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 03/03/17
 * Time: 00:21
 */

namespace App\Action\Estoque;


use App\Entity\RemessaFornecedor;
use App\Form\Pesquisa\GrupoProdutoForm;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template;

class RemessaListPageAction
{
    private $template;

    private $entityManager;

    public function __construct( Template\TemplateRendererInterface $template = null, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $repository = $this->entityManager->getRepository(RemessaFornecedor::class);
        $flash = $request->getAttribute('flash');
        $form = new GrupoProdutoForm();
        $form->get('status')->setAttribute('options', ["" => "Selecione", "Ativa" => "Ativa", "Inativa" => "Inativa" ] );

        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->populateValues($data);
            $remessas = $repository->getRemessasFornecedor($data);
        }else{
            $remessas = $repository->getRemessasFornecedor();
        }

        return new HtmlResponse($this->template->render('app::estoque/remessa/remessas', [
            'remessas' => $remessas,
            'flashMessages' => $flash->getMessages(),
            'form' => $form
        ]));
    }
}