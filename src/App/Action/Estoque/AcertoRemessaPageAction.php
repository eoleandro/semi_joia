<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10/03/17
 * Time: 09:05
 */

namespace App\Action\Estoque;

use App\Entity\Fornecedor;
use App\Entity\ItemRemessa;
use App\Entity\MovFinRemessa;
use App\Entity\RemessaFornecedor;
use App\Entity\Usuario;
use App\Service\Estoque\PersisteRemessa;
use App\Service\Financeiro\ParcelaRemessaService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class AcertoRemessaPageAction
{
    private $template;

    private $entityManager;

    private $router;

    private $persiteRemessaService;

    private $parcelaRemessaService;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager,
        PersisteRemessa $persisteRemessa,
        ParcelaRemessaService $parcelaRemessaService
    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->persiteRemessaService = $persisteRemessa;
        $this->parcelaRemessaService = $parcelaRemessaService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $idRemessaFornecedor = $request->getAttribute('remessa');
        $repository = $this->entityManager->getRepository(ItemRemessa::class);
        $produtosRemessa = $repository->getItensRemessaFornecedor($idRemessaFornecedor, ["Ativo", "Devolvida"]);
        $flash = $request->getAttribute('flash');

        if(empty($produtosRemessa)){
            $flash->addMessage('danger', "Esta remessa não possui produtos válidos!");
            $uri = $this->router->generateUri('remessa.list');
            return new RedirectResponse($uri);
        }

        $remessaFornecedor = $this->entityManager->getReference(RemessaFornecedor::class, $idRemessaFornecedor);

        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $hydrator = new ClassMethods();
            $hydrator->hydrate($data, $remessaFornecedor);
            if($data['status'] == "Ativa"){
                $produtosJaAtivos = $repository->getItensRemessaFornecedorEmRemessasAtivas($remessaFornecedor->getId());
                if(!empty($produtosJaAtivos)){
                    $flash->addMessage('danger', "Esta remessa não pode ser ativada, pois existem produtos nela que ja estão em outras remessa ativa!");
                    $uri = $this->router->generateUri('remessa.set_status', ['remessa' => $remessaFornecedor->getId()]);
                    return new RedirectResponse($uri);
                }
            }

            $remessaFornecedor->setValorTotal( $this->persiteRemessaService->calcularValorTotal($produtosRemessa) );
            $rf = $this->persiteRemessaService->alterarStatusRemessa($remessaFornecedor);

            if($rf->getStatus() == "Devolvida"){
                $flash->addMessage('info', "Esta remessa foi Desativada. Gere as parcelas de pagamentos ao fornecedor!");
                $uri = $this->router->generateUri('remessa.define_plots', ['remessa' => $rf->getId()]);
            }else{
                $repoMovFinRemessa = $this->entityManager->getRepository(MovFinRemessa::class);
                $infoParcelas = $repoMovFinRemessa->getParcelasRemessaFornecedor($idRemessaFornecedor, ['Quitada' => 'nao']);
                $this->parcelaRemessaService->removeParcelasDesconsiderar($infoParcelas);
                $flash->addMessage('info', "Remessa reativada com sucesso!");
                $uri = $this->router->generateUri('remessa.list');
            }

            return new RedirectResponse($uri);

        }

        return new HtmlResponse($this->template->render('app::estoque/remessa/acerto-remessa', [
            'infoRemessa' =>  $remessaFornecedor,
            'produtosRemessa'       => $produtosRemessa,
            'flashMessages' => $flash->getMessages()
        ]));
    }
}