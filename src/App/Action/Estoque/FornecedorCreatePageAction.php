<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 21/02/17
 * Time: 09:10
 */

namespace App\Action\Estoque;


use App\Entity\ContatoFornecedor;
use App\Entity\EnderecoFornecedor;
use App\Entity\Fornecedor;
use App\Filter\FornecedorInputFilter;
use App\Form\Estoque\FornecedorForm;
use App\Service\EnderecoService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class FornecedorCreatePageAction
{
    private $template;

    private $entityManager;

    private $router;

    private $enderecoService;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,  EntityManager $entityManager,
        EnderecoService $enderecoService)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->enderecoService = $enderecoService;

    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $form = new FornecedorForm();
        $hidrator = new ClassMethods();

        $form->setInputFilter(new FornecedorInputFilter());
        $form->get('UF')->setAttribute('options', ["" => "Selecione"] + $this->enderecoService->getListUf());

        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->setData($data);
            if($form->isValid()){

                $data = $form->getData();
                $fornecedor = new Fornecedor($data, $hidrator);
                $contatoFornecedor = new ContatoFornecedor($data, $hidrator);
                $enderecoFornecedor = new EnderecoFornecedor($data, $hidrator);
                $contatoFornecedor->setFornecedor($fornecedor);
                $enderecoFornecedor->setFornecedor($fornecedor);
                $this->entityManager->persist($contatoFornecedor);
                $this->entityManager->persist($enderecoFornecedor);
                $this->entityManager->flush();
                $flash->addMessage('success', "Registro cadastrado com sucesso! ");
                $uri = $this->router->generateUri('fornecedor.list');
                return new RedirectResponse($uri);
            }
        }

        $form->get('ativo')->setValue('1');

        return new HtmlResponse($this->template->render('app::estoque/fornecedor/create', [
            'form' => $form,
            'flashMessages' => $flash->getMessages(),
        ]));
    }
}