<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 22/02/17
 * Time: 15:07
 */

namespace App\Action\Estoque;

use App\Entity\Produto;
use App\Form\Pesquisa\PesquisaNomeForm;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template;

class ProdutoListPageAction
{
    private $template;

    private $entityManager;

    public function __construct( Template\TemplateRendererInterface $template = null, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $repository = $this->entityManager->getRepository(Produto::class);
        $form = new PesquisaNomeForm();

        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->populateValues($data);
            $produtos = $repository->getListaDados($data);
        }else{
            $produtos = $repository->getListaDados();
        }

        return new HtmlResponse($this->template->render('app::estoque/produto/list', [
            'produtos' => $produtos,
            'flashMessages' => $flash->getMessages(),
            'form' => $form
        ]));
    }
}