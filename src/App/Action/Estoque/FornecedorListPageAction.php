<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 21/02/17
 * Time: 09:06
 */

namespace App\Action\Estoque;


use App\Entity\Fornecedor;
use App\Form\Pesquisa\PesquisaCadastroFornecedorForm;
use App\Form\Pesquisa\PesquisaNomeForm;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template;

class FornecedorListPageAction
{
    private $template;

    private $entityManager;

    public function __construct( Template\TemplateRendererInterface $template = null, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $repository = $this->entityManager->getRepository(Fornecedor::class);
        //$fornecedores = $repository->getInfoFornecedor();
        $form = new PesquisaCadastroFornecedorForm();

        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->populateValues($data);
            $fornecedores = $repository->getListaDados($data);
        }else{
            $fornecedores = $repository->getListaDados();
        }

        return new HtmlResponse($this->template->render('app::estoque/fornecedor/list', [
            'fornecedores' => $fornecedores,
            'flashMessages' => $flash->getMessages(),
            'form' => $form
        ]));
    }
}