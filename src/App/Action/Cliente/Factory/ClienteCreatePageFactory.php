<?php

namespace App\Action\Cliente\Factory;

use App\Action\Cliente\ClienteCreatePageAction;
use App\Service\EnderecoService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class ClienteCreatePageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        $enderecoService = $container->get(EnderecoService::class);
        return new ClienteCreatePageAction( $template, $router, $entityManager, $enderecoService);
    }
}