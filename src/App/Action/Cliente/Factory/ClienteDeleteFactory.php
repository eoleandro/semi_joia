<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20/04/17
 * Time: 11:40
 */

namespace App\Action\Cliente\Factory;


use App\Action\Cliente\ClienteDeleteAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;


class ClienteDeleteFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new ClienteDeleteAction( $router, $entityManager);
    }
}