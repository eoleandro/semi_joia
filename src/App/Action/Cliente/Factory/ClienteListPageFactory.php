<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 22/02/17
 * Time: 16:46
 */

namespace App\Action\Cliente\Factory;


use App\Action\Cliente\ClienteListPageAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class ClienteListPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new ClienteListPageAction( $template, $entityManager);
    }
}