<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 23/02/17
 * Time: 11:00
 */

namespace App\Action\Cliente\Factory;


use App\Action\Cliente\ClienteUpdatePageAction;
use App\Service\EnderecoService;
use App\Service\Estoque\PersisteEntidadeRelacionadasService;
use App\Service\Form\PopulateValuesFormService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class ClienteUpdatePageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        $enderecoService = $container->get(EnderecoService::class);
        $populateValuesFormService = $container->get(PopulateValuesFormService::class);
        $persisteEntidadeRelacionadasService = $container->get(PersisteEntidadeRelacionadasService::class);
        return new ClienteUpdatePageAction(
            $template,
            $router,
            $entityManager,
            $enderecoService,
            $populateValuesFormService,
            $persisteEntidadeRelacionadasService
        );
    }
}