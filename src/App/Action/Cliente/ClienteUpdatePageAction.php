<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 23/02/17
 * Time: 10:39
 */

namespace App\Action\Cliente;


use App\Entity\Cliente;
use App\Entity\EnderecoCliente;
use App\Filter\ClientePFInputFilter;
use App\Form\Cliente\ClienteForm;
use App\Service\EnderecoService;
use App\Service\Estoque\PersisteEntidadeRelacionadasService;
use App\Service\Form\PopulateValuesFormService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class ClienteUpdatePageAction
{
    private $template;

    private $entityManager;

    private $router;

    private $enderecoService;

    private $populateValuesFormService;

    private $persistEntityRelatedService;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,  EntityManager $entityManager,
        EnderecoService $enderecoService,
        PopulateValuesFormService $populateValuesFormService,
        PersisteEntidadeRelacionadasService $persistEntityRelatedService)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->enderecoService = $enderecoService;
        $this->populateValuesFormService  = $populateValuesFormService ;
        $this->persistEntityRelatedService = $persistEntityRelatedService;

    }
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $repository = $this->entityManager->getRepository(EnderecoCliente::class);
        $id = $request->getAttribute('id');
        $enderecoCliente = $repository->find($id);
        if(empty($enderecoCliente)){
            $flash->addMessage('danger', "O cliente informado não é valido!");
            $uri = $this->router->generateUri('cliente.list');
            return new RedirectResponse($uri);
        }
        $tipoCliente = $enderecoCliente->getCliente()->getTipo();
        $form = new ClienteForm();
        $hidrator = new ClassMethods();
        $form->setHydrator($hidrator);

        $form->get('UF')->setAttribute('options', ["" => "Selecione"] + $this->enderecoService->getListUf());
        $form->get('id')->setValue($id);
        //$form->bind($enderecoCliente);

        $form->setInputFilter(new ClientePFInputFilter());

        if ($request->getMethod() == "POST") {
            $data = $request->getParsedBody();
            $tipoCliente =  $data['tipo'];
            if(empty($tipoCliente) || !in_array($tipoCliente, ['PF', 'PJ'])){
                $flash->addMessage('danger', "O tipo de cliente informado não é válido!");
                $uri = $this->router->generateUri('cliente.list');
                return new RedirectResponse($uri);
            }else{
                $classeValidaCliente = "\App\Filter\Cliente".$data['tipo']."InputFilter";
                $form->setInputFilter(new $classeValidaCliente);
            }

            $form->setData($data);
            if ($form->isValid()) {

                //$data = $form->getData();
                $cliente = $this->entityManager->getReference(Cliente::class, $id);
                $cliente = $hidrator->hydrate($data, $cliente);
                $this->persistEntityRelatedService->setEntityMain($cliente);
                $this->persistEntityRelatedService->updateDataEntitiesRelated($data, [
                    EnderecoCliente::class], [$enderecoCliente->getId()], $hidrator);
                $flash->addMessage('success', "Registro atualizado com sucesso!");
                $uri = $this->router->generateUri('cliente.list');
                return new RedirectResponse($uri);
            }else{
                $flash->addMessage('danger', "Ocorreu algum erro ao atualizar registro!");
            }
        }


        $this->populateValuesFormService->exportValuesPropertiesObjectToForm($enderecoCliente, $form);
        $this->populateValuesFormService->exportValuesPropertiesObjectToForm($enderecoCliente->getCliente(), $form);


        return new HtmlResponse($this->template->render('app::cliente/cliente/update', [
            'form' => $form,
            'tipoCliente' => $tipoCliente
        ]));
    }
}