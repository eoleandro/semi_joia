<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20/04/17
 * Time: 11:41
 */

namespace App\Action\Cliente;


use App\Entity\EnderecoCliente;
use App\Entity\Product;
use Doctrine\ORM\EntityManager;
use App\Entity\Cliente;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;

class ClienteDeleteAction
{

    private $entityManager;

    private $router;

    public function __construct(  RouterInterface $router,  EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $repository = $this->entityManager->getRepository(Cliente::class);
        $repoEndCliente = $this->entityManager->getRepository(EnderecoCliente::class);
        $cliente = $repository->find($request->getAttribute('id'));
        if(empty($cliente)){
            $flash->addMessage('danger', "O Cliente informado não é válido!");
            $uri = $this->router->generateUri('cliente.list');
            return new RedirectResponse($uri);
        }
        $enderecoCliente = $repoEndCliente->findOneBy(['cliente' => $cliente]);

        try{

            if($enderecoCliente){
                $this->entityManager->remove($enderecoCliente);
            }

            $this->entityManager->remove($cliente);
            $this->entityManager->flush();
            $flash->addMessage('success', "Cliente removido com sucesso!");
            $uri = $this->router->generateUri('cliente.list');
            return new RedirectResponse($uri);

        }catch (\Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException $exception){
            $flash->addMessage('danger', "Este cliente não pode ser removido, pois existe registros relacionados a ele em agendamentos e ou pedidos.
             Você pode desabilitar este cliente inativando-o ao editar o cliente!");
            $uri = $this->router->generateUri('cliente.list');
            return new RedirectResponse($uri);
        }


    }
}
