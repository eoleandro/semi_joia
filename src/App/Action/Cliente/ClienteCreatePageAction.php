<?php

namespace App\Action\Cliente;

use App\Entity\Cliente;
use App\Entity\EnderecoCliente;
use App\Filter\ClientePFInputFilter;
use App\Filter\ClientePJInputFilter;
use App\Form\Cliente\ClienteForm;
use App\Service\EnderecoService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class ClienteCreatePageAction
{
    private $template;

    private $entityManager;

    private $router;

    private $enderecoService;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,  EntityManager $entityManager,
        EnderecoService $enderecoService)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->enderecoService = $enderecoService;

    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $form = new ClienteForm();
        $hidrator = new ClassMethods();

        $form->get('UF')->setAttribute('options', ["" => "Selecione"] + $this->enderecoService->getListUf());
        $data = array();
        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();

            if(empty($data['tipo']) || !in_array($data['tipo'], ['PF', 'PJ'])){
                $flash->addMessage('danger', "O tipo de cliente informado não é válido!");
                $uri = $this->router->generateUri('cliente.list');
                return new RedirectResponse($uri);
            }else{
                $classeValidaCliente = "\App\Filter\Cliente".$data['tipo']."InputFilter";
                $form->setInputFilter(new $classeValidaCliente);
            }
            $form->setData($data);
            if($form->isValid()){


                $data = $form->getData();
                $cliente = new Cliente($data, $hidrator);

                $enderecoCliente = new EnderecoCliente($data, $hidrator);
                $cliente->setDataCadastro();
                $enderecoCliente->setCliente($cliente);

                $this->entityManager->persist($enderecoCliente);
                $this->entityManager->flush();


                $flash->addMessage('success', "Registro atualizado com sucesso!");
                $uri = $this->router->generateUri('cliente.list');
                return new RedirectResponse($uri);
            }else{
                var_dump($form->getMessages());
            }
        }


        $form->get('tipo')->setCheckedValue('PF')->setValue('PF');
        $form->get('ativo')->setCheckedValue('1')->setValue('1');

        return new HtmlResponse($this->template->render('app::cliente/cliente/create', [
            'form' => $form,
            'dadosFormulario' => $data
        ]));
    }
}