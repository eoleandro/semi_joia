<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 22/02/17
 * Time: 16:45
 */

namespace App\Action\Cliente;


use App\Entity\Cliente;
use App\Form\Pesquisa\PesquisaCadastroClienteForm;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template;

class ClienteListPageAction
{
    private $template;

    private $entityManager;

    public function __construct( Template\TemplateRendererInterface $template = null, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $repository = $this->entityManager->getRepository(Cliente::class);
        $form = new PesquisaCadastroClienteForm();

        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->populateValues($data);
            $clientes = $repository->getListaDados($data);
        }else{
            $clientes = $repository->getListaDados();
        }

        return new HtmlResponse($this->template->render('app::cliente/cliente/list', [
            'clientes' => $clientes,
            'flashMessages' => $flash->getMessages(),
            'form' => $form
        ]));
    }
}