<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28/03/17
 * Time: 22:27
 */

namespace App\Action\Financeiro;

use App\Entity\FormaPgto;
use App\Filter\Financeiro\FormaPgtoInputFilter;
use App\Form\Financeiro\FormaPgtoForm;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class ContasPagarPageAction {

    private $template;

    private $entityManager;

    private $router;

    public function __construct( Template\TemplateRendererInterface $template = null, RouterInterface $router,  EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

    }
} 