<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09/03/17
 * Time: 01:59
 */

namespace App\Action\Financeiro;

use App\Entity\ItemPedido;
use App\Entity\MovFinPedido;
use App\Entity\Pedido;
use App\Service\Financeiro\ParcelaPedidoService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;

class PagtoParcPedidoPageAction
{
    private $template;

    private $entityManager;

    private $router;

    private $parcelaPedidoService;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager,
        ParcelaPedidoService $parcelaPedidoService
    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->parcelaPedidoService= $parcelaPedidoService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $idPedido = $request->getAttribute('pedido');
        $flash = $request->getAttribute('flash');
        $repositoryPedido= $this->entityManager->getRepository(Pedido::class);
        $infoPedido = $repositoryPedido->find($idPedido);
        $repositoryItensPedido = $this->entityManager->getRepository(ItemPedido::class);
        $itensPedido = $repositoryItensPedido->getItensPedido($idPedido);

        if(is_null($infoPedido)){
            $flash->addMessage('danger', "O pedido informado não é válido!");
            $uri = $this->router->generateUri('pedido.list');
            return new RedirectResponse($uri);
        }
        $infoCliente = $infoPedido->getCliente();


        $repoMovFinPedido = $this->entityManager->getRepository(MovFinPedido::class);
        $parcelasEmAberto =$repoMovFinPedido->getParcelasPedido($idPedido, [ 'tipo' => ['C', 'D']]);

        $totalParcelasQuitadas = $repoMovFinPedido->encontrarParcelasStatus($parcelasEmAberto, "F");

        if(empty($parcelasEmAberto)){
            $flash->addMessage('danger', "O pedido não possui parcelas a serem quitadas!");
            $uri = $this->router->generateUri('pedido.list');
            return new RedirectResponse($uri);

        }
        if($totalParcelasQuitadas == count($parcelasEmAberto) ){
            $flash->addMessage('danger', "O pedido já possui todas as parcelas quitadas e portanto não pode ter itens alterados, crie um novo pedido!");
            $uri = $this->router->generateUri('pedido.list');
            return new RedirectResponse($uri);

        }

        if($request->getMethod() == "POST"){

            $data = $request->getParsedBody();
            $this->parcelaPedidoService->quitarParcelas($infoPedido, $data);
            $flash->addMessage('success', "Parcela(s) quitada(s) com sucesso!");
            $uri = $this->router->generateUri('parcela.pay_order_plots', ['pedido' => $infoPedido->getId()]);
            return new RedirectResponse($uri);

        }

        return new HtmlResponse($this->template->render('app::financeiro/pagto-parc/pedido', [
            'infoCliente' => $infoCliente,
            'infoPedido' => $infoPedido,
            'parcelasPedido' => $parcelasEmAberto,
            'flashMessages' => $flash->getMessages()
        ]));
    }
}