<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/17
 * Time: 11:06
 */

namespace App\Action\Financeiro;

use App\Entity\FormaPgto;
use App\Filter\Financeiro\FormaPgtoInputFilter;
use App\Form\Financeiro\FormaPgtoForm;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class FormaPgtoAddPageAction {
    private $template;

    private $entityManager;

    private $router;

    public function __construct( Template\TemplateRendererInterface $template = null, RouterInterface $router,  EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $form = new FormaPgtoForm();
        $form->setHydrator(new ClassMethods());
        $entity = new FormaPgto();
        $flash = $request->getAttribute('flash');
        $form->bind($entity);
        $form->setInputFilter(new FormaPgtoInputFilter());
        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->setData($data);
            if($form->isValid()){
                $entity = $form->getData();
                try{
                    $this->entityManager->persist($entity);
                    $this->entityManager->flush();
                    $flash->addMessage('success', "Registro inserido com sucesso!");
                }catch (\Exception $e){
                    $flash->addMessage('danger', "Registro inserido com sucesso!");
                }

                $uri = $this->router->generateUri('formapgto.list');
                return new RedirectResponse($uri);
            }
        }

        return new HtmlResponse($this->template->render('app::financeiro/forma-pgto/create', [
            'form' => $form,
            'flashMessages' => $flash->getMessages()
        ]));
    }
} 