<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/17
 * Time: 11:01
 */

namespace App\Action\Financeiro;

use App\Entity\MovFinRemessa;
use App\Entity\MovFinPedido;
use App\Service\Financeiro\MovFinanceiroService;
use Doctrine\ORM\EntityManager;
use App\Entity\MovFinanceiro;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;

class MovFinanceiroDeleteAction {

    private $template;

    private $entityManager;

    private $router;

    private $movFinanceiroService;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager,
        MovFinanceiroService $movFinanceiroService
    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->movFinanceiroService = $movFinanceiroService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $repository = $this->entityManager->getRepository(MovFinanceiro::class);
        $entity = $repository->find($request->getAttribute('id'));
        $repoMovFinPedido  = $this->entityManager->getRepository(MovFinPedido::class);
        $repoMovFinRemessa = $this->entityManager->getRepository(MovFinRemessa::class);
        $infoMovFinPedidoRelationship  = $repoMovFinPedido->getInfoRelacionadaMovFinanceiro($entity->getId());
        $infoMovFinRemessaRelationship = $repoMovFinRemessa->getInfoRelacionadaMovFinanceiro($entity->getId());

        if(empty($entity)){
            $flash->addMessage('danger', "O movimento financeiro selecionado não é válido!");
            $uri = $this->router->generateUri('movfinanceiro.list');
            return new RedirectResponse($uri);
        }
        try{
            $this->movFinanceiroService->removeDataMovFinanceiro($entity, $infoMovFinPedidoRelationship, $infoMovFinRemessaRelationship);
            $flash->addMessage('success', "Registro removido com sucesso!");
        }catch(\Exception $e){
            $flash->addMessage('danger', "Ocorreu algum erro ao excluir registro, tente novamente!");
        }
        $uri = $this->router->generateUri('movfinanceiro.list');


        return new RedirectResponse($uri);


    }
} 