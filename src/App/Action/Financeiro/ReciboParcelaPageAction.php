<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 08/04/17
 * Time: 12:00
 */

namespace App\Action\Financeiro;

use App\Entity\PlanoConta;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;

class ReciboParcelaPageAction
{
    private $template;

    private $entityManager;

    private $router;

    public function __construct( Template\TemplateRendererInterface $template = null, RouterInterface $router, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $tipoMovFin = $request->getAttribute('tipoMovFin');
        $idsMovFin   = explode(',', $request->getAttribute('pagamentos'));
        /*$tipoMovFin = preg_replace_callback('~-([a-z])~', function ($match) use($tipoMovFin) {
            return strtoupper($match[1]);
        }, $tipoMovFin);*/
        $nameClassMovFin = "\\App\\Entity\\MovFin" . ucfirst($tipoMovFin);

        if( !class_exists($nameClassMovFin) ){
            $flash->addMessage('danger', "O tipo de pagamento não é valido, pois não corresponde a pagamentos de pedido ou remessa de fornecedor!");
            $uri = $this->router->generateUri('movfinanceiro.list');
            return new RedirectResponse($uri);
        }
        $repository = $this->entityManager->getRepository($nameClassMovFin);
        $infoParcelas = $repository->getParcelasPorIds($idsMovFin,['status' => 'F']);
        if( !class_exists($nameClassMovFin) ){
            $flash->addMessage('danger', "O(s) pagamento(s) selecionado(s) não estão aptos para emissão de recibo!");
            $uri = $this->router->generateUri('movfinanceiro.list');
            return new RedirectResponse($uri);
        }

        return new HtmlResponse($this->template->render('app::financeiro/pagto-parc/recibo', [
            'layout' => 'layout/recibo',
            'infoParcelas' => $infoParcelas,
            'flashMessages' => $flash->getMessages()
        ]));
    }
}