<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/17
 * Time: 10:59
 */

namespace App\Action\Financeiro;

use App\Entity\MovFinanceiro;
use App\Entity\PlanoConta;
use App\Form\Pesquisa\PesquisaMovFinanceiroForm;
use App\Service\Filter\FormataDataService;
use Doctrine\ORM\EntityManager;
use App\Entity\FormaPgto;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;

class MovFinanceiroListPageAction
{
    private $template;

    private $entityManager;

    private $formataDataService;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router, EntityManager $entityManager,
        FormataDataService $formataDataService)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->formataDataService = $formataDataService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $repository = $this->entityManager->getRepository(MovFinanceiro::class);
        $form = new PesquisaMovFinanceiroForm();
        $repoFormaPgto = $this->entityManager->getRepository(FormaPgto::class);
        $repoPlanoConta = $this->entityManager->getRepository(PlanoConta::class);
        $form->get('formaPgto')->setAttribute('options', ["" => "Selecione"] + $repoFormaPgto->fetchPairs());
        $form->get('planoConta')->setAttribute('options', ["" => "Selecione"] + $repoPlanoConta->fetchPairs());

        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->setData($data);
            $data = $this->formataDataService->converteDataFormatoEUA($data, ['dataCadastroDe','dataCadastroAte','dataVencimentoDe','dataVencimentoAte']);

            $movFinanceiros = $repository->getMovFinanceiro($data);
        }else{
            $movFinanceiros = $repository->getMovFinanceiro();
        }

        return new HtmlResponse($this->template->render('app::financeiro/mov-financeiro/list', [
            'form' => $form,
            'movFinanceiros' => $movFinanceiros,
            'flashMessages' => $flash->getMessages()
        ]));
    }
}