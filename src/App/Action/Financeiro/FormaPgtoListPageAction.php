<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/17
 * Time: 11:05
 */

namespace App\Action\Financeiro;

use Doctrine\ORM\EntityManager;
use App\Entity\FormaPgto;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;

class FormaPgtoListPageAction
{
    private $template;

    private $entityManager;

    public function __construct( Template\TemplateRendererInterface $template = null, RouterInterface $router, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $repository = $this->entityManager->getRepository(FormaPgto::class);
        $formasPgto = $repository->findAll();

        return new HtmlResponse($this->template->render('app::financeiro/forma-pgto/list', [
            'formasPgto' => $formasPgto,
            'flashMessages' => $flash->getMessages()
        ]));
    }
}
