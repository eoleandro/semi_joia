<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09/03/17
 * Time: 01:59
 */

namespace App\Action\Financeiro\Factory;

use App\Action\Financeiro\PagtoParcPedidoPageAction;
use App\Service\Financeiro\ParcelaPedidoService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;


class PagtoParcPedidoPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        $parcelaPedidoService = $container->get(ParcelaPedidoService::class);

        return new PagtoParcPedidoPageAction(
            $template,
            $router,
            $entityManager,
            $parcelaPedidoService
        );
    }
}