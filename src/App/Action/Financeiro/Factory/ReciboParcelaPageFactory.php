<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 08/04/17
 * Time: 12:00
 */

namespace App\Action\Financeiro\Factory;

use App\Action\Financeiro\ReciboParcelaPageAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;


class ReciboParcelaPageFactory {

    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new ReciboParcelaPageAction(
            $template,
            $router,
            $entityManager
        );
    }
}