<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28/03/17
 * Time: 22:28
 */

namespace App\Action\Financeiro\Factory;

use App\Action\Financeiro\FormaPgtoAddPageAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;


class ContasPagarPageFactory {

    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new FormaPgtoAddPageAction(
            $template,
            $router,
            $entityManager
        );
    }
}