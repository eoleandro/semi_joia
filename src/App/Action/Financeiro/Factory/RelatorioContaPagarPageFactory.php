<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01/04/17
 * Time: 11:26
 */

namespace App\Action\Financeiro\Factory;

use App\Action\Financeiro\RelatorioContasPagarPageAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class RelatorioContaPagarPageFactory{

    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new RelatorioContasPagarPageAction(
            $template,
            $router,
            $entityManager
        );
    }
}