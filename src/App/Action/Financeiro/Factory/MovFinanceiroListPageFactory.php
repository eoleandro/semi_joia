<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/17
 * Time: 11:00
 */

namespace App\Action\Financeiro\Factory;

use App\Action\Financeiro\MovFinanceiroListPageAction;
use App\Service\Filter\FormataDataService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class MovFinanceiroListPageFactory {

    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        $formataDataService = $container->get(FormataDataService::class);
        return new MovFinanceiroListPageAction(
            $template,
            $router,
            $entityManager,
            $formataDataService
        );
    }
}