<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10/03/17
 * Time: 13:49
 */

namespace App\Action\Financeiro\Factory;

use App\Action\Financeiro\PagtoParcPedidoPageAction;
use App\Action\Financeiro\PagtoParcRemessaPageAction;
use App\Service\Financeiro\ParcelaRemessaService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;


class PagtoParcRemessaPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        $parcelaRemessaService = $container->get(ParcelaRemessaService::class);

        return new PagtoParcRemessaPageAction(
            $template,
            $router,
            $entityManager,
            $parcelaRemessaService
        );
    }
}