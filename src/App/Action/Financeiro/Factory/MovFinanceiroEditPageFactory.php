<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/17
 * Time: 10:13
 */

namespace App\Action\Financeiro\Factory;


use App\Action\Financeiro\MovFinanceiroEditPageAction;
use Doctrine\ORM\EntityManager;
use App\Service\ReferencesDatasService;
use App\Service\Financeiro\MovFinanceiroService;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class MovFinanceiroEditPageFactory {

    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        $referenceDataService = $container->get(ReferencesDatasService::class);
        $movFinanceiroService = $container->get(MovFinanceiroService::class);

        return new MovFinanceiroEditPageAction(
            $template,
            $router,
            $entityManager,
            $referenceDataService,
            $movFinanceiroService
        );
    }
}