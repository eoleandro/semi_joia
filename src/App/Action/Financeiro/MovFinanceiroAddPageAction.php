<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/17
 * Time: 09:58
 */

namespace App\Action\Financeiro;

use App\Form\Financeiro\MovFinanceiroForm;
use App\Filter\Financeiro\MovFinanceiroInputFilter;
use App\Service\Financeiro\MovFinanceiroService;
use App\Service\ReferencesDatasService;
use Doctrine\ORM\EntityManager;
use App\Entity\FormaPgto;
use App\Entity\MovFinanceiro;
use App\Entity\PlanoConta;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class MovFinanceiroAddPageAction {

    private $template;

    private $entityManager;

    private $router;

    private $referencesDatasService;

    private $movFinanceiroService;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager,
        ReferencesDatasService $referencesDatasService,
        MovFinanceiroService $movFinanceiroService
    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->referencesDatasService = $referencesDatasService;
        $this->movFinanceiroService = $movFinanceiroService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $form = new MovFinanceiroForm();
        $repoFormaPgto  = $this->entityManager->getRepository(FormaPgto::class);
        $repoPlanoConta = $this->entityManager->getRepository(PlanoConta::class);
        $form->get('formaPgto')->setAttribute('options', ["" => "Selecione"] + $repoFormaPgto->fetchPairs());
        $form->get('planoConta')->setAttribute('options', ["" => "Selecione"] + $repoPlanoConta->fetchPairs());

        if($request->getMethod() == "POST"){

            $data = $request->getParsedBody();
            $entity = new MovFinanceiro();
            $form->setHydrator(new ClassMethods());
            $form->bind($entity);
            $data  = $this->referencesDatasService->treatRefenceDataArrayEntity($data, MovFinanceiro::class);
            $form->setData($data);
            $form->setInputFilter(new MovFinanceiroInputFilter());
            if($form->isValid()){

                $entity = $form->getData();

                try{
                    $this->movFinanceiroService->gravarMovFinanceiro($entity);
                    $flash->addMessage('success', "Registro inserido com sucesso!");
                }catch(\Exception $e){
                    $flash->addMessage('danger', "Ocorreu algum erro ao inserir registro, tente novamente!");
                }
                $uri = $this->router->generateUri('movfinanceiro.list');


                return new RedirectResponse($uri);
            }
        }


        return new HtmlResponse($this->template->render('app::financeiro/mov-financeiro/create', [
            'form' => $form,
            'flashMessages' => $flash->getMessages()
        ]));
    }
} 