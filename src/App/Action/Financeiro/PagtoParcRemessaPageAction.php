<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10/03/17
 * Time: 13:49
 */

namespace App\Action\Financeiro;

use App\Entity\MovFinRemessa;
use App\Entity\RemessaFornecedor;
use App\Service\Financeiro\ParcelaRemessaService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;

class PagtoParcRemessaPageAction
{
    private $template;

    private $entityManager;

    private $router;

    private $parcelaRemessaService;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager,
        ParcelaRemessaService $parcelaRemessaService
    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->parcelaRemessaService= $parcelaRemessaService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $idRemessa = $request->getAttribute('remessa');
        $flash = $request->getAttribute('flash');

        $repositoryRemessa= $this->entityManager->getRepository(RemessaFornecedor::class);
        $infoRemessa = $repositoryRemessa->find($idRemessa);

        if(is_null($infoRemessa)){
            $flash->addMessage('danger', "O pedido informado não é válido!");
            $uri = $this->router->generateUri('remessa.list');
            return new RedirectResponse($uri);
        }
        $infoFornecedor = $infoRemessa->getFornecedor();
        $repoMovFinRemessa = $this->entityManager->getRepository(MovFinRemessa::class);
        $parcelasEmAberto =$repoMovFinRemessa->getParcelasRemessaFornecedor($idRemessa, ['tipo' => ['D']]);
        if(empty($parcelasEmAberto)){
            $flash->addMessage('danger', "A remessa não possuí parcelas a serem quitadas e portanto não pode ter itens alterados, crie parcelas para a remessa de produtos!");
            $uri = $this->router->generateUri('remessa.list');
            return new RedirectResponse($uri);
        }

        if($request->getMethod() == "POST"){

            $data = $request->getParsedBody();
            $this->parcelaRemessaService->quitarParcelas($infoRemessa, $data);
            $flash->addMessage('success', "Parcela(s) quitada(s) com sucesso!");
            $uri = $this->router->generateUri('parcela-remessa.pay_order_plots', ['remessa' => $infoRemessa->getId()]);
            return new RedirectResponse($uri);

        }


        return new HtmlResponse($this->template->render('app::financeiro/pagto-parc/remessa-fornecedor', [
            'infoFornecedor' => $infoFornecedor,
            'infoRemessa' => $infoRemessa,
            'parcelasPedido' => $parcelasEmAberto,
            'flashMessages' => $flash->getMessages()
        ]));
    }
}