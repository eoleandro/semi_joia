<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/17
 * Time: 11:05
 */

namespace App\Action\Financeiro;

use App\Entity\FormaPgto;
use App\Entity\PlanoConta;
use App\Filter\Financeiro\FormaPgtoInputFilter;
use App\Filter\Financeiro\PlanoContaInputFilter;
use App\Form\Financeiro\FormaPgtoForm;
use App\Form\Financeiro\PlanoContaForm;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class PlanoContaEditPageAction {
    private $template;

    private $entityManager;

    private $router;

    public function __construct( Template\TemplateRendererInterface $template = null, RouterInterface $router,  EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $repository = $this->entityManager->getRepository(PlanoConta::class);
        $flash = $request->getAttribute('flash');
        $entity = $repository->find($request->getAttribute('id'));
        $form = new PlanoContaForm();
        $form->setHydrator(new ClassMethods());
        $form->bind($entity);

        if(empty($entity)){
            $flash->addMessage('danger', "Dados inválidos para a forma de pagamento!");
            $uri = $this->router->generateUri('planoconta.list');
            return new RedirectResponse($uri);
        }

        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->setData($data);
            $form->setInputFilter(new PlanoContaInputFilter());
            if($form->isValid()){
                try{
                    $this->entityManager->flush();
                    $flash->addMessage('success', "Registro atualizado com sucesso!");
                }catch(\Exception $e){
                    $flash->addMessage('danger', "Ocorreu algum erro atualizar o registro, tente novamente!");
                }
                $uri = $this->router->generateUri('planoconta.list');
                return new RedirectResponse($uri);
            }
        }

        return new HtmlResponse($this->template->render('app::financeiro/plano-conta/update', [
            'form' => $form
        ]));
    }
}