<?php

namespace App\Action\Financeiro;

use App\Entity\FormaPgto;
use App\Entity\PlanoConta;
use App\Form\Financeiro\ContasQuitarForm;
use Doctrine\ORM\EntityManager;
use App\Entity\MovFinanceiro;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;

class RelatorioFluxoCaixaPageAction
{
    private $template;

    private $entityManager;

    public function __construct( Template\TemplateRendererInterface $template = null, RouterInterface $router, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $repository = $this->entityManager->getRepository(MovFinanceiro::class);
        $form = new  ContasQuitarForm();
        $repoFormaPgto  = $this->entityManager->getRepository(FormaPgto::class);
        $repoPlanoConta = $this->entityManager->getRepository(PlanoConta::class);
        $form->get('formaPgto')->setAttribute('options', ["" => "Selecione"] + $repoFormaPgto->fetchPairs());
        $form->get('planoConta')->setAttribute('options', ["" => "Selecione"] + $repoPlanoConta->fetchPairs());
        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->populateValues($data);

            $despesas = $repository->getContas(array_merge($data, ['tipo' => 'D', 'natureza' => 'D']));
            $receitas = $repository->getContas(array_merge($data, ['tipo' => 'C', 'natureza' => 'R']));
            $retiradas = $repository->getContas(array_merge($data, ['tipo' => 'D', 'natureza' => 'S']));
        }else{

            $despesas = $repository->getContas(['tipo' => 'D', 'natureza' => 'D']);
            $receitas = $repository->getContas(['tipo' => 'C', 'natureza' => 'R']);
            $retiradas = $repository->getContas(['tipo' => 'D', 'natureza' => 'S']);
        }


        return new HtmlResponse($this->template->render('app::financeiro/relatorio/fluxo-caixa', [
            'flashMessages' => $flash->getMessages(),
            'form' => $form,
            'despesas' => $despesas,
            'receitas' => $receitas,
            'retiradas' => $retiradas
        ]));
    }
}