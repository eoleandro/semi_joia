<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/17
 * Time: 10:12
 */

namespace App\Action\Financeiro;


use App\Form\Financeiro\MovFinanceiroForm;
use App\Filter\Financeiro\MovFinanceiroInputFilter;
use App\Service\Financeiro\MovFinanceiroService;
use App\Service\ReferencesDatasService;
use Doctrine\ORM\EntityManager;
use App\Entity\FormaPgto;
use App\Entity\MovFinanceiro;
use App\Entity\PlanoConta;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;
use Zend\Mail\Message;
use Zend\Mail\Transport\Sendmail;
use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;

class MovFinanceiroEditPageAction {

    private $template;

    private $entityManager;

    private $router;

    private $referencesDatasService;

    private $movFinanceiroService;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager,
        ReferencesDatasService $referencesDatasService,
        MovFinanceiroService $movFinanceiroService
    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->referencesDatasService = $referencesDatasService;
        $this->movFinanceiroService = $movFinanceiroService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $repository = $this->entityManager->getRepository(MovFinanceiro::class);
        $movFinanceiro = $repository->find($request->getAttribute('id'));
        $movFinanceiroSemAlteracao = clone $movFinanceiro;
        $form = new MovFinanceiroForm();
        $repoFormaPgto  = $this->entityManager->getRepository(FormaPgto::class);
        $repoPlanoConta = $this->entityManager->getRepository(PlanoConta::class);
        $form->get('formaPgto')->setAttribute('options', ["" => "Selecione"] + $repoFormaPgto->fetchPairs());
        $form->get('planoConta')->setAttribute('options', ["" => "Selecione"] + $repoPlanoConta->fetchPairs());


        if(empty($movFinanceiro)){
            $flash->addMessage('danger', "O movimento financeiro selecionado não é válido!");
            $uri = $this->router->generateUri('movfinanceiro.list');
            return new RedirectResponse($uri);
        }
        $form->setHydrator(new ClassMethods());
        $movFinanceiro->setValor( number_format($movFinanceiro->getValor(), 2, ',', '.') );
        $form->bind($movFinanceiro);
        if($request->getMethod() == "POST"){

            $data = $request->getParsedBody();
            $data  = $this->referencesDatasService->treatRefenceDataArrayEntity($data, MovFinanceiro::class);
            $form->setData($data);
            $form->setInputFilter(new MovFinanceiroInputFilter());
            if($form->isValid()){

                $movFinanceiro = $form->getData();

                try{
                    $this->movFinanceiroService->gravarMovFinanceiro($movFinanceiro, $movFinanceiroSemAlteracao);
                    $flash->addMessage('success', "Registro atualizado com sucesso!");
                }catch(\Exception $e){
                    $flash->addMessage('danger', "Ocorreu algum erro ao atualizar registro, tente novamente!");
                }
                $uri = $this->router->generateUri('movfinanceiro.list');


                return new RedirectResponse($uri);
            }
        }
        $form->get('formaPgto')->setValue($movFinanceiro->getFormaPgto()->getId());
        $form->get('planoConta')->setValue($movFinanceiro->getPlanoConta()->getId());
        if(!is_null($movFinanceiro->getDataVencimento())) $form->get('dataVencimento')->setValue($movFinanceiro->getDataVencimento()->format('d/m/Y'));
        if(!is_null($movFinanceiro->getDataQuitacao())) $form->get('dataQuitacao')->setValue($movFinanceiro->getDataQuitacao()->format('d/m/Y'));
        $infoDataRelationShipMovFinanceiro = $this->movFinanceiroService->getDataRelationShipMovFinanceiro($movFinanceiro->getId());
        $hasRelationShipFinancial = (!empty($infoDataRelationShipMovFinanceiro))? 1 : 0;
        return new HtmlResponse($this->template->render('app::financeiro/mov-financeiro/update', [
            'form' => $form,
            'flashMessages' => $flash->getMessages(),
            'statusMovFinanceiro' => $movFinanceiro->getStatus(),
            'movFinanceiro' => $movFinanceiro,
            'hasRelationShipFinancial' => $hasRelationShipFinancial
        ]));
    }
} 