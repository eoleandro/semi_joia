<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/04/17
 * Time: 09:54
 */

namespace App\Action\Venda;

use App\Entity\Pedido;
use App\Form\Pesquisa\GrupoProdutoForm;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template;

class PedidosEntregarPageAction
{
    private $template;

    private $entityManager;

    public function __construct( Template\TemplateRendererInterface $template = null, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $form = new GrupoProdutoForm();
        $repository = $this->entityManager->getRepository(Pedido::class);
        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->populateValues($data);
            $pedidos = $repository->getPedidosEntregar($data);
        }else{
            $pedidos = $repository->getPedidosEntregar();
        }

        return new HtmlResponse($this->template->render('app::venda/pedido/list-entregar', [
            'pedidos' => $pedidos,
            'flashMessages' => $flash->getMessages(),
            'form' => $form
        ]));
    }
}