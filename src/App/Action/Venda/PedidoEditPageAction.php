<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06/03/17
 * Time: 11:58
 */

namespace App\Action\Venda;

use App\Entity\ItemPedido;
use App\Entity\ItemRemessa;
use App\Entity\MovFinPedido;
use App\Entity\Pedido;
use App\Service\Venda\PedidoService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class PedidoEditPageAction
{
    private $template;

    private $entityManager;

    private $router;

    private $persitePedido;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager,
        PedidoService $persisteRemessa
    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->persitePedido = $persisteRemessa;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $idPedido = $request->getAttribute('pedido');
        $flash = $request->getAttribute('flash');
        $repository = $this->entityManager->getRepository(ItemRemessa::class);
        $produtosPedido = $repository->getProdutosDisponiveisRemessas();
        $produtosPedidoParaJSON = $repository->getProdutosItemsPedidoParaJSON($produtosPedido);
        $repositoryPedido= $this->entityManager->getRepository(Pedido::class);
        $infoPedido = $repositoryPedido->find($idPedido);

        if(empty($produtosPedido)){
            $flash->addMessage('danger', "Não existem produtos disponíveis para realizar pedidos, abasteça o estoque!");
            $uri = $this->router->generateUri('remessa.define_provider');
            return new RedirectResponse($uri);
        }
        if(is_null($infoPedido)){
            $flash->addMessage('danger', "O pedido informado não é válido!");
            $uri = $this->router->generateUri('pedido.list');
            return new RedirectResponse($uri);
        }
        if($infoPedido->getStatus() == "Cancelado"){
            $flash->addMessage('danger', "Pedidos cancelados não podem ser editados!");
            $uri = $this->router->generateUri('pedido.list');
            return new RedirectResponse($uri);
        }
        $infoCliente = $infoPedido->getCliente();
        $repoMovFinPedido = $this->entityManager->getRepository(MovFinPedido::class);
        $parcelasQuitadas = $repoMovFinPedido->getParcelasPedido($idPedido, ['Quitada' => 'sim']);
        if(!empty($parcelasQuitadas)){
            $flash->addMessage('danger', "O pedido já possui parcelas quitadas e portanto não pode ter itens alterados, crie um novo pedido!");
            $uri = $this->router->generateUri('pedido.list');
            return new RedirectResponse($uri);
        }

        $produtosPedidoJSON = json_encode($produtosPedidoParaJSON);
        //var_dump(); die;
        $repositoryItensPedido = $this->entityManager->getRepository(ItemPedido::class);



        if($request->getMethod() == "POST"){

            $data = $request->getParsedBody();
            $hydrator = new ClassMethods();
            $pedido = new Pedido();
            //var_dump($infoPedido->getCliente()->getId()); die;
            $itemPedido = new ItemPedido();
            $hydrator->hydrate($data, $pedido);
            $idItemPedido = array_filter( $data['idItensPedido'], function($v, $k) { return $v != "";}, ARRAY_FILTER_USE_BOTH);
            $ItensPedidoDesconsiderar = $repositoryItensPedido->getItensPedidoDesconsiderar($idPedido, $idItemPedido);
            $pedidoGravado = $this->persitePedido->gravarDadosPedido($infoPedido, $itemPedido, $hydrator, $data, $ItensPedidoDesconsiderar );

            if(!is_null($pedidoGravado->getId()) ){
                $flash->addMessage('success', "Itens do pedido definidos com sucesso! Redefina os parcelas do pedido!");
                $uri = $this->router->generateUri('parcela.define_plots', ['pedido' => $pedidoGravado->getId()]);
            }else{
                $flash->addMessage('danger', "Ocorreu um erro ao gerar pedido, tente novamente!");
                $uri = $this->router->generateUri('pedido.editar_itens', ['pedido' => $pedidoGravado->getId()]);
            }

            return new RedirectResponse($uri);

        }
        $itensPedido = $repositoryItensPedido->getItensPedido($idPedido);

        return new HtmlResponse($this->template->render('app::venda/pedido/editar-itens', [
            'infoCliente' => $infoCliente,
            'infoPedido' => $infoPedido,
            'produtosPedido' => $produtosPedido,
            'produtosPedidoJSON'=> $produtosPedidoJSON,
            'itensPedido' => $itensPedido,
            'mapIdProdutosCombo' => array_flip(array_column($produtosPedidoParaJSON, 'produto_id'))
        ]));
    }
}