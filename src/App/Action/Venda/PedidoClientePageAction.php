<?php

namespace App\Action\Venda;

use App\Entity\Cliente;
use App\Filter\Venda\ClientePedidoInputFilter;
use App\Form\Venda\ClientePedidoForm;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;


class PedidoClientePageAction
{
    private $template;


    private $router;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager
    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $form = new ClientePedidoForm();
        $form->setHydrator(new ClassMethods());
        $repository = $this->entityManager->getRepository(Cliente::class);
        $form->get('cliente')->setAttribute('options', ["" => "Selecione"] + $repository->fetchPairs());
        $flash = $request->getAttribute('flash');

        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->setData($data);
            $form->setInputFilter(new ClientePedidoInputFilter());
            if($form->isValid()){

                $data = $form->getData();
                $cliente = $repository->find($data['cliente']);

                if($cliente){
                    $uri = $this->router->generateUri('pedido.define_itens', ['cliente' => $cliente->getId()]);
                }else{
                    $flash->addMessage('danger', "Cliente Inválido!");
                    $uri = $this->router->generateUri('pedido.define_client');
                }

                return new RedirectResponse($uri);
            }
        }

        return new HtmlResponse($this->template->render('app::venda/pedido/definir-cliente', [
            'form' => $form,
            'flashMessages' => $flash->getMessage('danger')
        ]));
    }
}