<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 04/03/17
 * Time: 15:05
 */

namespace App\Action\Venda;

use App\Entity\ItemPedido;
use App\Entity\Pedido;
use App\Entity\MovFinPedido;
use App\Entity\FormaPgto;
use App\Service\Email\EnviaEmailService;
use App\Service\Financeiro\ParcelaPedidoService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;


class PedidoParcelaPageAction
{
    private $template;

    private $router;

    private $entityManager;

    private $parcelaPedidoService;

    private $enviaEmailService;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager,
        ParcelaPedidoService $parcelaPedidoService,
        EnviaEmailService $enviaEmailService

    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->parcelaPedidoService = $parcelaPedidoService;
        $this->enviaEmailService = $enviaEmailService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $idPedido = $request->getAttribute('pedido');
        $repoPedido = $this->entityManager->getRepository(Pedido::class);
        $repoParcelas = $this->entityManager->getRepository(MovFinPedido::class);
        $repoFormPag = $this->entityManager->getRepository(FormaPgto::class);
        $formasPgto = $repoFormPag->fetchPairs();
        $infoParcelas = $repoParcelas->getParcelasPedido($idPedido);

        $infoPedido = $repoPedido->find($idPedido);

        if(!$infoPedido){
            $flash->addMessage('danger', "O pedido informado não é valido");
            $uri = $this->router->generateUri('pedido.list');
            return new RedirectResponse($uri);
        }

        if($infoPedido->getStatus() == "Cancelado"){
            $flash->addMessage('danger', "Pedidos cancelados não podem ser parcelados!");
            $uri = $this->router->generateUri('pedido.list');
            return new RedirectResponse($uri);
        }

        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $pedido = $this->entityManager->getReference(Pedido::class, $idPedido);
            $repositoryItensPedido = $this->entityManager->getRepository(ItemPedido::class);
            $totalVendido = $pedido->getValorTotal( );
            $totalParcelado = $repoParcelas->calculaTotalParcelasLancadas($data);

            if($totalVendido != $totalParcelado){
                $flash->addMessage('danger', "O valor total de itens vendidos e o valor total do parcelamento estão diferente, defina as parcelas novamente!");
                $uri = $this->router->generateUri('parcela.define_plots', ['pedido' => $pedido->getId()]);
                return new RedirectResponse($uri);
            }

            $idMovFinPedido = array_filter( $data['idMovPedido'], function($v, $k) { return $v != "";}, ARRAY_FILTER_USE_BOTH);
            $parcelasDesconsiderar = $repoParcelas->getParcelasDesconsiderar($idPedido, $idMovFinPedido);

            $hydrator = new ClassMethods();
            $this->parcelaPedidoService->gravarDadosParcelas($pedido,$hydrator, $data, $parcelasDesconsiderar);

            $itensPedido = $repositoryItensPedido->getItensPedido($idPedido);
            $infoParcelas = $repoParcelas->getParcelasPedido($idPedido);
            $html = $this->template->render('app::email/venda/pedido-emitido', ['layout' => 'layout/email',
                'infoPedido' => $infoPedido,
                'itensPedido' => $itensPedido,
                'infoParcelas' => $infoParcelas
            ]);
            if(!empty($infoPedido->getCliente()->getEmail())){
                $emailEnviado = $this->enviaEmailService->enviarEmail(['destinatario' => $infoPedido->getCliente()->getEmail(),
                    'mensagem' => $html, 'assunto' => 'Cadastramento de Pedido - Semi joias' ]);
            }
            $msgEnvioEmail = ($emailEnviado== true)? "Alerta de e-email enviado ao cliente!" : "Alerta de e-email não enviado ao cliente!";

            if(in_array(date('d/m/Y'), $data['datasVencimento'])){
                $flash->addMessage('info', "Você cadastrou parcelas com vencimento hoje para o pedido com o código: ".
                    $pedido->getId() . " faça a quitação das parcelas com vencimento na data de hoje!");
                $uri = $this->router->generateUri('parcela.pay_order_plots', ['pedido' => $pedido->getId()]);
                return new RedirectResponse($uri);
            }

            $flash->addMessage('success', "Parcelas definidas com sucesso!");
            $uri = $this->router->generateUri('pedido.list');
            return new RedirectResponse($uri);
        }



        return new HtmlResponse($this->template->render('app::venda/pedido-parcela/definir-parcelas', [
            'flashMessages' => $flash->getMessages(),
            'infoPedido' => $infoPedido,
            'infoParcelas' => $infoParcelas,
            'formasPgto' =>$formasPgto,
            'formasPgtoJSON' => json_encode($formasPgto)
        ]));
    }
}