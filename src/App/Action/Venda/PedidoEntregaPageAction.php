<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29/03/17
 * Time: 10:30
 */

namespace App\Action\Venda;

use App\Entity\Pedido;
use App\Entity\MovFinPedido;
use App\Entity\FormaPgto;
use App\Service\Financeiro\ParcelaPedidoService;
use Doctrine\ORM\EntityManager;
use App\Entity\ItemPedido;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class PedidoEntregaPageAction {

    private $template;

    private $router;

    private $entityManager;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager

    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $idPedido = $request->getAttribute('pedido');
        $repoPedido = $this->entityManager->getRepository(Pedido::class);
        $repoParcelas = $this->entityManager->getRepository(MovFinPedido::class);

        $infoParcelas = $repoParcelas->getParcelasPedido($idPedido);
        $repositoryItensPedido = $this->entityManager->getRepository(ItemPedido::class);

        $infoPedido = $repoPedido->find($idPedido);

        if(!$infoPedido){
            $flash->addMessage('danger', "O pedido informado não é valido");
            $uri = $this->router->generateUri('pedido.list');
            return new RedirectResponse($uri);
        }
        if($infoPedido->getStatus() == "Cancelado"){
            $flash->addMessage('danger', "Pedidos cancelados não podem ser entregues!");
            $uri = $this->router->generateUri('pedido.list');
            return new RedirectResponse($uri);
        }
        if( !in_array($infoPedido->getStatus(), ["Entregue", "Parcelas em Quitação", "Pendente Entrega"])){
            $flash->addMessage('danger', "Pedidos a serem entregues devem ter itens e parcelas definidos!");
            $uri = $this->router->generateUri('pedido.list');
            return new RedirectResponse($uri);
        }
            $itensPedido = $repositoryItensPedido->getItensPedido($idPedido);


        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $pedido = $this->entityManager->getReference(Pedido::class, $idPedido);
            $pedido->setDataEntrega($data['dataEntrega']);
            $pedido->setStatus("Entregue");
            $this->entityManager->persist($pedido);
            $this->entityManager->flush();
            $flash->addMessage('success', "Pedido alterado com sucesso!");
            $uri = $this->router->generateUri('pedido.list');
            return new RedirectResponse($uri);
        }



        return new HtmlResponse($this->template->render('app::venda/pedido/entrega', [
            'flashMessages' => $flash->getMessage('success'),
            'infoPedido' => $infoPedido,
            'itensPedido' => $itensPedido,
            'infoParcelas' => $infoParcelas
        ]));
    }

} 