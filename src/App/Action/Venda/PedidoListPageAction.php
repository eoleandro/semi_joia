<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 04/03/17
 * Time: 15:20
 */

namespace App\Action\Venda;

use App\Entity\Cliente;
use App\Entity\Pedido;
use App\Form\Pesquisa\GrupoProdutoForm;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template;

class PedidoListPageAction
{
    private $template;

    private $entityManager;

    public function __construct( Template\TemplateRendererInterface $template = null, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $form = new GrupoProdutoForm();
        $repository = $this->entityManager->getRepository(Pedido::class);
        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->populateValues($data);
            $pedidos = $repository->getPedidos($data);
        }else{
            $pedidos = $repository->getPedidos();
        }

        return new HtmlResponse($this->template->render('app::venda/pedido/list', [
            'pedidos' => $pedidos,
            'flashMessages' => $flash->getMessages(),
            'form' => $form
        ]));
    }
}