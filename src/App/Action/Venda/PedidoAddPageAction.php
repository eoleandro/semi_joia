<?php

namespace App\Action\Venda;

use App\Entity\Cliente;
use App\Entity\ItemPedido;
use App\Entity\ItemRemessa;
use App\Entity\Pedido;
use App\Entity\Usuario;
use App\Service\Venda\PedidoService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class PedidoAddPageAction
{
    private $template;

    private $entityManager;

    private $router;

    private $persitePedido;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager,
        PedidoService $persisteRemessa
    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->persitePedido = $persisteRemessa;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $idCliente = $request->getAttribute('cliente');

        $repository = $this->entityManager->getRepository(ItemRemessa::class);
        $produtosPedido = $repository->getProdutosDisponiveisRemessas();
        $produtosPedidoParaJSON = $repository->getProdutosItemsPedidoParaJSON($produtosPedido);
        $repositoryCliente = $this->entityManager->getRepository(Cliente::class);
        $infoCliente = $repositoryCliente->find($idCliente);

        $flash = $request->getAttribute('flash');
        if(empty($produtosPedido)){
            $flash->addMessage('danger', "Não existem produtos disponíveis para realizar pedidos, abasteça o estoque!");
            $uri = $this->router->generateUri('remessa.define_provider');
            return new RedirectResponse($uri);
        }
        $produtosPedidoJSON = json_encode($produtosPedidoParaJSON);

        if($request->getMethod() == "POST"){

            $data = $request->getParsedBody();
            $hydrator = new ClassMethods();
            $pedido = new Pedido();
            $itemPedido = new ItemPedido();

            $hydrator->hydrate($data, $pedido);

            $pedidoGravado = $this->persitePedido->gravarDadosPedido($pedido, $itemPedido, $hydrator, $data);

            if(!is_null($pedidoGravado->getId()) ){
                $flash->addMessage('success', "Itens do pedido definidos com sucesso! Defina as parcelas do pedido!");
                $uri = $this->router->generateUri('parcela.define_plots', ['pedido' => $pedidoGravado->getId()]);
            }else{
                $flash->addMessage('danger', "Ocorreu um erro ao gerar pedido, tente novamente!");
                $uri = $this->router->generateUri('pedido.define_itens', ['cliente' => $idCliente]);
            }

            return new RedirectResponse($uri);

        }

        return new HtmlResponse($this->template->render('app::venda/pedido/definir-items', [
            'infoCliente' => $infoCliente,
            'produtosPedido' => $produtosPedido,
            'produtosPedidoJSON'=> $produtosPedidoJSON
        ]));
    }
}