<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01/04/17
 * Time: 15:38
 */

namespace App\Action\Venda;


use App\Form\Pesquisa\GrupoProdutoForm;
use Doctrine\ORM\EntityManager;
use App\Entity\Pedido;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;

class RelatorioPedidoPageAction
{
    private $template;

    private $entityManager;

    public function __construct( Template\TemplateRendererInterface $template = null, RouterInterface $router, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $repository = $this->entityManager->getRepository(Pedido::class);
        $form = new GrupoProdutoForm();
        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->populateValues($data);
            $pedidos = $repository->getPedidos($data);
        }else{
            $pedidos = $repository->getPedidos();
        }


        return new HtmlResponse($this->template->render('app::venda/pedido/relatorio', [
            'pedidos' => $pedidos,
            'flashMessages' => $flash->getMessages(),
            'form' => $form
        ]));
    }
}