<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29/03/17
 * Time: 12:18
 */

namespace App\Action\Venda;

use App\Entity\Pedido;
use App\Entity\MovFinPedido;
use App\Service\View\TemplateView;
use Doctrine\ORM\EntityManager;
use App\Entity\ItemPedido;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Expressive\ZendView\ZendViewRenderer;
use Zend\Mail\Message;
use Zend\Mail\Transport\SmtpOptions;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\View;

class PedidoShowPageAction {

    private $template;

    private $router;

    private $entityManager;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager

    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $idPedido = $request->getAttribute('pedido');
        $repoPedido = $this->entityManager->getRepository(Pedido::class);
        $repoParcelas = $this->entityManager->getRepository(MovFinPedido::class);

        $infoParcelas = $repoParcelas->getParcelasPedido($idPedido);
        $repositoryItensPedido = $this->entityManager->getRepository(ItemPedido::class);

        $infoPedido = $repoPedido->find($idPedido);

        if(!$infoPedido){
            $flash->addMessage('danger', "O pedido informado não é valido");
            $uri = $this->router->generateUri('pedido.list');
            return new RedirectResponse($uri);
        }
        $itensPedido = $repositoryItensPedido->getItensPedido($idPedido);

        $mail = new \PHPMailer();
        $mail->isSMTP();
        $mail->Host= "smtp.gmail.com";
        $mail->SMTPAuth = true;
        $mail->Username = 'erico.autocad@gmail.com';
        $mail->Password = '924440701987e';
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;
        $mail->CharSet = 'UTF-8';

        $mail->setFrom('erico.autocad@gmail.com', 'Erico Oliveira');
        $mail->addAddress('erico.autocad@gmail.com', 'Erico Oliveira');



        $html = $this->template->render('app::email/venda/pedido-emitido', [
            'layout' => 'layout/email',
            'infoPedido' => $infoPedido,
            'itensPedido' => $itensPedido,
            'infoParcelas' => $infoParcelas
        ]);


        $mail->isHTML(true);
        $mail->Subject = "Teste do Sistema Financeiro";
        $mail->Body = $html;
        $mail->AltBody = "Testano envio html Sistema Financeiro";
        //$mail->send();
        //echo $html;
        //die;

       /* $templateView = new TemplateView();
        $html = $templateView->render( './templates/app/email/venda/cancelamento-pedido.phtml', [
            'infoPedido' => $infoPedido,
            'itensPedido' => $itensPedido,
            'infoParcelas' => $infoParcelas
        ]);
        echo $html;
        die;
*/
        return new HtmlResponse($this->template->render('app::venda/pedido/show', [
            'flashMessages' => $flash->getMessage('success'),
            'infoPedido' => $infoPedido,
            'itensPedido' => $itensPedido,
            'infoParcelas' => $infoParcelas
        ]));

    }

}