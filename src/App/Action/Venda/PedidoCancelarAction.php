<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29/03/17
 * Time: 12:33
 */

namespace App\Action\Venda;

use App\Entity\ItemPedido;
use App\Entity\ItemRemessa;
use App\Entity\MovFinPedido;
use App\Entity\Pedido;
use App\Service\Venda\PedidoService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;

class PedidoCancelarAction
{
    private $template;

    private $entityManager;

    private $router;

    private $persitePedido;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager,
        PedidoService $persisteRemessa
    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->persitePedido = $persisteRemessa;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $idPedido = $request->getAttribute('pedido');
        $flash = $request->getAttribute('flash');

        $repositoryPedido= $this->entityManager->getRepository(Pedido::class);
        $pedido = $this->entityManager->getReference(Pedido::class, $idPedido);

        if(is_null($pedido)){
            $flash->addMessage('danger', "O pedido informado não é válido!");
            $uri = $this->router->generateUri('pedido.list');
            return new RedirectResponse($uri);
        }
        $repoMovFinPedido = $this->entityManager->getRepository(MovFinPedido::class);
        $parcelasPedido = $repoMovFinPedido->getParcelasPedido($idPedido);
        $repositoryItensPedido = $this->entityManager->getRepository(ItemPedido::class);
        $itensPedido = $repositoryItensPedido->getItensPedido($idPedido);

        $pedidoGravado = $this->persitePedido->cancelarPedido($pedido, $itensPedido, $parcelasPedido);

        if(!is_null($pedidoGravado->getId()) ){
            $flash->addMessage('success', "Pedido cancelado com sucesso!");
            $uri = $this->router->generateUri('pedido.list');
        }else{
            $flash->addMessage('danger', "Ocorreu um erro ao cancelar pedido, tente novamente!");
            $uri = $this->router->generateUri('pedido.show', ['pedido' => $pedidoGravado->getId()]);
        }

        return new RedirectResponse($uri);





    }
}