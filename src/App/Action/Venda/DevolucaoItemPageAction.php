<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 08/03/17
 * Time: 09:36
 */

namespace App\Action\Venda;

use App\Entity\ItemPedido;
use App\Entity\ItemRemessa;
use App\Entity\MovFinPedido;
use App\Entity\Pedido;
use App\Service\Financeiro\ParcelaPedidoService;
use App\Service\Venda\DevolucaoItemService;
use App\Service\Venda\PedidoService;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;
use Zend\Hydrator\ClassMethods;

class DevolucaoItemPageAction
{
    private $template;

    private $entityManager;

    private $router;

    private $devolucaoItemService;

    private $parcelaPedidoService;

    public function __construct(
        Template\TemplateRendererInterface $template = null,
        RouterInterface $router,
        EntityManager $entityManager,
        DevolucaoItemService $devolucaoItemService,
        ParcelaPedidoService $parcelaPedidoService
    )
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->devolucaoItemService= $devolucaoItemService;
        $this->parcelaPedidoService= $parcelaPedidoService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {

        $idPedido = $request->getAttribute('pedido');
        $flash = $request->getAttribute('flash');
        $repositoryPedido= $this->entityManager->getRepository(Pedido::class);
        $infoPedido = $repositoryPedido->find($idPedido);
        $repositoryItensPedido = $this->entityManager->getRepository(ItemPedido::class);
        $itensPedido = $repositoryItensPedido->getItensPedido($idPedido);

        if(is_null($infoPedido)){
            $flash->addMessage('danger', "O pedido informado não é válido!");
            $uri = $this->router->generateUri('pedido.list');
            return new RedirectResponse($uri);
        }
        $infoCliente = $infoPedido->getCliente();
        $repoMovFinPedido = $this->entityManager->getRepository(MovFinPedido::class);
        $parcelasEmAberto =$repoMovFinPedido->getParcelasPedido($idPedido, ['Quitada' => 'nao']);
        if(empty($parcelasEmAberto)){
            $flash->addMessage('danger', "O pedido já possui todas as parcelas quitadas e portanto não pode ter itens alterados, crie um novo pedido!");
            $uri = $this->router->generateUri('pedido.list');
            return new RedirectResponse($uri);
        }

        if($request->getMethod() == "POST"){

            $data = $request->getParsedBody();

            $idItensPedido = array_filter( $data['idItensPedido'], function($v, $k) { return $v != "";}, ARRAY_FILTER_USE_BOTH);

            $itensDevolverForaDeLinha = $repositoryItensPedido->getItensDevolverForaDeLinha($idPedido,$idItensPedido);

            if(count($itensDevolverForaDeLinha) != count($idItensPedido)){
                $flash->addMessage('danger', "Não foi possível realizar a devolução. Existem produtos na devolução que ".
                    "não estão mais ativos no sistema! Faça um lançamento de reembolso no sistema para o cliente!");
                $uri = $this->router->generateUri('devolucao.definir_itens', ['pedido' => $infoPedido->getId()]);
            }else{
                $valorDevolver = $this->devolucaoItemService->gravarDadosDevolucaoItens($infoPedido, $data);
                $movFinanceiroReembolso = $this->parcelaPedidoService->recalcularParcelasDevolucaoPedido($infoPedido, $valorDevolver,$parcelasEmAberto);
                $uri = $this->router->generateUri('parcela.pay_order_plots', ['pedido' => $infoPedido->getId()]);

            }

            return new RedirectResponse($uri);

        }

        return new HtmlResponse($this->template->render('app::venda/devolucao/definir-itens', [
            'infoCliente' => $infoCliente,
            'infoPedido' => $infoPedido,
            'itensPedido' => $itensPedido,
            'flashMessages' => $flash->getMessages()
        ]));
    }
}