<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/04/17
 * Time: 08:07
 */

namespace App\Action\Venda\Factory;

use App\Action\Venda\RelatorioVendaClientePageAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class RelatorioVendaClientePageFactory {

    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new RelatorioVendaClientePageAction(
            $template,
            $router,
            $entityManager
        );
    }
}