<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 04/03/17
 * Time: 15:07
 */

namespace App\Action\Venda\Factory;

use App\Action\Venda\PedidoParcelaPageAction;
use App\Service\Email\EnviaEmailService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use App\Service\Financeiro\ParcelaPedidoService;


class PedidoParcelaPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);
        $parcelaPedidoService = $container->get(ParcelaPedidoService::class);
        $enviaEmailService = $container->get(EnviaEmailService::class);


        return new PedidoParcelaPageAction( $template, $router, $entityManager, $parcelaPedidoService, $enviaEmailService);
    }
}