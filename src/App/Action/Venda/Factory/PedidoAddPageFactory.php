<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 03/03/17
 * Time: 11:32
 */

namespace App\Action\Venda\Factory;

use App\Action\Venda\PedidoAddPageAction;
use App\Service\Venda\PedidoService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;


class PedidoAddPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);

        $persistePedido = $container->get(PedidoService::class);

        return new PedidoAddPageAction( $template, $router, $entityManager, $persistePedido);
    }
}