<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 08/03/17
 * Time: 09:37
 */

namespace App\Action\Venda\Factory;


use App\Action\Venda\DevolucaoItemPageAction;
use App\Service\Venda\DevolucaoItemService;
use App\Service\Financeiro\ParcelaPedidoService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;


class DevolucaoItemPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);

        $devolucaoItemService = $container->get(DevolucaoItemService::class);
        $parcelaPedidoService = $container->get(ParcelaPedidoService::class);

        return new DevolucaoItemPageAction(
            $template,
            $router,
            $entityManager,
            $devolucaoItemService,
            $parcelaPedidoService
        );
    }
}