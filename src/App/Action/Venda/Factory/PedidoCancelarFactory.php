<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29/03/17
 * Time: 12:32
 */

namespace App\Action\Venda\Factory;

use App\Action\Venda\PedidoCancelarAction;
use App\Action\Venda\PedidoEditPageAction;
use App\Service\Venda\PedidoService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class PedidoCancelarFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);

        $persistePedido = $container->get(PedidoService::class);

        return new PedidoCancelarAction( $template, $router, $entityManager, $persistePedido);
    }
}