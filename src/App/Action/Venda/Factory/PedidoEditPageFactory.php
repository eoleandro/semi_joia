<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06/03/17
 * Time: 11:59
 */

namespace App\Action\Venda\Factory;

use App\Action\Venda\PedidoEditPageAction;
use App\Service\Venda\PedidoService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class PedidoEditPageFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);

        $persistePedido = $container->get(PedidoService::class);

        return new PedidoEditPageAction( $template, $router, $entityManager, $persistePedido);
    }
}