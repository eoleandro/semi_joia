<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/04/17
 * Time: 08:06
 */

namespace App\Action\Venda;

use App\Form\Pesquisa\GrupoProdutoForm;
use Doctrine\ORM\EntityManager;
use App\Entity\Pedido;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;

class RelatorioVendaClientePageAction
{
    private $template;

    private $entityManager;

    public function __construct( Template\TemplateRendererInterface $template = null, RouterInterface $router, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $repository = $this->entityManager->getRepository(Pedido::class);
        $form = new GrupoProdutoForm();
        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->populateValues($data);
            $pedidos = $repository->getVendasPorCliente($data);
        }else{
            $pedidos = $repository->getVendasPorCliente();
        }


        return new HtmlResponse($this->template->render('app::venda/pedido/venda-por-cliente', [
            'pedidos' => $pedidos,
            'flashMessages' => $flash->getMessages(),
            'form' => $form
        ]));
    }
}