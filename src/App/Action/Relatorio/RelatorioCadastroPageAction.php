<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/04/17
 * Time: 13:46
 */

namespace App\Action\Relatorio;

use App\Form\Pesquisa\PesquisaRelatorioCadastroForm;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;

class RelatorioCadastroPageAction
{
    private $template;

    private $entityManager;

    private $router;

    public function __construct( Template\TemplateRendererInterface $template = null, RouterInterface $router, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');
        $nomeCadastro = $request->getAttribute('nomeCadastro');
        $nomeCadastro = trim($nomeCadastro);
        if(empty($nomeCadastro)){
            $flash->addMessage('danger', "O cadastro selecionado não é válido!");
            $uri = $this->router->generateUri('relatorio.record_select');
            return new RedirectResponse($uri);
        }
        $nomeEntity = '\\App\\Entity\\'. $nomeCadastro ;
        $nomeForm   = "\\App\\Form\\Pesquisa\\PesquisaCadastro".$nomeCadastro."Form";

        if (!class_exists($nomeForm)){
            $nomeForm = "\\App\\Form\\Pesquisa\\PesquisaNomeForm";
        }
        $repository = $this->entityManager->getRepository($nomeEntity);
        $cabecalhosRelatorio = $repository->getCabecalhosRelatorioCadastro();
        $form = new $nomeForm;

        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->populateValues($data);
            $dadosListar = $repository->getListaDados($data);
        }else{
            $dadosListar =  $repository->getListaDados();
        }

        return new HtmlResponse($this->template->render('app::relatorio/cadastro/list', [
            'flashMessages' => $flash->getMessages(),
            'form' => $form,
            'cabecalhosRelatorio' => $cabecalhosRelatorio,
            'dadosListar' => $dadosListar,
            'nomeCadastro' => $nomeCadastro,
            'nomeEntity' => $nomeEntity

        ]));
    }
}