<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/04/17
 * Time: 09:45
 */

namespace App\Action\Relatorio;

use App\Entity\Pedido;
use App\Form\Pesquisa\AgendaForm;
use App\Form\Pesquisa\PesquisaRelatorioCadastroForm;
use Doctrine\ORM\EntityManager;
use App\Entity\Agenda;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template;

class EscolherCadastroPageAction
{
    private $template;

    private $entityManager;

    private $router;

    public function __construct( Template\TemplateRendererInterface $template = null, RouterInterface $router, EntityManager $entityManager)
    {
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $flash = $request->getAttribute('flash');

        $form = new PesquisaRelatorioCadastroForm();
        if($request->getMethod() == "POST"){
            $data = $request->getParsedBody();
            $form->populateValues($data);
            $uri = $this->router->generateUri('relatorio.record', ['nomeCadastro' => $data['cadastro']]);
            return new RedirectResponse($uri);
        }

        return new HtmlResponse($this->template->render('app::relatorio/cadastro/escolher-cadastro', [
            'flashMessages' => $flash->getMessages(),
            'form' => $form
        ]));
    }
}