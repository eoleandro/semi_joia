<?php

namespace App\Action\Relatorio\Factory;

use App\Action\Relatorio\RelatorioCadastroPageAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class RelatorioCadastroPageFactory {

    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new RelatorioCadastroPageAction(
            $template,
            $router,
            $entityManager
        );
    }
}