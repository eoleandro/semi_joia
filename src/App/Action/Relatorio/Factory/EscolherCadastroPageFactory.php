<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/04/17
 * Time: 09:46
 */

namespace App\Action\Relatorio\Factory;

use App\Action\Relatorio\EscolherCadastroPageAction;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class EscolherCadastroPageFactory {

    public function __invoke(ContainerInterface $container)
    {

        $template = $container->get(TemplateRendererInterface::class);
        $router   = $container->get(RouterInterface::class);
        $entityManager = $container->get(EntityManager::class);

        return new EscolherCadastroPageAction(
            $template,
            $router,
            $entityManager
        );
    }
}