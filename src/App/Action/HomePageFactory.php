<?php

namespace App\Action;

use App\Service\AnnotationReaderService;
use App\Service\TesteService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class HomePageFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $router   = $container->get(RouterInterface::class);
        $template = ($container->has(TemplateRendererInterface::class))
            ? $container->get(TemplateRendererInterface::class)
            : null;

        $entityManager = $container->get(EntityManager::class);

        return new HomePageAction($router, $template, $entityManager);
    }
}
