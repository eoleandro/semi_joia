<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MovFinPedido
 *
 * @ORM\Table(name="mov_fin_pedido", indexes={@ORM\Index(name="fk_mov_fin_pedido_pedido1_idx", columns={"pedido_id"}), @ORM\Index(name="fk_mov_fin_pedido_mov_financeiro1_idx", columns={"mov_financeiro_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\Financeiro\MovFinPedidoRepository")
 */
class MovFinPedido
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \App\Entity\MovFinanceiro
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\MovFinanceiro", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mov_financeiro_id", referencedColumnName="id")
     * })
     */
    private $movFinanceiro;

    /**
     * @var \App\Entity\Pedido
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Pedido", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pedido_id", referencedColumnName="id")
     * })
     */
    private $pedido;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \App\Entity\MovFinanceiro $movFinanceiro
     */
    public function setMovFinanceiro($movFinanceiro)
    {
        $this->movFinanceiro = $movFinanceiro;
    }

    /**
     * @return \App\Entity\MovFinanceiro
     */
    public function getMovFinanceiro()
    {
        return $this->movFinanceiro;
    }

    /**
     * @param \App\Entity\Pedido $pedido
     */
    public function setPedido($pedido)
    {
        $this->pedido = $pedido;
    }

    /**
     * @return \App\Entity\Pedido
     */
    public function getPedido()
    {
        return $this->pedido;
    }




}

