<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Hydrator\ClassMethods;


/**
 * RemessaFornecedor
 *
 * @ORM\Table(name="remessa_fornecedor", indexes={@ORM\Index(name="fk_produto_estoque_fornecedor1_idx", columns={"fornecedor_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\Estoque\RemessaFornecedorRepository")
 */
class RemessaFornecedor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_total", type="float", precision=10, scale=0, nullable=true)
     */
    private $valorTotal;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_parcial", type="float", precision=10, scale=0, nullable=true)
     */
    private $valorParcial;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;
    //private $dataCadastro = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_entrada", type="datetime", nullable=true)
     */
    private $dataEntrada;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_devolucao", type="datetime", nullable=true)
     */
    private $dataDevolucao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_quitacao", type="datetime", nullable=true)
     */
    private $dataQuitacao;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=45, nullable=true)
     */
    private $status;

    /**
     * @var \Fornecedor
     *
     * @ORM\ManyToOne(targetEntity="Fornecedor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fornecedor_id", referencedColumnName="id")
     * })
     */
    private $fornecedor;

    /**
     * @var \Usuario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return RemessaFornecedor
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getValorTotal()
    {
        return $this->valorTotal;
    }

    /**
     * @param int $valorTotal
     * @return RemessaFornecedor
     */
    public function setValorTotal($valorTotal)
    {
        $this->valorTotal = $valorTotal;
        return $this;
    }

    /**
     * @return int
     */
    public function getValorParcial()
    {
        return $this->valorParcial;
    }

    /**
     * @param int $valorParcial
     * @return RemessaFornecedor
     */
    public function setValorParcial($valorParcial)
    {
        $this->valorParcial = $valorParcial;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param DateTime $dataCadastro
     * @return RemessaFornecedor
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDataEntrada()
    {
        return $this->dataEntrada;
    }

    /**
     * @param DateTime $dataEntrada
     * @return RemessaFornecedor
     */
    public function setDataEntrada($dataEntrada)
    {
        //$this->dataEntrada = $dataEntrada;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDataDevolucao()
    {
        return $this->dataDevolucao;
    }

    /**
     * @param DateTime $dataDevolucao
     * @return RemessaFornecedor
     */
    public function setDataDevolucao($dataDevolucao)
    {
        if(!empty($dataDevolucao)){
            $dataDevolucao = \DateTime::createFromFormat("d/m/Y", $dataDevolucao);
        }else{
            $dataDevolucao = null;
        }
        $this->dataDevolucao = $dataDevolucao;
        return $this;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDataQuitacao()
    {
        return $this->dataQuitacao;
    }

    /**
     * @param DateTime $dataQuitacao
     * @return RemessaFornecedor
     */
    public function setDataQuitacao($dataQuitacao)
    {
        if(!empty($dataQuitacao)){
            $dataQuitacao = \DateTime::createFromFormat("d/m/Y", $dataQuitacao);
        }else{
            $dataQuitacao = null;
        }
        $this->dataQuitacao = $dataQuitacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return RemessaFornecedor
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Fornecedor
     */
    public function getFornecedor()
    {
        return $this->fornecedor;
    }

    /**
     * @param Fornecedor $fornecedor
     * @return RemessaFornecedor
     */
    public function setFornecedor($fornecedor)
    {
        $this->fornecedor = $fornecedor;
        return $this;
    }

    public function __construct(array $data = array(), ClassMethods $hydrator = null)
    {
        if(!empty($data) && !is_null($hydrator)){
            $hydrator->hydrate($data, $this);
        }
    }

    /**
     * @param \Usuario $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * @return \Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

}
