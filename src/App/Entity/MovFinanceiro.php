<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Hydrator\ClassMethods;

/**
 * MovFinanceiro
 *
 * @ORM\Table(name="mov_financeiro", indexes={@ORM\Index(name="fk_mov_financeiro_forma_pgto1_idx", columns={"forma_pgto_id"}), @ORM\Index(name="fk_mov_financeiro_fornecedor1_idx", columns={"fornecedor_id"}), @ORM\Index(name="fk_mov_financeiro_plano_conta1_idx", columns={"plano_conta_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\Financeiro\MovFinanceiroRepository")
 */
class MovFinanceiro
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=250, nullable=true)
     */
    private $descricao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_cadastro", type="datetime", nullable=false)
     */
    private $dataCadastro ;
    //private $dataCadastro = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_vencimento", type="datetime", nullable=true)
     */
    private $dataVencimento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_quitacao", type="datetime", nullable=true)
     */
    private $dataQuitacao;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", precision=10, scale=0, nullable=true)
     */
    private $valor;

    /**
     * @var string
     *
     * @ORM\Column(name="natureza", type="string", length=45, nullable=true)
     */
    private $natureza;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=45, nullable=true)
     */
    private $tipo;

    /**
     * @var integer
     *
     * @ORM\Column(name="numero_parc", type="integer", nullable=true)
     */
    private $numeroParc;

    /**
     * @var string
     *
     * @ORM\Column(name="observacao", type="string", length=255, nullable=true)
     */
    private $observacao;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=45, nullable=true)
     */
    private $status;

    /**
     * @var \FormaPgto
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\FormaPgto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="forma_pgto_id", referencedColumnName="id")
     * })
     */
    private $formaPgto;

    /**
     * @var \PlanoConta
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\PlanoConta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plano_conta_id", referencedColumnName="id")
     * })
     */
    private $planoConta;

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataQuitacao
     */
    public function setDataQuitacao($dataQuitacao)
    {
        $this->dataQuitacao = $dataQuitacao;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataQuitacao()
    {
        return $this->dataQuitacao;
    }

    /**
     * @param \DateTime $dataVencimento
     */
    public function setDataVencimento($dataVencimento)
    {
        $this->dataVencimento = $dataVencimento;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataVencimento()
    {
        return $this->dataVencimento;
    }

    /**
     * @param string $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param \FormaPgto $formaPgto
     */
    public function setFormaPgto($formaPgto)
    {
        $this->formaPgto = $formaPgto;
        return $this;
    }

    /**
     * @return \FormaPgto
     */
    public function getFormaPgto()
    {
        return $this->formaPgto;
    }


    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $natureza
     */
    public function setNatureza($natureza)
    {
        $this->natureza = $natureza;
    }

    /**
     * @return string
     */
    public function getNatureza()
    {
        return $this->natureza;
    }

    /**
     * @param int $numeroParc
     */
    public function setNumeroParc($numeroParc)
    {
        $this->numeroParc = $numeroParc;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumeroParc()
    {
        return $this->numeroParc;
    }

    /**
     * @param string $observacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param \PlanoConta $planoConta
     */
    public function setPlanoConta($planoConta)
    {
        $this->planoConta = $planoConta;
        return $this;
    }

    /**
     * @return \PlanoConta
     */
    public function getPlanoConta()
    {
        return $this->planoConta;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param float $valor
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
        return $this;
    }

    /**
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    public function toArray(ClassMethods $hydrator )
    {
        return $hydrator->extract($this);
    }



}

