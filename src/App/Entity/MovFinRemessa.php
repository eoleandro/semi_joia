<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MovFinRemessa
 *
 * @ORM\Table(name="mov_fin_remessa", indexes={@ORM\Index(name="fk_mov_fin_remessa_mov_financeiro1_idx", columns={"mov_financeiro_id"}), @ORM\Index(name="fk_mov_fin_remessa_remessa_fornecedor1_idx", columns={"remessa_fornecedor_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\Financeiro\MovFinRemessaRepository")
 */
class MovFinRemessa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \App\Entity\MovFinanceiro
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\MovFinanceiro", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mov_financeiro_id", referencedColumnName="id")
     * })
     */
    private $movFinanceiro;

    /**
     * @var \App\Entity\RemessaFornecedor
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\RemessaFornecedor", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="remessa_fornecedor_id", referencedColumnName="id")
     * })
     */
    private $remessaFornecedor;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \App\Entity\MovFinanceiro $movFinanceiro
     */
    public function setMovFinanceiro($movFinanceiro)
    {
        $this->movFinanceiro = $movFinanceiro;
    }

    /**
     * @return \App\Entity\MovFinanceiro
     */
    public function getMovFinanceiro()
    {
        return $this->movFinanceiro;
    }

    /**
     * @param \App\Entity\RemessaFornecedor $remessaFornecedor
     */
    public function setRemessaFornecedor($remessaFornecedor)
    {
        $this->remessaFornecedor = $remessaFornecedor;
    }

    /**
     * @return \App\Entity\RemessaFornecedor
     */
    public function getRemessaFornecedor()
    {
        return $this->remessaFornecedor;
    }




}

