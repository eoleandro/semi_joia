<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PermissaoAcl
 *
 * @ORM\Table(name="permissao_acl", indexes={@ORM\Index(name="fk_permissao_acl_recurso_sistema1_idx", columns={"recurso_sistema_id"}), @ORM\Index(name="fk_permissao_acl_grupo1_idx", columns={"grupo_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\Usuario\PermissaoAclRepository")
 */
class PermissaoAcl
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="permissao", type="string", length=45, nullable=true)
     */
    private $permissao;

    /**
     * @var \Grupo
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Grupo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="grupo_id", referencedColumnName="id")
     * })
     */
    private $grupo;

    /**
     * @var \RecursoSistema
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\RecursoSistema", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="recurso_sistema_id", referencedColumnName="id")
     * })
     */
    private $recursoSistema;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return PermissaoAcl
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getPermissao()
    {
        return $this->permissao;
    }

    /**
     * @param string $permissao
     * @return PermissaoAcl
     */
    public function setPermissao($permissao)
    {
        $this->permissao = $permissao;
        return $this;
    }

    /**
     * @return \Grupo
     */
    public function getGrupo()
    {
        return $this->grupo;
    }

    /**
     * @param \Grupo $grupo
     * @return PermissaoAcl
     */
    public function setGrupo($grupo)
    {
        $this->grupo = $grupo;
        return $this;
    }

    /**
     * @return \RecursoSistema
     */
    public function getRecursoSistema()
    {
        return $this->recursoSistema;
    }

    /**
     * @param \RecursoSistema $recursoSistema
     * @return PermissaoAcl
     */
    public function setRecursoSistema($recursoSistema)
    {
        $this->recursoSistema = $recursoSistema;
        return $this;
    }




}

