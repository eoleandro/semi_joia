<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Hydrator\ClassMethods;

/**
 * Fornecedor
 *
 * @ORM\Table(name="fornecedor")
 * @ORM\Entity(repositoryClass="App\Repository\Estoque\FornecedorRepository")
 */
class Fornecedor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="razao_social", type="string", length=255, nullable=true)
     */
    private $razaoSocial;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_fantasia", type="string", length=255, nullable=true)
     */
    private $nomeFantasia;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=60, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="CNPJ", type="string", length=45, nullable=true)
     */
    private $cnpj;

    /**
     * @var string
     *
     * @ORM\Column(name="telefone_fixo", type="string", length=45, nullable=true)
     */
    private $telefoneFixo;

    /**
     * @var string
     *
     * @ORM\Column(name="telefone_celular", type="string", length=45, nullable=true)
     */
    private $telefoneCelular;

    /**
     * @var string
     *
     * @ORM\Column(name="insc_estadual", type="string", length=45, nullable=true)
     */
    private $inscEstadual;

    /**
     * @var string
     *
     * @ORM\Column(name="insc_municipal", type="string", length=45, nullable=true)
     */
    private $inscMunicipal;

    /**
     * @var string
     *
     * @ORM\Column(name="ramo_atividade", type="string", length=100, nullable=true)
     */
    private $ramoAtividade;

    /**
     * @var string
     *
     * @ORM\Column(name="observacao", type="string", length=250, nullable=true)
     */
    private $observacao;

    /**
     * @var integer
     *
     * @ORM\Column(name="ativo", type="integer", nullable=true)
     */
    private $ativo = '1';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Fornecedor
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getRazaoSocial()
    {
        return $this->razaoSocial;
    }

    /**
     * @param string $razaoSocial
     * @return Fornecedor
     */
    public function setRazaoSocial($razaoSocial)
    {
        $this->razaoSocial = $razaoSocial;
        return $this;
    }

    /**
     * @return string
     */
    public function getNomeFantasia()
    {
        return $this->nomeFantasia;
    }

    /**
     * @param string $nomeFantasia
     * @return Fornecedor
     */
    public function setNomeFantasia($nomeFantasia)
    {
        $this->nomeFantasia = $nomeFantasia;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Fornecedor
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * @param string $cnpj
     * @return Fornecedor
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelefoneFixo()
    {
        return $this->telefoneFixo;
    }

    /**
     * @param string $telefoneFixo
     * @return Fornecedor
     */
    public function setTelefoneFixo($telefoneFixo)
    {
        $this->telefoneFixo = $telefoneFixo;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelefoneCelular()
    {
        return $this->telefoneCelular;
    }

    /**
     * @param string $telefoneCelular
     * @return Fornecedor
     */
    public function setTelefoneCelular($telefoneCelular)
    {
        $this->telefoneCelular = $telefoneCelular;
        return $this;
    }

    /**
     * @return string
     */
    public function getInscEstadual()
    {
        return $this->inscEstadual;
    }

    /**
     * @param string $inscEstadual
     * @return Fornecedor
     */
    public function setInscEstadual($inscEstadual)
    {
        $this->inscEstadual = $inscEstadual;
        return $this;
    }

    /**
     * @return string
     */
    public function getInscMunicipal()
    {
        return $this->inscMunicipal;
    }

    /**
     * @param string $inscMunicipal
     * @return Fornecedor
     */
    public function setInscMunicipal($inscMunicipal)
    {
        $this->inscMunicipal = $inscMunicipal;
        return $this;
    }

    /**
     * @return string
     */
    public function getRamoAtividade()
    {
        return $this->ramoAtividade;
    }

    /**
     * @param string $ramoAtividade
     * @return Fornecedor
     */
    public function setRamoAtividade($ramoAtividade)
    {
        $this->ramoAtividade = $ramoAtividade;
        return $this;
    }

    /**
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param string $observacao
     * @return Fornecedor
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
        return $this;
    }

    public function __construct(array $data = array(), ClassMethods $hydrator = null)
    {
        if(!empty($data) && !is_null($hydrator)){
            $hydrator->hydrate($data, $this);
        }
    }

    /**
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * @param int $ativo
     * @return Produto
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
        return $this;
    }

    public function __toString()
    {
        return $this->nomeFantasia;
    }


}

