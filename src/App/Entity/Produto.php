<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Produto
 *
 * @ORM\Table(name="produto")
 * @ORM\Entity(repositoryClass="App\Repository\Estoque\ProdutoRepository")
 */
class Produto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=250, nullable=true)
     */
    private $nome;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var float
     *
     * @ORM\Column(name="preco", type="float", precision=10, scale=0, nullable=true)
     */
    private $preco;

    /**
     * @var string
     *
     * @ORM\Column(name="metal", type="string", length=100, nullable=true)
     */
    private $metal;

    /**
     * @var string
     *
     * @ORM\Column(name="peso", type="string", length=45, nullable=true)
     */
    private $peso;

    /**
     * @var string
     *
     * @ORM\Column(name="formato", type="string", length=45, nullable=true)
     */
    private $formato;

    /**
     * @var string
     *
     * @ORM\Column(name="pedra", type="string", length=255, nullable=true)
     */
    private $pedra;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_pedra", type="string", length=45, nullable=true)
     */
    private $tipoPedra;

    /**
     * @var string
     *
     * @ORM\Column(name="cor_pedra", type="string", length=45, nullable=true)
     */
    private $corPedra;

    /**
     * @var string
     *
     * @ORM\Column(name="dimensao_pedra", type="string", length=45, nullable=true)
     */
    private $dimensaoPedra;

    /**
     * @var string
     *
     * @ORM\Column(name="garantia", type="string", length=45, nullable=true)
     */
    private $garantia;

    /**
     * @var integer
     *
     * @ORM\Column(name="ativo", type="integer", nullable=true)
     */
    private $ativo = '1';

    /**
     * @var \Fornecedor
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Fornecedor", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fornecedor_id", referencedColumnName="id")
     * })
     */
    private $fornecedor;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Produto
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Fornecedor
     */
    public function getFornecedor()
    {
        return $this->fornecedor;
    }

    /**
     * @param Fornecedor $fornecedor
     * @return Produto
     */
    public function setFornecedor($fornecedor)
    {
        $this->fornecedor = $fornecedor;
        return $this;
    }



    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     * @return Produto
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param DateTime $dataCadastro
     * @return Produto
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
        return $this;
    }

    /**
     * @return float
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * @param float $preco
     * @return Produto
     */
    public function setPreco($preco)
    {
        $this->preco = $preco;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetal()
    {
        return $this->metal;
    }

    /**
     * @param string $metal
     * @return Produto
     */
    public function setMetal($metal)
    {
        $this->metal = $metal;
        return $this;
    }

    /**
     * @return string
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * @param string $peso
     * @return Produto
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;
        return $this;
    }

    /**
     * @return string
     */
    public function getFormato()
    {
        return $this->formato;
    }

    /**
     * @param string $formato
     * @return Produto
     */
    public function setFormato($formato)
    {
        $this->formato = $formato;
        return $this;
    }

    /**
     * @return string
     */
    public function getPedra()
    {
        return $this->pedra;
    }

    /**
     * @param string $pedra
     * @return Produto
     */
    public function setPedra($pedra)
    {
        $this->pedra = $pedra;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipoPedra()
    {
        return $this->tipoPedra;
    }

    /**
     * @param string $tipoPedra
     * @return Produto
     */
    public function setTipoPedra($tipoPedra)
    {
        $this->tipoPedra = $tipoPedra;
        return $this;
    }

    /**
     * @return string
     */
    public function getCorPedra()
    {
        return $this->corPedra;
    }

    /**
     * @param string $corPedra
     * @return Produto
     */
    public function setCorPedra($corPedra)
    {
        $this->corPedra = $corPedra;
        return $this;
    }

    /**
     * @return string
     */
    public function getDimensaoPedra()
    {
        return $this->dimensaoPedra;
    }

    /**
     * @param string $dimensaoPedra
     * @return Produto
     */
    public function setDimensaoPedra($dimensaoPedra)
    {
        $this->dimensaoPedra = $dimensaoPedra;
        return $this;
    }

    /**
     * @return string
     */
    public function getGarantia()
    {
        return $this->garantia;
    }

    /**
     * @param string $garantia
     * @return Produto
     */
    public function setGarantia($garantia)
    {
        $this->garantia = $garantia;
        return $this;
    }

    /**
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * @param int $ativo
     * @return Produto
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
        return $this;
    }

    public function __toString()
    {
        return $this->nome;
    }



}

