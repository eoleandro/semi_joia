<?php



namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Element\DateTime;
use Zend\Hydrator\ClassMethods;

/**
 * ItemRemessa
 *
 * @ORM\Table(name="item_remessa", indexes={@ORM\Index(name="fk_item_remessa_produto_estoque1_idx", columns={"produto_estoque_id"}), @ORM\Index(name="fk_item_remessa_produto1_idx", columns={"produto_id"}), @ORM\Index(name="fk_item_remessa_usuario1_idx", columns={"usuario_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\Estoque\ItemRemessaRepository")
 */
class ItemRemessa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="preco_custo", type="float", precision=10, scale=0, nullable=true)
     */
    private $precoCusto;

    /**
     * @var float
     *
     * @ORM\Column(name="preco_cliente", type="float", precision=10, scale=0, nullable=true)
     */
    private $precoCliente;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantidade", type="integer", nullable=true)
     */
    private $quantidade;

    /**
     * @var integer
     *
     * @ORM\Column(name="quant_parcial", type="integer", nullable=true)
     */
    private $quantParcial;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_entrada", type="datetime", nullable=true)
     */
    private $dataEntrada;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_devolucao", type="datetime", nullable=true)
     */
    private $dataDevolucao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_quitacao", type="datetime", nullable=true)
     */
    private $dataQuitacao;

    /**
     * @var \Produto
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Produto", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="produto_id", referencedColumnName="id")
     * })
     */
    private $produto;

    /**
     * @var \RemessaFornecedor
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\RemessaFornecedor", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="remessa_fornecedor_id", referencedColumnName="id")
     * })
     */
    private $remessaFornecedor;

    /**
     * @var \Usuario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ItemRemessa
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrecoCusto()
    {
        return $this->precoCusto;
    }

    /**
     * @param float $precoCusto
     * @return ItemRemessa
     */
    public function setPrecoCusto($precoCusto)
    {
        $this->precoCusto = $precoCusto;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrecoCliente()
    {
        return $this->precoCliente;
    }

    /**
     * @param float $precoCliente
     * @return ItemRemessa
     */
    public function setPrecoCliente($precoCliente)
    {
        $this->precoCliente = $precoCliente;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @param int $quantidade
     * @return ItemRemessa
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantParcial()
    {
        return $this->quantParcial;
    }

    /**
     * @param int $quantParcial
     * @return ItemRemessa
     */
    public function setQuantParcial($quantParcial)
    {
        $this->quantParcial = $quantParcial;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param DateTime $dataCadastro
     * @return ItemRemessa
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDataEntrada()
    {
        return $this->dataEntrada;
    }

    /**
     * @param DateTime $dataEntrada
     * @return ItemRemessa
     */
    public function setDataEntrada($dataEntrada)
    {
        //$this->dataEntrada = $dataEntrada;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDataDevolucao()
    {
        return $this->dataDevolucao;
    }

    /**
     * @param DateTime $dataDevolucao
     * @return ItemRemessa
     */
    public function setDataDevolucao($dataDevolucao)
    {
        //$this->dataDevolucao = $dataDevolucao;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDataQuitacao()
    {
        return $this->dataQuitacao;
    }

    /**
     * @param DateTime $dataQuitacao
     * @return ItemRemessa
     */
    public function setDataQuitacao($dataQuitacao)
    {
        $this->dataQuitacao = $dataQuitacao;
        return $this;
    }

    /**
     * @return Produto
     */
    public function getProduto()
    {
        return $this->produto;
    }

    /**
     * @param Produto $produto
     * @return ItemRemessa
     */
    public function setProduto($produto)
    {
        $this->produto = $produto;
        return $this;
    }


    /**
     * @return Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param Usuario $usuario
     * @return ItemRemessa
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
        return $this;
    }

    /**
     * @return \RemessaFornecedor
     */
    public function getRemessaFornecedor()
    {
        return $this->remessaFornecedor;
    }

    /**
     * @param \RemessaFornecedor $remessaFornecedor
     * @return ItemRemessa
     */
    public function setRemessaFornecedor($remessaFornecedor)
    {
        $this->remessaFornecedor = $remessaFornecedor;
        return $this;
    }



    public function __construct(array $data = array(), ClassMethods $hydrator = null)
    {
        if(!empty($data) && !is_null($hydrator)){
            $hydrator->hydrate($data, $this);
        }
    }




}

