<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Hydrator\ClassMethods;

/**
 * Cliente
 *
 * @ORM\Table(name="cliente")
 * @ORM\Entity(repositoryClass="App\Repository\Venda\ClienteRepository")
 */
class Cliente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=250, nullable=true)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="CPF", type="string", length=45, nullable=true)
     */
    private $cpf;

    /**
     * @var string
     *
     * @ORM\Column(name="sexo", type="string", length=45, nullable=true)
     */
    private $sexo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_nascimento", type="datetime", nullable=true)
     */
    private $dataNascimento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_casamento", type="datetime", nullable=true)
     */
    private $dataCasamento;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_conjuge", type="string", length=250, nullable=true)
     */
    private $nomeConjuge;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=60, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="telefone_fixo", type="string", length=45, nullable=true)
     */
    private $telefoneFixo;

    /**
     * @var string
     *
     * @ORM\Column(name="telefone_celular", type="string", length=45, nullable=true)
     */
    private $telefoneCelular;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=45, nullable=true)
     */
    private $tipo = 'PF';

    /**
     * @var string
     *
     * @ORM\Column(name="CNPJ", type="string", length=45, nullable=true)
     */
    private $cnpj;

    /**
     * @var string
     *
     * @ORM\Column(name="razao_social", type="string", length=255, nullable=true)
     */
    private $razaoSocial;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_fantasia", type="string", length=255, nullable=true)
     */
    private $nomeFantasia;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var integer
     *
     * @ORM\Column(name="ativo", type="integer", nullable=true)
     */
    private $ativo = '1';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Cliente
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     * @return Cliente
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return string
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param string $cpf
     * @return Cliente
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
        return $this;
    }

    /**
     * @return string
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * @param string $sexo
     * @return Cliente
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataNascimento()
    {
        if(!is_null($this->dataNascimento)){
            $anoNascimento = $this->dataNascimento->format('Y');
            $anoAtual      = (new \DateTime('now'))->format('Y');
            if( ($anoAtual - $anoNascimento) < 18 ){
                return $this->dataNascimento->format('d/m');
            }else{
                return $this->dataNascimento->format('d/m/Y');
            }

        }else{
            return null;
        }

        //return $this->dataNascimento;
    }

    /**
     * @param \DateTime $dataNascimento
     * @return Cliente
     */
    public function setDataNascimento($dataNascimento)
    {
        if(!empty($dataNascimento)){
            $infoData = explode('/', $dataNascimento);
            if(count($infoData) == 3){
                $dataNascimento = \DateTime::createFromFormat("d/m/Y", $dataNascimento);
            }else{
                $dataNascimento = implode('/', $infoData) . '/' . (new \DateTime('now'))->format('Y');
                $dataNascimento = \DateTime::createFromFormat("d/m/Y", $dataNascimento);
            }

        }else{
            $dataNascimento = null;
        }
        $this->dataNascimento = $dataNascimento;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataCasamento()
    {
        if(!is_null($this->dataCasamento))
        return $this->dataCasamento->format('d/m/Y');
        //return $this->dataCasamento;
    }

    /**
     * @param \DateTime $dataCasamento
     * @return Cliente
     */
    public function setDataCasamento($dataCasamento)
    {
        if(!empty($dataCasamento)){
            $dataCasamento = \DateTime::createFromFormat("d/m/Y", $dataCasamento);
        }else{
            $dataCasamento = null;
        }
        $this->dataCasamento = $dataCasamento;
        return $this;
    }

    /**
     * @return string
     */
    public function getNomeConjuge()
    {
        return $this->nomeConjuge;
    }

    /**
     * @param string $nomeConjuge
     * @return Cliente
     */
    public function setNomeConjuge($nomeConjuge)
    {
        $this->nomeConjuge = $nomeConjuge;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Cliente
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelefoneFixo()
    {
        return $this->telefoneFixo;
    }

    /**
     * @param string $telefoneFixo
     * @return Cliente
     */
    public function setTelefoneFixo($telefoneFixo)
    {
        $this->telefoneFixo = $telefoneFixo;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelefoneCelular()
    {
        return $this->telefoneCelular;
    }

    /**
     * @param string $telefoneCelular
     * @return Cliente
     */
    public function setTelefoneCelular($telefoneCelular)
    {
        $this->telefoneCelular = $telefoneCelular;
        return $this;
    }

    /**
     * @param int $ativo
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

    /**
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * @param string $cnpj
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;
    }

    /**
     * @return string
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro()
    {
        $dataCadastro = new \DateTime('now');
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param string $nomeFantasia
     */
    public function setNomeFantasia($nomeFantasia)
    {
        $this->nomeFantasia = $nomeFantasia;
    }

    /**
     * @return string
     */
    public function getNomeFantasia()
    {
        return $this->nomeFantasia;
    }

    /**
     * @param string $razaoSocial
     */
    public function setRazaoSocial($razaoSocial)
    {
        $this->razaoSocial = $razaoSocial;
    }

    /**
     * @return string
     */
    public function getRazaoSocial()
    {
        return $this->razaoSocial;
    }

    /**
     * @param string $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }


    public function __construct(array $data = array(), ClassMethods $hydrator = null)
    {
        if(!empty($data) && !is_null($hydrator)){
            $hydrator->hydrate($data, $this);
        }
    }

    public function toArray()
    {
        $hydrator = new ClassMethods();
        return $hydrator->extract($this);
    }

    public function __toString()
    {
        return $this->nome;
    }

}

