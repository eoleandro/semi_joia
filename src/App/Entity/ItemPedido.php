<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItemPedido
 *
 * @ORM\Table(name="item_pedido", indexes={@ORM\Index(name="fk_item_pedido_usuario1_idx", columns={"usuario_id"}), @ORM\Index(name="fk_item_pedido_pedido1_idx", columns={"pedido_id"}), @ORM\Index(name="fk_item_pedido_produto1_idx", columns={"produto_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\Venda\ItemPedidoRepository")
 */
class ItemPedido
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantidade", type="integer", nullable=true)
     */
    private $quantidade;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", precision=10, scale=0, nullable=true)
     */
    private $valor;

    /**
     * @var \Pedido
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Pedido", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pedido_id", referencedColumnName="id")
     * })
     */
    private $pedido;

    /**
     * @var \Produto
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Produto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="produto_id", referencedColumnName="id")
     * })
     */
    private $produto;

    /**
     * @var \Usuario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ItemPedido
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @param int $quantidade
     * @return ItemPedido
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
        return $this;
    }

    /**
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param float $valor
     * @return ItemPedido
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
        return $this;
    }

    /**
     * @return \Pedido
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    /**
     * @param \Pedido $pedido
     * @return ItemPedido
     */
    public function setPedido($pedido)
    {
        $this->pedido = $pedido;
        return $this;
    }

    /**
     * @return \Produto
     */
    public function getProduto()
    {
        return $this->produto;
    }

    /**
     * @param \Produto $produto
     * @return ItemPedido
     */
    public function setProduto($produto)
    {
        $this->produto = $produto;
        return $this;
    }

    /**
     * @return \Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param \Usuario $usuario
     * @return ItemPedido
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
        return $this;
    }




}

