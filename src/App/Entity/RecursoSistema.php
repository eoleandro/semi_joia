<?php
namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * RecursoSistema
 *
 * @ORM\Table(name="recurso_sistema")
 * @ORM\Entity(repositoryClass="App\Repository\Usuario\RecursoSistemaRepository")
 */
class RecursoSistema
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return RecursoSistema
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return RecursoSistema
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }


}

