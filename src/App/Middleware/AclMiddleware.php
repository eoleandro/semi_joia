<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 17/02/17
 * Time: 09:44
 */

namespace App\Middleware;



use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;


class AclMiddleware
{

    private $router;
    private $authenticationService;

    public function __construct( RouterInterface $router,
                                 \Zend\Authentication\AuthenticationService $authenticationService)
    {
        $this->router = $router;
        $this->authenticationService = $authenticationService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $routeResult = $this->router->match($request);
        $routName = $routeResult->getMatchedRouteName();
        if($routName != "usuario.auth" && $this->authenticationService->hasIdentity() == false){
            $uri = $this->router->generateUri('usuario.auth');
            return new RedirectResponse($uri);
        }

        //var_dump($this->authenticationService->getIdentity());

        //var_dump($routName);
        //var_dump(password_hash("erico", PASSWORD_DEFAULT));
        /*if(!$routName){
            $uri = $this->router->generateUri('grupo.list');
            return new RedirectResponse($uri);
        } else{
            #var_dump($routName, "checar permissao acl");
           // die;
        }*/

        return $next($request, $response);
    }
}