<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 17/02/17
 * Time: 09:51
 */

namespace App\Middleware\Factory;

use App\Middleware\AclMiddleware;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Authentication\AuthenticationService;


class AclMiddlewareFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $router   = $container->get(RouterInterface::class);
        $authAdapterService = $container->get(AuthenticationService::class);

        return new AclMiddleware( $router, $authAdapterService);
    }
}