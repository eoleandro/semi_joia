<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 20/02/17
 * Time: 15:50
 */

namespace App\Middleware\Factory;

use Slim\Flash\Messages;

class SlimFlashMiddlewareFactory
{
    public function __invoke($container)
    {
        return function ($request, $response, $next){
            if(empty($_SESSION)){
                session_start();
            }

            return $next(
                $request->withAttribute('flash', new Messages()),
                $response
            );
        };
    }

}