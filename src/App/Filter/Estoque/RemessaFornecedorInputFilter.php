<?php

namespace  App\Filter\Estoque;

use Zend\InputFilter\InputFilter;

class RemessaFornecedorInputFilter extends InputFilter
{
    public function __construct()
    {
        $this->add([
            'name' => 'fornecedor',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'fornecedor' Deve ser preenchido"]]],
            ]
        ]);
    }
}