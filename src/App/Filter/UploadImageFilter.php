<?php

namespace App\Filter;

class UploadImageFilter
{
    private $fileInfo;
    private $messageError;

    public function __construct()
    {
        $this->fileInfo = $_FILES;
    }

    public function isValid($fileName)
    {
        try{
            $this->validUploadOperation($fileName);
            $this->validFormatFile($fileName);
            return true;
        }catch (\Exception $e){
            $this->messageError = $e->getMessage();
            return false;
        }

    }

    public function validUploadOperation($fileName)
    {
        if(!empty($this->fileInfo) && $this->fileInfo[$fileName]['error'] != UPLOAD_ERR_OK) {
            throw new \LogicException("Ocorreu o erro de código: " . $this->fileInfo[$fileName]['error'] . " ao enviar o arquivo para o servidor.");
        }
    }
    public function validFormatFile($fileName)
    {
        $fileInfo = finfo_open(FILEINFO_MIME_TYPE);
        if(is_uploaded_file($this->fileInfo[$fileName]['tmp_name'])){
            $mimeTypeFile = finfo_file($fileInfo, $this->fileInfo[$fileName]['tmp_name']);
            $mimeTypesFileValid = array('image/gif', 'image/jpeg', 'image/png');

            if(!in_array($mimeTypeFile, $mimeTypesFileValid )){
                throw new \UnexpectedValueException("O formato do arquivo não é válido como imagem para o upload.");
            }
        }else{
            throw new \UnexpectedValueException("O arquivo não é válido para upload.");
        }
    }
}