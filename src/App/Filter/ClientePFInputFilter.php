<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 22/02/17
 * Time: 16:33
 */

namespace App\Filter;


use Zend\InputFilter\InputFilter;

class ClientePFInputFilter extends InputFilter
{
    public function __construct()
    {
        $this->add([
            'name' => 'nome',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Nome' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'CPF',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'CPF' Deve ser preenchido"]]],
            ]
        ]);

            $this->add([
                'name' => 'sexo',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Sexo' Deve ser preenchido"]]],
                ]
            ]);

            $this->add([
                'name' => 'telefoneFixo',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Telefone Fixo' Deve ser preenchido"]]],
                ]
            ]);

            $this->add([
                'name' => 'logradouro',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Logradouro' Deve ser preenchido"]]],
                ]
            ]);

            $this->add([
                'name' => 'bairro',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Bairro' Deve ser preenchido"]]],
                ]
            ]);

            $this->add([
                'name' => 'cidade',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Cidade' Deve ser preenchido"]]],
                ]
            ]);

            $this->add([
                'name' => 'UF',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'UF' Deve ser preenchido"]]],
                ]
            ]);

            $this->add([
                'name' => 'CEP',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'CEP' Deve ser preenchido"]]],
                ]
            ]);

            $this->add([
                'name' => 'numero',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Numero' Deve ser preenchido"]]],
                ]
            ]);

            $this->add([
                'name' => 'email',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'E-mail' Deve ser preenchido"]]],
                    ['name' => 'EmailAddress', 'options' => ['domain' => false, 'message' => "O e-mail não é válido"]],
                ]
            ]);
    }
}