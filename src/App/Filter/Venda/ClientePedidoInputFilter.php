<?php

namespace  App\Filter\Venda;

use Zend\InputFilter\InputFilter;

class ClientePedidoInputFilter extends InputFilter
{
    public function __construct()
    {
        $this->add([
            'name' => 'cliente',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'cliente' Deve ser preenchido"]]],
            ]
        ]);
    }
}