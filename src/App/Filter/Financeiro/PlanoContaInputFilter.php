<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/17
 * Time: 12:43
 */

namespace App\Filter\Financeiro;


use Zend\InputFilter\InputFilter;

class PlanoContaInputFilter  extends InputFilter
{

    public function __construct()
    {
        $this->add([
            'name' => 'descricao',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'descrição' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'ativo',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Ativo' Deve ser preenchido"]]],
            ]
        ]);
    }
}