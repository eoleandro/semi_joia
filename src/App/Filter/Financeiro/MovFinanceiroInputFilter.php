<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/17
 * Time: 10:30
 */

namespace App\Filter\Financeiro;


use Zend\InputFilter\InputFilter;

class MovFinanceiroInputFilter extends InputFilter
{

    public function __construct()
    {
        $this->add([
            'name' => 'descricao',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'descrição' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'formaPgto',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Forma de Pagamento' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'planoConta',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Plano de Contas' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'dataVencimento',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Data de Vencimento' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'valor',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Valor' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'natureza',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Natureza' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'dataVencimento',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Data de Vencimento' Deve ser preenchido"]]],
            ]
        ]);
    }
}