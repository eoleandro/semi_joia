<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 21/02/17
 * Time: 14:29
 */

namespace App\Filter;


use Zend\InputFilter\InputFilter;

class FornecedorInputFilter extends InputFilter
{
    public function __construct()
    {
        $this->add([
            'name' => 'razaoSocial',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Razão social' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'nomeFantasia',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Nome fantasia' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'logradouro',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Logradouro' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'bairro',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Bairro' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'cidade',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Cidade' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'UF',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'UF' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'CEP',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'CEP' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'numero',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Numero' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'email',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'E-mail' Deve ser preenchido"]]],
                ['name' => 'EmailAddress', 'options' => ['domain' => false, 'message' => "O e-mail não é válido"]],
            ]
        ]);

        $this->add([
            'name' => 'CNPJ',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'CNPJ' Deve ser preenchido"]]],
            ]
        ]);



        $this->add([
            'name' => 'telefoneFixo',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Telefone fixo' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'inscEstadual',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Inscrição Estadual' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'inscMunicipal',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Inscrição Municipal' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'nome',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Nome do contato' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'funcao',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Função do contato' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'telefone',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Telefone do contato' Deve ser preenchido"]]],
            ]
        ]);
    }
}