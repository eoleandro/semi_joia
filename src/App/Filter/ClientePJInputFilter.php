<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20/04/17
 * Time: 14:18
 */

namespace App\Filter;


use Zend\InputFilter\InputFilter;

class ClientePJInputFilter extends InputFilter
{
    public function __construct()
    {
        $this->add([
            'name' => 'razaoSocial',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Razão Social' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'CNPJ',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'CNPJ' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'nomeFantasia',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Nome Fantasia' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'telefoneFixo',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Telefone Fixo' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'logradouro',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Logradouro' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'bairro',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Bairro' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'cidade',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Cidade' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'UF',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'UF' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'CEP',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'CEP' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'numero',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Numero' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'email',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'E-mail' Deve ser preenchido"]]],
                ['name' => 'EmailAddress', 'options' => ['domain' => false, 'message' => "O e-mail não é válido"]],
            ]
        ]);

        $this->add([
            'name' => 'sexo',
            'required' => false,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'Sexo' Deve ser preenchido"]]],
            ]
        ]);
    }
}