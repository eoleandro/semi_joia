<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 22/02/17
 * Time: 13:59
 */

namespace App\Filter;


use Zend\InputFilter\InputFilter;

class ProdutoInputFilter extends InputFilter
{

    public function __construct()
    {
        $this->add([
            'name' => 'nome',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'nome' Deve ser preenchido"]]],
                ['name' => 'StringLength', 'options' => ['min' =>3, 'max' => 30,'message' => "O nome deve ter de 3 a 30 carateres."]],
            ]
        ]);

        $this->add([
            'name' => 'fornecedor',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'fornecedor' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'garantia',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'garantia' Deve ser preenchido"]]],
            ]
        ]);
    }
}