<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 21/02/17
 * Time: 13:11
 */

namespace App\Filter;


use Zend\InputFilter\InputFilter;

class UsuarioFilter extends InputFilter
{

    public function __construct()
    {
        $this->add([
            'name' => 'nome',
            'required' => true,
            'filters' => [
              ['name' => 'StripTags'],
              ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'nome' Deve ser preenchido"]]],
                //['name' => 'StringLength', 'options' => ['min' =>3]],
            ]
        ]);

        $this->add([
            'name' => 'grupo',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'grupo' Deve ser preenchido"]]],
                //['name' => 'StringLength', 'options' => ['min' =>3]],
            ]
        ]);

        $this->add([
            'name' => 'email',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'email' Deve ser preenchido"]]],
                ['name' => 'EmailAddress', 'options' => ['domain' => false, 'message' => "O e-mail não é válido"]]
            ]
        ]);

        $this->add([
            'name' => 'login',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'login' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'senha',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'senha' Deve ser preenchido"]]],
            ]
        ]);

        $this->add([
            'name' => 'ativo',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'ativo' Deve ser preenchido"]]],
            ]
        ]);
    }

}