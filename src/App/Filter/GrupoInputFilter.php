<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 21/02/17
 * Time: 14:26
 */

namespace App\Filter;


use Zend\InputFilter\InputFilter;

class GrupoInputFilter extends InputFilter
{

    public function __construct()
    {
        $this->add([
            'name' => 'nome',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => "O campo 'nome' Deve ser preenchido"]]],
                //['name' => 'StringLength', 'options' => ['min' =>3]],
            ]
        ]);
    }
}