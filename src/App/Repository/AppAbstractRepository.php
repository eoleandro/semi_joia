<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 17/02/17
 * Time: 08:51
 */

namespace App\Repository;


use Doctrine\ORM\EntityRepository;

class AppAbstractRepository extends EntityRepository
{
    public function fetchPairs()
    {
        $result = $this->findAll();
        $arrResult = [];
        if($result){
            foreach ($result as $item){
                $arrResult[$item->getId()] = $item->getNome();
            }
        }
        return $arrResult;
    }

    public function treatmentDate($filters = array(), $fieldsData =  array())
    {
        foreach($fieldsData as $field){
            if(!empty($filters[$field])){
                $filters[$field] = \DateTime::createFromFormat('d/m/Y', $filters[$field]);
                $filters[$field] = $filters[$field]->format('Y-m-d');
            }
        }
        return $filters;
    }
}