<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 20/02/17
 * Time: 11:41
 */

namespace App\Repository\Usuario;


use App\Entity\Grupo;
use App\Repository\AppAbstractRepository;
use App\Repository\Relatorio\IRelatorioCadastro;
use App\Entity\Usuario;

class UsuarioRepository extends AppAbstractRepository implements IRelatorioCadastro
{
    public function findByLoginAndSenha($login, $senha)
    {
        $userLogin = $this->createQueryBuilder('u')
            ->where('u.login = :a1')
            ->setParameter('a1', $login)->getQuery()->getOneOrNullResult();

        if(!empty($userLogin)){

            if(password_verify($senha, $userLogin->getSenha())){
                return $userLogin;
            }
            return false;
        }else{
            return false;
        }
    }

    public function getListaDados($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("u")->from(Usuario::class, "u")
                ->innerJoin(Grupo::class , "g", "with", "g.id=u.grupo");


            if(!empty($filtros['nome'])){
                $qb->andWhere($qb->expr()->like('c.nome', $qb->expr()->literal("%". $filtros['nome'] . "%")) );
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('u.nome', 'ASC');


            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function getCabecalhosRelatorioCadastro()
    {
        return [ 'nome' => 'Nome do Cliente', 'grupo' => 'Grupo', 'email' => 'E-mail', 'login' => 'Login' ];
    }



}