<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 18/02/17
 * Time: 22:13
 */

namespace App\Repository\Usuario;


use App\Repository\AppAbstractRepository;
use App\Repository\Relatorio\IRelatorioCadastro;
use App\Entity\Grupo;

class GrupoRepository extends AppAbstractRepository implements IRelatorioCadastro
{

    public function getListaDados($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("g")->from(Grupo::class, "g");


            if(!empty($filtros['nome'])){
                $qb->andWhere($qb->expr()->like('g.nome', $qb->expr()->literal("%". $filtros['nome'] . "%")) );
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('g.nome', 'ASC');


            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function getCabecalhosRelatorioCadastro()
    {
        return [ 'nome' => 'Nome do Grupo' ];
    }
}