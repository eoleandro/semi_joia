<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 17/02/17
 * Time: 10:44
 */

namespace App\Repository\Usuario;

use App\Entity\PermissaoAcl;
use App\Entity\RecursoSistema;
use App\Repository\AppAbstractRepository;


class PermissaoAclRepository extends  AppAbstractRepository
{
    public function getPermissionGroup($idGroup)
    {
        try{
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("p")->from(PermissaoAcl::class, "p")
                ->innerJoin(RecursoSistema::class, "rs", "with", "rs.id=p.recursoSistema")
                ->where('p.grupo = :grupo1')
                ->setParameter('grupo1', $idGroup)
                ->getQuery()
                ->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

    }

    public function getPermissooesGrupoDesautorizar($idGrupo, $idRecursosDesautorizar = array())
    {
        try{
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("p")->from(PermissaoAcl::class, "p")
                ->innerJoin(RecursoSistema::class, "rs", "with", "rs.id=p.recursoSistema")
                ->where('p.grupo = :grupo1')
                ->setParameter('grupo1', $idGrupo)
                ->andWhere('rs.id NOT IN(:recursosDesautorizar)')
                ->setParameter('recursosDesautorizar', $idRecursosDesautorizar)
                ->getQuery()
                ->getResult();

                return (empty($qb))? array() : $qb;

        }catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function getRecursosSistema()
    {
        try{
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("rs")->from(RecursoSistema::class, "rs")
                ->getQuery()
                ->getResult();
            if(!empty($qb)){
               $recursosSistema = array();
                foreach ($qb as $result){
                    $recursosSistema[$result->getId()] = $result->getUrl();
                }
                return $recursosSistema;
            }
            return array();

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

    }

}