<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 16/03/17
 * Time: 00:01
 */

namespace App\Repository\Evento;


use App\Entity\Cliente;
use App\Repository\AppAbstractRepository;
use App\Entity\Agenda;
use App\Repository\Relatorio\IRelatorioCadastro;
use Zend\I18n\Validator\DateTime;

class AgendaRepository extends AppAbstractRepository implements IRelatorioCadastro{

    public function getRegistroAgenda(){
        try{
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("a")->from(Agenda::class, "a")
                ->leftJoin(Cliente::class, "c", "with", "a.cliente = c.id")
                ->addOrderBy('a.dataHora','ASC');
            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;
        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getDataRegistroAgendaJSON($dataRegistroAgenda)
    {
        $dataAgendaJSON = array();
        foreach($dataRegistroAgenda as $dataAgenda){
            $dataAgendaJSON[] = [
                'id' => $dataAgenda->getId(),
                'title' => $dataAgenda->getEvento(),
                'start' => $dataAgenda->getDataHora()->format(\DateTime::ISO8601),
                'end' => $dataAgenda->getDataHora()->format(\DateTime::ISO8601),
                'className' => ($dataAgenda->getStatus() == "M"? "label-info" :"label-success")];
        }
        return json_encode($dataAgendaJSON);
    }

    public function getListaVisitas($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("a")->from(Agenda::class, "a")
                ->innerJoin(Cliente::class, "c", "with", "c.id=a.cliente");

            $filtros = $this->treatmentDate($filtros, ['dataDe', 'dataAte']);
            if(!empty($filtros['status'])){
                $qb->andWhere("a.status = :param1")->setParameter('param1', $filtros['status']);
            }
            if(!empty($filtros['dataDe']) || !empty($filtros['dataAte'])){
                if($filtros['dataCadastroDe'] == $filtros['dataCadastroAte']){
                    $qb->andWhere("DATE(a.dataHora) = DATE(:param2)")->setParameter('param2', $filtros['dataDe']);
                }else{
                    if(!empty($filtros['dataDe'])){
                        $qb->andWhere("DATE(a.dataHora) > DATE(:param3)")->setParameter('param3', $filtros['dataDe']);
                    }
                    if(!empty($filtros['dataAte'])){
                        $qb->andWhere("DATE(a.dataHora) < DATE(:param4)")->setParameter('param4', $filtros['dataAte']);
                    }
                }
            }else{
                $qb->andWhere("a.status = :param5")->setParameter('param5', "M");
                $qb->andWhere("DATE(a.dataHora) <= DATE(:param6)")->setParameter('param6', date('Y-m-d'));
            }

            if(!empty($filtros['evento'])){
                $qb->andWhere($qb->expr()->like('a.evento', $qb->expr()->literal("%". $filtros['evento'] . "%")) );
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('a.id', 'ASC');


            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getListaVisitasRelatorio($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("a")->from(Agenda::class, "a")
                ->innerJoin(Cliente::class, "c", "with", "c.id=a.cliente");

            $filtros = $this->treatmentDate($filtros, ['dataDe', 'dataAte']);
            if(!empty($filtros['status'])){
                $qb->andWhere("a.status = :param1")->setParameter('param1', $filtros['status']);
            }
            if(!empty($filtros['dataDe']) || !empty($filtros['dataAte'])){
                if($filtros['dataCadastroDe'] == $filtros['dataCadastroAte']){
                    $qb->andWhere("DATE(a.dataHora) = DATE(:param2)")->setParameter('param2', $filtros['dataDe']);
                }else{
                    if(!empty($filtros['dataDe'])){
                        $qb->andWhere("DATE(a.dataHora) > DATE(:param3)")->setParameter('param3', $filtros['dataDe']);
                    }
                    if(!empty($filtros['dataAte'])){
                        $qb->andWhere("DATE(a.dataHora) < DATE(:param4)")->setParameter('param4', $filtros['dataAte']);
                    }
                }
            }

            if(!empty($filtros['evento'])){
                $qb->andWhere($qb->expr()->like('a.evento', $qb->expr()->literal("%". $filtros['evento'] . "%")) );
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('a.id', 'ASC');


            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getTotalVisitasRealizar()
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->addSelect('count(a.id) as total')->from(Agenda::class, "a")
                ->innerJoin(Cliente::class, "c", "with", "c.id=a.cliente");

            $qb->andWhere("a.status = :param1")->setParameter('param1', "M");
            $qb->andWhere("DATE(a.dataHora) <= DATE(:param2)")->setParameter('param2', date('Y-m-d'));

            $qb->setMaxResults(500);
            $qb->addOrderBy('a.id', 'DESC');


            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? 0 : $qb[0]['total'];

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function getListaDados($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("a")->from(Agenda::class, "a")
                ->innerJoin(Cliente::class, "c", "with", "c.id=a.cliente");
            $filtros = $this->treatmentDate($filtros, ['dataDe', 'dataAte']);
            if(!empty($filtros['status'])){
                $qb->andWhere("a.status = :param1")->setParameter('param1', $filtros['status']);
            }
            if(!empty($filtros['dataDe']) || !empty($filtros['dataAte'])){
                if($filtros['dataCadastroDe'] == $filtros['dataCadastroAte']){
                    $qb->andWhere("DATE(a.dataHora) = DATE(:param2)")->setParameter('param2', $filtros['dataDe']);
                }else{
                    if(!empty($filtros['dataDe'])){
                        $qb->andWhere("DATE(a.dataHora) >= DATE(:param3)")->setParameter('param3', $filtros['dataDe']);
                    }
                    if(!empty($filtros['dataAte'])){
                        $qb->andWhere("DATE(a.dataHora) <= DATE(:param4)")->setParameter('param4', $filtros['dataAte']);
                    }
                }
            }else{
                //$qb->andWhere("DATE(a.dataHora) >= DATE(:param6)")->setParameter('param6', date('Y-m-d'));
            }

            if(!empty($filtros['nome'])){
                $qb->andWhere($qb->expr()->like('a.evento', $qb->expr()->literal("%". $filtros['nome'] . "%")) );
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('a.id', 'ASC');


            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function getCabecalhosRelatorioCadastro()
    {
        return [ 'evento' => 'Descrição do Agendamento', 'dataHora' => 'Data e Hora', 'status' => 'Status' ];
    }

} 