<?php

namespace  App\Repository\Estoque;
use App\Entity\ContatoFornecedor;
use App\Entity\EnderecoFornecedor;
use App\Entity\Fornecedor;
use App\Repository\AppAbstractRepository;
use App\Repository\Relatorio\IRelatorioCadastro;

class FornecedorRepository extends AppAbstractRepository implements IRelatorioCadastro
{
    public function getInfoFornecedor($idFornecedor = null)
    {
        try{
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("cf")->from(ContatoFornecedor::class, "cf")
                ->innerJoin(Fornecedor::class, "f", "with", "f.id=cf.fornecedor")
                ->innerJoin(EnderecoFornecedor::class, "ef", "with", "f.id=ef.fornecedor");

                if(!is_null($idFornecedor)){
                    $qb->addSelect("f");
                    $qb->addSelect("ef");
                    $qb->addSelect("ef.id as endereco_fornecedor_id");
                    $qb->addSelect("ef.id as contato_fornecedor_id");
                    $qb->where('f.id = :param1')
                        ->setParameter('param1', $idFornecedor)->getQuery();
                    $qb = $qb->getQuery()->getArrayResult();
                }else{
                    $qb =     $qb->getQuery()->getResult();
                }

            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

    }

    public function getInfoFornecedorForm($idFornecedor = null)
    {
        try{
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->from(ContatoFornecedor::class, "cf")
                ->innerJoin(Fornecedor::class, "f", "with", "f.id=cf.fornecedor")
                ->innerJoin(EnderecoFornecedor::class, "ef", "with", "f.id=ef.fornecedor");

            if(!is_null($idFornecedor)){
                $qb->addSelect("f.id as idFornecedor");
                $qb->addSelect("f.razaoSocial");
                $qb->addSelect("f.nomeFantasia");
                $qb->addSelect("f.email");
                $qb->addSelect("f.cnpj as CNPJ");
                $qb->addSelect("f.telefoneFixo");
                $qb->addSelect("f.telefoneCelular");
                $qb->addSelect("f.inscEstadual");
                $qb->addSelect("f.inscMunicipal");
                $qb->addSelect("f.ramoAtividade");
                $qb->addSelect("f.observacao");
                $qb->addSelect("f.ativo");
                $qb->addSelect("ef.id as idEnderecoFornecedor");
                $qb->addSelect("ef.logradouro");
                $qb->addSelect("ef.numero");
                $qb->addSelect("ef.bairro");
                $qb->addSelect("ef.cidade");
                $qb->addSelect("ef.uf as UF");
                $qb->addSelect("ef.cep as CEP");
                $qb->addSelect("cf.id as idContatoFornecedor");
                $qb->addSelect("cf.nome");
                $qb->addSelect("cf.funcao");
                $qb->addSelect("cf.telefone");
                $qb->where('f.id = :param1')
                    ->setParameter('param1', $idFornecedor)->getQuery();
                $qb = $qb->getQuery()->getScalarResult();
            }else{
                $qb =     $qb->getQuery()->getResult();
            }

            return (empty($qb))? array() : $qb[0];

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function fetchPairs()
    {
        $result = $this->findAll();
        $arrResult = [];
        if($result){
            foreach ($result as $item){
                $arrResult[$item->getId()] = $item->getNomeFantasia();
            }
        }
        return $arrResult;
    }

    public function getFornecedorAtivo()
    {
        try{
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->from(Fornecedor::class, "f")
                ->select("f");
            $qb->andWhere('f.ativo =:param1')->setParameter('param1', '1');
            $qb =     $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getFetchPairsFornecedorAtivo()
    {
        $result = $this->getFornecedorAtivo();
        $arrResult = [];
        if($result){
            foreach ($result as $item){
                $arrResult[$item->getId()] = $item->getNomeFantasia();
            }
        }
        return $arrResult;
    }

    public function getListaDados($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("cf")->from(ContatoFornecedor::class, "cf")
                ->innerJoin(Fornecedor::class , "f", "with", "f.id=cf.fornecedor")
                ->innerJoin(EnderecoFornecedor::class , "ef", "with", "f.id=ef.fornecedor")
                ;


            if(!empty($filtros['nome'])){
                $qb->andWhere($qb->expr()->like('f.nomeFantasia', $qb->expr()->literal("%". $filtros['nome'] . "%")) );
            }

            if(!empty($filtros['email'])){
                $qb->andWhere("f.email = :param7")->setParameter('param7', $filtros['email']);
            }

            if(!empty($filtros['telefoneFixo'])){
                $qb->andWhere("f.telefoneFixo = :param8")->setParameter('param8', $filtros['telefoneFixo']);
            }

            if(!empty($filtros['telefoneCelular'])){
                $qb->andWhere("f.telefoneCelular = :param9")->setParameter('param9', $filtros['telefoneCelular']);
            }

            if(!empty($filtros['codigo'])){
                $qb->andWhere("f.id = :param10")->setParameter('param10', $filtros['codigo']);
            }


            if(!empty($filtros['CNPJ'])){
                $qb->andWhere("f.CNPJ = :param11")->setParameter('param10', $filtros['CNPJ']);
            }

            if(!empty($filtros['nomeContato'])){
                //$qb->andWhere("cf.nome = :param12")->setParameter('param12', trim($filtros['nomeContato']));
                $qb->andWhere($qb->expr()->like('cf.nome', $qb->expr()->literal("%". trim($filtros['nomeContato']) . "%")) );
            }

            if(!empty($filtros['telefoneContato'])){
                $qb->andWhere("cf.telefone = :param13")->setParameter('param13', $filtros['telefoneContato']);
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('f.nomeFantasia', 'ASC');


            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getCabecalhosRelatorioCadastro()
    {
        return [ 'nomeFantasia' => 'Nome do Fornecedor', 'email' => 'E-mail', 'telefoneFixo' => 'Tel. Fixo',
            'telefoneCelular' => 'Tel. Celular', 'nome' => 'Contato', 'telefone' => "Tel. Contato"];
    }


}