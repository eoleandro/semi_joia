<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 22/02/17
 * Time: 13:57
 */

namespace App\Repository\Estoque;


use App\Entity\Fornecedor;
use App\Entity\Produto;
use App\Repository\AppAbstractRepository;
use App\Repository\Relatorio\IRelatorioCadastro;

class ProdutoRepository  extends AppAbstractRepository implements IRelatorioCadastro
{
    public function getProdutosFornecedor($idFornecedor, $status = null){


            try{
                $qb = $this->getEntityManager()->createQueryBuilder()
                    ->select("p")
                    ->from(Produto::class, "p")
                    ->innerJoin(Fornecedor::class, "f", "with", "f.id=p.fornecedor")
                    ->where('f.id = :param1')
                        ->setParameter('param1', $idFornecedor);
                if(!is_null($status)){
                    $qb->andWhere('p.ativo = :param2')
                        ->setParameter('param2', $status)->getQuery();
                    $qb = $qb->getQuery()->getResult();
                }else{
                    $qb = $qb->getQuery()->getResult();
                }
                return (empty($qb))? array() : $qb;

            }catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
    }

    public function getProdutosItemsRemessaJSON($produtosFornecedor = array())
    {
        $infoProdutosFornecedor = [];
        foreach ($produtosFornecedor as $infoProduto){
            $infoProdutosFornecedor[] = [
                'produto_id' => $infoProduto->getId(),
                'produto' => $infoProduto->getNome(),
                'fornedor_id' => $infoProduto->getFornecedor()->getId()
            ];
        }
        return $infoProdutosFornecedor;

    }

    public function getListaDados($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("p")->from(Produto::class, "p")
                ->innerJoin(Fornecedor::class, "f", "with", "f.id=p.fornecedor");


            if(!empty($filtros['nome'])){
                $qb->andWhere($qb->expr()->like('p.nome', $qb->expr()->literal("%". $filtros['nome'] . "%")) );
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('p.nome', 'ASC');


            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getCabecalhosRelatorioCadastro()
    {
      return ['nome' => 'Nome do Produto', 'fornecedor' => 'Fornecedor', 'metal' => 'Metal','peso'=> 'Peso',
          'formato'=> 'Formato', 'pedra' => 'Pedra' ];
    }
}