<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31/03/17
 * Time: 11:14
 */

namespace App\Repository\Estoque;


use App\Entity\Fornecedor;
use App\Repository\AppAbstractRepository;
use App\Entity\RemessaFornecedor;
use App\Repository\Relatorio\IRelatorioCadastro;

class RemessaFornecedorRepository extends AppAbstractRepository implements IRelatorioCadastro{

    public function getRemessasFornecedor($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("rf")->from(RemessaFornecedor::class, "rf")
                ->innerJoin(Fornecedor::class, "f", "with", "f.id=rf.fornecedor");

            $filtros = $this->treatmentDate($filtros, ['dataCadastroDe', 'dataCadastroAte']);
            if(!empty($filtros['status'])){
                $qb->andWhere("rf.status = :param1")->setParameter('param1', $filtros['status']);
            }
            if(!empty($filtros['dataCadastroDe']) || !empty($filtros['dataCadastroAte'])){
                if($filtros['dataCadastroDe'] == $filtros['dataCadastroAte']){
                    $qb->andWhere("DATE(rf.dataCadastro) = DATE(:param2)")->setParameter('param2', $filtros['dataCadastroDe']);
                }else{
                    if(!empty($filtros['dataCadastroDe'])){
                        $qb->andWhere("DATE(rf.dataVencimento) >= DATE(:param3)")->setParameter('param3', $filtros['dataCadastroDe']);
                    }
                    if(!empty($filtros['dataCadastroAte'])){
                        $qb->andWhere("DATE(rf.dataCadastro) <= DATE(:param4)")->setParameter('param4', $filtros['dataCadastroAte']);
                    }
                }

            }

            if(!empty($filtros['nome'])){
                $qb->andWhere($qb->expr()->like('f.nomeFantasia', $qb->expr()->literal("%". $filtros['nome'] . "%")) );
            }

            if(!empty($filtros['valor'])){
                $qb->andWhere("p.valor = :param9")->setParameter('param9', $filtros['valor']);
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('rf.id', 'DESC');


            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getListaDados($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("rf")->from(RemessaFornecedor::class, "rf")
                ->innerJoin(Fornecedor::class, "f", "with", "f.id=rf.fornecedor");

            $filtros = $this->treatmentDate($filtros, ['dataCadastroDe', 'dataCadastroAte']);
            if(!empty($filtros['status'])){
                $qb->andWhere("rf.status = :param1")->setParameter('param1', $filtros['status']);
            }
            if(!empty($filtros['dataCadastroDe']) || !empty($filtros['dataCadastroAte'])){
                if($filtros['dataCadastroDe'] == $filtros['dataCadastroAte']){
                    $qb->andWhere("DATE(rf.dataCadastro) = DATE(:param2)")->setParameter('param2', $filtros['dataCadastroDe']);
                }else{
                    if(!empty($filtros['dataCadastroDe'])){
                        $qb->andWhere("DATE(rf.dataVencimento) >= DATE(:param3)")->setParameter('param3', $filtros['dataCadastroDe']);
                    }
                    if(!empty($filtros['dataCadastroAte'])){
                        $qb->andWhere("DATE(rf.dataCadastro) <= DATE(:param4)")->setParameter('param4', $filtros['dataCadastroAte']);
                    }
                }
            }

            if(!empty($filtros['nome'])){
                $qb->andWhere($qb->expr()->like('f.nomeFantasia', $qb->expr()->literal("%". $filtros['nome'] . "%")) );
            }

            if(!empty($filtros['valor'])){
                $qb->andWhere("p.valor = :param9")->setParameter('param9', $filtros['valor']);
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('rf.id', 'DESC');


            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getCabecalhosRelatorioCadastro()
    {
        return [ 'fornecedor' => 'Fornecedor', 'valorCusto' => 'Valor T. Custo','dataCadastro'=> 'Data de Cadastro',
            'status'=> 'Status' ];
    }

} 