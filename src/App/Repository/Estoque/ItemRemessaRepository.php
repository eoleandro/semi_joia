<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 02/03/17
 * Time: 20:17
 */

namespace App\Repository\Estoque;


use App\Entity\Fornecedor;
use App\Entity\ItemRemessa;
use App\Entity\Produto;
use App\Entity\RemessaFornecedor;
use App\Repository\AppAbstractRepository;

class ItemRemessaRepository extends AppAbstractRepository
{
    public function getItensRemessaFornecedor($idRemessaFornecedor){
        try{
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("ir")->from(ItemRemessa::class, "ir")
                ->innerJoin(RemessaFornecedor::class, "rf", "with", "rf.id=ir.remessaFornecedor")
                ->innerJoin(Fornecedor::class, "f", "with", "f.id=rf.fornecedor");

                $qb->where('rf.id = :param1')
                    ->setParameter('param1', $idRemessaFornecedor);
                $qb = $qb->getQuery()->getResult();

            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function getItensAtivosRemessa( array $idProduto = array()){
        try{
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("ir")->from(ItemRemessa::class, "ir")
                ->innerJoin(RemessaFornecedor::class, "rf", "with", "rf.id=ir.remessaFornecedor")
                ->innerJoin(Fornecedor::class, "f", "with", "f.id=rf.fornecedor")
                ->innerJoin(Produto::class, "p", "with", "p.id=ir.produto");

            $qb->where('p.id IN(:param1)')->setParameter('param1', $idProduto);
            $qb->andWhere('rf.status = :param2')->setParameter('param2', "Ativa");
            $qb = $qb->getQuery()->getResult();

            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getItensRemessaFornecedorEmRemessasAtivas($idRemessaFornecedor)
    {
        $itensRemessa = $this->getItensRemessaFornecedor($idRemessaFornecedor);
        $idProdutos = array();
        foreach($itensRemessa as $itemRemessa){
            $idProdutos[] = $itemRemessa->getProduto()->getId();
        }
        return $this->getItensAtivosRemessa($idProdutos);
    }

    public function getItensRemessaFornecedorAtivo($idFornecedor, $idProdutos = array(), $statusRemessa = "Ativa")
    {
        try{
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("ir")->from(ItemRemessa::class, "ir")
                ->innerJoin(RemessaFornecedor::class, "rf", "with", "rf.id=ir.remessaFornecedor")
                ->innerJoin(Produto::class, "p", "with", "p.id=ir.produto")
                ->innerJoin(Fornecedor::class, "f", "with", "f.id=rf.fornecedor");

            $qb->where('f.id = :param1')
                ->setParameter('param1', $idFornecedor)
                ->andWhere('p.id IN (:param2)')
                ->setParameter('param2',$idProdutos)
                ->andWhere('rf.status = :param3')
                ->setParameter('param3',$statusRemessa)
            ;
            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getProdutosDisponiveisRemessas()
    {
        try{
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("ir")->from(ItemRemessa::class, "ir")
                ->innerJoin(RemessaFornecedor::class, "rf", "with", "rf.id=ir.remessaFornecedor")
                ->innerJoin(Produto::class, "p", "with", "p.id=ir.produto")
                ->innerJoin(Fornecedor::class, "f", "with", "f.id=rf.fornecedor");

            $qb->where('ir.quantParcial > :param1')
                ->setParameter('param1', 0)
                ->andWhere('rf.status = :param2')
                ->setParameter('param2',"Ativa")
                ->andWhere('p.ativo = :param3')
                ->setParameter('param3', "1")
            ;
            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getProdutosItemsPedidoParaJSON($produtosPedido = array())
    {
        $infoProdutosPedido = [];
        foreach ($produtosPedido as $infoProduto){
            $infoProdutosPedido[] = [
                'produto_id' => $infoProduto->getProduto()->getId(),
                'produto' => utf8_encode($infoProduto->getProduto()->getNome()),
                'quant_disponivel' => $infoProduto->getQuantParcial(),
                'preco' => $infoProduto->getPrecoCliente(),
                'fornedor_id' => $infoProduto->getProduto()->getFornecedor()->getId(),
                'fornedor_nome' => utf8_encode($infoProduto->getProduto()->getFornecedor()->getNomeFantasia()),
            ];
        }
        return $infoProdutosPedido;
    }

    public function getInfoItemRemessa($idProduto, $status = array("Ativa"))
    {
        try{
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("ir")->from(ItemRemessa::class, "ir")
                ->innerJoin(RemessaFornecedor::class, "rf", "with", "rf.id=ir.remessaFornecedor")
                ->innerJoin(Produto::class, "p", "with", "p.id=ir.produto")
                ->innerJoin(Fornecedor::class, "f", "with", "f.id=rf.fornecedor");

            $qb->where('p.id = :param1')
                ->setParameter('param1', $idProduto)
                ->andWhere('ir.quantParcial > :param2')
                ->setParameter('param2', 0)
                ->andWhere('rf.status  IN(:param3)')
                ->setParameter('param3',$status);
            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb[0];

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getValorTotalItensRemessa(  $infoItensRemessa = array())
    {
        $valorTotalRemessa = 0;
        foreach ($infoItensRemessa['produto'] as $indice => $produtoId){
            $quantidade = $infoItensRemessa['quantidade'][$indice];
            $precoCusto = $infoItensRemessa['precoCusto'][$indice];
            if(!empty($produtoId) && !empty($quantidade) && !empty($precoCusto)){
                $valorFloat = strtr($precoCusto, ['.' =>'', ',' => '.']);
                $valorTotalRemessa += ( $valorFloat * $quantidade );
            }
        }
        return $valorTotalRemessa;
    }

    public function getValorTotalItensRemessaCadastrados($itensRemessa = array())
    {
        $total = 0;
        foreach($itensRemessa as $itemRemessa){
            $total += ( $itemRemessa->getPrecoCusto() * ($itemRemessa->getQuantidade() -  $itemRemessa->getQuantParcial()) );
        }
        return $total;
    }


}