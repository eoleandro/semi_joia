<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 04/03/17
 * Time: 20:53
 */

namespace App\Repository\Financeiro;


use App\Entity\FormaPgto;
use App\Repository\AppAbstractRepository;
use App\Repository\Relatorio\IRelatorioCadastro;

class FormaPgtoRepository extends  AppAbstractRepository implements IRelatorioCadastro {

    public function fetchPairs()
    {
        $result = $this->findAll();
        //var_dump($result[0]->getId()); die;
        $arrResult = [];
        if($result){
            foreach ($result as $item){
                $arrResult[$item->getId()] = $item->getDescricao();
            }
        }
        return $arrResult;
    }
    public function getListaDados($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("fp")->from(FormaPgto::class, "fp");

            if(!empty($filtros['nome'])){
                $qb->andWhere($qb->expr()->like('fp.descricao', $qb->expr()->literal("%". $filtros['nome'] . "%")) );
            }

            if(!empty($filtros['numero'])){
                $qb->andWhere("fp.id = :param9")->setParameter('param9', $filtros['numero']);
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('fp.descricao', 'ASC');

            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function getCabecalhosRelatorioCadastro()
    {
        return [ 'descricao' => 'Descrição', 'ativo' => 'Ativo?' ];
    }
} 