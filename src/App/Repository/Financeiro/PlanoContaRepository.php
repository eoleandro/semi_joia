<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/17
 * Time: 10:43
 */

namespace App\Repository\Financeiro;


use App\Entity\PlanoConta;
use App\Repository\AppAbstractRepository;
use App\Repository\Relatorio\IRelatorioCadastro;

class PlanoContaRepository extends  AppAbstractRepository implements IRelatorioCadastro {

    public function fetchPairs()
    {
        $result = $this->findAll();
        $arrResult = [];
        if($result){
            foreach ($result as $item){
                $arrResult[$item->getId()] = $item->getDescricao();
            }
        }
        return $arrResult;
    }

    public function getListaDados($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("pc")->from(PlanoConta::class, "pc");

            if(!empty($filtros['nome'])){
                $qb->andWhere($qb->expr()->like('pc.descricao', $qb->expr()->literal("%". $filtros['nome'] . "%")) );
            }

            if(!empty($filtros['numero'])){
                $qb->andWhere("pc.id = :param9")->setParameter('param9', $filtros['numero']);
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('pc.descricao', 'ASC');

            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function getCabecalhosRelatorioCadastro()
    {
        return [ 'descricao' => 'Descrição', 'ativo' => 'Ativo?' ];
    }
}