<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/17
 * Time: 16:39
 */

namespace App\Repository\Financeiro;


use App\Entity\Cliente;
use App\Entity\FormaPgto;
use App\Entity\Fornecedor;
use App\Entity\MovFinanceiro;
use App\Entity\MovFinPedido;
use App\Entity\MovFinRemessa;
use App\Entity\Pedido;
use App\Entity\PlanoConta;
use App\Entity\RemessaFornecedor;
use App\Repository\AppAbstractRepository;
use Zend\I18n\Validator\DateTime;

class MovFinanceiroRepository extends  AppAbstractRepository{
    public function getMovFinanceiro($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE_FORMAT', \DoctrineExtensions\Query\Mysql\DateFormat::class);
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("mf")->from(MovFinanceiro::class, "mf")
                ->innerJoin(FormaPgto::class, "fp", "with", "mf.formaPgto=fp.id")
                ->innerJoin(PlanoConta::class, "pc", "with", "mf.planoConta=pc.id");

            $filtros = $this->treatmentDate($filtros, ['dataVencimentoDe', 'dataVencimentoAte']);
            if(!empty($filtros['natureza'])){
                $qb->andWhere("mf.natureza = :param1")->setParameter('param1', $filtros['natureza']);
            }
            if(!empty($filtros['dataVencimentoDe']) || !empty($filtros['dataVencimentoAte'])){
                if($filtros['dataVencimentoDe'] == $filtros['dataVencimentoAte']){
                    $qb->andWhere("DATE(mf.dataVencimento) = DATE(:param2)")->setParameter('param2', $filtros['dataVencimentoDe']);
                }else{
                    if(!empty($filtros['dataVencimentoDe'])){
                        $qb->andWhere("DATE(mf.dataVencimento) >= DATE(:param3)")->setParameter('param3', $filtros['dataVencimentoDe']);
                    }
                    if(!empty($filtros['dataVencimentoAte'])){
                        $qb->andWhere("DATE(mf.dataVencimento) <= DATE(:param4)")->setParameter('param4', $filtros['dataVencimentoAte']);
                    }
                }

            }

            if(!empty($filtros['dataCadastroDe']) || !empty($filtros['dataCadastroAte'])){
                if($filtros['dataCadastroDe'] == $filtros['dataCadastroAte']){
                    $qb->andWhere("DATE(mf.dataCadastro) = DATE(:param5)")->setParameter('param5', $filtros['dataCadastroDe']);
                }else{
                    if(!empty($filtros['dataCadastroDe'])){
                        $qb->andWhere("DATE(mf.dataCadastro) >= DATE(:param6)")->setParameter('param6', $filtros['dataCadastroDe']);
                    }
                    if(!empty($filtros['dataCadastroAte'])){
                        $qb->andWhere("DATE(mf.dataCadastro) <= DATE(:param7)")->setParameter('param7', $filtros['dataCadastroAte']);
                    }
                }

            }
            if(!empty($filtros['descricao'])){
                $qb->andWhere($qb->expr()->like('mf.descricao', $qb->expr()->literal("%". $filtros['descricao'] . "%")) );
            }

            if(!empty($filtros['valor'])){
                $filtros['valor'] = strtr($filtros['valor'], ['.' => '', ',' => '.']);
                $qb->andWhere("mf.valor = :param9")->setParameter('param9', $filtros['valor']);
            }
            if(!empty($filtros['formaPgto'])){
                $qb->andWhere("fp.id = :param10")->setParameter('param10', $filtros['formaPgto']);
            }
            if(!empty($filtros['planoConta'])){
                $qb->andWhere("pc.id = :param11")->setParameter('param11', $filtros['planoConta']);
            }
            if(!empty($filtros['situacao'])){
                if($filtros['situacao'] == "F"){
                    $qb->andWhere("mf.dataQuitacao IS NOT NULL");
                }
                if($filtros['situacao'] == "A"){
                    $qb->andWhere("mf.dataQuitacao IS NULL");
                }
                if($filtros['situacao'] == "P"){
                    $qb->andWhere("mf.dataQuitacao IS NULL AND DATE(mf.dataVencimento) > DATE('".date('Y-m-d')."')");

                }
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('mf.id', 'DESC');
            //$qb->addOrderBy('mf.id', 'ASCs');

            //var_dump($qb->getQuery()->getSQL()); die;

            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getContasPagar($filtros = array() )
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("mf")->from(MovFinanceiro::class, "mf")
                ->innerJoin(FormaPgto::class, "fp", "with", "mf.formaPgto=fp.id")
                ->innerJoin(PlanoConta::class, "pc", "with", "mf.planoConta=pc.id");
            $filtros = $this->treatmentDate($filtros, ['dataVencimentoDe', 'dataVencimentoAte']);
            if(!empty($filtros['dataVencimentoDe']) || !empty($filtros['dataVencimentoAte'])){
                if($filtros['dataVencimentoDe'] == $filtros['dataVencimentoAte']){
                    $qb->andWhere("DATE(mf.dataVencimento) = DATE(:param2)")->setParameter('param2', $filtros['dataVencimentoDe']);
                }else{
                    if(!empty($filtros['dataVencimentoDe'])){
                        $qb->andWhere("DATE(mf.dataVencimento) >= DATE(:param3)")->setParameter('param3', $filtros['dataVencimentoDe']);
                    }
                    if(!empty($filtros['dataVencimentoAte'])){
                        $qb->andWhere("DATE(mf.dataVencimento) <= DATE(:param4)")->setParameter('param4', $filtros['dataVencimentoAte']);
                    }
                }
            }

        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function getContas($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("mf")->from(MovFinanceiro::class, "mf")
                ->innerJoin(FormaPgto::class, "fp", "with", "mf.formaPgto=fp.id")
                ->innerJoin(PlanoConta::class, "pc", "with", "mf.planoConta=pc.id");

            $filtros = $this->treatmentDate($filtros, ['dataVencimentoDe', 'dataVencimentoAte']);

            $qb->andWhere("mf.tipo IN(:param1)")->setParameter('param1', $filtros['tipo']);
            if(!empty($filtros['descricao'])){
                $qb->andWhere($qb->expr()->like('mf.descricao', $qb->expr()->literal("%". $filtros['descricao'] . "%")) );
            }

            if(!empty($filtros['dataVencimentoDe']) || !empty($filtros['dataVencimentoAte'])){
                if($filtros['dataVencimentoDe'] == $filtros['dataVencimentoAte']){
                    $qb->andWhere("DATE(mf.dataVencimento) <= DATE(:param2)")
                        ->setParameter('param2', $filtros['dataVencimentoAte']);
                }else{
                    if(!empty($filtros['dataVencimentoDe'])){
                        $qb->andWhere("DATE(mf.dataVencimento) >= DATE(:param3)")
                            ->setParameter('param3', $filtros['dataVencimentoDe']);
                    }
                    if(!empty($filtros['dataVencimentoAte'])){
                        $qb->andWhere("DATE(mf.dataVencimento) <= DATE(:param4)")
                            ->setParameter('param4', $filtros['dataVencimentoAte']);
                    }
                }
            }

            if(!empty($filtros['natureza'])){
                $qb->andWhere("mf.natureza =:param5")->setParameter('param5', $filtros['natureza']);
            }
            if(!empty($filtros['valor'])){
               $filtros['valor'] = strtr($filtros['valor'], ['.' => '', ',' => '.']);
                $qb->andWhere("mf.valor =:param6")->setParameter('param6', $filtros['valor']);
            }
            if(!empty($filtros['status'])){
                $qb->andWhere("mf.status =:param7")->setParameter('param7', $filtros['status']);
            }

            if(!empty($filtros['formaPgto'])){
                $qb->andWhere("fp.id =:param8")->setParameter('param8', $filtros['formaPgto']);
            }

            if(!empty($filtros['planoConta'])){
                $qb->andWhere("pc.id =:param9")->setParameter('param9', $filtros['planoConta']);
            }
            if(empty($filtros['status'])){
                //$qb->andWhere("mf.status =:param10")->setParameter('param10', 'A');
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('mf.id', 'DESC');

            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

    }

    public function getMovFinanceiroVencimento($categoriaMov = '', $filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("mf")->from(MovFinanceiro::class, "mf")
                ->innerJoin(FormaPgto::class, "fp", "with", "mf.formaPgto=fp.id")
                ->innerJoin(PlanoConta::class, "pc", "with", "mf.planoConta=pc.id");
            $qb->addSelect('fp.descricao as forma_pgto');
            if($categoriaMov == 'pagar'){
                $qb->leftJoin(MovFinRemessa::class, "mfr", "with", "mfr.movFinanceiro = mf.id");
                $qb->leftJoin(RemessaFornecedor::class, "rf", "with", "mfr.remessaFornecedor = rf.id");
                $qb->leftJoin(Fornecedor::class, "f", "with", "rf.fornecedor = f.id");
                $qb->addSelect('f.nomeFantasia as fornecedor');
                $qb->addSelect('f.telefoneFixo as tel_fixo');
                $qb->addSelect('f.telefoneCelular as tel_celular');
            }
            if($categoriaMov == 'receber'){
                $qb->leftJoin(MovFinPedido::class, "mfp", "with", "mfp.movFinanceiro = mf.id");
                $qb->leftJoin(Pedido::class, "p", "with", "mfp.pedido = p.id");
                $qb->leftJoin(Cliente::class, "c", "with", "p.cliente = c.id");
                $qb->addSelect('c.nome as cliente');
                $qb->addSelect('c.telefoneFixo as tel_fixo');
                $qb->addSelect('c.telefoneCelular as tel_celular');

            }
            $filtros = $this->treatmentDate($filtros, ['dataVencimentoDe', 'dataVencimentoAte']);

            $qb->andWhere("mf.tipo IN(:param1)")->setParameter('param1', $filtros['tipo']);
            if(!empty($filtros['descricao'])){
                $qb->andWhere($qb->expr()->like('mf.descricao', $qb->expr()->literal("%". $filtros['descricao'] . "%")) );
            }

            if(!empty($filtros['dataVencimentoDe']) || !empty($filtros['dataVencimentoAte'])){
                if($filtros['dataVencimentoDe'] == $filtros['dataVencimentoAte']){
                    $qb->andWhere("DATE(mf.dataVencimento) <= DATE(:param2)")
                        ->setParameter('param2', $filtros['dataVencimentoAte']);
                }else{
                    if(!empty($filtros['dataVencimentoDe'])){
                        $qb->andWhere("DATE(mf.dataVencimento) >= DATE(:param3)")
                            ->setParameter('param3', $filtros['dataVencimentoDe']);
                    }
                    if(!empty($filtros['dataVencimentoAte'])){
                        $qb->andWhere("DATE(mf.dataVencimento) <= DATE(:param4)")
                            ->setParameter('param4', $filtros['dataVencimentoAte']);
                    }
                }
            }

            if(!empty($filtros['natureza'])){
                $qb->andWhere("mf.natureza =:param5")->setParameter('param5', $filtros['natureza']);
            }
            if(!empty($filtros['valor'])){
                $filtros['valor'] = strtr($filtros['valor'], ['.' => '', ',' => '.']);
                $qb->andWhere("mf.valor =:param6")->setParameter('param6', $filtros['valor']);
            }
            if(!empty($filtros['status'])){
                $qb->andWhere("mf.status =:param7")->setParameter('param7', $filtros['status']);
            }

            if(!empty($filtros['formaPgto'])){
                $qb->andWhere("fp.id =:param8")->setParameter('param8', $filtros['formaPgto']);
            }

            if(!empty($filtros['planoConta'])){
                $qb->andWhere("pc.id =:param9")->setParameter('param9', $filtros['planoConta']);
            }
            if(empty($filtros['status'])){
                $qb->andWhere("mf.status =:param10")->setParameter('param10', 'A');
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('mf.id', 'DESC');

            $qb = $qb->getQuery()->getScalarResult();
            return (empty($qb))? array() : $qb;
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

    }

    public function getContasPendentesQuitacao()
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("mf")->from(MovFinanceiro::class, "mf")
                ->innerJoin(FormaPgto::class, "fp", "with", "mf.formaPgto=fp.id")
                ->innerJoin(PlanoConta::class, "pc", "with", "mf.planoConta=pc.id");

            $qb->andWhere("DATE(mf.dataVencimento) <= DATE(:param1)")
                ->setParameter('param1', date('Y-m-d'));

            $qb->andWhere("mf.natureza IN(:param2)")->setParameter('param2', ['D', 'R']);
            $qb->andWhere("mf.status =:param3")->setParameter('param3', "A");

            $qb->setMaxResults(500);
            $qb->addOrderBy('mf.id', 'ASC');

            //var_dump($qb->getQuery()->getSQL()); die;

            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

    }

    public function calculaTotaisContas($infoContas = array())
    {
        $infoTotaisContas = array('TotalPagar' => 0, 'TotalReceber' => 0);
        foreach($infoContas as $infoConta){
            if($infoConta->getNatureza() == "R"){
                $infoTotaisContas["TotalReceber"] ++;
            }else{
                $infoTotaisContas["TotalPagar"] ++;
            }
        }
        return $infoTotaisContas;
    }
} 