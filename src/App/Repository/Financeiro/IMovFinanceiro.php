<?php

namespace App\Repository\Financeiro;


interface IMovFinanceiro {
public function getParcelasPorIds(array $idsMovFinanceiro);
} 