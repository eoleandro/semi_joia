<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10/03/17
 * Time: 12:11
 */

namespace App\Repository\Financeiro;


use App\Entity\FormaPgto;
use App\Entity\MovFinanceiro;
use App\Entity\MovFinRemessa;
use App\Entity\PlanoConta;
use App\Entity\RemessaFornecedor;
use App\Repository\AppAbstractRepository;

class MovFinRemessaRepository extends  AppAbstractRepository implements IMovFinanceiro{

    public function getParcelasRemessaFornecedor($idRemessa, $filtros = array())
    {
        try{
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("mfr")->from(MovFinRemessa::class, "mfr")
                ->innerJoin(MovFinanceiro::class, "mf", "with", "mf.id=mfr.movFinanceiro")
                ->innerJoin(RemessaFornecedor::class,"rf", "with", "rf.id=mfr.remessaFornecedor");

            $qb->where('rf.id = :param1')
                ->setParameter('param1', $idRemessa);

            if(!empty($filtros['Quitada'])){
                if($filtros['Quitada'] == 'sim'){
                    $qb->andWhere('mf.dataQuitacao IS NOT NULL');
                }else{
                    $qb->andWhere('mf.dataQuitacao IS NULL');
                }
            }
            if(empty($filtros['tipo'])){
                $qb->andWhere("mf.tipo = 'D'");
            }else{
                $qb->andWhere('mf.tipo IN (:param2)')->setParameter('param2', $filtros['tipo']);
            }


            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getParcelasDesconsiderar($idRemessa, array $idMovFinRemessa = array())
    {
        try{
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("mfr")->from(MovFinRemessa::class, "mfr")
                ->innerJoin(MovFinanceiro::class, "mf", "with", "mf.id=mfr.movFinanceiro")
                ->innerJoin(RemessaFornecedor::class,"rf", "with", "rf.id=mfr.remessaFornecedor");

            if(!empty($idMovFinRemessa)){
                $qb->where('mfr.id NOT IN(:param1)')->setParameter('param1', $idMovFinRemessa);
            }

            $qb->andWhere('rf.id = :param2')->setParameter('param2', $idRemessa)->andWhere("mf.tipo = 'D'");
            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function getInfoRelacionadaMovFinanceiro($idMovFinanceiro)
    {
        try{
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("mfr")->from(MovFinRemessa::class, "mfr")
                ->innerJoin(MovFinanceiro::class, "mf", "with", "mf.id=mfr.movFinanceiro")
                ->innerJoin(RemessaFornecedor::class,"rf", "with", "rf.id=mfr.remessaFornecedor");

            $qb->where('mf.id = :param1')
                ->setParameter('param1', $idMovFinanceiro);

            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function calculaTotalParcelasLancadas($data)
    {
        $data['valores'] = array_filter( $data['valores'], function($v, $k) { return $v != "";}, ARRAY_FILTER_USE_BOTH);
        $totalParcelas = 0;
        foreach($data['valores'] as $valor){
           $totalParcelas += $valor;
        }
        return $totalParcelas;
    }

    public function getParcelasPorIds(array $idsMovFinanceiro, $filtros = array())
    {
        try{
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("mfr")->from(MovFinRemessa::class, "mfr")
                ->innerJoin(MovFinanceiro::class, "mf", "with", "mf.id=mfr.movFinanceiro")
                ->innerJoin(RemessaFornecedor::class, "rf", "with", "rf.id=mfr.remessaFornecedor");

            $qb->where('mf.id IN(:param1)')
                ->setParameter('param1', $idsMovFinanceiro);
            if(!empty($filtros['status'])){
                $qb->andWhere('mf.status = :param2')->setParameter('param2', $filtros['status']);
            }
            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getContas($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("mfr")->from(MovFinRemessa::class, "mfr")
                ->innerJoin(MovFinanceiro::class, "mf", "with", "mf.id = mfr.movFinanceiro")
                ->innerJoin(FormaPgto::class, "fp", "with", "mf.formaPgto=fp.id")
                ->innerJoin(PlanoConta::class, "pc", "with", "mf.planoConta=pc.id");

            $filtros = $this->treatmentDate($filtros, ['dataVencimentoDe', 'dataVencimentoAte']);

            $qb->andWhere("mf.tipo IN(:param1)")->setParameter('param1', $filtros['tipo']);
            if(!empty($filtros['descricao'])){
                $qb->andWhere($qb->expr()->like('mf.descricao', $qb->expr()->literal("%". $filtros['descricao'] . "%")) );
            }

            if(!empty($filtros['dataVencimentoDe']) || !empty($filtros['dataVencimentoAte'])){
                if($filtros['dataVencimentoDe'] == $filtros['dataVencimentoAte']){
                    $qb->andWhere("DATE(mf.dataVencimento) <= DATE(:param2)")
                        ->setParameter('param2', $filtros['dataVencimentoAte']);
                }else{
                    if(!empty($filtros['dataVencimentoDe'])){
                        $qb->andWhere("DATE(mf.dataVencimento) >= DATE(:param3)")
                            ->setParameter('param3', $filtros['dataVencimentoDe']);
                    }
                    if(!empty($filtros['dataVencimentoAte'])){
                        $qb->andWhere("DATE(mf.dataVencimento) <= DATE(:param4)")
                            ->setParameter('param4', $filtros['dataVencimentoAte']);
                    }
                }
            }

            if(!empty($filtros['natureza'])){
                $qb->andWhere("mf.natureza =:param5")->setParameter('param5', $filtros['natureza']);
            }
            if(!empty($filtros['valor'])){
                $filtros['valor'] = strtr($filtros['valor'], ['.' => '', ',' => '.']);
                $qb->andWhere("mf.valor =:param6")->setParameter('param6', $filtros['valor']);
            }
            if(!empty($filtros['status'])){
                $qb->andWhere("mf.status =:param7")->setParameter('param7', $filtros['status']);
            }

            if(!empty($filtros['formaPgto'])){
                $qb->andWhere("fp.id =:param8")->setParameter('param8', $filtros['formaPgto']);
            }

            if(!empty($filtros['planoConta'])){
                $qb->andWhere("pc.id =:param9")->setParameter('param9', $filtros['planoConta']);
            }
            if(empty($filtros['status'])){
                $qb->andWhere("mf.status =:param10")->setParameter('param10', 'A');
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('mf.id', 'DESC');

            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

    }
} 