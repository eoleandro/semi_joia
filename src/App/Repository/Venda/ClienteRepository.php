<?php

namespace  App\Repository\Venda;

use App\Repository\AppAbstractRepository;
use App\Entity\Cliente;
use App\Entity\EnderecoCliente;

class ClienteRepository extends AppAbstractRepository
{
    public  function getClienteJSON()
    {
        $dataClienteJSON = array();
        foreach($this->getClientesAtivos() as $dataCliente){
            $nome = $dataCliente->getNome();
            if(empty($nome)){
                $nome = $dataCliente->getNomeFantasia();
            }
            $dataClienteJSON[] = [
                'id' => $dataCliente->getId(),
                'value' => $dataCliente->getId() . " -  " .$nome . " - " .
                    (is_null($dataCliente->getTelefoneCelular())?
                        $dataCliente->getTelefoneFixo() : $dataCliente->getTelefoneCelular())
                ];
        }
        return json_encode($dataClienteJSON);
    }

    public function fetchPairs()
    {
        $result = $this->getClientesAtivos();
        $arrResult = [];
        if($result){
            foreach ($result as $item){
                $nome = $item->getNome();
                if(empty($nome)){
                    $nome = $item->getNomeFantasia();
                }
                $arrResult[$item->getId()] = $nome;
            }
        }
        return $arrResult;
    }

    public function getClientesAtivos()
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("c")->from(Cliente::class, "c")
                ->innerJoin(EnderecoCliente::class , "ec", "with", "c.id=ec.cliente");


                $qb->andWhere("c.ativo =:param1")->setParameter('param1', '1');

            $qb->setMaxResults(500);
            $qb->addOrderBy('c.nome', 'ASC');


            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getListaDados($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("c")->from(Cliente::class, "c")
                ->innerJoin(EnderecoCliente::class , "ec", "with", "c.id=ec.cliente");

            $filtros = $this->treatmentDate($filtros, ['dataCadastroDe', 'dataCadastroAte']);
            if(!empty($filtros['ativo'])){
                $qb->andWhere("c.ativo = :param1")->setParameter('param1', $filtros['ativo']);
            }
            if(!empty($filtros['dataCadastroDe']) || !empty($filtros['dataCadastroAte'])){
                if($filtros['dataCadastroDe'] == $filtros['dataCadastroAte']){
                    $qb->andWhere("DATE(c.dataCadastro) = DATE(:param2)")->setParameter('param2', $filtros['dataCadastroDe']);
                }else{
                    if(!empty($filtros['dataCadastroDe'])){
                        $qb->andWhere("DATE(c.dataCadastro) >= DATE(:param3)")->setParameter('param3', $filtros['dataCadastroDe']);
                    }
                    if(!empty($filtros['dataCadastroAte'])){
                        $qb->andWhere("DATE(c.dataCadastro) <= DATE(:param4)")->setParameter('param4', $filtros['dataCadastroAte']);
                    }
                }
            }

            if(!empty($filtros['tipo']) ){
                $qb->andWhere("c.tipo = :param5")->setParameter('param5', $filtros['tipo']);
                if($filtros['tipo'] == "PF"){
                    if(!empty($filtros['CPF'])){
                        $qb->andWhere("c.cpf = :param6")->setParameter('param6', $filtros['CPF']);
                    }
                    if(!empty($filtros['nome']) ){
                        $qb->andWhere($qb->expr()->like('c.nome', $qb->expr()->literal("%". $filtros['nome'] . "%")) );
                    }
                }else{
                    if(!empty($filtros['nome']) ){
                        $qb->andWhere($qb->expr()->like('c.nomeFantasia', $qb->expr()->literal("%". $filtros['nome'] . "%")) );
                    }
                    if(!empty($filtros['CNPJ'])){
                        $qb->andWhere("c.cnpj = :param6")->setParameter('param6', $filtros['CNPJ']);
                    }
                }
            }

            if(!empty($filtros['email'])){
                $qb->andWhere("c.email = :param7")->setParameter('param7', $filtros['email']);
            }

            if(!empty($filtros['telefoneFixo'])){
                $qb->andWhere("c.telefoneFixo = :param8")->setParameter('param8', $filtros['telefoneFixo']);
            }

            if(!empty($filtros['telefoneCelular'])){
                $qb->andWhere("c.telefoneCelular = :param9")->setParameter('param9', $filtros['telefoneCelular']);
            }

            if(!empty($filtros['codigo'])){
                $qb->andWhere("c.id = :param10")->setParameter('param10', $filtros['codigo']);
            }


            $qb->setMaxResults(500);
            $qb->addOrderBy('c.nome', 'ASC');
            $qb->addOrderBy('c.nomeFantasia', 'ASC');


            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    public function getCabecalhosRelatorioCadastro()
    {
        return [ 'id' =>'ID','nome' => 'Nome / Razão Social ', 'sexo' => 'Sexo', 'dataNascimento' => "Aniversário", 'telefoneFixo' => "Tel. Fixo",
            'telefoneCelular' => "Tel. Cel.", 'email' => "E-mail", 'dataCadastro' => "D. Cad." ];
    }
}