<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06/03/17
 * Time: 12:31
 */

namespace App\Repository\Venda;


use App\Entity\ItemPedido;
use App\Entity\Pedido;
use App\Entity\Produto;
use App\Entity\ItemRemessa;
use App\Entity\RemessaFornecedor;
use App\Repository\AppAbstractRepository;

class ItemPedidoRepository extends AppAbstractRepository{

    public function getItensPedido($idPedido)
    {
        try{
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("ip")->from(ItemPedido::class, "ip")
                ->innerJoin(Pedido::class, "p", "with", "p.id=ip.pedido");

            $qb->where('p.id = :param1')
                ->setParameter('param1', $idPedido);
            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getItensPedidoDesconsiderar($idPedido, array $idItensPedido)
    {
        try{
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("ip")->from(ItemPedido::class, "ip")
                ->innerJoin(Pedido::class, "p", "with", "p.id=ip.pedido");

            $qb->where('p.id = :param1')
                ->setParameter('param1', $idPedido)
                ->andWhere('ip.id NOT IN(:param2)')
                ->setParameter('param2', $idItensPedido)
            ;
            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getItensDevolverForaDeLinha($idPedido, array $idItensPedido)
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->from(ItemPedido::class, "ip")
                ->addSelect('pr.id as id_pr')
                ->addSelect('ir.quantParcial')
                ->addSelect('rf.id as id_rf')

                ->innerJoin(Pedido::class, "p", "with", "p.id=ip.pedido")
                ->innerJoin(Produto::class, "pr", "with", "pr.id=ip.produto")
                ->innerJoin(ItemRemessa::class, "ir", "with", "ir.produto=pr.id")
                ->innerJoin(RemessaFornecedor::class, "rf", "with", "rf.id=ir.remessaFornecedor");

            $qb->where('p.id = :param1')->setParameter('param1', $idPedido)
                ->andWhere('ip.id IN(:param2)')
                ->setParameter('param2', $idItensPedido)
                ->andWhere('rf.status = :param3')
                ->setParameter('param3', "Ativa")
                //Avaliar se a data do pedido é superior a data da remessa
                ->andWhere('DATE(p.dataCadastro) >= DATE(rf.dataCadastro)');
            $qb->orderBy('rf.id', 'DESC');
            $qb->distinct(true);
            //echo $qb->getQuery()->getSQL(); die;
            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }


} 