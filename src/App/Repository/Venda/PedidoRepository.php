<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 30/03/17
 * Time: 00:13
 */

namespace App\Repository\Venda;


use App\Repository\AppAbstractRepository;
use App\Entity\Pedido;
use App\Entity\ItemPedido;
use App\Entity\Cliente;
use App\Entity\Usuario;

class PedidoRepository extends AppAbstractRepository {

    public function getPedidos($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("p")->from(Pedido::class, "p")
                ->innerJoin(ItemPedido::class, "ip", "with", "ip.pedido=p.id")
                ->leftJoin(Usuario::class, "u", "with", "u.id=p.usuario")
                ->innerJoin(Cliente::class, "c", "with", "c.id=p.cliente")

                ->addGroupBy('p.id');

            $filtros = $this->treatmentDate($filtros, ['dataCadastroDe', 'dataCadastroAte']);
            if(!empty($filtros['status'])){
                $qb->andWhere("p.status = :param1")->setParameter('param1', $filtros['status']);
            }
            if(!empty($filtros['dataCadastroDe']) || !empty($filtros['dataCadastroAte'])){
                if($filtros['dataCadastroDe'] == $filtros['dataCadastroAte']){
                    $qb->andWhere("DATE(p.dataCadastro) = DATE(:param2)")->setParameter('param2', $filtros['dataCadastroDe']);
                }else{
                    if(!empty($filtros['dataCadastroDe'])){
                        $qb->andWhere("DATE(p.dataCadastro) >= DATE(:param3)")->setParameter('param3', $filtros['dataCadastroDe']);
                    }
                    if(!empty($filtros['dataCadastroAte'])){
                        $qb->andWhere("DATE(p.dataCadastro) <= DATE(:param4)")->setParameter('param4', $filtros['dataCadastroAte']);
                    }
                }

            }

            if(!empty($filtros['nome'])){
                $qb->andWhere($qb->expr()->like('c.nome', $qb->expr()->literal("%". $filtros['nome'] . "%")) );
            }

            if(!empty($filtros['vendedor'])){
                $qb->andWhere($qb->expr()->like('u.nome', $qb->expr()->literal("%". $filtros['vendedor'] . "%")) );
            }


            if(!empty($filtros['valor'])){
                $filtros['valor'] = strtr($filtros['valor'], ['.' => '', ',' => '.']);
                $qb->andWhere("p.valorTotal = :param9")->setParameter('param9', $filtros['valor']);
            }

            if(!empty($filtros['nome'])){
                $qb->andWhere($qb->expr()->like('c.nome', $qb->expr()->literal("%". $filtros['nome'] . "%")) );
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('p.id', 'DESC');

            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getVendasPorCliente($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->from(Pedido::class, "p")
                ->innerJoin(Cliente::class, "c", "with", "c.id=p.cliente");
            $qb->addSelect("COUNT(c.id) as quantVenda");
            $qb->addSelect("SUM(p.valorTotal) as valorVenda");
            $qb->addSelect("c.nome as cliente");
            $qb->addSelect("c.id as idCliente");
            $qb->addSelect("c.telefoneFixo");
            $qb->addSelect("c.telefoneCelular");
            $qb->addSelect("c.email");

            $filtros = $this->treatmentDate($filtros, ['dataCadastroDe', 'dataCadastroAte']);
            if(!empty($filtros['status'])){
                $qb->andWhere("p.status = :param1")->setParameter('param1', $filtros['status']);
            }
            if(!empty($filtros['dataCadastroDe']) || !empty($filtros['dataCadastroAte'])){
                if($filtros['dataCadastroDe'] == $filtros['dataCadastroAte']){
                    $qb->andWhere("DATE(p.dataCadastro) = DATE(:param2)")->setParameter('param2', $filtros['dataCadastroDe']);
                }else{
                    if(!empty($filtros['dataCadastroDe'])){
                        $qb->andWhere("DATE(p.dataCadastro) >= DATE(:param3)")->setParameter('param3', $filtros['dataCadastroDe']);
                    }
                    if(!empty($filtros['dataCadastroAte'])){
                        $qb->andWhere("DATE(p.dataCadastro) <= DATE(:param4)")->setParameter('param4', $filtros['dataCadastroAte']);
                    }
                }

            }

            if(!empty($filtros['nome'])){
                $qb->andWhere($qb->expr()->like('c.nome', $qb->expr()->literal("%". $filtros['nome'] . "%")) );
            }

            if(!empty($filtros['valor'])){
                $filtros['valor'] = strtr($filtros['valor'], ['.' => '', ',' => '.']);
               $qb->andHaving("SUM(p.valorTotal) = :param9")->setParameter('param9', $filtros['valor']);
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('c.nome', 'ASC');
            //$qb->groupBy('p.id');
            $qb->addGroupBy('c.id');

            $qb = $qb->getQuery()->getArrayResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getPedidosEntregar($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("p")->from(Pedido::class, "p")
                ->innerJoin(ItemPedido::class, "ip", "with", "ip.pedido=p.id")
                ->innerJoin(Cliente::class, "c", "with", "c.id=p.cliente");

            $filtros = $this->treatmentDate($filtros, ['dataCadastroDe', 'dataCadastroAte']);
            if(!empty($filtros['status'])){
                $qb->andWhere("p.status = :param1")->setParameter('param1', $filtros['status']);
            }
            if(!empty($filtros['dataCadastroDe']) || !empty($filtros['dataCadastroAte'])){
                if($filtros['dataCadastroDe'] == $filtros['dataCadastroAte']){
                    $qb->andWhere("DATE(p.dataCadastro) = DATE(:param2)")->setParameter('param2', $filtros['dataCadastroDe']);
                }else{
                    if(!empty($filtros['dataCadastroDe'])){
                        $qb->andWhere("DATE(p.dataCadastro) >= DATE(:param3)")->setParameter('param3', $filtros['dataCadastroDe']);
                    }
                    if(!empty($filtros['dataCadastroAte'])){
                        $qb->andWhere("DATE(p.dataCadastro) <= DATE(:param4)")->setParameter('param4', $filtros['dataCadastroAte']);
                    }
                }
            }

            if(empty($filtros['status'])){
                $qb->andWhere("p.status = :param10")->setParameter('param10', "Pendente Entrega");
                $qb->andWhere("DATE(p.dataEntrega) <= DATE(:param11)")->setParameter('param11', date('Y-m-d'));
            }

            if(!empty($filtros['nome'])){
                $qb->andWhere($qb->expr()->like('c.nome', $qb->expr()->literal("%". $filtros['nome'] . "%")) );
            }

            if(!empty($filtros['valor'])){
                $filtros['valor'] = strtr($filtros['valor'], ['.' => '', ',' => '.']);
                $qb->andWhere("p.valor = :param9")->setParameter('param9', $filtros['valor']);
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('p.id', 'ASC');

            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }



    public function getListaDados($filtros = array())
    {
        try{
            $config = $this->getEntityManager()->getConfiguration();
            $config->addCustomStringFunction('DATE', \DoctrineExtensions\Query\Mysql\Date::class);
            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select("p")->from(Pedido::class, "p")
                ->innerJoin(ItemPedido::class, "ip", "with", "ip.pedido=p.id")
                ->innerJoin(Cliente::class, "c", "with", "c.id=p.cliente");

            $filtros = $this->treatmentDate($filtros, ['dataCadastroDe', 'dataCadastroAte']);
            if(!empty($filtros['status'])){
                $qb->andWhere("p.status = :param1")->setParameter('param1', $filtros['status']);
            }
            if(!empty($filtros['dataCadastroDe']) || !empty($filtros['dataCadastroAte'])){
                if($filtros['dataCadastroDe'] == $filtros['dataCadastroAte']){
                    $qb->andWhere("DATE(p.dataCadastro) = DATE(:param2)")->setParameter('param2', $filtros['dataCadastroDe']);
                }else{
                    if(!empty($filtros['dataCadastroDe'])){
                        $qb->andWhere("DATE(p.dataCadastro) >= DATE(:param3)")->setParameter('param3', $filtros['dataCadastroDe']);
                    }
                    if(!empty($filtros['dataCadastroAte'])){
                        $qb->andWhere("DATE(p.dataCadastro) <= DATE(:param4)")->setParameter('param4', $filtros['dataCadastroAte']);
                    }
                }

            }

            if(!empty($filtros['nome'])){
                $qb->andWhere($qb->expr()->like('c.nome', $qb->expr()->literal("%". $filtros['nome'] . "%")) );
            }

            if(!empty($filtros['valor'])){
                $filtros['valor'] = strtr($filtros['valor'], ['.' => '', ',' => '.']);
                $qb->andWhere("p.valor = :param9")->setParameter('param9', $filtros['valor']);
            }

            $qb->setMaxResults(500);
            $qb->addOrderBy('p.id', 'DESC');

            $qb = $qb->getQuery()->getResult();
            return (empty($qb))? array() : $qb;

        }catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getCabecalhosRelatorioCadastro()
    {
        return [ 'cliente' => 'Nome do Cliente', 'dataCadastro' => 'D. de Cad.', 'dataEntrega' => "D. de Ent.",
            'valorTotal' => "V. Parcial", 'desconto' => "V. Desc.", 'status' => "Status"];
    }
} 