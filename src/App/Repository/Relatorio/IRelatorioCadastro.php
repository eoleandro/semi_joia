<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/04/17
 * Time: 12:45
 */

namespace App\Repository\Relatorio;


interface IRelatorioCadastro {

    public function getCabecalhosRelatorioCadastro();
    public function getListaDados($filtros = array());

} 