<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 22/02/17
 * Time: 16:17
 */

namespace App\Form\Cliente;


use Zend\Form\Form;
use Zend\Form\Element;


class ClienteForm extends Form
{
    public function __construct($name = 'fornecedor', array $options = [])
    {
        parent::__construct($name, $options);


        $this->add([
            'name' => 'id',
            'type' => Element\Hidden::class
        ]);

        $this->add([
            'name' => 'nome',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Nome'
            ],
            'attributes' => [
                'id' => 'nome',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'CPF',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'CPF'
            ],
            'attributes' => [
                'id' => 'CPF',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'sexo',
            'type' => Element\Radio::class,
            'options' => [
                //'label' => 'Está Ativo?',
                'value_options' => array(
                    'F' => 'Feminino',
                    'M' => 'Masculino',
                ),
            ],
            'attributes' => [
                'id' => 'sexo',
                'class' => 'form-control dimensionar-radio'
            ]
        ]);


        $this->add([
            'name' => 'dataNascimento',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'D. Nascimento'
            ],
            'attributes' => [
                'id' => 'dataNascimento',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'nomeConjuge',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Nome do Conjuge'
            ],
            'attributes' => [
                'id' => 'nomeConjuge',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'dataCasamento',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'D. Casamento'
            ],
            'attributes' => [
                'id' => 'dataCasamento',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'email',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Email'
            ],
            'attributes' => [
                'id' => 'email',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'telefoneFixo',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Telefone fixo'
            ],
            'attributes' => [
                'id' => 'telefoneFixo',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'telefoneCelular',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Telefone celular'
            ],
            'attributes' => [
                'id' => 'telefoneCelular',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'logradouro',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Logradouro'
            ],
            'attributes' => [
                'id' => 'logradouro',
                'class' => 'form-control'
            ]
        ]);
        $this->add([
            'name' => 'numero',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Número'
            ],
            'attributes' => [
                'id' => 'numero',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'bairro',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Bairro'
            ],
            'attributes' => [
                'id' => 'bairro',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'cidade',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Cidade'
            ],
            'attributes' => [
                'id' => 'cidade',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'UF',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'UF'
            ],
            'attributes' => [
                'id' => 'UF',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'CEP',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'CEP'
            ],
            'attributes' => [
                'id' => 'CEP',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'referencia',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Referência'
            ],
            'attributes' => [
                'id' => 'referencia',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'tipo',
            'type' => Element\Radio::class,
            'options' => [
                //'label' => 'Está Ativo?',
                'value_options' => array(
                    'PF' => 'Pessoa Física',
                    'PJ' => 'Pessoa Jurídica',
                ),
                'checked_value' => 'PF',
                'value' => 'PF'
            ],
            'attributes' => [
                //'id' => 'tipo',
                'class' => 'form-control dimensionar-radio tipocliente'
            ]
        ]);


        $this->add([
            'name' => 'CNPJ',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'CNPJ'
            ],
            'attributes' => [
                'id' => 'CNPJ',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'razaoSocial',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Razao Social'
            ],
            'attributes' => [
                'id' => 'razaoSocial',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'nomeFantasia',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Nome Fantasia'
            ],
            'attributes' => [
                'id' => 'nomeFantasia',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'ativo',
            'type' => Element\Radio::class,
            'options' => [
                //'label' => 'Está Ativo?',
                'value_options' => array(
                    '1' => 'Sim',
                    '0' => 'Não',
                ),
            ],
            'attributes' => [
                'id' => 'ativo',
                'class' => 'form-control dimensionar-radio'
            ]
        ]);



        $this->add([
            'name' => 'submit',
            'type' => Element\Button::class,
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
                //'label' => 'Salvar'
            ]
        ]);
    }
}