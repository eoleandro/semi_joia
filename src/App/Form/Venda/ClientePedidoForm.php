<?php


namespace App\Form\Venda;

use Zend\Form\Form;
use Zend\Form\Element;

class ClientePedidoForm extends  Form
{
    public function __construct($name = 'clientepedido', array $options = [])
    {
        parent::__construct($name, $options);


        $this->add([
            'name' => 'id',
            'type' => Element\Hidden::class
        ]);

        $this->add([
            'name' => 'cliente',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Escolha o cliente do pedido'
            ],
            'attributes' => [
                'id' => 'cliente',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Element\Button::class,
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary'
            ]
        ]);
    }
}