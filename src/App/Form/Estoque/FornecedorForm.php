<?php

namespace  App\Form\Estoque;

use Zend\Form\Form;
use Zend\Form\Element;

class FornecedorForm extends  Form
{
    public function __construct($name = 'fornecedor', array $options = [])
    {
        parent::__construct($name, $options);


        $this->add([
            'name' => 'id',
            'type' => Element\Hidden::class
        ]);

        $this->add([
            'name' => 'razaoSocial',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Razao Social'
            ],
            'attributes' => [
                'id' => 'razaoSocial',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'nomeFantasia',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Nome Fantasia'
            ],
            'attributes' => [
                'id' => 'nomeFantasia',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'email',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Email'
            ],
            'attributes' => [
                'id' => 'email',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'CNPJ',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'CNPJ'
            ],
            'attributes' => [
                'id' => 'CNPJ',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'telefoneFixo',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Telefone fixo'
            ],
            'attributes' => [
                'id' => 'telefoneFixo',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'telefoneCelular',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Telefone celular'
            ],
            'attributes' => [
                'id' => 'telefoneCelular',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'inscEstadual',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Inscrição Estadual'
            ],
            'attributes' => [
                'id' => 'inscEstadual',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'inscMunicipal',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Inscrição Municipal'
            ],
            'attributes' => [
                'id' => 'inscMunicipal',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'ramoAtividade',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Ramo de atividade'
            ],
            'attributes' => [
                'id' => 'ramoAtividade',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'observacao',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Observação'
            ],
            'attributes' => [
                'id' => 'observacao',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'ativo',
            'type' => Element\Radio::class,
            'options' => [
                //'label' => 'Está Ativo?',
                'value_options' => array(
                    '1' => 'Sim',
                    '0' => 'Não',
                ),
            ],
            'attributes' => [
                'id' => 'ativo',
                'class' => 'form-control dimensionar-radio'
            ]
        ]);

        $this->add([
            'name' => 'nome',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Nome'
            ],
            'attributes' => [
                'id' => 'nome',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'funcao',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Função'
            ],
            'attributes' => [
                'id' => 'funcao',
                'class' => 'form-control'
            ]
        ]);


        $this->add([
            'name' => 'telefone',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Telefone'
            ],
            'attributes' => [
                'id' => 'telefone',
                'class' => 'form-control'
            ]
        ]);


        $this->add([
            'name' => 'logradouro',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Logradouro'
            ],
            'attributes' => [
                'id' => 'logradouro',
                'class' => 'form-control'
            ]
        ]);
        $this->add([
            'name' => 'numero',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Número'
            ],
            'attributes' => [
                'id' => 'numero',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'bairro',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Bairro'
            ],
            'attributes' => [
                'id' => 'bairro',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'cidade',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Cidade'
            ],
            'attributes' => [
                'id' => 'cidade',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'UF',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'UF'
            ],
            'attributes' => [
                'id' => 'UF',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'CEP',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'CEP'
            ],
            'attributes' => [
                'id' => 'CEP',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'referencia',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Referência'
            ],
            'attributes' => [
                'id' => 'referencia',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Element\Button::class,
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
                //'label' => 'Salvar'
            ]
        ]);
    }

}