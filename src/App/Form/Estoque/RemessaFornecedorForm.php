<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 23/02/17
 * Time: 14:28
 */

namespace App\Form\Estoque;

use Zend\Form\Form;
use Zend\Form\Element;

class RemessaFornecedorForm extends Form
{
    public function __construct($name = 'remessafornecedor', array $options = [])
    {
        parent::__construct($name, $options);


        $this->add([
            'name' => 'id',
            'type' => Element\Hidden::class
        ]);

        $this->add([
            'name' => 'fornecedor',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Escolha o fornecedor da Remessa de produtos'
            ],
            'attributes' => [
                'id' => 'name',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Element\Button::class,
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
                //'label' => 'Salvar'
            ]
        ]);
    }

}