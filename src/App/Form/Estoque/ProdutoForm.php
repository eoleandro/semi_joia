<?php

namespace App\Form\Estoque;

use Zend\Form\Form;
use Zend\Form\Element;

class ProdutoForm extends Form
{
    public function __construct($name = 'produto', array $options = [])
    {
        parent::__construct($name, $options);


        $this->add([
            'name' => 'id',
            'type' => Element\Hidden::class
        ]);

        $this->add([
            'name' => 'nome',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Descrição'
            ],
            'attributes' => [
                'id' => 'name',
                'class' => 'form-control'
            ]
        ]);
        $this->add([
            'name' => 'fornecedor',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Fornecedor'
            ],
            'attributes' => [
                'id' => 'fornecedor',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'preco',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Preço'
            ],
            'attributes' => [
                'id' => 'preco',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'metal',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Metal'
            ],
            'attributes' => [
                'id' => 'metal',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'peso',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Peso'
            ],
            'attributes' => [
                'id' => 'peso',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'formato',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Formato'
            ],
            'attributes' => [
                'id' => 'formato',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'pedra',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Pedra'
            ],
            'attributes' => [
                'id' => 'pedra',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'tipoPedra',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Tipo de Pedra'
            ],
            'attributes' => [
                'id' => 'tipoPedra',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'corPedra',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Cor da Pedra'
            ],
            'attributes' => [
                'id' => 'corPedra',
                'class' => 'form-control'
            ]
        ]);


        $this->add([
            'name' => 'dimensaoPedra',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Dimensão da Pedra'
            ],
            'attributes' => [
                'id' => 'dimensaoPedra',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'garantia',
            'type' => Element\Radio::class,
            'options' => [
                'label' => 'Tem Garantia?',
                'value_options' => array(
                    '1' => 'Sim',
                    '0' => 'Não',
                ),
            ],
            'attributes' => [
                'id' => 'garantia',
                'class' => 'form-control dimensionar-radio'
            ]
        ]);

        $this->add([
            'name' => 'ativo',
            'type' => Element\Radio::class,
            'options' => [
                //'label' => 'Está Ativo?',
                'value_options' => array(
                    '1' => 'Sim',
                    '0' => 'Não',
                ),
            ],
            'attributes' => [
                'id' => 'ativo',
                'class' => 'form-control dimensionar-radio'
            ]
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Element\Button::class,
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary'
            ]
        ]);
    }

}
