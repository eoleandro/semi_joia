<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01/04/17
 * Time: 03:20
 */

namespace App\Form\Pesquisa;


use Zend\Form\Form;
use Zend\Form\Element;

class AgendaForm extends  Form
{
    public function __construct($name = 'pesquisaagenda', array $options = [])
    {
        parent::__construct($name, $options);


        $this->add([
            'name' => 'evento',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Evento'
            ],
            'attributes' => [
                'id' => 'evento',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'dataDe',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Data De'
            ],
            'attributes' => [
                'id' => 'dataDe',
                'class' => 'form-control data'
            ]
        ]);

        $this->add([
            'name' => 'dataAte',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Data Até'
            ],
            'attributes' => [
                'id' => 'dataAte',
                'class' => 'form-control data'
            ]
        ]);

        $this->add([
            'name' => 'status',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Situação',
                'value_options' => array(
                    '' => 'Selecione',
                    'M' => "Agendada",
                    'R' => "Realizada"
                ),
            ],
            'attributes' => [
                'id' => 'status',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Element\Button::class,
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success'
            ]
        ]);
    }
}