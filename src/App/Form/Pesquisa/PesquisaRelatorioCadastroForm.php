<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/04/17
 * Time: 13:20
 */

namespace App\Form\Pesquisa;


use Zend\Form\Form;
use Zend\Form\Element;

class PesquisaRelatorioCadastroForm extends  Form
{
    public function __construct($name = 'pesquisanome', array $options = [])
    {
        parent::__construct($name, $options);


        $this->add([
            'name' => 'cadastro',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Cadastro',
                'value_options' => array(
                    //'' => 'Selecione',
                    'Agenda' => 'Agenda',
                    'Cliente' => 'Cliente',
                    'Fornecedor' => 'Fornecedor',
                    'FormaPgto' => 'Forma de Pagamento',
                    'Pedido' => 'Pedido',
                    'PlanoConta' => 'Plano de Conta',
                    'Produto' => 'Produto',
                    'RemessaFornecedor' => 'Remessa de Fornecedor',
                    'Usuario' => 'Usuário',
                ),
            ],
            'attributes' => [
                'id' => 'cadastro',
                'class' => 'form-control'
            ]
        ]);
        $this->add([
            'name' => 'submit',
            'type' => Element\Button::class,
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success'
            ]
        ]);
    }
}