<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 24/04/17
 * Time: 11:10
 */

namespace App\Form\Pesquisa;

use Zend\Form\Form;
use Zend\Form\Element;

class PesquisaCadastroFormaPgtoForm extends  Form
{
    public function __construct($name = 'pesquisaformapgto', array $options = [])
    {
        parent::__construct($name, $options);


        $this->add([
            'name' => 'nome',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Descrição'
            ],
            'attributes' => [
                'id' => 'nome',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Element\Button::class,
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success'
            ]
        ]);
    }
}