<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/17
 * Time: 17:44
 */

namespace App\Form\Pesquisa;

use Zend\Form\Form;
use Zend\Form\Element;

class PesquisaMovFinanceiroForm extends Form
{
    public function __construct($name = 'pesquisamovfinanceiro', array $options = [])
    {
        parent::__construct($name, $options);


        $this->add([
            'name' => 'descricao',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Decrição'
            ],
            'attributes' => [
                'id' => 'descricao',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'dataCadastroDe',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'D. Cadastro De'
            ],
            'attributes' => [
                'id' => 'dataCadastroDe',
                'class' => 'form-control data'
            ]
        ]);

        $this->add([
            'name' => 'dataCadastroAte',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'D. Cadastro Até'
            ],
            'attributes' => [
                'id' => 'dataCadastroAte',
                'class' => 'form-control data'
            ]
        ]);

        $this->add([
            'name' => 'dataVencimentoDe',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'D. Vencimento De'
            ],
            'attributes' => [
                'id' => 'dataVencimentoDe',
                'class' => 'form-control data'
            ]
        ]);

        $this->add([
            'name' => 'dataVencimentoAte',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'D. Vencimento Até'
            ],
            'attributes' => [
                'id' => 'dataVencimentoAte',
                'class' => 'form-control data'
            ]
        ]);

        $this->add([
            'name' => 'valor',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Valor'
            ],
            'attributes' => [
                'id' => 'valor',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'natureza',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Natureza',
                'value_options' => array(
                    '' => 'Selecione',
                    'D' => 'Despesa',
                    'R' => 'Receita',
                    'S' => 'Retirada'
                ),
            ],
            'attributes' => [
                'id' => 'natureza',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'formaPgto',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Forma de Pagamento',
            ],
            'attributes' => [
                'id' => 'formaPgto',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'planoConta',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Plano de Conta',
            ],
            'attributes' => [
                'id' => 'planoConta',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'situacao',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Situação',
                'value_options' => array(
                    '' => 'Selecione',
                    'F' => 'Quitado',
                    'A' => 'Em Aberto',
                    'P' => 'Pendente'
                ),
            ],
            'attributes' => [
                'id' => 'situacao',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Element\Button::class,
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success',
                //'label' => 'Salvar'
            ]
        ]);
    }

}