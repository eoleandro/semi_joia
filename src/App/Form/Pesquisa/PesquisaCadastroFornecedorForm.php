<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 24/04/17
 * Time: 10:21
 */

namespace App\Form\Pesquisa;


use Zend\Form\Form;
use Zend\Form\Element;

class PesquisaCadastroFornecedorForm extends  Form
{
    public function __construct($name = 'pesquisafornecedor', array $options = [])
    {
        parent::__construct($name, $options);

        $this->add([
            'name' => 'nome',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Nome Fantasia'
            ],
            'attributes' => [
                'id' => 'nome',
                'class' => 'form-control'
            ]
        ]);


        $this->add([
            'name' => 'ativo',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Ativo?',
                'value_options' => array(
                    '1'=> "Sim",
                    '0'=> "Não",
                ),
            ],
            'attributes' => [
                'id' => 'ativo',
                'class' => 'form-control'
            ]
        ]);


        $this->add([
            'name' => 'CNPJ',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'CNPJ'
            ],
            'attributes' => [
                'id' => 'CNPJ',
                'class' => 'form-control'
            ]
        ]);


        $this->add([
            'name' => 'email',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Email'
            ],
            'attributes' => [
                'id' => 'email',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'telefoneFixo',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Tel. Fixo'
            ],
            'attributes' => [
                'id' => 'telefoneFixo',
                'class' => 'form-control telefone'
            ]
        ]);
        $this->add([
            'name' => 'telefoneCelular',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Tel. Celular'
            ],
            'attributes' => [
                'id' => 'telefoneCelular',
                'class' => 'form-control telefone'
            ]
        ]);

        $this->add([
            'name' => 'telefoneContato',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Tel. Contato'
            ],
            'attributes' => [
                'id' => 'telefoneContato',
                'class' => 'form-control telefone'
            ]
        ]);


        $this->add([
            'name' => 'nomeContato',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Contato'
            ],
            'attributes' => [
                'id' => 'nomeContato',
                'class' => 'form-control'
            ]
        ]);
        /*$this->add([
            'name' => 'dataCadastroDe',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'D. Cadastro De'
            ],
            'attributes' => [
                'id' => 'dataCadastroDe',
                'class' => 'form-control data'
            ]
        ]);

        $this->add([
            'name' => 'dataCadastroAte',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'D. Cadastro Até'
            ],
            'attributes' => [
                'id' => 'dataCadastroAte',
                'class' => 'form-control data'
            ]
        ]);*/

        $this->add([
            'name' => 'codigo',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Código'
            ],
            'attributes' => [
                'id' => 'codigo',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Element\Button::class,
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success'
            ]
        ]);
    }
}