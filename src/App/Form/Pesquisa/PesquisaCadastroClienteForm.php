<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20/04/17
 * Time: 16:18
 */

namespace App\Form\Pesquisa;


use Zend\Form\Form;
use Zend\Form\Element;

class PesquisaCadastroClienteForm extends  Form
{
    public function __construct($name = 'pesquisacliente', array $options = [])
    {
        parent::__construct($name, $options);

        $this->add([
            'name' => 'nome',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Nome Cliente / Nome Fantasia'
            ],
            'attributes' => [
                'id' => 'nome',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'tipo',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Tipo',
                'value_options' => array(
                    'PF'=> "Pessoa Física",
                    'PJ'=> "Pessoa Jurídica",
                ),
            ],
            'attributes' => [
                'id' => 'tipo',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'ativo',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Ativo?',
                'value_options' => array(
                    '1'=> "Sim",
                    '0'=> "Não",
                ),
            ],
            'attributes' => [
                'id' => 'ativo',
                'class' => 'form-control'
            ]
        ]);


        $this->add([
            'name' => 'CPF',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'CPF'
            ],
            'attributes' => [
                'id' => 'CPF',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'CNPJ',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'CNPJ'
            ],
            'attributes' => [
                'id' => 'CNPJ',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'email',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Email'
            ],
            'attributes' => [
                'id' => 'email',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'telefoneFixo',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Tel. Fixo'
            ],
            'attributes' => [
                'id' => 'telefoneFixo',
                'class' => 'form-control telefone'
            ]
        ]);
        $this->add([
            'name' => 'telefoneCelular',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Tel. Celular'
            ],
            'attributes' => [
                'id' => 'telefoneCelular',
                'class' => 'form-control telefone'
            ]
        ]);

        $this->add([
            'name' => 'dataCadastroDe',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'D. Cadastro De'
            ],
            'attributes' => [
                'id' => 'dataCadastroDe',
                'class' => 'form-control data'
            ]
        ]);

        $this->add([
            'name' => 'dataCadastroAte',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'D. Cadastro Até'
            ],
            'attributes' => [
                'id' => 'dataCadastroAte',
                'class' => 'form-control data'
            ]
        ]);

        $this->add([
            'name' => 'codigo',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Código'
            ],
            'attributes' => [
                'id' => 'codigo',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Element\Button::class,
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success'
            ]
        ]);
    }
}