<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01/04/17
 * Time: 03:40
 */

namespace App\Form\Pesquisa;

use Zend\Form\Form;
use Zend\Form\Element;

class PesquisaNomeForm extends  Form
{
    public function __construct($name = 'pesquisanome', array $options = [])
    {
        parent::__construct($name, $options);


        $this->add([
            'name' => 'nome',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Nome'
            ],
            'attributes' => [
                'id' => 'nome',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Element\Button::class,
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success'
            ]
        ]);
    }
}