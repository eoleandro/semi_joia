<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29/03/17
 * Time: 09:11
 */

namespace App\Form\Pesquisa;

use Zend\Form\Form;
use Zend\Form\Element;

class GrupoProdutoForm extends  Form
{
    public function __construct($name = 'pesquisaloteitens', array $options = [])
    {
        parent::__construct($name, $options);


        $this->add([
            'name' => 'id',
            'type' => Element\Hidden::class
        ]);

        $this->add([
            'name' => 'nome',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Nome'
            ],
            'attributes' => [
                'id' => 'nome',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'vendedor',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Vendedor'
            ],
            'attributes' => [
                'id' => 'vendedor',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'dataCadastroDe',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'D. Cadastro De'
            ],
            'attributes' => [
                'id' => 'dataCadastroDe',
                'class' => 'form-control data'
            ]
        ]);

        $this->add([
            'name' => 'dataCadastroAte',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'D. Cadastro Até'
            ],
            'attributes' => [
                'id' => 'dataCadastroAte',
                'class' => 'form-control data'
            ]
        ]);

        $this->add([
            'name' => 'status',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Status',
                'value_options' => array(
                    '' => 'Selecione',
                    'Cancelado' => 'Cancelado',
                    'Definir Parcelamento' => 'Definir Parcelamento',
                    'Entregue' => 'Entregue',
                    'Itens definidos' => 'Itens definidos',
                    'Parcelas em Quitação' => 'Parcelas em Quitação',
                    'Itens definidos' => 'Pendente Entrega',
                ),
            ],
            'attributes' => [
                'id' => 'status',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'valor',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Valor'
            ],
            'attributes' => [
                'id' => 'valor',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Element\Button::class,
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success'
            ]
        ]);
    }
}