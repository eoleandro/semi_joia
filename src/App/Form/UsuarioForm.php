<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 18/02/17
 * Time: 18:51
 */

namespace App\Form;

use Zend\Form\Form;
use Zend\Form\Element;


class UsuarioForm extends  Form
{
    public function __construct($name = 'usuario', array $options = [])
    {
        parent::__construct($name, $options);

        $this->setAttribute("enctype", "multipart/form-data");

        $this->add([
            'name' => 'id',
            'type' => Element\Hidden::class
        ]);

        $this->add([
            'name' => 'nome',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Nome'
            ],
            'attributes' => [
                'id' => 'name',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'grupo',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Grupo'
            ],
            'attributes' => [
                'id' => 'name',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'email',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'E-mail'
            ],
            'attributes' => [
                'id' => 'email',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'login',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Login'
            ],
            'attributes' => [
                'id' => 'login',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'senha',
            'type' => Element\Password::class,
            'options' => [
                'label' => 'Senha'
            ],
            'attributes' => [
                'id' => 'senha',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'foto',
            'type' => Element\File::class,
            'options' => [
                'label' => 'Foto'
            ],
            'attributes' => [
                'id' => 'foto',
                'class' => 'form-control'
            ]
        ]);

         $this->add([
            'name' => 'ativo',
            'type' => Element\Radio::class,
            'options' => [
                'label' => 'Usuário ativo?',
                'value_options' => array(
                    '1' => 'Sim',
                    '0' => 'Não',
                ),
            ],
            'attributes' => [
                'id' => 'ativo',
                'class' => 'form-control'
            ]
        ]);




        $this->add([
            'name' => 'submit',
            'type' => Element\Button::class,
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
                //'label' => 'Salvar'
            ]
        ]);
    }

}