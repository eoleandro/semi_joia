<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 20/02/17
 * Time: 11:10
 */

namespace App\Form;


use Zend\Form\Element;
use Zend\Form\Form;

class LoginForm extends Form
{
    public function __construct($name = 'usuario', array $options = [])
    {
        parent::__construct($name, $options);

        //$this->setAttribute("enctype", "multipart/form-data");


        $this->add([
            'name' => 'login',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Login'
            ],
            'attributes' => [
                'id' => 'login',
                'class' => 'form-control',
                'placeholder' => "Login"
            ]
        ]);

        $this->add([
            'name' => 'senha',
            'type' => Element\Password::class,
            'options' => [
                'label' => 'Senha'
            ],
            'attributes' => [
                'id' => 'senha',
                'class' => 'form-control',
                'placeholder' => "Senha"
            ]
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Element\Button::class,
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary'
            ]
        ]);
    }
}