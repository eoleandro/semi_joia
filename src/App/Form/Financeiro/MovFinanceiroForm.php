<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/17
 * Time: 10:16
 */

namespace App\Form\Financeiro;


use Zend\Form\Form;
use Zend\Form\Element;

class MovFinanceiroForm extends Form
{
    public function __construct($name = 'movfinanceira', array $options = [])
    {
        parent::__construct($name, $options);


        $this->add([
            'name' => 'id',
            'type' => Element\Hidden::class
        ]);

        $this->add([
            'name' => 'descricao',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Decrição'
            ],
            'attributes' => [
                'id' => 'descricao',
                'class' => 'form-control'
            ]
        ]);


        $this->add([
            'name' => 'formaPgto',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Forma de Pagamento'
            ],
            'attributes' => [
                'id' => 'formaPgto',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'planoConta',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Plano de Contas'
            ],
            'attributes' => [
                'id' => 'planoConta',
                'class' => 'form-control'
            ]
        ]);


        $this->add([
            'name' => 'valor',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Valor'
            ],
            'attributes' => [
                'id' => 'valor',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'natureza',
            'type' => Element\Radio::class,
            'options' => [
                'label' => 'Natureza',
                'value_options' => array(
                    'D' => 'Despesa',
                    'R' => 'Receita',
                    'S' => 'Retirada'
                ),
            ],
            'attributes' => [
                'id' => 'natureza',
                'class' => ''
            ]
        ]);

        $this->add([
            'name' => 'dataVencimento',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Data de Vencimento'
            ],
            'attributes' => [
                'id' => 'dataVencimento',
                'class' => 'form-control data'
            ]
        ]);

        $this->add([
            'name' => 'dataQuitacao',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Data de Quitação'
            ],
            'attributes' => [
                'id' => 'dataQuitacao',
                'class' => 'form-control data   '
            ]
        ]);

        $this->add([
            'name' => 'observacao',
            'type' => Element\Textarea::class,
            'options' => [
                'label' => 'Observação'
            ],
            'attributes' => [
                'id' => 'observacao',
                'class' => 'form-control'
            ]
        ]);


        $this->add([
            'name' => 'submit',
            'type' => Element\Button::class,
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
                'id' => 'gravar'
            ]
        ]);
    }

}