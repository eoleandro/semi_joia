<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/17
 * Time: 11:08
 */

namespace App\Form\Financeiro;

use Zend\Form\Form;
use Zend\Form\Element;

class FormaPgtoForm extends Form
{
    public function __construct($name = 'formapgto', array $options = [])
    {
        parent::__construct($name, $options);


        $this->add([
            'name' => 'id',
            'type' => Element\Hidden::class
        ]);

        $this->add([
            'name' => 'descricao',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Decrição'
            ],
            'attributes' => [
                'id' => 'descricao',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'ativo',
            'type' => Element\Radio::class,
            'options' => [
                'label' => 'Ativa?',
                'value_options' => array(
                    '0' => 'Não',
                    '1' => 'Sim'
                ),
            ],
            'attributes' => [
                'id' => 'ativo',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Element\Button::class,
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
                //'label' => 'Salvar'
            ]
        ]);
    }

}