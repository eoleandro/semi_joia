<?php

namespace App\Form\Financeiro;

use Zend\Form\Form;
use Zend\Form\Element;

class ContasQuitarForm extends Form
{
    public function __construct($name = 'contaquitarform', array $options = [])
    {
        parent::__construct($name, $options);

        $this->add([
            'name' => 'descricao',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Decrição'
            ],
            'attributes' => [
                'id' => 'descricao',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'dataVencimentoDe',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Data Venc. De'
            ],
            'attributes' => [
                'id' => 'dataVencimentoDe',
                'class' => 'form-control data'
            ]
        ]);

        $this->add([
            'name' => 'dataVencimentoAte',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Data Venc. Até'
            ],
            'attributes' => [
                'id' => 'dataVencimentoAte',
                'class' => 'form-control data'
            ]
        ]);

        $this->add([
            'name' => 'valor',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Valor'
            ],
            'attributes' => [
                'id' => 'valor',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'natureza',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Natureza',
                'value_options' => array(
                    '' => 'Selecione',
                    'D' => "Despesa",
                    'R' => "Receita",
                    'S' => "Retirada"
                ),
            ],
            'attributes' => [
                'id' => 'status',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'formaPgto',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Forma de Pagamento',
            ],
            'attributes' => [
                'id' => 'formaPgto',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'planoConta',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Plano de Contas',
            ],
            'attributes' => [
                'id' => 'planoConta',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'status',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Situação',
                'value_options' => array(
                    '' => 'Selecione',
                    'A' => "Em aberto",
                    'F' => "Quitada",
                ),
            ],
            'attributes' => [
                'id' => 'status',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Element\Button::class,
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
                //'label' => 'Salvar'
            ]
        ]);
    }

}