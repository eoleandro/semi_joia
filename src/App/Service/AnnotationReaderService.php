<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 19/02/17
 * Time: 07:23
 */

namespace App\Service;

use Doctrine\Common\Annotations\AnnotationReader;

class AnnotationReaderService extends AnnotationReader
{

    public function __construct()
    {
        parent::__construct();
        return $this;
    }


}