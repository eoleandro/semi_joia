<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 20/02/17
 * Time: 08:26
 */

namespace App\Service;


class UploadFileService
{
    private $fileInfo;

    private $pathFileUpload;

    private $fileNameUploaded;

    public function __construct( )
    {
       $this->fileInfo = $_FILES;
    }

    public function UploadFile($fileName)
    {
        $fileNameClient = $this->fileInfo[$fileName]['name'];
        $pathUpload = APPLICATION_PATH . "/img/usuario/";
        $fileNameUploaded = md5(uniqid(microtime() . $fileNameClient)) . "." . end(explode('.', $fileNameClient));
        $this->setFileNameUploaded($fileNameUploaded);

        $pathFileUpload = $pathUpload . $fileNameUploaded;
        $this->setPathFileUpload($pathFileUpload);

        if( move_uploaded_file($this->fileInfo[$fileName]['tmp_name'], $pathFileUpload)){
            return $fileNameUploaded;
        }else{
            return null;
        }
    }

    /**
     * @return mixed
     */
    public function getFileInfo()
    {
        return $this->fileInfo;
    }

    /**
     * @param mixed $fileInfo
     * @return UploadFileService
     */
    public function setFileInfo($fileInfo)
    {
        $this->fileInfo = $fileInfo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPathFileUpload()
    {
        return $this->pathFileUpload;
    }

    /**
     * @param mixed $pathFileUpload
     * @return UploadFileService
     */
    public function setPathFileUpload($pathFileUpload)
    {
        $this->pathFileUpload = $pathFileUpload;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileNameUploaded()
    {
        return $this->fileNameUploaded;
    }

    /**
     * @param mixed $fileNameUploaded
     * @return UploadFileService
     */
    public function setFileNameUploaded($fileNameUploaded)
    {
        $this->fileNameUploaded = $fileNameUploaded;
        return $this;
    }




}