<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 16/02/17
 * Time: 16:09
 */

namespace App\Service;



use App\Entity\Grupo;
use App\Entity\PermissaoAcl;
use App\Entity\RecursoSistema;
use App\Repository\Usuario\PermissaoAclRepository;
use Doctrine\ORM\EntityManager;

class RecursoSistemaService
{
    private $routes;
    private $repositoryPermissaoAcl;
    private $entityManager;

    public function __construct(array $routes,
                                PermissaoAclRepository $repositoryPermissaoAcl,
                                EntityManager $entityManager
                                )
    {
        $this->routes = $routes;
        $this->repositoryPermissaoAcl = $repositoryPermissaoAcl;
        $this->entityManager = $entityManager;

    }

    public function getIdUrlsRecursosSistemaPermissoesGrupo($idGrupo)
    {
        $permissoesGrupo = $this->repositoryPermissaoAcl->getPermissionGroup($idGrupo);
        $urlsPermissoes = [];
        foreach ($permissoesGrupo as $item){
            $urlsPermissoes[$item->getRecursoSistema()->getId()] = $item->getRecursoSistema()->getUrl();
        }
        return $urlsPermissoes;
    }

    public function carregarPermissoesGrupo($idGroup){
        $rotasSistema = $this->routes;


        $urlsPermissoes = $this->getIdUrlsRecursosSistemaPermissoesGrupo($idGroup);
        $recursosSistema = $this->repositoryPermissaoAcl->getRecursosSistema();


        foreach($rotasSistema as $rota){

            $infoPermissao =  ['nome_rota' => $rota['name'], 'uri' => $rota['path'], 'id_recurso_sistema' => null];
            if( in_array($rota['name'], $recursosSistema)){
                $infoPermissao['id_recurso_sistema'] = array_search($rota['name'], $recursosSistema);
            }
            if(in_array($rota['name'], $urlsPermissoes)){

                $infoPermissao['permissao'] = 'allow';
            }else{
                $infoPermissao['permissao'] = 'deny';
            }
            $permissoesGrupoRotas[]  = $infoPermissao;
        }

        return $permissoesGrupoRotas;
    }

    public function gravarPermissoesGrupo($idGroup, $data,  RecursoSistema $entityRecursoSistema, PermissaoAcl $entityPermissaoAcl ){
        $recursosExistentes = $this->getIdRecursoSistemaExistentes($data);
        $recursosNovos      = $this->getIdRecursoSistemaNovos($data);

        /* 1 remover recusos que foram desautorizados */
        $this->desautorizaRecursosGrupo($idGroup, $recursosExistentes);
        /* 2 adicionar permissao para recursos existentes */
        $result = $this->adicionarPermissaoRecursosExitentes($idGroup, $recursosExistentes, $entityPermissaoAcl);
        /* 3  adicionar permissao para novos recursos*/
        $result = $this->adicionarPermissaoRecursosNovos($idGroup, $recursosNovos,  $entityRecursoSistema, $entityPermissaoAcl);

    }

    private function getIdRecursoSistemaExistentes($data){
        $recursosExistentes = array_filter($data['permissoes_acl'], function( $valor ){
            return is_numeric($valor);
        });
        return $recursosExistentes;
    }

    private function getIdRecursoSistemaNovos($data){
        $recursosNovos = array_filter($data['permissoes_acl'], function( $valor ){
            return (!is_numeric($valor));
        }, ARRAY_FILTER_USE_BOTH);
        return $recursosNovos;
    }

    public function desautorizaRecursosGrupo($idGrupo, $recursosDesautorizar = array())
    {
        $arrRecursosDesautorizar = $this->repositoryPermissaoAcl->getPermissooesGrupoDesautorizar($idGrupo, $recursosDesautorizar);
        foreach($arrRecursosDesautorizar as $recursoDesautorizar){
            $this->entityManager->remove($recursoDesautorizar);
            $this->entityManager->flush();

        }
    }

    public function adicionarPermissaoRecursosExitentes($idGrupo, $recursosAutorizados = array(), PermissaoAcl $entityPermissaoAcl)
    {
        $recursosGrupo = array_keys( $this->getIdUrlsRecursosSistemaPermissoesGrupo($idGrupo) );

        $recursosAutorizados = array_combine( array_values($recursosAutorizados), array_values($recursosAutorizados));
        $recursosExistenteAdicionar = array_diff($recursosAutorizados, $recursosGrupo);

        if(!empty($recursosExistenteAdicionar)){
            $novosRecursosGrupo = array();
            $grupo = $this->entityManager->getReference(Grupo::class, $idGrupo);
            foreach($recursosExistenteAdicionar as $recurso){
                $entityPermissaoAclPersist =  clone $entityPermissaoAcl;

                $entityPermissaoAclPersist->setGrupo($grupo)
                    ->setRecursoSistema( $this->entityManager->getReference(RecursoSistema::class, $recurso) )
                    ->setPermissao('allow');

                 $this->entityManager->persist($entityPermissaoAclPersist);
                $this->entityManager->flush();
                $novosRecursosGrupo[] = $entityPermissaoAclPersist;
                unset($entityPermissaoAclPersist);
            }
            return $novosRecursosGrupo;
        }

        return false;

    }

    public function adicionarPermissaoRecursosNovos($idGrupo, $recursosNovos = array(), RecursoSistema $entityRecursoSistema, PermissaoAcl $entityPermissaoAcl)
    {

        if(!empty($recursosNovos)){
            $grupo = $this->entityManager->getReference(Grupo::class, $idGrupo);
            $novosRecursosGrupo = array();
            foreach ($recursosNovos as $recurso){
                $repository = $this->entityManager->getRepository(RecursoSistema::class);
                $recursoSistemaFind = $repository->findOneByUrl($recurso);

                if(!$recursoSistemaFind){
                    $entityRecurosSistemaPersist = clone $entityRecursoSistema;

                    $entityRecurosSistemaPersist->setUrl($recurso);

                    $entityPermissaoAclPersist =  clone $entityPermissaoAcl;
                    $entityPermissaoAclPersist->setGrupo($grupo)
                        ->setRecursoSistema($entityRecurosSistemaPersist)
                        ->setPermissao('allow');

                    $this->entityManager->persist($entityPermissaoAclPersist);
                    $this->entityManager->flush();
                    $novosRecursosGrupo[] = $entityPermissaoAclPersist;
                    unset($entityRecurosSistemaPersist, $entityPermissaoAclPersist);

                }

            }
            return $novosRecursosGrupo;
        }
        return false;
    }


}