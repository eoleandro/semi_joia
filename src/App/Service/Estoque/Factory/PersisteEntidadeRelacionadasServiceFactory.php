<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 22/02/17
 * Time: 10:18
 */

namespace App\Service\Estoque\Factory;

use App\Service\Estoque\PersisteEntidadeRelacionadasService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class PersisteEntidadeRelacionadasServiceFactory
{

    public function __invoke(ContainerInterface $container)
    {
        $entityManager = $container->get(EntityManager::class);
        return new PersisteEntidadeRelacionadasService($entityManager);
    }

}