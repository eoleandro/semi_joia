<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 16/02/17
 * Time: 16:08
 */

namespace App\Service\Factory;


use App\Entity\PermissaoAcl;
use App\Service\RecursoSistemaService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class RecursoSistemaServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        /**
         * @var \Doctrine\ORM\EntityManager $entityManager
         */
        $entityManager = $container->get(EntityManager::class);
        $repository = $entityManager->getRepository(PermissaoAcl::class);

        return new RecursoSistemaService($config['routes'], $repository, $entityManager );
    }
}