<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 19/02/17
 * Time: 07:31
 */

namespace App\Service;


use Doctrine\ORM\EntityManager;

class PersistEntityWithRelationshipsService
{
    private $entityManager;

    private $annotationReader;

    public function __construct(AnnotationReaderService $annotationReaderService, EntityManager $entityManager)
    {
        $this->annotationReader = $annotationReaderService;
        $this->entityManager = $entityManager;
    }

    public function treatRefenceDataArrayEntity($data , $namespaceEntity)
    {
        $docReader = $this->annotationReader;

        $reflect = new \ReflectionClass($namespaceEntity);
        if(count($reflect->getProperties() ) == 0){
            $namespaceEntity = get_parent_class($namespaceEntity);
            $reflect = new \ReflectionClass($namespaceEntity);
        }

        foreach($reflect->getProperties() as $informacaoPropertie){
            $docInfos = $docReader->getPropertyAnnotations($reflect->getProperty($informacaoPropertie->name));
            if(!empty($docInfos[0]->targetEntity) && !empty($data[$informacaoPropertie->name])){
                $data[$informacaoPropertie->name] = $this->entityManager->getReference($docInfos[0]->targetEntity, $data[$informacaoPropertie->name]);
            }
        }
        return $data;
    }

}