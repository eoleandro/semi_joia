<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 04/03/17
 * Time: 22:43
 */

namespace App\Service\Financeiro\Factory;


use App\Entity\MovFinanceiro;
use App\Entity\MovFinPedido;
use App\Service\Financeiro\ParcelaPedidoService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class ParcelaPedidoServiceFactory
{

    public function __invoke(ContainerInterface $container)
    {
        $entityManager = $container->get(EntityManager::class);
        $movFinanceiro = new MovFinanceiro();
        $movFinPedido = new MovFinPedido();
        return new ParcelaPedidoService($entityManager, $movFinanceiro, $movFinPedido);
    }

}