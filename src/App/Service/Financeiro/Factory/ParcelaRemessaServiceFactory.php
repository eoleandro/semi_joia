<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10/03/17
 * Time: 12:06
 */

namespace App\Service\Financeiro\Factory;

use App\Entity\MovFinanceiro;
use App\Entity\MovFinRemessa;
use App\Service\Financeiro\ParcelaRemessaService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class ParcelaRemessaServiceFactory
{

    public function __invoke(ContainerInterface $container)
    {
        $entityManager = $container->get(EntityManager::class);
        $movFinanceiro = new MovFinanceiro();
        $movFinRemessa = new MovFinRemessa();
        return new ParcelaRemessaService($entityManager, $movFinanceiro, $movFinRemessa);
    }

}