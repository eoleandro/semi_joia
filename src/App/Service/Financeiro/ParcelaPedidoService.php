<?php

namespace App\Service\Financeiro;

use App\Entity\ItemRemessa;
use App\Entity\MovFinanceiro;
use App\Entity\MovFinPedido;
use App\Entity\FormaPgto;
use App\Entity\Pedido;
use App\Entity\PlanoConta;
use App\Entity\RemessaFornecedor;
use Doctrine\ORM\EntityManager;
use Zend\Form\Element\DateTime;
use Zend\Http\Header\Date;
use Zend\Hydrator\ClassMethods;

class ParcelaPedidoService
{
    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;
    private $movFinPedido;
    private $movFinanceiro;

    public function __construct(EntityManager $entityManager, MovFinanceiro $movFinanceiro, MovFinPedido $movFinPedido)
    {
        $this->entityManager = $entityManager;
        $this->movFinPedido = $movFinPedido;
        $this->movFinanceiro = $movFinanceiro;
    }

    public function gravarDadosParcelas
    (
        Pedido $pedido,
        ClassMethods $hydrator,
        array $data,
        $infoMovFinParcelas = array()
    )
    {
        $this->removeParcelasDesconsiderar($infoMovFinParcelas);
        $pedido->setStatus("Pendente Entrega");
        foreach($data['idMovPedido'] as $indice => $idMovPedido){
            if(!empty($data['formasPgto'][$indice])){
                $dataMovFin = $this->getDataParcela($pedido, $data, $indice);


                if(!empty($idMovPedido)){
                    $this->atualizarParcela($dataMovFin, $idMovPedido, $hydrator);
                }else{
                    $dataMovFin['pedido'] = $pedido;
                    $this->inserirParcela($dataMovFin, $hydrator);
                }
            }
        }

    }

    public function getDataParcela($pedido,$data, $indice)
    {
        $formaPgto = $this->entityManager->getReference(FormaPgto::class, $data['formasPgto'][$indice]);
        $planoConta = $this->entityManager->getReference(PlanoConta::class, 1);
        $dataMovFin = ['formaPgto' => $formaPgto, 'planoConta' =>$planoConta,
            'valor' => number_format($data['valores'][$indice], 2, '.' , ',') ,
            'dataVencimento' => $data['datasVencimento'][$indice]];
        $dataMovFin['dataVencimento'] = (!empty($dataMovFin['dataVencimento']))?
            \DateTime::createFromFormat("d/m/Y", $dataMovFin['dataVencimento']) : null;
        $dataMovFin['dataCadastro'] = new \DateTime('now');
        $dataMovFin['natureza'] = "R";
        $dataMovFin['tipo'] = "C";
        $dataMovFin['descricao'] = "Parcela $indice referente ao pedido: " . $pedido->getId();
        $dataMovFin['status'] = "A";
        return $dataMovFin;
    }

    public function removeParcelasDesconsiderar(array $infoMovFinParcelas)
    {
        foreach ($infoMovFinParcelas as $infoMovFin){
                $idMovFin = $infoMovFin->getMovFinanceiro()->getId();
                $itemMovFinPedidoRemover = $this->entityManager->getReference(MovFinPedido::class, $infoMovFin->getId());
                $this->entityManager->remove($itemMovFinPedidoRemover);
                $this->entityManager->flush();
                $itemMovFinRemover = $this->entityManager->getReference(MovFinanceiro::class, $idMovFin);
                $this->entityManager->remove($itemMovFinRemover);
                $this->entityManager->flush();
        }
    }

    public function inserirParcela($dataMovFin, ClassMethods $hydrator)
    {
        $iteMovPedido = clone $this->movFinPedido;
        $iteMovFinanceiro = clone $this->movFinanceiro;
        $hydrator->hydrate($dataMovFin, $iteMovPedido);
        $hydrator->hydrate($dataMovFin, $iteMovFinanceiro);
        $iteMovPedido->setMovFinanceiro($iteMovFinanceiro);
        $this->entityManager->persist($iteMovPedido);
        $this->entityManager->flush();
    }

    public function atualizarParcela($dataMovFin, $idMovPedido, ClassMethods $hydrator)
    {
        $movFinPedido = $this->entityManager->getReference(MovFinPedido::class, $idMovPedido);
        $movFinanceiro = $movFinPedido->getMovFinanceiro();
        $hydrator->hydrate($dataMovFin, $movFinanceiro);
        $movFinPedido->setMovFinanceiro($movFinanceiro);
        $this->entityManager->persist($movFinPedido);
        $this->entityManager->flush();
    }

    public function recalcularParcelasDevolucaoPedido($infoPedido, $valorDevolver, array $parcelasPedido )
    {
        $totalParcelas = count($parcelasPedido);
        $valorDevolverParcela  = round($valorDevolver  / $totalParcelas, 2, PHP_ROUND_HALF_DOWN);
        $totalDevolvidoParcela = 0;
        $valorTotalParcelas = $this->getTotalParcelas($parcelasPedido);
        $movFinanceiroReembolso = false;
        if($valorTotalParcelas < $valorDevolver){
            $valorReembolso =  $valorDevolver - $valorTotalParcelas;
            $valorDevolver =  $valorTotalParcelas;
            $movFinanceiroReembolso = $this->geraParcelaReembolsoDevolucao($infoPedido, $valorReembolso);
        }

        foreach($parcelasPedido as $indice => $parcela){
            $movFinPedido = $this->entityManager->getReference(MovFinPedido::class, $parcela->getId());
            $movFinanceiro = $movFinPedido->getMovFinanceiro();

           if($indice == $totalParcelas - 1){
                $movFinanceiro->setValor( $movFinanceiro->getValor() - ($valorDevolver - $totalDevolvidoParcela));
            }else{
                $movFinanceiro->setValor( $movFinanceiro->getValor() - $valorDevolverParcela);
            }

            $movFinPedido->setMovFinanceiro($movFinanceiro);
            $totalDevolvidoParcela += $valorDevolverParcela;
            $this->entityManager->persist($movFinPedido);
            $this->entityManager->flush();
        }
        return $movFinanceiroReembolso;

    }

    public function getTotalParcelas(array $parcelasPedido)
    {
        $valorTotal = 0;
        foreach($parcelasPedido as  $parcela){
            $valorTotal += $parcela->getMovFinanceiro()->getValor();
        }
        return $valorTotal;
    }

    public function geraParcelaReembolsoDevolucao($infoPedido, $valorReembolso)
    {
        $movFinanceiro = new MovFinanceiro();
        $movFinPedido = new MovFinPedido();
        $formaPgto = $this->entityManager->getReference(FormaPgto::class, 1);
        $planoConta = $this->entityManager->getReference(PlanoConta::class, 2);
        $movFinanceiro->setValor($valorReembolso)
            ->setDataCadastro(new \DateTime('now'))
            ->setDataVencimento(new \DateTime('now'))
            ->setPlanoConta($planoConta)
            ->setFormaPgto($formaPgto)
            ->setNatureza("D")
            ->setTipo("D")
            ->setDescricao("Reembolso de devolução de items do pedido: ", $infoPedido->getId())
            ->setStatus("A");
        $movFinPedido->setMovFinanceiro($movFinanceiro);
        $movFinPedido->setPedido($infoPedido);
            $this->entityManager->persist($movFinPedido);
            $this->entityManager->flush();
    }

    public function quitarParcelas(Pedido $pedido, $data)
    {
        $atualizado = false;
        //$valorParcial = $pedido->getValorTotal() - $pedido->getDesconto();
        $valorParcial = $pedido->getValorTotal();
        foreach($data['idMovFinanceiro'] as $indice => $idMovFinanceiro){

            $movFinanceiro = $this->entityManager->getReference(MovFinanceiro::class, $idMovFinanceiro);

            if( !empty($data['dataQuitacao'][$indice]) ){
                $dataQuitacao = (empty($data['dataQuitacao'][$indice]))?$movFinanceiro->getDataQuitacao() :
                    \DateTime::createFromFormat('d/m/Y', $data['dataQuitacao'][$indice]);
                $movFinanceiro->setDataQuitacao($dataQuitacao);
                $movFinanceiro->setStatus("F");
            }else{
                $movFinanceiro->setStatus("A");
                $movFinanceiro->setDataQuitacao(null);
                $valorParcial -= $movFinanceiro->getValor();

            }
            $this->entityManager->persist($movFinanceiro);

        }
        $pedido->setValorParcial($valorParcial);
        $pedido->setStatus("Parcelas em Quitação");
        $this->entityManager->persist($pedido);
        $this->entityManager->flush();
        //die;

    }
}