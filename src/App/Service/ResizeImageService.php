<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 20/02/17
 * Time: 10:03
 */

namespace App\Service;

use claviska\SimpleImage;

class ResizeImageService extends SimpleImage
{
    public function __construct()
    {
        parent::__construct();
    }

    public function resizeImage($imageName, $fullPathImage, $prefix, $width, $height)
    {
        if(!is_null($imageName)){
            $extension = strtolower(end(explode('.', $imageName)));
            $mimeTypes = ['jpeg' => 'image/jpeg', 'jpg' => 'image/jpeg', 'png' => 'image/png'];
            $newNameImage = str_replace($imageName, $prefix.$imageName, $fullPathImage);
            $image = $this;
            $image->fromFile($fullPathImage)
                //->autoOrient()
                ->resize($width, $height)
                //->flip('x')
                ->toFile($newNameImage, $mimeTypes[$extension]);
            //$this->toScreen();
        }
    }
}