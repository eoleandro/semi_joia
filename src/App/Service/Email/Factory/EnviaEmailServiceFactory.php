<?php

namespace App\Service\Email\Factory;

use App\Service\Email\EnviaEmailService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
class EnviaEmailServiceFactory {

    public function __invoke(ContainerInterface $container)
    {
        $entityManager = $container->get(EntityManager::class);
        $phpMailer = new \PHPMailer();
        return new EnviaEmailService($phpMailer);
    }
} 