<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/17
 * Time: 21:02
 */

namespace App\Service\Filter;


class FormataDataService {
    public function converteDataFormatoEUA($data, $camposData)
    {
        foreach($camposData as $campo){
            if(!empty($data[$campo])){
                $data[$campo] = \DateTime::createFromFormat('d/m/Y', $data[$campo])->format('Y-m-d');
            }
        }
        return $data;
    }

} 