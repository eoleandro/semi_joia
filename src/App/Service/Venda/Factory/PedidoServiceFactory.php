<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/03/17
 * Time: 22:02
 */

namespace App\Service\Venda\Factory;

use App\Service\Venda\PedidoService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;
use App\Service\Financeiro\ParcelaPedidoService;

class PedidoServiceFactory
{

    public function __invoke(ContainerInterface $container)
    {
        $entityManager = $container->get(EntityManager::class);
        $authAdapterService = $container->get(AuthenticationService::class);
        $parcelaPedidoService = $container->get(ParcelaPedidoService::class);
        return new PedidoService($entityManager, $authAdapterService, $parcelaPedidoService);
    }

}