<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 08/03/17
 * Time: 10:50
 */

namespace App\Service\Venda\Factory;

use App\Service\Venda\DevolucaoItemService;
use App\Service\Venda\PedidoService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use App\Service\Financeiro\ParcelaPedidoService;

class DevolucaoItemServiceFactory
{

    public function __invoke(ContainerInterface $container)
    {
        $entityManager = $container->get(EntityManager::class);
        $parcelaPedidoService = $container->get(ParcelaPedidoService::class);
        return new DevolucaoItemService($entityManager,  $parcelaPedidoService);
    }

}