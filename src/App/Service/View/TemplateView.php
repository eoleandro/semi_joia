<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29/03/17
 * Time: 17:41
 */

namespace App\Service\View;


class TemplateView {

    public function render($templateName, Array $variablesTemplate = array())
    {
        $this->setVarablesTemplate($variablesTemplate);
        ob_start();
        include_once $templateName;
        $contentTemplate = ob_get_contents();
        ob_clean();
        return $contentTemplate;
    }
    public function setVarablesTemplate(Array $variablesTemplate = array())
    {
        foreach($variablesTemplate as $variableName => $variableValue){
            $this->{$variableName} = $variableValue;
        }

    }

} 