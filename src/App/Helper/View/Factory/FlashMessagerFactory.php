<?php

namespace App\Helper\View\Factory;

use App\Helper\View\FlashMessager;
use Interop\Container\ContainerInterface;
class FlashMessagerFactory
{
    public function __invoke( ContainerInterface $container)
    {
        return new FlashMessager("flash");
    }

}