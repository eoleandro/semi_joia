<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10/04/17
 * Time: 15:41
 */

namespace App\Helper\View;

use Zend\Form\View\Helper\AbstractHelper;

class AbreviarNome extends  AbstractHelper
{

    public function __invoke($nome)
    {
        $nome = trim($nome);
        $infoNome = explode(' ', $nome);
        if(count($infoNome) <3){
            return $nome;
        }else{
            $nome = trim($infoNome[0])." ";
            foreach($infoNome as $indice => $parteNome){
                if($indice != 0 && $parteNome !== "")
                $nome .= ucfirst($parteNome)[0] . ". ";
            }
        }
        return $nome;
    }
}