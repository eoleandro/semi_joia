<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/04/17
 * Time: 12:57
 */

namespace App\Helper\View;

use Zend\Form\View\Helper\AbstractHelper;

class ExibeDadoRelatorioCadastro extends  AbstractHelper
{

    public function __invoke($dadoExibir, $nomeMetodo, $objetoData)
    {
        if($nomeMetodo == "getAtivo" && in_array($dadoExibir, [1, 0]) ){
            $dadoExibir = ( $dadoExibir == 1)? "Sim" : "Não";
        }
        if($nomeMetodo == "getTipo" && in_array($dadoExibir, ['A', 'F']) ){
            $dadoExibir = ( $dadoExibir == "A")? "Em Aberto" : "Quitada";
        }
        if($nomeMetodo == "getDataNascimento" && is_string($dadoExibir) ){
            $dadoExibir = \DateTime::createFromFormat('d/m/Y', $dadoExibir);
            if($dadoExibir)
            $dadoExibir = $dadoExibir->format("d/m");
        }
        if($nomeMetodo == "getDataHora" ){
            $dadoExibir = $dadoExibir->format("d/m/Y H:i");
        }
        if($nomeMetodo == "getStatus" && in_array($dadoExibir, ['M', 'R'])){
            $dadoExibir = ( $dadoExibir == "M")? "Agendada" : "Realizada";
        }
        if($nomeMetodo == "getNome" && empty($dadoExibir) ){
            $nomeMetodoAlternativo = "getNomeFantasia";
            if(method_exists($objetoData, $nomeMetodoAlternativo)){
                return $objetoData->$nomeMetodoAlternativo();
            }
        }
        if(is_null($dadoExibir) ){
            return "";
        }
        elseif($dadoExibir instanceof \DateTime){
            return $dadoExibir->format('d/m/Y');
        }
        elseif(is_object($dadoExibir) and !($dadoExibir instanceof \DateTime) ){
            return $dadoExibir->__toString();
        }
        elseif(is_float($dadoExibir)){
            return "R$ " . number_format($dadoExibir, 2, ',', '.');
        }
        elseif(is_string($dadoExibir)){
            return mb_check_encoding($dadoExibir, 'UTF-8')? $dadoExibir : utf8_encode($dadoExibir);
        }



    }
}