<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/04/17
 * Time: 15:13
 */

namespace App\Helper\View;

use Zend\Form\View\Helper\AbstractHelper;

class ExibeTituloRelatorioCadastro extends  AbstractHelper
{

    public function __invoke($nomeCadastro)
    {
        $nomesCadastro = [
                            'Agenda' => 'dos Agendamentos',
                            'Cliente' => 'dos Clientes',
                            'Fornecedor' => 'dos Fornecedores',
                            'FormaPgto' => 'das Forma de Pagamentos',
                            'Pedido' => 'dos Pedidos',
                            'PlanoConta' => 'dos Plano de Contas',
                            'Produto' => 'dos Produtos',
                            'RemessaFornecedor' => 'das Remessa de Fornecedores',
                            'Usuario' => 'dos Usuários'
                        ];

        return (!empty($nomesCadastro[$nomeCadastro]))? $nomesCadastro[$nomeCadastro] : "";



    }
}