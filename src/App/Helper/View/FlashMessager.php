<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 23/02/17
 * Time: 16:29
 */

namespace App\Helper\View;


use Zend\Form\View\Helper\AbstractHelper;

class FlashMessager extends  AbstractHelper
{
    private $flash;

    public function __construct($flash)
    {
        $this->flash = $flash;
    }

    public function __invoke($tamanho, $infoUsuario = null)
    {
        $nomeFotoUsuario = $infoUsuario['foto'];
        if( !is_null($nomeFotoUsuario) ){
            return $tamanho . "_" . $nomeFotoUsuario;
        }else{

        }
    }
}