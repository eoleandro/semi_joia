<?php

namespace App\Helper\View;

use Zend\Form\View\Helper\AbstractHelper;

class FormataNomeFoto extends  AbstractHelper
{

    public function __invoke($tamanho, $infoUsuario = null)
    {
        $nomeFotoUsuario = $infoUsuario['foto'];

        if( !empty($nomeFotoUsuario) ){

            return $tamanho . "_" . $nomeFotoUsuario;
        }else{
            return "usuario_sem_foto.png";
        }
    }
}