<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 15/02/17
 * Time: 15:51
 */

namespace App\Helper;


use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Config;
use Zend\ServiceManager\ServiceLocatorInterface;

class FormHelpersDelegatorFactory
{

    public function __invoke( ContainerInterface $container, $name, callable  $callback, array $options = null)
    {
        $helpers = $callback();

        $config = $container->has('config') ? $container->get('config') : [];
        $config = new Config($config['view_helpers']);
        $config->configureServiceManager($helpers);
        return $helpers;
    }

    public function createDelegatorWithName(
        ServiceLocatorInterface $container,
        $name,
        $requestedName,
        $callback
    ){
        return $this($container, $name, $callback);
    }

}