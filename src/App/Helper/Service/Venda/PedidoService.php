<?php

namespace App\Service\Venda;

use App\Entity\ItemPedido;
use App\Entity\ItemRemessa;
use App\Entity\MovFinPedido;
use App\Entity\Pedido;
use App\Entity\Produto;
use App\Entity\Usuario;
use App\Service\Financeiro\ParcelaPedidoService;
use Doctrine\ORM\EntityManager;
use Zend\Hydrator\ClassMethods;

class PedidoService
{
    private $entityManager;
    private $authenticationService;
    private $parcelaPedidoService;

    public function __construct(EntityManager $entityManager,
                                \Zend\Authentication\AuthenticationService $authenticationService,
    ParcelaPedidoService $parcelaPedidoService)
    {
        $this->entityManager = $entityManager;
        $this->authenticationService = $authenticationService;
        $this->parcelaPedidoService = $parcelaPedidoService;
    }

    public function gravarDadosPedido(
        Pedido $pedidoCliente,
        ItemPedido $itemPedido,
        ClassMethods $hydrator,
        array $data, $itensPedidoRemover = array())
    {
        $hydrator->hydrate($data, $pedidoCliente);
        $valorPedidoOriginal = $pedidoCliente->getValorTotal();
        $valorTotalPedido = 0;
        if(!empty($data['desconto'])){
            $valorTotalPedido = $valorTotalPedido - strtr($data['desconto'], ['.' =>'', ',' => '.']);
        }
        $dataAtual = new \DateTime('now');
        $data['produto'] = array_filter( $data['produto'], function($v, $k) { return $k != 0 || $v != "";}, ARRAY_FILTER_USE_BOTH);
        $data['quantidade'] = array_filter( $data['quantidade'], function($v, $k) { return $k != 0 || $v != "";}, ARRAY_FILTER_USE_BOTH);

        $this->removeItensPedido($itensPedidoRemover);
        $repositoryItemRemessa = $this->entityManager->getRepository(ItemRemessa::class);
        $pedidoCliente->setDataCadastro($dataAtual);
        $pedidoCliente->setStatus('Definir Parcelamento');
        if(is_numeric($pedidoCliente->getCliente())){
            $pedidoCliente->setCliente( $this->entityManager->getReference(\App\Entity\Cliente::class, $pedidoCliente->getCliente()));
        }

        $usuario = $this->entityManager->getReference(
            Usuario::class, $this->authenticationService->getIdentity()['id']);
        $pedidoCliente->setUsuario($usuario);

        foreach ($data['produto'] as $indice => $produtoId){

            if(!empty($produtoId) && $data['quantidade'][$indice] > 0) {
                $itemPedidoInserir = clone $itemPedido;
                if(!empty($data['idItensPedido'][$indice])){
                    $itemPedido = $this->devolverProdutoItemRemessa($data['idItensPedido'][$indice]);
                    $itemRemessa =  $repositoryItemRemessa->getInfoItemRemessa($produtoId);
                    $dataItemPedido = $this->getDataItem($pedidoCliente, $itemRemessa, $data, $indice);
                    $this->atualizarItemPedido($dataItemPedido, $itemPedido, $hydrator);
                }else{

                     $itemRemessa =  $repositoryItemRemessa->getInfoItemRemessa($produtoId);
                     $dataItemPedido = $this->getDataItem($pedidoCliente, $itemRemessa, $data, $indice);
                   // var_dump($dataItemPedido);
                    //die;
                     $this->inserirItemPedido($dataItemPedido, $itemPedidoInserir, $hydrator);
                }
                $itemRemessa->setQuantParcial($itemRemessa->getQuantParcial() - $dataItemPedido['quantidade'] );
                $this->entityManager->persist($itemRemessa);
                $valorTotalPedido += $dataItemPedido['valor'];
                $pedidoCliente->setValorTotal($valorTotalPedido);
            }
        }
        // Exclui as parcelas se o valor total do pedido mudar
        if($valorPedidoOriginal != 0 && $valorPedidoOriginal != $pedidoCliente->getValorTotal()){
            $repoParcelas = $this->entityManager->getRepository(MovFinPedido::class);
            $infoParcelas = $repoParcelas->getParcelasPedido($pedidoCliente->getId());
            $this->parcelaPedidoService->removeParcelasDesconsiderar($infoParcelas);
        }
        $this->entityManager->flush();
        return $pedidoCliente;
    }

    public function cancelarPedido($pedidoCliente, $itensPedidoDevolver, $infoParcelas)
    {
        $pedidoCliente->setStatus("Cancelado");
        $pedidoCliente->setDesconto(0);
        $pedidoCliente->setValorTotal(0);
        $this->parcelaPedidoService->removeParcelasDesconsiderar($infoParcelas);
        $this->devolverItensPedido($itensPedidoDevolver);
        $this->entityManager->flush();
        return $pedidoCliente;
    }

    public function devolverProdutoItemRemessa($idItemPedido)
    {
        $repositoryItemRemessa = $this->entityManager->getRepository(ItemRemessa::class);
        $itemPedido = $this->entityManager->getReference(ItemPedido::class, $idItemPedido);
        $itemRemessa =  $repositoryItemRemessa->getInfoItemRemessa($itemPedido->getProduto()->getId());
        //Primeiro deve ocorrer a devolução da quantidade do item anterior
        $itemRemessa->setQuantParcial( $itemRemessa->getQuantParcial() + $itemPedido->getQuantidade());
        $this->entityManager->persist($itemRemessa);
        $this->entityManager->flush();
        return $itemPedido;
    }

    public function getDataItem($pedido,$itemRemessa,$data, $indice)
    {
        $produto = $this->entityManager->getReference(Produto::class, $data['produto'][$indice]);

        $dataItemPedido = ['produto' => $produto, 'pedido' => $pedido, 'usuario' => $pedido->getUsuario()];
        $dataItemPedido['quantidade'] = $data['quantidade'][$indice];
        $data['quantidade'][$indice] = $this->calculaQuantidadeItemPedido( $itemRemessa, $dataItemPedido['quantidade']);
        $dataItemPedido['valor'] = ( $itemRemessa->getPrecoCliente() * $dataItemPedido['quantidade']);
        return $dataItemPedido;
    }

    public function inserirItemPedido($dataItemPedido, $itemPedidoInserir, ClassMethods $hydrator)
    {
        $hydrator->hydrate($dataItemPedido, $itemPedidoInserir);

        $this->entityManager->persist($itemPedidoInserir);
        $this->entityManager->flush();
    }

    public function atualizarItemPedido($dataItemPedido, $itemPedido, ClassMethods $hydrator)
    {
        $hydrator->hydrate($dataItemPedido, $itemPedido);
        $this->entityManager->persist($itemPedido);
        $this->entityManager->flush();
    }

    private function removeItensPedido($produtosPedido = array())
    {
        $repositoryItemRemessa = $this->entityManager->getRepository(ItemRemessa::class);
        foreach ($produtosPedido as $itemPedido){
            $itemPedidoRemover = $this->entityManager->getReference(ItemPedido::class, $itemPedido->getId());
            //Devolver a quantidade do produto do item ao estoque
            $itemRemessa = $repositoryItemRemessa->getInfoItemRemessa($itemPedidoRemover->getProduto()->getId());
            $itemRemessa->setQuantParcial( $itemRemessa->getQuantParcial() + $itemPedidoRemover->getQuantidade());
            $this->entityManager->remove($itemPedidoRemover);
            $this->entityManager->flush();
        }
    }
    private function devolverItensPedido($produtosPedido = array())
    {
        $repositoryItemRemessa = $this->entityManager->getRepository(ItemRemessa::class);
        foreach ($produtosPedido as $itemPedido){
            $itemPedidoDevolver = $this->entityManager->getReference(ItemPedido::class, $itemPedido->getId());
            //Devolver a quantidade do produto do item ao estoque
            $itemRemessa = $repositoryItemRemessa->getInfoItemRemessa($itemPedidoDevolver->getProduto()->getId());
            $itemRemessa->setQuantParcial( $itemRemessa->getQuantParcial() + $itemPedidoDevolver->getQuantidade());

            $itemPedidoDevolver->setQuantidade(0);
            $itemPedidoDevolver->setValor(0);
            $this->entityManager->persist($itemPedidoDevolver);
            $this->entityManager->flush();
        }
    }

    private function calculaQuantidadeItemPedido(ItemRemessa $itemRemessa, $quantidade)
    {
        if($itemRemessa->getQuantParcial() - $quantidade < 0){
            $quantidade = $quantidade - ($itemRemessa->getQuantParcial() - $quantidade);
        }
        return $quantidade;
    }
}