<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 08/03/17
 * Time: 10:50
 */

namespace App\Service\Venda;

use App\Entity\ItemPedido;
use App\Entity\ItemRemessa;
use App\Entity\MovFinPedido;
use App\Entity\Pedido;
use App\Entity\Produto;
use App\Entity\Usuario;
use App\Service\Financeiro\ParcelaPedidoService;
use Doctrine\ORM\EntityManager;
use Zend\Hydrator\ClassMethods;

class DevolucaoItemService {
    private $entityManager;
    private $parcelaPedidoService;

    public function __construct(EntityManager $entityManager,
                                ParcelaPedidoService $parcelaPedidoService)
    {
        $this->entityManager = $entityManager;
        $this->parcelaPedidoService = $parcelaPedidoService;
    }


    public function gravarDadosDevolucaoItens(Pedido $pedido,$data)
    {
        $valorTotalPedido = $pedido->getValorTotal();
        $repositoryItemPedido = $this->entityManager->getRepository(ItemPedido::class);
        $valorDevolver = 0;
        $repoItemRemessa = $this->entityManager->getRepository(ItemRemessa::class);
        foreach($data['idItensPedido'] as $indice => $idItemPedido){
            if(!empty($data['quantDevolver'][$indice])){
                //coletar a quantidade a devolver e devolver em estoque
                $itemPedido = $this->entityManager->getReference(ItemPedido::class, $idItemPedido);
                $quantDevolver = $data['quantDevolver'][$indice];
                $itemRemessa = $repoItemRemessa->getInfoItemRemessa($itemPedido->getProduto()->getId());
                $this->devolverItemRemessa($itemRemessa, $quantDevolver );
                $valorDevolver +=$this->devolverItemPedido($itemPedido, $quantDevolver, $pedido, $itemRemessa );
            }
        }
        return $valorDevolver;
    }

    public function devolverItemPedido(ItemPedido $itemPedido, $quantidadeDevolver, Pedido $pedido, ItemRemessa $itemRemessa)
    {

        if($itemPedido->getQuantidade() - $quantidadeDevolver >= 0){
            $valorDevolvidoItem = $quantidadeDevolver * $itemRemessa->getPrecoCliente();
            $pedido->setValorTotal( $pedido->getValorTotal() - $valorDevolvidoItem);

            if( $itemPedido->getQuantidade() - $quantidadeDevolver == 0){
                $this->entityManager->remove($itemPedido);
            }else{
                $quantItem = $itemPedido->getQuantidade() - $quantidadeDevolver;
                $valorItem = $quantItem * $itemRemessa->getPrecoCliente();
                $itemPedido->setQuantidade($quantItem)->setValor($valorItem);
                $this->entityManager->persist($itemPedido);
            }
            $this->entityManager->persist($pedido);
            $this->entityManager->flush();
            return $valorDevolvidoItem;
        }else{
            return 0;
        }

    }

    public function devolverItemRemessa($itemRemessa,  $quantidadeDevolver )
    {
        $itemRemessa->setQuantParcial( $itemRemessa->getQuantParcial() + $quantidadeDevolver );
        $this->entityManager->persist($itemRemessa);
        $this->entityManager->flush();
    }

} 