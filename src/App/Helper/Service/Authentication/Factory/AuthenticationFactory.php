<?php
/**
 * Created by PhpStorm.
 * User: erico.oliveira
 * Date: 21/08/15
 * Time: 17:08
 */

namespace App\Service\Authentication\Factory;



use App\Service\Authentication\Adapter;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session;


class AuthenticationFactory  {


    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function __invoke(ContainerInterface $container)
    {
        $entityManager = $container->get(EntityManager::class);

        $adapter = new Adapter($entityManager);
        return new AuthenticationService(new Session(),  $adapter);
    }

}