<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 05/04/17
 * Time: 23:23
 */

namespace App\Service\Email;


class EnviaEmailService {

    private $phpmailer;

    public function __construct(\PHPMailer $phpmailer){
        $this->phpmailer = $phpmailer;
        $this->configuracaoPHPMailer();
    }

    private function configuracaoPHPMailer()
    {
        $this->phpmailer->isSMTP();
        $this->phpmailer->Host= "smtp.gmail.com";
        $this->phpmailer->SMTPAuth = true;
        $this->phpmailer->Username = '*************';
        $this->phpmailer->Password = '********';
        $this->phpmailer->SMTPSecure = 'tls';
        $this->phpmailer->Port = 587;
        $this->phpmailer->CharSet = 'UTF-8';
        $this->phpmailer->isHTML(true);
        $this->phpmailer->setFrom('*************', '************');
    }

    public function enviarEmail($infoMessage)
    {
        $this->phpmailer->clearAllRecipients();
        $this->phpmailer->clearAddresses();
        $this->phpmailer->addAddress($infoMessage['destinatario']);
        $this->phpmailer->isHTML(true);
        $this->phpmailer->Subject = $infoMessage['assunto'];
        $this->phpmailer->Body = $infoMessage['mensagem'];
        //$this->phpmailer->AltBody = "Testano envio html Sistema Financeiro";
        return $this->phpmailer->send();
    }
} 