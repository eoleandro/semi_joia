<?php

namespace App\Service\Factory;

use App\Service\TesteService;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;

class TesteServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {

        return new TesteService($container->get(RouterInterface::class));
        //return new UrlHelper($container->get(RouterInterface::class));
    }
}