<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 19/02/17
 * Time: 07:32
 */

namespace App\Service\Factory;


use App\Service\AnnotationReaderService;
use App\Service\PersistEntityWithRelationshipsService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;

class PersistEntityWithRelationshipsServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $entityManager = $container->get(EntityManager::class);
        $annotationHeader = $container->get(AnnotationReaderService::class);
        return new PersistEntityWithRelationshipsService($annotationHeader,$entityManager);

        //return new TesteService($container->get(RouterInterface::class));
        //return new UrlHelper($container->get(RouterInterface::class));
    }
}