<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 19/02/17
 * Time: 08:44
 */

namespace App\Service\Factory;

use App\Service\AnnotationReaderService;
use App\Service\ReferencesDatasService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class ReferencesDataServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $entityManager = $container->get(EntityManager::class);
        $annotationHeader = $container->get(AnnotationReaderService::class);
        return new ReferencesDatasService($annotationHeader,$entityManager);

    }
}