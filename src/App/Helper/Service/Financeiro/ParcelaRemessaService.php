<?php

namespace App\Service\Financeiro;

use App\Entity\ItemRemessa;
use App\Entity\MovFinanceiro;
use App\Entity\MovFinPedido;
use App\Entity\FormaPgto;
use App\Entity\MovFinRemessa;
use App\Entity\Pedido;
use App\Entity\PlanoConta;
use App\Entity\RemessaFornecedor;
use Doctrine\ORM\EntityManager;
use Zend\Form\Element\DateTime;
use Zend\Http\Header\Date;
use Zend\Hydrator\ClassMethods;

class ParcelaRemessaService
{
    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;
    private $movFinRemessa;
    private $movFinanceiro;

    public function __construct(EntityManager $entityManager, MovFinanceiro $movFinanceiro, MovFinRemessa $movFinRemessa)
    {
        $this->entityManager = $entityManager;
        $this->movFinRemessa = $movFinRemessa;
        $this->movFinanceiro = $movFinanceiro;
    }

    public function gravarDadosParcelas
    (
        RemessaFornecedor $remessaFornecedor,
        ClassMethods $hydrator,
        array $data,
        $infoMovFinParcelas = array()
    )
    {
        $this->removeParcelasDesconsiderar($infoMovFinParcelas);

        foreach($data['idMovRemessa'] as $indice => $idMovRemessa){
            if(!empty($data['formasPgto'][$indice])){
                $dataMovFin = $this->getDataParcela($remessaFornecedor, $data, $indice);
                if(!empty($idMovRemessa)){
                    $this->atualizarParcela($dataMovFin, $idMovRemessa, $hydrator);
                }else{
                    $dataMovFin['remessaFornecedor'] = $remessaFornecedor;
                    $this->inserirParcela($dataMovFin, $hydrator);
                }
            }
        }
    }

    public function getDataParcela($remessaFornecedor,$data, $indice)
    {
        $formaPgto = $this->entityManager->getReference(FormaPgto::class, $data['formasPgto'][$indice]);
        $planoConta = $this->entityManager->getReference(PlanoConta::class, 3);
        $dataMovFin = ['formaPgto' => $formaPgto, 'planoConta' =>$planoConta,
            'valor' => $data['valores'][$indice] ,
            'dataVencimento' => $data['datasVencimento'][$indice]];
        $dataMovFin['dataVencimento'] = (!empty($dataMovFin['dataVencimento']))?
            \DateTime::createFromFormat("d/m/Y", $dataMovFin['dataVencimento']) : null;
        $dataMovFin['dataCadastro'] = new \DateTime('now');
        $dataMovFin['natureza'] = "D";
        $dataMovFin['tipo'] = "D";
        $dataMovFin['descricao'] = "Parcela $indice referente a remessa de fornecedor: " . $remessaFornecedor->getId();
        $dataMovFin['status'] = "A";
        return $dataMovFin;
    }

    public function removeParcelasDesconsiderar(array $infoMovFinParcelas)
    {
        foreach ($infoMovFinParcelas as $infoMovFin){
            $idMovFin = $infoMovFin->getMovFinanceiro()->getId();
            $itemMovFinRemessaRemover = $this->entityManager->getReference(MovFinRemessa::class, $infoMovFin->getId());
            $this->entityManager->remove($itemMovFinRemessaRemover);
            $this->entityManager->flush();
            $itemMovFinRemover = $this->entityManager->getReference(MovFinanceiro::class, $idMovFin);
            $this->entityManager->remove($itemMovFinRemover);
            $this->entityManager->flush();
        }
    }

    public function inserirParcela($dataMovFin, ClassMethods $hydrator)
    {
        $iteMovPedido = clone $this->movFinRemessa;
        $iteMovFinanceiro = clone $this->movFinanceiro;
        $hydrator->hydrate($dataMovFin, $iteMovPedido);
        $hydrator->hydrate($dataMovFin, $iteMovFinanceiro);
        $iteMovPedido->setMovFinanceiro($iteMovFinanceiro);
        $this->entityManager->persist($iteMovPedido);
        $this->entityManager->flush();
    }

    public function atualizarParcela($dataMovFin, $idMovRemessa, ClassMethods $hydrator)
    {
        $movFinRemessa = $this->entityManager->getReference(MovFinRemessa::class,  $idMovRemessa);
        $movFinanceiro = $movFinRemessa->getMovFinanceiro();
        $hydrator->hydrate($dataMovFin, $movFinanceiro);
        $movFinRemessa->setMovFinanceiro($movFinanceiro);
        $this->entityManager->persist($movFinRemessa);
        $this->entityManager->flush();
    }

    public function recalcularParcelasDevolucaoPedido($infoPedido, $valorDevolver, array $parcelasPedido )
    {
        $totalParcelas = count($parcelasPedido);
        $valorDevolverParcela  = round($valorDevolver  / $totalParcelas, 2, PHP_ROUND_HALF_DOWN);
        $totalDevolvidoParcela = 0;
        $valorTotalParcelas = $this->getTotalParcelas($parcelasPedido);
        $movFinanceiroReembolso = false;
        if($valorTotalParcelas < $valorDevolver){
            $valorReembolso =  $valorDevolver - $valorTotalParcelas;
            $valorDevolver =  $valorTotalParcelas;
            $movFinanceiroReembolso = $this->geraParcelaReembolsoDevolucao($infoPedido, $valorReembolso);
        }

        foreach($parcelasPedido as $indice => $parcela){
            $movFinPedido = $this->entityManager->getReference(MovFinPedido::class, $parcela->getId());
            $movFinanceiro = $movFinPedido->getMovFinanceiro();

            if($indice == $totalParcelas - 1){
                $movFinanceiro->setValor( $movFinanceiro->getValor() - ($valorDevolver - $totalDevolvidoParcela));
            }else{
                $movFinanceiro->setValor( $movFinanceiro->getValor() - $valorDevolverParcela);
            }

            $movFinPedido->setMovFinanceiro($movFinanceiro);
            $totalDevolvidoParcela += $valorDevolverParcela;
            $this->entityManager->persist($movFinPedido);
            $this->entityManager->flush();
        }
        return $movFinanceiroReembolso;

    }

    public function getTotalParcelas(array $parcelasPedido)
    {
        $valorTotal = 0;
        foreach($parcelasPedido as  $parcela){
            $valorTotal += $parcela->getMovFinanceiro()->getValor();
        }
        return $valorTotal;
    }

    public function geraParcelaReembolsoDevolucao($infoPedido, $valorReembolso)
    {
        $movFinanceiro = new MovFinanceiro();
        $movFinPedido = new MovFinPedido();
        $formaPgto = $this->entityManager->getReference(FormaPgto::class, 1);
        $planoConta = $this->entityManager->getReference(PlanoConta::class, 2);
        $movFinanceiro->setValor($valorReembolso)
            ->setDataCadastro(new \DateTime('now'))
            ->setDataVencimento(new \DateTime('now'))
            ->setPlanoConta($planoConta)
            ->setFormaPgto($formaPgto)
            ->setNatureza("D")
            ->setTipo("D")
            ->setDescricao("Reembolso de devolução de items do pedido: ", $infoPedido->getId())
            ->setStatus("A");
        $movFinPedido->setMovFinanceiro($movFinanceiro);
        $movFinPedido->setPedido($infoPedido);
        $this->entityManager->persist($movFinPedido);
        $this->entityManager->flush();
    }

    public function quitarParcelas(RemessaFornecedor $pedido, $data)
    {
        $atualizado = false;
        $valorParcial = $pedido->getValorTotal();
        foreach($data['idMovFinanceiro'] as $indice => $idMovFinanceiro){

            $movFinanceiro = $this->entityManager->getReference(MovFinanceiro::class, $idMovFinanceiro);

            if( !empty($data['dataQuitacao'][$indice]) ){
                $dataQuitacao = (empty($data['dataQuitacao'][$indice]))?$movFinanceiro->getDataQuitacao() :
                    \DateTime::createFromFormat('d/m/Y', $data['dataQuitacao'][$indice]);
                $movFinanceiro->setDataQuitacao($dataQuitacao);
                $movFinanceiro->setStatus("F");
            }else{
                $movFinanceiro->setStatus("A");
                $movFinanceiro->setDataQuitacao(null);
                $valorParcial -= $movFinanceiro->getValor();

            }
            $this->entityManager->persist($movFinanceiro);

        }

        $pedido->setValorParcial($valorParcial);
        $this->entityManager->persist($pedido);
        $this->entityManager->flush();
    }

    public function removerParcelasRemessa($idRemessa)
    {

    }
}