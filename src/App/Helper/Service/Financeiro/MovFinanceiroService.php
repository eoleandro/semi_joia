<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15/03/17
 * Time: 00:22
 */

namespace App\Service\Financeiro;

use App\Entity\MovFinPedido;
use App\Entity\MovFinRemessa;
use Doctrine\ORM\EntityManager;
use App\Entity\MovFinanceiro;
use App\Entity\RemessaFornecedor;
use App\Entity\Pedido;

class MovFinanceiroService {

    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function gravarMovFinanceiro(MovFinanceiro $movFinanceiro, MovFinanceiro $movFinanceiroSemAlteracao )
    {
        $idMovFinanceiro = $movFinanceiro->getId();
        $movFinanceiro->setDataVencimento(\DateTime::createFromFormat('d/m/Y',$movFinanceiro->getDataVencimento() ));
        $movFinanceiro->setDataQuitacao(!empty($movFinanceiro->getDataQuitacao() )? \DateTime::createFromFormat('d/m/Y',$movFinanceiro->getDataQuitacao() ) : null);
        if(is_null($movFinanceiro->getDataQuitacao()) && $movFinanceiro->getNatureza() == 'S'){
            $movFinanceiro->setDataQuitacao(new \DateTime('now'));
        }

        $movFinanceiro->setDataCadastro(new \DateTime('now'));
        $movFinanceiro->setValor(strtr($movFinanceiro->getValor(), ['.' => '', ',' => '.']));

        (!is_null($movFinanceiro->getDataQuitacao()) )? $movFinanceiro->setStatus("F") : $movFinanceiro->setStatus("A") ;
        ($movFinanceiro->getNatureza() == 'R') ? $movFinanceiro->setTipo("C") : $movFinanceiro->setTipo("D");

        $infoMovFinRelationship = $this->getDataRelationShipMovFinanceiro($idMovFinanceiro);
        if(!empty($infoMovFinRelationship)  ){
            $movFinanceiro->setValor( $movFinanceiroSemAlteracao->getValor());
            $this->alterarQuitacaoRelacionamentoMovFinanceira($infoMovFinRelationship,$movFinanceiro, $movFinanceiroSemAlteracao);
        }
        $this->entityManager->persist($movFinanceiro);
        $this->entityManager->flush();
        return $movFinanceiro;
    }

    public function removeDataMovFinanceiro(MovFinanceiro $movFinanceiro, array $infoMovFinPedido, array $infoMovFinRemessa)
    {
        $this->removeInfoRelatedMovFinanceiro($infoMovFinPedido);
        $this->removeInfoRelatedMovFinanceiro($infoMovFinRemessa);
        $this->entityManager->remove($movFinanceiro);
        $this->entityManager->flush();
    }

    public function removeInfoRelatedMovFinanceiro($infoMovFinRelationship)
    {

        foreach($infoMovFinRelationship as $infoMovFinRelated){
            $classEntityRelationship = get_class($infoMovFinRelated);
            if($infoMovFinRelated instanceof \App\Entity\MovFinRemessa){
                $remessa = $this->entityManager->getReference( RemessaFornecedor::class, $infoMovFinRelated
                    ->getRemessaFornecedor()->getId());
                if( $remessa->getValorParcial() != null &&  $remessa->getValorParcial() > 0){
                    $remessa->setValorParcial( $remessa->getValorParcial() - $infoMovFinRelated->getMovFinanceiro()->getValor());
                }
                $this->entityManager->persist($remessa);
            }else{
                $pedido = $this->entityManager->getReference( Pedido::class, $infoMovFinRelated
                    ->getPedido()->getId());
                if( $pedido->getValorParcial() != null &&  $pedido->getValorParcial() > 0){
                    $pedido->setValorParcial( $pedido->getValorParcial() - $infoMovFinRelated->getMovFinanceiro()->getValor());
                }
                $this->entityManager->persist($pedido);
            }
            $infoMovFinRelated = $this->entityManager->getReference($classEntityRelationship, $infoMovFinRelated->getId());
            $this->entityManager->remove($infoMovFinRelated);
        }
    }

    public function alterarQuitacaoRelacionamentoMovFinanceira($infoMovFinRelationship, MovFinanceiro $movFinanceiro, MovFinanceiro $movFinanceiroSemAlteracao)
    {
        foreach($infoMovFinRelationship as $infoMovFinRelated){

            if( $movFinanceiro->getStatus() != $movFinanceiroSemAlteracao->getStatus()){

                if($infoMovFinRelated instanceof \App\Entity\MovFinRemessa){

                    $remessa = $this->entityManager->getReference( RemessaFornecedor::class, $infoMovFinRelated
                        ->getRemessaFornecedor()->getId());

                    if($movFinanceiro->getStatus() == "A"){
                        $remessa->setValorParcial( $remessa->getValorParcial() - $infoMovFinRelated->getMovFinanceiro()->getValor());
                    }else{
                        $remessa->setValorParcial( $remessa->getValorParcial() + $infoMovFinRelated->getMovFinanceiro()->getValor());
                    }
                    $this->entityManager->persist($remessa);
                }else{

                    $pedido = $this->entityManager->getReference( Pedido::class, $infoMovFinRelated
                        ->getPedido()->getId());

                    if($movFinanceiro->getStatus() == "A"){
                    $pedido->setValorParcial( $pedido->getValorParcial() - $infoMovFinRelated->getMovFinanceiro()->getValor());
                    }else{
                        $pedido->setValorParcial( $pedido->getValorParcial() + $infoMovFinRelated->getMovFinanceiro()->getValor());

                    }
                    $this->entityManager->persist($pedido);
                }
            }
        }
    }

    public function getDataRelationShipMovFinanceiro($idMovFinanceiro)
    {
        $repoMovFinPedido  = $this->entityManager->getRepository(MovFinPedido::class);
        $repoMovFinRemessa = $this->entityManager->getRepository(MovFinRemessa::class);
        $infoMovFinRelationship  = $repoMovFinPedido->getInfoRelacionadaMovFinanceiro($idMovFinanceiro);
        if(empty($infoMovFinRelationship)){
            $infoMovFinRelationship = $repoMovFinRemessa->getInfoRelacionadaMovFinanceiro($idMovFinanceiro);
        }
        return $infoMovFinRelationship;
    }

} 