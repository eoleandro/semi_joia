<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15/03/17
 * Time: 00:22
 */

namespace App\Service\Financeiro\Factory;



use App\Service\Financeiro\MovFinanceiroService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class MovFinanceiroServiceFactory {

    public function __invoke(ContainerInterface $container)
    {
        $entityManager = $container->get(EntityManager::class);

        return new MovFinanceiroService($entityManager);
    }
} 