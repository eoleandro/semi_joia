<?php

namespace App\Service\Estoque;

use App\Entity\Fornecedor;
use Doctrine\ORM\EntityManager;
use Zend\Hydrator\ClassMethods;

class PersisteEntidadeRelacionadasService
{
    private $entityManager;
    private $entityMain;

    public function __construct( EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function updateDataEntitiesRelated(array $data, array $arrEntityRelated, array $idEntity, ClassMethods $hidratory)
    {

        foreach ($arrEntityRelated as $indice => $entity){
            $entity = $this->entityManager->getReference($entity, $idEntity[$indice]);
            $entityMainName =  end( explode("\\", get_class($this->entityMain)));
            $methodNameSetEntityMain = "set".$entityMainName;
            if($entity){
                $hidratory->hydrate($data, $entity);
                $entity->{$methodNameSetEntityMain}($this->entityMain);
                $this->entityManager->persist($entity);
            }
        }
        $this->entityManager->flush();
    }

    /**
     * @return mixed
     */
    public function getEntityMain()
    {
        return $this->entityMain;
    }

    /**
     * @param mixed $entityMain
     * @return PersisteEntidadeRelacionadasService
     */
    public function setEntityMain($entityMain)
    {
        $this->entityMain = $entityMain;
        return $this;
    }

}