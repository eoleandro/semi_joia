<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 02/03/17
 * Time: 16:54
 */

namespace App\Service\Estoque;


use App\Entity\ItemRemessa;
use App\Entity\Produto;
use App\Entity\RemessaFornecedor;
use App\Entity\Usuario;
use Doctrine\ORM\EntityManager;
use Zend\Form\Element\DateTime;
use Zend\Hydrator\ClassMethods;

class PersisteRemessa
{
    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;
    private $authenticationService;

    public function __construct(EntityManager $entityManager,
                                \Zend\Authentication\AuthenticationService $authenticationService)
    {
        $this->entityManager = $entityManager;
        $this->authenticationService = $authenticationService;
    }

    public function gravarDadosRemessa(
        RemessaFornecedor $remessaFornecedor,
        ItemRemessa $itemRemessa,
        ClassMethods $hydrator,
        array $data, $produtosRemessa = array()
    )
    {
        $hydrator->hydrate($data, $remessaFornecedor);
        $valorTotalRemessa = 0;
        $dataAtual = new \DateTime('now');

        $usuario = $this->entityManager->getReference(
            Usuario::class, $this->authenticationService->getIdentity()['id']);
        $remessaFornecedor->setUsuario($usuario);
        
        $this->removeItensRemessa($produtosRemessa);

        foreach ($data['produto'] as $indice => $produtoId){
            if(!empty($produtoId)){
                $item = clone $itemRemessa;
                $produto = $this->entityManager->getReference(Produto::class, $produtoId);
                $remessaFornecedor->setDataCadastro($dataAtual);
                $remessaFornecedor->setStatus('Ativa');
                $dataItemRemessa = ['remessa' => $remessaFornecedor, 'produto' => $produto];
                $dataItemRemessa['quantParcial'] = $data['quantidade'][$indice];
                $dataItemRemessa['quantidade'] = $data['quantidade'][$indice];
                $dataItemRemessa['precoCusto'] = strtr($data['precoCusto'][$indice], ['.' =>'', ',' => '.']);
                $dataItemRemessa['precoCliente'] = strtr($data['precoCliente'][$indice], ['.' =>'', ',' => '.']);
                $dataItemRemessa['dataCadastro'] = $dataAtual;

                $item->setRemessaFornecedor($remessaFornecedor);
                $hydrator->hydrate($dataItemRemessa, $item);
                $valorTotalRemessa += ( strtr($data['precoCusto'][$indice], ['.' =>'', ',' => '.'])
                    * $data['quantidade'][$indice] );
                $remessaFornecedor->setValorTotal($valorTotalRemessa);

                $this->entityManager->persist($item);
            }
        }
        $this->entityManager->flush();
        return $remessaFornecedor;
    }

    private function removeItensRemessa($produtosRemessa = array())
    {
        foreach ($produtosRemessa as $itemRemessa){
                $itemRemessaRemover = $this->entityManager->getReference(ItemRemessa::class, $itemRemessa->getId());
                $this->entityManager->remove($itemRemessaRemover);
                $this->entityManager->flush();
        }
    }

    public function alterarStatusRemessa($remessaFornecedor)
    {
        $this->entityManager->persist($remessaFornecedor);
        $this->entityManager->flush();
        return $remessaFornecedor;
    }
    public function calcularValorTotal(array $itensRemessa )
    {
        $valorTotalRemessa = 0;
        foreach($itensRemessa as $itemRemessa){
            $valorItem = (($itemRemessa->getQuantidade() - $itemRemessa->getQuantParcial())
                         * $itemRemessa->getPrecoCusto());
            $valorTotalRemessa += $valorItem;
        }
        return $valorTotalRemessa;
    }


}