<?php
/**
 * Created by PhpStorm.
 * User: erico
 * Date: 02/03/17
 * Time: 17:36
 */

namespace App\Service\Estoque\Factory;

use App\Service\Estoque\PersisteRemessa;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;

class PersisteRemessaFactory
{

    public function __invoke(ContainerInterface $container)
    {
        $entityManager = $container->get(EntityManager::class);
        $authAdapterService = $container->get(AuthenticationService::class);
        return new PersisteRemessa($entityManager, $authAdapterService);
    }

}