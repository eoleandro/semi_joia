<?php

namespace App\Service\Form;

use \Zend\Form\Form as ZendForm;

class PopulateValuesFormService
{

    public function exportValuesPropertiesObjectToForm($object, ZendForm $form, $fieldsNoSetValues = array())
    {
        $fieldsForm = $form->getElements();
        foreach ($fieldsForm as $field => $properties){
            if(!in_array($field, $fieldsNoSetValues)){
                $methodGetValueField = "get" . ucfirst($field);
                if(method_exists($object, $methodGetValueField)){
                    if($form->has($field)){
                        $form->get($field)->setValue($object->{$methodGetValueField}());
                        //var_dump($object->{$methodGetValueField}());
                    }
                }
            }
        }
        return $form;
    }
    public function exportValuesKeyArrayToForm($dataValues, ZendForm $form, $fieldsNoSetValues = array())
    {
        $fieldsForm = $form->getElements();
        foreach ($fieldsForm as $field => $properties){
            if(!in_array($field, $fieldsNoSetValues)){
                if(!empty($dataValues[$field])){
                    $form->get($field)->setValue($dataValues[$field]);
                }
            }
        }
        return $form;
    }
}