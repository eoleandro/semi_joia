function getEndereco(idCampoCep, sufixoCampo) {
    // Se o campo CEP n�o estiver vazio
    var str = $("#"+idCampoCep).val();
    var cepSemHifem = str.replace(" - ","");
    if($.trim(cepSemHifem) != ""){
        /*
         Para conectar no servi�o e executar o json, precisamos usar a fun��o
         getScript do jQuery, o getScript e o dataType:"jsonp" conseguem fazer o cross-domain, os outros
         dataTypes n�o possibilitam esta intera��o entre dom�nios diferentes
         Estou chamando a url do servi�o passando o par�metro "formato=javascript" e o CEP digitado no formul�rio
         http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+$("#cep").val()
         */
        $.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+cepSemHifem, function(){
            // o getScript d� um eval no script, ent�o � s� ler!
            //Se o resultado for igual a 1
            if(resultadoCEP["resultado"]){
                // troca o valor dos elementos
                // $("#tipo_endereco").val(unescape(resultadoCEP["tipo_logradouro"]));
                $("#logradouro"+sufixoCampo).val( unescape(resultadoCEP["tipo_logradouro"])+" "+unescape(resultadoCEP["logradouro"]) );
                $("#bairro"+sufixoCampo).val(unescape(resultadoCEP["bairro"]));
                $("#cidade"+sufixoCampo).val(unescape(resultadoCEP["cidade"]));
                $("#UF"+sufixoCampo).val(unescape(resultadoCEP["uf"]));
                // $("#pais").val(unescape(resultadoCEP["pais"]));
                //console.log(resultadoCEP);
                /*$('.uppercase_field').each(function(){
                 var texto = $(this).val();
                 $(this).val(texto.toUpperCase());
                 });
                 */
                return resultadoCEP;

            }else{
                //alert("Endere�o n�o encontrado");
                return null;
            }
        });
    }
}