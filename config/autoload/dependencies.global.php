<?php
use Zend\Expressive\Application;
use Zend\Expressive\Container\ApplicationFactory;
use Zend\Expressive\Helper;

return [
    // Provides application-wide services.
    // We recommend using fully-qualified class names whenever possible as
    // service names.
    'dependencies' => [
        // Use 'invokables' for constructor-less services, or services that do
        // not require arguments to the constructor. Map a service name to the
        // class name.
        'invokables' => [
            // Fully\Qualified\InterfaceName::class => Fully\Qualified\ClassName::class,
            Helper\ServerUrlHelper::class => Helper\ServerUrlHelper::class,
            \App\Service\AnnotationReaderService::class => App\Service\AnnotationReaderService::class,
            \App\Service\UploadFileService::class => App\Service\UploadFileService::class,
            \App\Service\ResizeImageService::class => \App\Service\ResizeImageService::class,
            \App\Service\EnderecoService::class => \App\Service\EnderecoService::class,
            \App\Service\Form\PopulateValuesFormService::class => App\Service\Form\PopulateValuesFormService::class,
            \App\Service\Filter\FormataDataService::class => App\Service\Filter\FormataDataService::class,
            \App\Service\View\TemplateView::class => App\Service\View\TemplateView::class,

        ],
        // Use 'factories' for services provided by callbacks/factory classes.
        'factories' => [
            Application::class => ApplicationFactory::class,
            Helper\UrlHelper::class => Helper\UrlHelperFactory::class,
            App\Service\TesteService::class => App\Service\Factory\TesteServiceFactory::class,

            //Custom services factory
            App\Service\RecursoSistemaService::class => App\Service\Factory\RecursoSistemaServiceFactory::class,
            App\Service\PersistEntityWithRelationshipsService::class => App\Service\Factory\PersistEntityWithRelationshipsServiceFactory::class,
            App\Service\ReferencesDatasService::class => \App\Service\Factory\ReferencesDataServiceFactory::class,
            'Zend\Authentication\AuthenticationService' => \App\Service\Authentication\Factory\AuthenticationFactory::class,
            App\Service\Estoque\PersisteEntidadeRelacionadasService::class => App\Service\Estoque\Factory\PersisteEntidadeRelacionadasServiceFactory::class,
            App\Service\Estoque\PersisteRemessa::class => App\Service\Estoque\Factory\PersisteRemessaFactory::class,
            App\Service\Venda\PedidoService::class => App\Service\Venda\Factory\PedidoServiceFactory::class,
            App\Service\Financeiro\ParcelaPedidoService::class => App\Service\Financeiro\Factory\ParcelaPedidoServiceFactory::class,
            App\Service\Financeiro\ParcelaRemessaService::class => App\Service\Financeiro\Factory\ParcelaRemessaServiceFactory::class,
            App\Service\Venda\DevolucaoItemService::class => App\Service\Venda\Factory\DevolucaoItemServiceFactory::class,
            App\Service\Financeiro\MovFinanceiroService::class => App\Service\Financeiro\Factory\MovFinanceiroServiceFactory::class,
            App\Service\Email\EnviaEmailService::class => App\Service\Email\Factory\EnviaEmailServiceFactory::class,


        ],
        'aliases' => [
            'Config' => 'config',
            'Configuration' => 'config'
        ]
    ],
];
