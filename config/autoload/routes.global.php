<?php

use App\Action\Product;
use App\Action\Usuario;
use App\Action\Estoque;
use App\Action\Cliente;
use App\Action\Venda;
use App\Action\Financeiro;
use App\Action\Evento;
use App\Action\Compromisso;
use App\Action\Relatorio;

return [

    'dependencies' => [
        'invokables' => [
            Zend\Expressive\Router\RouterInterface::class => Zend\Expressive\Router\AuraRouter::class,
            App\Action\PingAction::class => App\Action\PingAction::class,
        ],
        'factories' => [
            App\Action\HomePageAction::class => App\Action\HomePageFactory::class,

            Usuario\GrupoListPageAction::class => Usuario\Factory\GrupoListPageFactory::class,
            Usuario\GrupoCreatePageAction::class => Usuario\Factory\GrupoCreatePageFactory::class,
            Usuario\GrupoUpdatePageAction::class => Usuario\Factory\GrupoUpdatePageFactory::class,
            Usuario\GrupoDeleteAction::class => Usuario\Factory\GrupoDeleteFactory::class,
            Usuario\PermissaoAclGerenciarPageAction::class => Usuario\Factory\PermissaoAclGerenciarPageFactory::class,

            Usuario\UsuarioListPageAction::class => Usuario\Factory\UsuarioListPageFactory::class,
            Usuario\UsuarioCreatePageAction::class => Usuario\Factory\UsuarioCreatePageFactory::class,
            Usuario\UsuarioUpdatePageAction::class => Usuario\Factory\UsuarioUpdatePageFactory::class,
            Usuario\UsuarioDeleteAction::class => Usuario\Factory\UsuarioDeleteFactory::class,
            Usuario\UsuarioAuthPageAction::class => Usuario\Factory\UsuarioAuthPageFactory::class,
            Usuario\UsuarioLogoutAction::class  => Usuario\Factory\UsuarioLogoutFactory::class,
            Usuario\PerfilUsuarioPageAction::class => Usuario\Factory\PerfilUsuarioPageFactory::class,

            Estoque\FornecedorListPageAction::class => Estoque\Factory\FornecedorListPageFactory::class,
            Estoque\FornecedorCreatePageAction::class => Estoque\Factory\FornecedorCreatePageFactory::class,
            Estoque\FornecedorUpdatePageAction::class => Estoque\Factory\FornecedorUpdatePageFactory::class,
            Estoque\FornecedorDeleteAction::class => Estoque\Factory\FornecedorDeleteFactory::class,

            Estoque\ProdutoCreatePageAction::class => Estoque\Factory\ProdutoCreatePageFactory::class,
            Estoque\ProdutoListPageAction::class => Estoque\Factory\ProdutoListPageFactory::class,
            Estoque\ProdutoUpdatePageAction::class => Estoque\Factory\ProdutoUpdatePageFactory::class,

            Estoque\RemessaFornecedorActionPage::class => Estoque\Factory\RemessaFornecedorPageFactory::class,
            Estoque\RemessaItemsAddPageAction::class => Estoque\Factory\RemessaItemsAddPageFactory::class,
            Estoque\RemessaItemsEditPageAction::class => Estoque\Factory\RemessaItemsEditPageFactory::class,
            Estoque\RemessaListPageAction::class => Estoque\Factory\RemessaListPageFactory::class,
            Estoque\AcertoRemessaPageAction::class => Estoque\Factory\AcertoRemessaPageFactory::class,
            Estoque\RemessaParcelaPageAction::class => Estoque\Factory\RemessaParcelaPageFactory::class,
            Estoque\RemessaShowPageAction::class => Estoque\Factory\RemessaShowPageFactory::class,

            Cliente\ClienteCreatePageAction::class => Cliente\Factory\ClienteCreatePageFactory::class,
            Cliente\ClienteListPageAction::class => Cliente\Factory\ClienteListPageFactory::class,
            Cliente\ClienteUpdatePageAction::class => Cliente\Factory\ClienteUpdatePageFactory::class,
            Cliente\ClienteDeleteAction::class => Cliente\Factory\ClienteDeleteFactory::class,

            Venda\PedidoClientePageAction::class => Venda\Factory\PedidoClientePageFactory::class,
            Venda\PedidoEditPageAction::class => Venda\Factory\PedidoEditPageFactory::class,
            Venda\PedidoAddPageAction::class => Venda\Factory\PedidoAddPageFactory::class,
            Venda\PedidoListPageAction::class => Venda\Factory\PedidoListPageFactory::class,
            Venda\PedidoParcelaPageAction::class => Venda\Factory\PedidoParcelaPageFactory::class,
            Venda\DevolucaoItemPageAction::class => Venda\Factory\DevolucaoItemPageFactory::class,
            Venda\PedidoEntregaPageAction::class => Venda\Factory\PedidoEntregaPageFactory::class,
            Venda\PedidoShowPageAction::class => Venda\Factory\PedidoShowPageFactory::class,
            Venda\PedidoCancelarAction::class => Venda\Factory\PedidoCancelarFactory::class,
            Venda\RelatorioPedidoPageAction::class => Venda\Factory\RelatorioPedidoPageFactory::class,
            Venda\PedidosEntregarPageAction::class => Venda\Factory\PedidosEntregarPageFactory::class,
            Venda\RelatorioVendaClientePageAction::class => Venda\Factory\RelatorioVendaClientePageFactory::class,

            Financeiro\PagtoParcPedidoPageAction::class => Financeiro\Factory\PagtoParcPedidoPageFactory::class,
            Financeiro\PagtoParcRemessaPageAction::class => Financeiro\Factory\PagtoParcRemessaPageFactory::class,

            Financeiro\FormaPgtoAddPageAction::class => Financeiro\Factory\FormaPgtoAddPageFactory::class,
            Financeiro\FormaPgtoListPageAction::class => Financeiro\Factory\FormaPgtoListPageFactory::class,
            Financeiro\FormaPgtoEditPageAction::class => Financeiro\Factory\FormaPgtoEditPageFactory::class,

            Financeiro\PlanoContaAddPageAction::class => Financeiro\Factory\PlanoContaAddPageFactory::class,
            Financeiro\PlanoContaListPageAction::class => Financeiro\Factory\PlanoContaListPageFactory::class,
            Financeiro\PlanoContaEditPageAction::class => Financeiro\Factory\PlanoContaEditPageFactory::class,

            Financeiro\MovFinanceiroAddPageAction::class => Financeiro\Factory\MovFinanceiroAddPageFactory::class,
            Financeiro\MovFinanceiroListPageAction::class => Financeiro\Factory\MovFinanceiroListPageFactory::class,
            Financeiro\MovFinanceiroEditPageAction::class => Financeiro\Factory\MovFinanceiroEditPageFactory::class,
            Financeiro\MovFinanceiroDeleteAction::class => Financeiro\Factory\MovFinanceiroDeleteFactory::class,

            Financeiro\RelatorioContasPagarPageAction::class => Financeiro\Factory\RelatorioContaPagarPageFactory::class,
            Financeiro\RelatorioContasReceberPageAction::class => Financeiro\Factory\RelatorioContaReceberPageFactory::class,
            Financeiro\RelatorioFluxoCaixaPageAction::class => Financeiro\Factory\RelatorioFluxoCaixaPageFactory::class,
            Financeiro\ReciboParcelaPageAction::class => Financeiro\Factory\ReciboParcelaPageFactory::class,

            Evento\AgendaVisitaPageAction::class => Evento\Factory\AgendaVisitaPageFactory::class,
            Evento\AgendaListPageAction::class => Evento\Factory\AgendaListPageFactory::class,
            Evento\AgendaDeleteAction::class => Evento\Factory\AgendaDeleteFactory::class,
            Evento\RelatorioAgendaPageAction::class => Evento\Factory\RelatorioAgendaPageFactory::class,

            Compromisso\ContasPagarPageAction::class => Compromisso\Factory\ContasPagarPageFactory::class,
            Relatorio\EscolherCadastroPageAction::class => Relatorio\Factory\EscolherCadastroPageFactory::class,
            Relatorio\RelatorioCadastroPageAction::class => Relatorio\Factory\RelatorioCadastroPageFactory::class,
        ],
    ],

    'routes' => [
        [
            'name' => 'home',
            'path' => '/',
            'middleware' => App\Action\HomePageAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'api.ping',
            'path' => '/api/ping',
            'middleware' => App\Action\PingAction::class,
            'allowed_methods' => ['GET'],
        ],

        [
            'name' => 'product.list',
            'path' => '/admin/products',
            'middleware' => Product\ProductListPageAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'product.create',
            'path' => '/admin/product/create',
            'middleware' => Product\ProductCreatePageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'product.update',
            'path' => '/admin/product/{id}/update',
            'middleware' => Product\ProductUpdatePageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'product.delete',
            'path' => '/admin/product/{id}/delete',
            'middleware' => Product\ProductDeleteAction::class,
            'allowed_methods' => ['GET'],
        ],

        /* Rotas para o resource de grupo de usuario*/
        [
            'name' => 'grupo.list',
            'path' => '/usuario/grupos',
            'middleware' => Usuario\GrupoListPageAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'grupo.create',
            'path' => '/usuario/grupo/cadastrar',
            'middleware' => Usuario\GrupoCreatePageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'grupo.update',
            'path' => '/usuario/grupo/{id}/editar',
            'middleware' => Usuario\GrupoUpdatePageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'grupo.delete',
            'path' => '/usario/grupo/{id}/excluir',
            'middleware' => Usuario\GrupoDeleteAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'permissao_acl.manager',
            'path' => '/usuario/permissoes-grupo/{id}/gerenciar',
            'middleware' => Usuario\PermissaoAclGerenciarPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        /* Rotas para o resource de usuario*/
        [
            'name' => 'usuario.list',
            'path' => '/usuario/usuarios',
            'middleware' => Usuario\UsuarioListPageAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'usuario.create',
            'path' => '/usuario/usuario/cadastrar',
            'middleware' => Usuario\UsuarioCreatePageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'usuario.update',
            'path' => '/usuario/usuario/{id}/editar',
            'middleware' => Usuario\UsuarioUpdatePageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'usuario.delete',
            'path' => '/usuario/usuario/{id}/excluir',
            'middleware' => Usuario\UsuarioDeleteAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'usuario.auth',
            'path' => '/usuario/login',
            'middleware' => Usuario\UsuarioAuthPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'usuario.logout',
            'path' => '/usuario/logout',
            'middleware' => Usuario\UsuarioLogoutAction::class,
            'allowed_methods' => ['GET'],
        ],

        [
            'name' => 'usuario.profile',
            'path' => '/usuario/perfil',
            'middleware' => Usuario\PerfilUsuarioPageAction::class,
            'allowed_methods' => ['GET','POST'],
        ],
        /*Rotas para a secao do sistema destinada ao estoque */
        [
            'name' => 'fornecedor.create',
            'path' => '/estoque/fornecedor/cadastrar',
            'middleware' => Estoque\FornecedorCreatePageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'fornecedor.list',
            'path' => '/estoque/fornecedores',
            'middleware' => Estoque\FornecedorListPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'fornecedor.update',
            'path' => '/estoque/fornecedor/{id}/editar',
            'middleware' => Estoque\FornecedorUpdatePageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'fornecedor.delete',
            'path' => '/estoque/fornecedor/{id}/excluir',
            'middleware' => Estoque\FornecedorDeleteAction::class,
            'allowed_methods' => ['GET'],
        ],

        [
            'name' => 'produto.create',
            'path' => '/estoque/produto/cadastrar',
            'middleware' => Estoque\ProdutoCreatePageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'produto.list',
            'path' => '/estoque/produtos',
            'middleware' => Estoque\ProdutoListPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'produto.update',
            'path' => '/estoque/produto/{id}/editar',
            'middleware' => Estoque\ProdutoUpdatePageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'produto.delete',
            'path' => '/estoque/produto/{id}/excluir',
            'middleware' => Estoque\FornecedorDeleteAction::class,
            'allowed_methods' => ['GET'],
        ],

        [
            'name' => 'cliente.create',
            'path' => '/cliente/cliente/cadastrar',
            'middleware' => Cliente\ClienteCreatePageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],

        [
            'name' => 'cliente.list',
            'path' => '/cliente/clientes',
            'middleware' => Cliente\ClienteListPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],

        [
            'name' => 'cliente.update',
            'path' => '/cliente/cliente/{id}/editar',
            'middleware' => Cliente\ClienteUpdatePageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],

        [
            'name' => 'cliente.delete',
            'path' => '/cliente/cliente/{id}/excluir',
            'middleware' => Cliente\ClienteDeleteAction::class,
            'allowed_methods' => ['GET'],
        ],

        [
            'name' => 'remessa.define_provider',
            'path' => '/estoque/remessa/definir-fornecedor',
            'middleware' => Estoque\RemessaFornecedorActionPage::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'remessa.define_itens',
            'path' => '/estoque/remessa/{fornecedor}/definir-items',
            'middleware' => Estoque\RemessaItemsAddPageAction::class,
            'allowed_methods' => ['GET','POST'],
        ],
        [
            'name' => 'remessa.edit_itens',
            'path' => '/estoque/remessa/{remessa}/editar-items',
            'middleware' => Estoque\RemessaItemsEditPageAction::class,
            'allowed_methods' => ['GET','POST'],
        ],
        [
            'name' => 'remessa.list',
            'path' => '/estoque/remessas',
            'middleware' => Estoque\RemessaListPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'remessa.show',
            'path' => '/estoque/remessa/{remessa}/show',
            'middleware' => Estoque\RemessaShowPageAction::class,
            'allowed_methods' => ['GET','POST'],
        ],
        [
            'name' => 'remessa.set_status',
            'path' => '/estoque/remessa/{remessa}/acerto-remessa',
            'middleware' => Estoque\AcertoRemessaPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'remessa.define_plots',
            'path' => '/estoque/remessa/{remessa}/definir-pacelas',
            'middleware' => Estoque\RemessaParcelaPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],

        [
            'name' => 'pedido.define_client',
            'path' => '/venda/pedido/definir-cliente',
            'middleware' => Venda\PedidoClientePageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'pedido.define_itens',
            'path' => '/venda/pedido/{cliente}/definir-itens',
            'middleware' => Venda\PedidoAddPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'pedido.list',
            'path' => '/venda/pedidos',
            'middleware' => Venda\PedidoListPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'parcela.define_plots',
            'path' => '/venda/parcela-pedido/{pedido}/definir-parcelas',
            'middleware' => Venda\PedidoParcelaPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'pedido.edit_itens',
            'path' => '/venda/pedido/{pedido}/editar-itens',
            'middleware' => Venda\PedidoEditPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'pedido.entrega',
            'path' => '/venda/pedido/{pedido}/entrega',
            'middleware' => Venda\PedidoEntregaPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'pedido.show',
            'path' => '/venda/pedido/{pedido}/show',
            'middleware' => Venda\PedidoShowPageAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'pedido.cancelar',
            'path' => '/venda/pedido/{pedido}/cancelar',
            'middleware' => Venda\PedidoCancelarAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'parcela.pay_order_plots',
            'path' => '/financeiro/pagamento-parcela/{pedido}/pedido',
            'middleware' => Financeiro\PagtoParcPedidoPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'devolucao.definir_itens',
            'path' => '/venda/devolucao/{pedido}/definir-itens',
            'middleware' => Venda\DevolucaoItemPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'parcela-remessa.pay_order_plots',
            'path' => '/financeiro/pagamento-parcela/{remessa}/remessa-fornecedor',
            'middleware' => Financeiro\PagtoParcRemessaPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'formapgto.create',
            'path' => '/financeiro/forma-pagamento/cadastrar',
            'middleware' => Financeiro\FormaPgtoAddPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'formapgto.list',
            'path' => '/financeiro/formas-pagamento',
            'middleware' => Financeiro\FormaPgtoListPageAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'formapgto.update',
            'path' => '/financeiro/forma-pagamento/{id}/atualizar',
            'middleware' => Financeiro\FormaPgtoEditPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],

        [
            'name' => 'planoconta.create',
            'path' => '/financeiro/plano-conta/cadastrar',
            'middleware' => Financeiro\PlanoContaAddPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'planoconta.list',
            'path' => '/financeiro/plano-conta',
            'middleware' => Financeiro\PlanoContaListPageAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'planoconta.update',
            'path' => '/financeiro/plano-conta/{id}/atualizar',
            'middleware' => Financeiro\PlanoContaEditPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],

        [
            'name' => 'movfinanceiro.create',
            'path' => '/financeiro/mov-financeiro/cadastrar',
            'middleware' => Financeiro\MovFinanceiroAddPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'movfinanceiro.list',
            'path' => '/financeiro/mov-financeiro',
            'middleware' => Financeiro\MovFinanceiroListPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'movfinanceiro.update',
            'path' => '/financeiro/mov-financeiro/{id}/atualizar',
            'middleware' => Financeiro\MovFinanceiroEditPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'movfinanceiro.delete',
            'path' => '/financeiro/mov-financeiro/{id}/excluir',
            'middleware' => Financeiro\MovFinanceiroDeleteAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'movfinanceiro.receipt',
            'path' => '/financeiro/pagamento-parcela/{tipoMovFin}/{pagamentos}/emitir-recibo',
            'middleware' => Financeiro\ReciboParcelaPageAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'agenda.save',
            'path' => '/evento/agenda-visita/cadastrar',
            'middleware' => Evento\AgendaVisitaPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'agenda.list',
            'path' => '/evento/agenda-visita/list',
            'middleware' => Evento\AgendaListPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'agenda.delete',
            'path' => '/evento/agenda-visita/{agenda}/remove',
            'middleware' => Evento\AgendaDeleteAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'agenda.report',
            'path' => '/relatorio/evento/visitas',
            'middleware' => Evento\RelatorioAgendaPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'relatorio.pay_count',
            'path' => '/relatorio/financeiro/contas-pagar',
            'middleware' => Financeiro\RelatorioContasPagarPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'relatorio.receive_count',
            'path' => '/relatorio/financeiro/contas-receber',
            'middleware' => Financeiro\RelatorioContasReceberPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'relatorio.flow_cashier',
            'path' => '/relatorio/financeiro/fluxo-caixa',
            'middleware' => Financeiro\RelatorioFluxoCaixaPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'relatorio.order',
            'path' => '/relatorio/venda/pedido',
            'middleware' => Venda\RelatorioPedidoPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'relatorio.record_select',
            'path' => '/relatorio/cadastro/escolher-cadastro',
            'middleware' => Relatorio\EscolherCadastroPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'relatorio.record',
            'path' => '/relatorio/cadastro/{nomeCadastro}',
            'middleware' => Relatorio\RelatorioCadastroPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'relatorio.sales_per_customer',
            'path' => '/relatorio/venda/venda-por-cliente',
            'middleware' => Venda\RelatorioVendaClientePageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],


        [
            'name' => 'pagamentos.list',
            'path' => '/compromisso/financeiro/conta/{tipo}',
            'middleware' => Compromisso\ContasPagarPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'pedido.deliver',
            'path' => '/venda/pedidos/entregar',
            'middleware' => Venda\PedidosEntregarPageAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],

    ],

];
