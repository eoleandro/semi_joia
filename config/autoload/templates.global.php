<?php

return [
    'dependencies' => [
        /*'delegators' =>[
            \Zend\View\HelperPluginManager::class => [
                \App\Helper\FormHelpersDelegatorFactory::class,
            ],
        ],*/
        'factories' => [
            'Zend\Expressive\FinalHandler' =>
                Zend\Expressive\Container\TemplatedErrorHandlerFactory::class,

            Zend\Expressive\Template\TemplateRendererInterface::class =>
                Zend\Expressive\ZendView\ZendViewRendererFactory::class,

            Zend\View\HelperPluginManager::class =>
                Zend\Expressive\ZendView\HelperPluginManagerFactory::class,
            \Zend\View\HelperPluginManager::class =>
             \App\Helper\HelperPluginManagerFactory::class
        ],
    ],

    'templates' => [
        'layout' => 'layout/default',
        'map' => [
            'layout/default' => 'templates/layout/default.phtml',
            'error/error'    => 'templates/error/error.phtml',
            'error/404'      => 'templates/error/404.phtml',
            'auth'      => 'templates/app/usuario/usuario/login.phtml',
            'layout/email' => 'templates/layout/email.phtml',
            'layout/login' => 'templates/layout/login.phtml',
            'layout/recibo' => 'templates/layout/recibo.phtml',

        ],
        'paths' => [
            'app'    => ['templates/app'],
            'layout' => ['templates/layout'],
            'error'  => ['templates/error'],
            'auth'   => ['templates/app/usuario/usuario'],
        ],

    ],

    'view_helpers' => [
        // zend-servicemanager-style configuration for adding view helpers:
        // - 'aliases'
        // - 'invokables'
        // - 'factories'
        // - 'abstract_factories'
        // - etc.
        'invokables' => [
            'formataNomeFoto' => App\Helper\View\FormataNomeFoto::class,
            'exibeDadoRelatorio' => App\Helper\View\ExibeDadoRelatorioCadastro::class,
            'exibeTituloRelatorio' => App\Helper\View\ExibeTituloRelatorioCadastro::class,
            'abreviarNome' => App\Helper\View\AbreviarNome::class,
        ],
        'factories' => [
            'flashMessager' => \App\Helper\View\Factory\FlashMessagerFactory::class
        ]
    ],
];
