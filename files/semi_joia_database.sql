-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 11-Abr-2017 às 16:49
-- Versão do servidor: 5.7.17-0ubuntu0.16.04.1
-- PHP Version: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `semi_joia`
--
CREATE DATABASE IF NOT EXISTS `semi_joia` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `semi_joia`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `agenda`
--

DROP TABLE IF EXISTS `agenda`;
CREATE TABLE `agenda` (
  `id` int(11) NOT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `usuario_id` int(11) NOT NULL,
  `evento` varchar(100) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `data_hora` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT 'M'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `agenda`
--

TRUNCATE TABLE `agenda`;
--
-- Extraindo dados da tabela `agenda`
--

INSERT INTO `agenda` (`id`, `cliente_id`, `usuario_id`, `evento`, `descricao`, `data_hora`, `status`) VALUES
(2, 3, 1, '3 -  Lorrayne Neves de Souza - (31) 92845 - 189', NULL, '2017-04-03 23:42:28', 'M'),
(3, NULL, 1, 'Visita - Birthday Party', NULL, '2017-03-16 19:00:00', 'M'),
(4, NULL, 1, 'Visita - Repeating Event', NULL, '2017-03-12 16:00:00', 'M'),
(5, NULL, 1, 'Visita - Tester', NULL, '2017-03-01 00:00:00', 'M'),
(6, 1, 1, '1 -  Erico de Oliveira Leandro - (31) 99244 - 4070', NULL, '2017-03-01 00:51:00', 'R'),
(8, 4, 1, '4 -  Renan Santos Viglioni - (23) 12121 - 2122', NULL, '2017-04-05 11:40:00', 'R'),
(9, 3, 1, '3 -  Lorrayne Neves de Souza - (31) 92845 - 189', NULL, '2017-04-06 17:56:00', 'R'),
(10, 4, 1, '4 -  Renan Santos Viglioni - (23) 12121 - 2122', NULL, '2017-04-06 12:40:00', 'M'),
(11, 3, 1, '3 -  Lorrayne Neves de Souza - (31) 92845 - 189', NULL, '2017-04-13 17:54:00', 'M'),
(12, 1, 1, '1 -  Erico de Oliveira Leandro - (31) 99244 - 4070', NULL, '2017-04-11 09:40:00', 'R'),
(13, 3, 1, '3 -  Lorrayne Neves de Souza - (31) 92845 - 189', NULL, '2017-04-07 13:20:00', 'M');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `CPF` varchar(45) DEFAULT NULL,
  `sexo` varchar(45) DEFAULT NULL,
  `data_nascimento` datetime DEFAULT NULL,
  `data_casamento` datetime DEFAULT NULL,
  `nome_conjuge` varchar(250) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `telefone_fixo` varchar(45) DEFAULT NULL,
  `telefone_celular` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `cliente`
--

TRUNCATE TABLE `cliente`;
--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`id`, `nome`, `CPF`, `sexo`, `data_nascimento`, `data_casamento`, `nome_conjuge`, `email`, `telefone_fixo`, `telefone_celular`) VALUES
(1, 'Erico de Oliveira Leandro', '085.133.086-09', 'M', '1987-12-15 16:24:28', NULL, '', 'erico.autocad@gmail.com', '(31) 3627 - 3701', '(31) 99244 - 4070'),
(2, 'Erico de Oliveira Leandros', '085.133.086-09', 'M', '1987-12-15 12:51:59', NULL, 'Lorrayne Neves de Souza', 'erico.autocad@gmail.com', '(31) 3627 - 3701', '(31) 99244 - 4070'),
(3, 'Lorrayne Neves de Souza', '019.665.465-46', 'F', '1997-03-13 09:20:16', NULL, 'Érico de Oliveira', 'lorrayneneves23@gmail.com', '(31) 9284 - 5189', '(31) 92845 - 189'),
(4, 'Renan Santos Viglioni', '455.231.255-15', 'M', '1990-04-05 11:39:36', NULL, '', 'erico.autocad@gmail.com', '(32) 32323 - 2323', '(23) 12121 - 2122');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato_fornecedor`
--

DROP TABLE IF EXISTS `contato_fornecedor`;
CREATE TABLE `contato_fornecedor` (
  `id` int(11) NOT NULL,
  `fornecedor_id` int(11) NOT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `funcao` varchar(100) DEFAULT NULL,
  `telefone` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `contato_fornecedor`
--

TRUNCATE TABLE `contato_fornecedor`;
--
-- Extraindo dados da tabela `contato_fornecedor`
--

INSERT INTO `contato_fornecedor` (`id`, `fornecedor_id`, `nome`, `funcao`, `telefone`) VALUES
(1, 1, 'Erico de Oliveira Leandro', 'Diretor', '36273701'),
(2, 2, 'Lorrayne Neves', 'Diretora', '(31) 99284 - 5189'),
(3, 3, 'Higor', 'Diretor', '(11) 11111 - 1111'),
(4, 4, 'Rosalvo  Afonso', 'Diretor', '(31) 99244 - 4070');

-- --------------------------------------------------------

--
-- Estrutura da tabela `endereco_cliente`
--

DROP TABLE IF EXISTS `endereco_cliente`;
CREATE TABLE `endereco_cliente` (
  `id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `logradouro` varchar(250) DEFAULT NULL,
  `numero` varchar(45) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `UF` varchar(45) DEFAULT NULL,
  `CEP` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `endereco_cliente`
--

TRUNCATE TABLE `endereco_cliente`;
--
-- Extraindo dados da tabela `endereco_cliente`
--

INSERT INTO `endereco_cliente` (`id`, `cliente_id`, `logradouro`, `numero`, `bairro`, `cidade`, `UF`, `CEP`) VALUES
(1, 1, 'Rua Emerenciana Camargo Batista', '5', 'Santa Marta', 'Ribeirão das Neves', 'MG', '33880 - 120'),
(2, 2, 'Rua Emerenciana Camargo Batista', '7', 'Santa Marta', 'Ribeirão das Neves', 'MG', '33880 - 120'),
(3, 3, 'Rua Maria Adelaide', '1312', 'Veneza', 'Ribeirão das Neves', 'MG', '33820 - 080'),
(4, 4, 'Rua Timbiras', '2903', 'Barro Preto', 'Belo Horizonte', 'MG', '30140 - 062');

-- --------------------------------------------------------

--
-- Estrutura da tabela `endereco_fornecedor`
--

DROP TABLE IF EXISTS `endereco_fornecedor`;
CREATE TABLE `endereco_fornecedor` (
  `id` int(11) NOT NULL,
  `fornecedor_id` int(11) NOT NULL,
  `logradouro` varchar(250) DEFAULT NULL,
  `numero` varchar(45) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `UF` varchar(45) DEFAULT NULL,
  `CEP` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `endereco_fornecedor`
--

TRUNCATE TABLE `endereco_fornecedor`;
--
-- Extraindo dados da tabela `endereco_fornecedor`
--

INSERT INTO `endereco_fornecedor` (`id`, `fornecedor_id`, `logradouro`, `numero`, `bairro`, `cidade`, `UF`, `CEP`) VALUES
(1, 1, 'Rua Emereciana Camargo Batista', '5', 'Santa Marta', 'Ribeirão das Neves', 'MG', '33880120'),
(2, 2, 'Rua Maria Adelaide', '1312', 'Veneza', 'Ribeirão das Neves', 'MG', '33820 - 080'),
(3, 3, 'Rua Aimorés', '56454', 'Lourdes', 'Belo Horizonte', 'MG', '30140 - 072'),
(4, 4, 'Alameda Judiciários', '8245', 'Cândida Ferreira', 'Contagem', 'MG', '32145 - 696');

-- --------------------------------------------------------

--
-- Estrutura da tabela `forma_pgto`
--

DROP TABLE IF EXISTS `forma_pgto`;
CREATE TABLE `forma_pgto` (
  `id` int(11) NOT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  `ativo` varchar(45) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `forma_pgto`
--

TRUNCATE TABLE `forma_pgto`;
--
-- Extraindo dados da tabela `forma_pgto`
--

INSERT INTO `forma_pgto` (`id`, `descricao`, `ativo`) VALUES
(1, 'Dinheiro', '1'),
(2, 'Crédito', '1'),
(3, 'Débito', '1'),
(4, 'Cheque', '1'),
(5, 'Fiado', '0');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fornecedor`
--

DROP TABLE IF EXISTS `fornecedor`;
CREATE TABLE `fornecedor` (
  `id` int(11) NOT NULL,
  `razao_social` varchar(255) DEFAULT NULL,
  `nome_fantasia` varchar(255) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `CNPJ` varchar(45) DEFAULT NULL,
  `telefone_fixo` varchar(45) DEFAULT NULL,
  `telefone_celular` varchar(45) DEFAULT NULL,
  `insc_estadual` varchar(45) DEFAULT NULL,
  `insc_municipal` varchar(45) DEFAULT NULL,
  `ramo_atividade` varchar(100) DEFAULT NULL,
  `observacao` varchar(250) DEFAULT NULL,
  `ativo` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `fornecedor`
--

TRUNCATE TABLE `fornecedor`;
--
-- Extraindo dados da tabela `fornecedor`
--

INSERT INTO `fornecedor` (`id`, `razao_social`, `nome_fantasia`, `email`, `CNPJ`, `telefone_fixo`, `telefone_celular`, `insc_estadual`, `insc_municipal`, `ramo_atividade`, `observacao`, `ativo`) VALUES
(1, 'RM Sistemas LTDA', 'RM sistemas', 'erico.autocad@gmail.com', '098080980j', '29999', '', '39435353453', '3453453543', '', '', NULL),
(2, 'Erico Empreendimentos LTDA', 'Erico Empreendimentos SA', 'erico.autocad@gmail.com', '11.111.111/1111-11', '(31) 3627 - 3701', '(31) 99244 - 4070', 'dsfsdfs323323', 'sdfsdfsdfsdf', '', '', NULL),
(3, 'Romanel LTDA', 'Romanel', 'romanel@gmail.com', '26.564.654/6954-99', '(12) 3456 - 7899', '(12) 34567 - 8999', '41sdfdmkkjfda', '65646515621', 'joalheria', '', 1),
(4, 'Alianças BH LTDA', 'Alianças BH', 'erico.autocad@gmail.com', '14.412.542/2522-22', '(31) 31456 - 5656', '(54) 54545 - 4545', '1487452152', '9784164', 'joalheria', '', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `grupo`
--

DROP TABLE IF EXISTS `grupo`;
CREATE TABLE `grupo` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `grupo`
--

TRUNCATE TABLE `grupo`;
--
-- Extraindo dados da tabela `grupo`
--

INSERT INTO `grupo` (`id`, `nome`) VALUES
(1, 'Super Administrador'),
(3, 'Suporte');

-- --------------------------------------------------------

--
-- Estrutura da tabela `item_pedido`
--

DROP TABLE IF EXISTS `item_pedido`;
CREATE TABLE `item_pedido` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `pedido_id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `quantidade` int(11) DEFAULT NULL,
  `valor` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `item_pedido`
--

TRUNCATE TABLE `item_pedido`;
--
-- Extraindo dados da tabela `item_pedido`
--

INSERT INTO `item_pedido` (`id`, `usuario_id`, `pedido_id`, `produto_id`, `quantidade`, `valor`) VALUES
(1, 1, 1, 10, 2, 100),
(3, 1, 2, 11, 0, 0),
(4, 1, 3, 10, 3, 150),
(9, 1, 2, 10, 0, 0),
(10, 1, 4, 11, 0, 0),
(11, 1, 4, 10, 0, 0),
(12, 1, 4, 1, 0, 0),
(13, 1, 5, 10, 1, 50),
(14, 1, 6, 1, 1, 50),
(15, 1, 7, 11, 1, 80),
(16, 1, 8, 11, 1, 80),
(17, 4, 9, 10, 2, 100),
(18, 4, 9, 14, 1, 30),
(19, 1, 10, 13, 4, 200),
(20, 1, 10, 14, 1, 30),
(21, 1, 10, 10, 2, 35.1),
(22, 1, 11, 15, 1, 200),
(23, 1, 11, 16, 2, 40),
(24, 1, 11, 13, 2, 100),
(25, 1, 12, 13, 1, 150);

-- --------------------------------------------------------

--
-- Estrutura da tabela `item_remessa`
--

DROP TABLE IF EXISTS `item_remessa`;
CREATE TABLE `item_remessa` (
  `id` int(11) NOT NULL,
  `remessa_fornecedor_id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `preco_custo` float DEFAULT NULL,
  `preco_cliente` float DEFAULT NULL,
  `quantidade` int(11) DEFAULT NULL,
  `quant_parcial` int(11) NOT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `data_entrada` datetime DEFAULT NULL,
  `data_devolucao` datetime DEFAULT NULL,
  `data_quitacao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `item_remessa`
--

TRUNCATE TABLE `item_remessa`;
--
-- Extraindo dados da tabela `item_remessa`
--

INSERT INTO `item_remessa` (`id`, `remessa_fornecedor_id`, `produto_id`, `usuario_id`, `preco_custo`, `preco_cliente`, `quantidade`, `quant_parcial`, `data_cadastro`, `data_entrada`, `data_devolucao`, `data_quitacao`) VALUES
(6, 3, 1, 1, 20, 50, 10, 9, '2017-03-03 10:40:09', NULL, NULL, NULL),
(9, 2, 10, 1, 25, 50, 10, 2, '2017-03-03 10:48:00', NULL, NULL, NULL),
(10, 2, 11, 1, 60, 80, 10, 8, '2017-03-03 10:48:00', NULL, NULL, NULL),
(11, 1, 1, 1, 25, 50, 10, 5, '2017-03-03 00:00:00', NULL, NULL, NULL),
(20, 4, 13, 1, 50, 100, 20, 18, '2017-04-06 00:05:36', NULL, NULL, NULL),
(21, 4, 14, 1, 20, 40, 30, 29, '2017-04-06 00:05:36', NULL, NULL, NULL),
(30, 8, 10, 1, 12.55, 17.55, 10, 8, '2017-04-07 18:08:55', NULL, NULL, NULL),
(31, 8, 2, 1, 15, 20, 10, 10, '2017-04-07 18:08:55', NULL, NULL, NULL),
(32, 5, 13, 1, 20, 50, 10, 6, '2017-04-09 21:41:08', NULL, NULL, NULL),
(33, 5, 14, 1, 20, 30, 20, 20, '2017-04-09 21:41:08', NULL, NULL, NULL),
(34, 9, 15, 1, 100, 200, 10, 9, '2017-04-10 23:06:06', NULL, NULL, NULL),
(35, 9, 16, 1, 15, 20, 15, 13, '2017-04-10 23:06:06', NULL, NULL, NULL),
(36, 9, 17, 1, 30, 50, 20, 20, '2017-04-10 23:06:06', NULL, NULL, NULL),
(40, 10, 1, 1, 20, 30, 20, 20, '2017-04-11 16:36:44', NULL, NULL, NULL),
(41, 10, 14, 1, 10, 15, 10, 10, '2017-04-11 16:36:44', NULL, NULL, NULL),
(42, 10, 13, 1, 100, 150, 8, 7, '2017-04-11 16:36:44', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `mov_financeiro`
--

DROP TABLE IF EXISTS `mov_financeiro`;
CREATE TABLE `mov_financeiro` (
  `id` int(11) NOT NULL,
  `forma_pgto_id` int(11) DEFAULT NULL,
  `plano_conta_id` int(11) NOT NULL,
  `descricao` varchar(250) DEFAULT NULL,
  `data_cadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_vencimento` datetime DEFAULT NULL,
  `data_quitacao` datetime DEFAULT NULL,
  `valor` float DEFAULT NULL,
  `natureza` varchar(45) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `numero_parc` int(11) DEFAULT NULL,
  `observacao` varchar(255) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `mov_financeiro`
--

TRUNCATE TABLE `mov_financeiro`;
--
-- Extraindo dados da tabela `mov_financeiro`
--

INSERT INTO `mov_financeiro` (`id`, `forma_pgto_id`, `plano_conta_id`, `descricao`, `data_cadastro`, `data_vencimento`, `data_quitacao`, `valor`, `natureza`, `tipo`, `numero_parc`, `observacao`, `status`) VALUES
(46, 1, 3, 'Parcela 1 referente a remessa de fornecedor: 2', '2017-04-06 14:24:39', '2017-04-01 14:24:39', NULL, 200, 'D', 'D', NULL, NULL, 'A'),
(48, 1, 7, 'Retirada Para Passeios do Feriado', '2017-03-15 01:11:11', '2017-03-15 01:11:11', '2017-03-15 01:11:11', 50, 'S', 'D', NULL, NULL, 'F'),
(49, 1, 1, 'Parcela 1 referente ao pedido: 3', '2017-03-29 09:44:54', '2017-03-30 09:44:54', NULL, 150, 'R', 'C', NULL, NULL, 'A'),
(50, 1, 1, 'Parcela 1 referente ao pedido: 8', '2017-04-03 10:17:12', '2017-04-01 10:17:12', NULL, 80, 'R', 'C', NULL, NULL, 'A'),
(51, 1, 1, 'Parcela 1 referente ao pedido: 6', '2017-04-03 10:18:39', '2017-04-20 10:18:39', NULL, 50, 'R', 'C', NULL, NULL, 'A'),
(52, 1, 1, 'Parcela 1 referente ao pedido: 7', '2017-04-03 11:19:05', '2017-04-20 11:19:05', '2017-04-03 11:34:09', 80, 'R', 'C', NULL, NULL, 'F'),
(53, 1, 3, 'Parcela 1 referente a remessa de fornecedor: 3', '2017-04-03 20:16:36', '2017-04-20 20:16:36', NULL, 20, 'D', 'D', NULL, NULL, 'A'),
(58, 1, 1, 'Parcela 1 referente ao pedido: 9', '2017-04-06 11:39:57', '2017-04-20 11:39:57', NULL, 40, 'R', 'C', NULL, NULL, 'A'),
(59, 1, 1, 'Parcela 2 referente ao pedido: 9', '2017-04-06 11:39:57', '2017-04-30 11:39:57', NULL, 40, 'R', 'C', NULL, NULL, 'A'),
(60, 1, 1, 'Parcela 3 referente ao pedido: 9', '2017-04-06 11:39:57', '2017-04-05 11:39:57', NULL, 30, 'R', 'C', NULL, NULL, 'A'),
(61, 1, 3, 'Parcela 2 referente a remessa de fornecedor: 2', '2017-04-06 14:24:39', '2017-04-06 14:24:39', NULL, 60, 'D', 'D', NULL, NULL, 'A'),
(62, 1, 3, 'Parcela 3 referente a remessa de fornecedor: 2', '2017-04-06 14:24:39', '2017-04-06 14:24:39', NULL, 60, 'D', 'D', NULL, NULL, 'A'),
(67, 1, 3, 'Parcela 1 referente a remessa de fornecedor: 1', '2017-04-07 17:43:09', '2017-04-07 17:43:09', '2017-04-15 11:31:31', 62.5, 'D', 'D', NULL, NULL, 'F'),
(68, 1, 3, 'Parcela 2 referente a remessa de fornecedor: 1', '2017-04-07 17:43:10', '2017-04-14 17:43:10', NULL, 62.5, 'D', 'D', NULL, NULL, 'A'),
(95, 1, 1, 'Parcela 1 referente ao pedido: 10', '2017-04-09 22:45:13', '2017-04-10 22:45:13', '2017-04-15 22:45:43', 63.1, 'R', 'C', NULL, NULL, 'F'),
(96, 2, 1, 'Parcela 2 referente ao pedido: 10', '2017-04-09 22:45:13', '2017-04-20 22:45:13', NULL, 100, 'R', 'C', NULL, NULL, 'A'),
(97, 2, 1, 'Parcela 3 referente ao pedido: 10', '2017-04-09 22:45:14', '2017-04-30 22:45:14', NULL, 100, 'R', 'C', NULL, NULL, 'A'),
(101, 1, 1, 'Parcela 1 referente ao pedido: 11', '2017-04-10 23:24:47', '2017-04-10 23:24:47', '2017-04-10 23:25:28', 100, 'R', 'C', NULL, NULL, 'F'),
(102, 2, 1, 'Parcela 2 referente ao pedido: 11', '2017-04-10 23:24:47', '2017-05-10 23:24:47', NULL, 100, 'R', 'C', NULL, NULL, 'A'),
(103, 2, 1, 'Parcela 3 referente ao pedido: 11', '2017-04-10 23:24:47', '2017-06-10 23:24:47', NULL, 100, 'R', 'C', NULL, NULL, 'A'),
(104, 1, 3, 'Parcela 1 referente a remessa de fornecedor: 5', '2017-04-11 12:50:27', '2017-04-11 12:50:27', '2017-04-11 12:54:45', 80, 'D', 'D', NULL, NULL, 'F'),
(105, 1, 1, 'Parcela 1 referente ao pedido: 12', '2017-04-11 16:40:52', '2017-04-11 16:40:52', '2017-04-11 16:41:16', 150, 'R', 'C', NULL, NULL, 'F'),
(106, 1, 3, 'Parcela 1 referente a remessa de fornecedor: 10', '2017-04-11 16:42:36', '2017-04-11 16:42:36', '2017-04-11 16:43:15', 100, 'D', 'D', NULL, NULL, 'F');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mov_fin_pedido`
--

DROP TABLE IF EXISTS `mov_fin_pedido`;
CREATE TABLE `mov_fin_pedido` (
  `id` int(11) NOT NULL,
  `pedido_id` int(11) NOT NULL,
  `mov_financeiro_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `mov_fin_pedido`
--

TRUNCATE TABLE `mov_fin_pedido`;
--
-- Extraindo dados da tabela `mov_fin_pedido`
--

INSERT INTO `mov_fin_pedido` (`id`, `pedido_id`, `mov_financeiro_id`) VALUES
(40, 3, 49),
(41, 8, 50),
(42, 6, 51),
(43, 7, 52),
(46, 9, 58),
(47, 9, 59),
(48, 9, 60),
(75, 10, 95),
(76, 10, 96),
(77, 10, 97),
(81, 11, 101),
(82, 11, 102),
(83, 11, 103),
(84, 12, 105);

-- --------------------------------------------------------

--
-- Estrutura da tabela `mov_fin_remessa`
--

DROP TABLE IF EXISTS `mov_fin_remessa`;
CREATE TABLE `mov_fin_remessa` (
  `id` int(11) NOT NULL,
  `mov_financeiro_id` int(11) NOT NULL,
  `remessa_fornecedor_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `mov_fin_remessa`
--

TRUNCATE TABLE `mov_fin_remessa`;
--
-- Extraindo dados da tabela `mov_fin_remessa`
--

INSERT INTO `mov_fin_remessa` (`id`, `mov_financeiro_id`, `remessa_fornecedor_id`) VALUES
(7, 46, 2),
(8, 53, 3),
(11, 61, 2),
(12, 62, 2),
(17, 67, 1),
(18, 68, 1),
(19, 104, 5),
(20, 106, 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido`
--

DROP TABLE IF EXISTS `pedido`;
CREATE TABLE `pedido` (
  `id` int(11) NOT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `data_entrega` datetime DEFAULT NULL,
  `valor_total` float DEFAULT NULL,
  `valor_parcial` float DEFAULT NULL,
  `desconto` float DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `observacao` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `pedido`
--

TRUNCATE TABLE `pedido`;
--
-- Extraindo dados da tabela `pedido`
--

INSERT INTO `pedido` (`id`, `cliente_id`, `data_cadastro`, `data_entrega`, `valor_total`, `valor_parcial`, `desconto`, `status`, `observacao`) VALUES
(1, 1, '2017-03-09 01:37:41', '2017-03-11 01:37:41', 90, 80, 10, 'Itens definidos', NULL),
(2, 2, '2017-03-07 01:46:35', '2017-03-28 01:46:35', 0, 145, 0, 'Cancelado', NULL),
(3, 2, '2017-03-29 09:44:32', '2017-04-10 21:31:58', 150, NULL, 0, 'Entregue', NULL),
(4, 3, '2017-03-29 13:39:26', '2017-04-10 12:12:22', 0, NULL, 0, 'Cancelado', NULL),
(5, 3, '2017-03-29 10:18:41', '2017-04-09 00:00:00', 50, NULL, 0, 'Definir Parcelamento', NULL),
(6, 3, '2017-03-29 10:19:24', '2017-04-09 00:00:00', 50, NULL, 0, 'Pendente Entrega', NULL),
(7, 1, '2017-03-29 10:21:04', '2017-04-09 00:00:00', 80, 80, 0, 'Parcelas em Quitação', NULL),
(8, 3, '2017-03-29 10:26:32', '2017-03-29 10:26:32', 80, NULL, 0, 'Pendente Entrega', NULL),
(9, 4, '2017-04-04 12:50:38', '2017-04-20 12:50:38', 110, 0, 20, 'Pendente Entrega', NULL),
(10, 4, '2017-04-09 22:44:24', '2017-04-10 22:44:24', 263.1, 63.1, 2, 'Parcelas em Quitação', NULL),
(11, 4, '2017-04-10 23:23:50', '2017-04-14 23:29:30', 300, 100, 40, 'Entregue', NULL),
(12, 4, '2017-04-11 16:40:35', '2017-04-20 16:40:35', 150, 150, 0, 'Parcelas em Quitação', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `permissao_acl`
--

DROP TABLE IF EXISTS `permissao_acl`;
CREATE TABLE `permissao_acl` (
  `id` int(11) NOT NULL,
  `recurso_sistema_id` int(11) NOT NULL,
  `grupo_id` int(11) NOT NULL,
  `permissao` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `permissao_acl`
--

TRUNCATE TABLE `permissao_acl`;
--
-- Extraindo dados da tabela `permissao_acl`
--

INSERT INTO `permissao_acl` (`id`, `recurso_sistema_id`, `grupo_id`, `permissao`) VALUES
(1, 19, 1, 'allow'),
(3, 4, 1, 'allow'),
(4, 5, 1, 'allow'),
(5, 3, 1, 'allow'),
(8, 20, 1, 'allow'),
(9, 21, 1, 'allow'),
(10, 22, 1, 'allow'),
(11, 1, 1, 'allow'),
(12, 2, 1, 'allow'),
(13, 6, 1, 'allow'),
(14, 7, 1, 'allow');

-- --------------------------------------------------------

--
-- Estrutura da tabela `plano_conta`
--

DROP TABLE IF EXISTS `plano_conta`;
CREATE TABLE `plano_conta` (
  `id` int(11) NOT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `ativo` varchar(45) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `plano_conta`
--

TRUNCATE TABLE `plano_conta`;
--
-- Extraindo dados da tabela `plano_conta`
--

INSERT INTO `plano_conta` (`id`, `descricao`, `ativo`) VALUES
(1, 'Parcela pedido', '1'),
(2, 'Reembolso Pedido', '1'),
(3, 'Parcela remessa', '1'),
(4, 'Alimentação', '1'),
(5, 'Despesas Domética', '1'),
(6, 'Combustível', '1'),
(7, 'Despesas com Lazer', '1'),
(8, 'Acerto de Caixa', '1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

DROP TABLE IF EXISTS `produto`;
CREATE TABLE `produto` (
  `id` int(11) NOT NULL,
  `fornecedor_id` int(11) DEFAULT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `preco` float DEFAULT NULL,
  `metal` varchar(100) DEFAULT NULL,
  `peso` varchar(45) DEFAULT NULL,
  `formato` varchar(45) DEFAULT NULL,
  `pedra` varchar(255) DEFAULT NULL,
  `tipo_pedra` varchar(45) DEFAULT NULL,
  `cor_pedra` varchar(45) DEFAULT NULL,
  `dimensao_pedra` varchar(45) DEFAULT NULL,
  `garantia` varchar(45) DEFAULT NULL,
  `ativo` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `produto`
--

TRUNCATE TABLE `produto`;
--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id`, `fornecedor_id`, `nome`, `data_cadastro`, `preco`, `metal`, `peso`, `formato`, `pedra`, `tipo_pedra`, `cor_pedra`, `dimensao_pedra`, `garantia`, `ativo`) VALUES
(1, 1, 'Brinco foleado', NULL, NULL, 'ouro', '300mg', 'circular', 'granizo', 'mineral', 'prateado', '1mm', '1', 1),
(2, 2, 'Brinco foleado teste', NULL, NULL, 'ouro', '300mg', 'circular', 'granizo', 'mineral', 'prateado', '1mm', '1', 1),
(10, 2, 'Pulseira', NULL, NULL, 'cobre', '0.5mg', 'circular', '', '', '', '', '1', 1),
(11, 2, 'Relogio', NULL, NULL, 'inox', '20g', 'circular', '', '', '', '', '1', 1),
(12, 2, 'Anel', NULL, NULL, 'prata', '0.5mg', 'redondo', 'marmore', '', '', '', '1', 1),
(13, 1, 'Relogio de ouro', NULL, NULL, 'Ouro', '20g', 'Redondo', '', '', '', '', '1', 1),
(14, 1, 'Bracelete', NULL, NULL, 'Prata', '5g', 'Redondo', 'Granito', 'Mineral', 'Prateada', '', '1', 1),
(15, 3, 'Colar de perolas', NULL, NULL, 'Prata', '0,2g', 'Redondo', 'perolas', 'preciosa', 'bege', 'redonda', '1', 1),
(16, 3, 'Broche', NULL, NULL, 'Prata', '0,1g', 'redondo', 'agata', 'semi-preciosa', 'verde', '', '1', 1),
(17, 3, 'Pingente de Perola', NULL, NULL, 'Prata', '0,1g', 'Redondo', 'Perola', 'Preciosa', 'Bege', '', '1', 1),
(18, 3, 'Gargantilha', NULL, NULL, 'Prata', '0,4g', 'Redondo', 'Rubi', 'preciosa', 'vermelho', '', '1', 1),
(19, 4, 'Pircing', NULL, NULL, 'prata', '0.2g', 'palito', 'marmore', 'mineral', 'prata', '', '0', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `recurso_sistema`
--

DROP TABLE IF EXISTS `recurso_sistema`;
CREATE TABLE `recurso_sistema` (
  `id` int(11) NOT NULL,
  `url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `recurso_sistema`
--

TRUNCATE TABLE `recurso_sistema`;
--
-- Extraindo dados da tabela `recurso_sistema`
--

INSERT INTO `recurso_sistema` (`id`, `url`) VALUES
(1, 'home'),
(2, 'api.ping'),
(3, 'grupo.list'),
(4, 'grupo.create'),
(5, 'grupo.update'),
(6, 'grupo.delete'),
(7, 'permissao_acl.manager'),
(19, 'product.list'),
(20, 'product.create'),
(21, 'product.update'),
(22, 'product.delete');

-- --------------------------------------------------------

--
-- Estrutura da tabela `remessa_fornecedor`
--

DROP TABLE IF EXISTS `remessa_fornecedor`;
CREATE TABLE `remessa_fornecedor` (
  `id` int(11) NOT NULL,
  `fornecedor_id` int(11) NOT NULL,
  `valor_total` float DEFAULT NULL,
  `valor_parcial` float DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `data_entrada` datetime DEFAULT NULL,
  `data_devolucao` datetime DEFAULT NULL,
  `data_quitacao` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `remessa_fornecedor`
--

TRUNCATE TABLE `remessa_fornecedor`;
--
-- Extraindo dados da tabela `remessa_fornecedor`
--

INSERT INTO `remessa_fornecedor` (`id`, `fornecedor_id`, `valor_total`, `valor_parcial`, `data_cadastro`, `data_entrada`, `data_devolucao`, `data_quitacao`, `status`) VALUES
(1, 2, 125, 62.5, '2017-03-02 23:57:22', NULL, '2017-04-07 00:00:00', NULL, 'Devolvida'),
(2, 2, 320, 95, '2017-03-03 10:48:00', NULL, NULL, NULL, 'Devolvida'),
(3, 1, 20, NULL, '2017-03-03 10:40:09', NULL, NULL, NULL, 'Devolvida'),
(4, 1, 120, NULL, '2017-04-06 00:05:36', NULL, NULL, '2017-04-30 23:55:36', 'Devolvida'),
(5, 1, 80, 80, '2017-04-09 21:41:08', NULL, '2017-05-07 21:41:08', '2017-04-20 20:15:39', 'Devolvida'),
(8, 2, 275.5, NULL, '2017-04-07 18:08:55', NULL, '2017-04-20 18:08:55', NULL, 'Ativa'),
(9, 3, 1825, NULL, '2017-04-10 23:06:06', NULL, '2017-05-30 23:06:06', NULL, 'Ativa'),
(10, 1, 100, 100, '2017-04-11 16:36:44', NULL, '2017-04-30 16:36:44', NULL, 'Devolvida');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `grupo_id` int(11) NOT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `login` varchar(60) DEFAULT NULL,
  `senha` varchar(250) DEFAULT NULL,
  `salt` varchar(250) DEFAULT NULL,
  `ativo` int(11) DEFAULT '1',
  `foto` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `usuario`
--

TRUNCATE TABLE `usuario`;
--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `grupo_id`, `nome`, `email`, `login`, `senha`, `salt`, `ativo`, `foto`) VALUES
(1, 3, 'Erico de Oliveira Leandro', 'erico.autocad@gmail.com', 'erico.leandro', '$2y$10$W2X5eSB36.6FD5l28kjGhuAyjdVg4KsxvZoKwGGANTaTpS9cfNEMi', '.idccm.mte2r.roc@oadgicgcouocr2ug@ocaaaigomc.oioae.ocaaioero@cac', 1, '6b554a452fb22b160eaa84cd87d78d80.jpg'),
(2, 1, 'Luiz Henrique', 'rosalvo.afonso@gmail.com', 'rosalvo.afonso', '$2y$10$SKFi6j6HR6WRQcZHZGeVsOne2SRL24W2QS1pOLGcvPoBDqMJjnHA6', NULL, 1, NULL),
(3, 1, 'Lorrayne Neves de Souza', 'lorrayneneves23@gmail.com', 'lorrayne.neves', '$2y$10$wODOZZLyac2vs0Ry2BFEq.Nu63dRu/p9cdGjAfnLmGorpmScCh6Z6', NULL, 1, '16c62ee1e6ae229af29d6ca04e8b064b.jpg'),
(4, 3, 'Paloma Soares', 'erico.autocad@gmail.com', 'paloma.soares', '$2y$10$qxKwm.KLgrT/R2oNPHPcdO0nR9dlUwDkzAG46OlV19M/rOtIFLevK', NULL, 1, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_agenda_cliente1_idx` (`cliente_id`),
  ADD KEY `fk_agenda_usuario1_idx` (`usuario_id`);

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato_fornecedor`
--
ALTER TABLE `contato_fornecedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_contato_fornecedor_fornecedor1_idx` (`fornecedor_id`);

--
-- Indexes for table `endereco_cliente`
--
ALTER TABLE `endereco_cliente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_endereco_cliente_cliente1_idx` (`cliente_id`);

--
-- Indexes for table `endereco_fornecedor`
--
ALTER TABLE `endereco_fornecedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_endereco_cliente_copy1_fornecedor1_idx` (`fornecedor_id`);

--
-- Indexes for table `forma_pgto`
--
ALTER TABLE `forma_pgto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fornecedor`
--
ALTER TABLE `fornecedor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_pedido`
--
ALTER TABLE `item_pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_item_pedido_usuario1_idx` (`usuario_id`),
  ADD KEY `fk_item_pedido_pedido1_idx` (`pedido_id`),
  ADD KEY `fk_item_pedido_produto1_idx` (`produto_id`);

--
-- Indexes for table `item_remessa`
--
ALTER TABLE `item_remessa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_item_remessa_produto_estoque1_idx` (`remessa_fornecedor_id`),
  ADD KEY `fk_item_remessa_produto1_idx` (`produto_id`),
  ADD KEY `fk_item_remessa_usuario1_idx` (`usuario_id`);

--
-- Indexes for table `mov_financeiro`
--
ALTER TABLE `mov_financeiro`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mov_financeiro_forma_pgto1_idx` (`forma_pgto_id`),
  ADD KEY `fk_mov_financeiro_plano_conta1_idx` (`plano_conta_id`);

--
-- Indexes for table `mov_fin_pedido`
--
ALTER TABLE `mov_fin_pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mov_fin_pedido_pedido1_idx` (`pedido_id`),
  ADD KEY `fk_mov_fin_pedido_mov_financeiro1_idx` (`mov_financeiro_id`);

--
-- Indexes for table `mov_fin_remessa`
--
ALTER TABLE `mov_fin_remessa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mov_fin_remessa_mov_financeiro1_idx` (`mov_financeiro_id`),
  ADD KEY `fk_mov_fin_remessa_remessa_fornecedor1_idx` (`remessa_fornecedor_id`);

--
-- Indexes for table `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pedido_cliente1_idx` (`cliente_id`);

--
-- Indexes for table `permissao_acl`
--
ALTER TABLE `permissao_acl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_permissao_acl_recurso_sistema1_idx` (`recurso_sistema_id`),
  ADD KEY `fk_permissao_acl_grupo1_idx` (`grupo_id`);

--
-- Indexes for table `plano_conta`
--
ALTER TABLE `plano_conta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_produto_fornecedor1_idx` (`fornecedor_id`);

--
-- Indexes for table `recurso_sistema`
--
ALTER TABLE `recurso_sistema`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `remessa_fornecedor`
--
ALTER TABLE `remessa_fornecedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_produto_estoque_fornecedor1_idx` (`fornecedor_id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_usuario_grupo_idx` (`grupo_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `contato_fornecedor`
--
ALTER TABLE `contato_fornecedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `endereco_cliente`
--
ALTER TABLE `endereco_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `endereco_fornecedor`
--
ALTER TABLE `endereco_fornecedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `forma_pgto`
--
ALTER TABLE `forma_pgto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `fornecedor`
--
ALTER TABLE `fornecedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `grupo`
--
ALTER TABLE `grupo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `item_pedido`
--
ALTER TABLE `item_pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `item_remessa`
--
ALTER TABLE `item_remessa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `mov_financeiro`
--
ALTER TABLE `mov_financeiro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT for table `mov_fin_pedido`
--
ALTER TABLE `mov_fin_pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `mov_fin_remessa`
--
ALTER TABLE `mov_fin_remessa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `pedido`
--
ALTER TABLE `pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `permissao_acl`
--
ALTER TABLE `permissao_acl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `plano_conta`
--
ALTER TABLE `plano_conta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `recurso_sistema`
--
ALTER TABLE `recurso_sistema`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `remessa_fornecedor`
--
ALTER TABLE `remessa_fornecedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `agenda`
--
ALTER TABLE `agenda`
  ADD CONSTRAINT `fk_agenda_cliente1` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_agenda_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `contato_fornecedor`
--
ALTER TABLE `contato_fornecedor`
  ADD CONSTRAINT `fk_contato_fornecedor_fornecedor1` FOREIGN KEY (`fornecedor_id`) REFERENCES `fornecedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `endereco_cliente`
--
ALTER TABLE `endereco_cliente`
  ADD CONSTRAINT `fk_endereco_cliente_cliente1` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `endereco_fornecedor`
--
ALTER TABLE `endereco_fornecedor`
  ADD CONSTRAINT `fk_endereco_cliente_copy1_fornecedor1` FOREIGN KEY (`fornecedor_id`) REFERENCES `fornecedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `item_pedido`
--
ALTER TABLE `item_pedido`
  ADD CONSTRAINT `fk_item_pedido_pedido1` FOREIGN KEY (`pedido_id`) REFERENCES `pedido` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_pedido_produto1` FOREIGN KEY (`produto_id`) REFERENCES `produto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_pedido_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `item_remessa`
--
ALTER TABLE `item_remessa`
  ADD CONSTRAINT `fk_item_remessa_produto1` FOREIGN KEY (`produto_id`) REFERENCES `produto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_remessa_produto_estoque1` FOREIGN KEY (`remessa_fornecedor_id`) REFERENCES `remessa_fornecedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_remessa_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `mov_financeiro`
--
ALTER TABLE `mov_financeiro`
  ADD CONSTRAINT `fk_mov_financeiro_forma_pgto1` FOREIGN KEY (`forma_pgto_id`) REFERENCES `forma_pgto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mov_financeiro_plano_conta1` FOREIGN KEY (`plano_conta_id`) REFERENCES `plano_conta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `mov_fin_pedido`
--
ALTER TABLE `mov_fin_pedido`
  ADD CONSTRAINT `fk_mov_fin_pedido_mov_financeiro1` FOREIGN KEY (`mov_financeiro_id`) REFERENCES `mov_financeiro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mov_fin_pedido_pedido1` FOREIGN KEY (`pedido_id`) REFERENCES `pedido` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `mov_fin_remessa`
--
ALTER TABLE `mov_fin_remessa`
  ADD CONSTRAINT `fk_mov_fin_remessa_mov_financeiro1` FOREIGN KEY (`mov_financeiro_id`) REFERENCES `mov_financeiro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mov_fin_remessa_remessa_fornecedor1` FOREIGN KEY (`remessa_fornecedor_id`) REFERENCES `remessa_fornecedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `fk_pedido_cliente1` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `permissao_acl`
--
ALTER TABLE `permissao_acl`
  ADD CONSTRAINT `fk_permissao_acl_grupo1` FOREIGN KEY (`grupo_id`) REFERENCES `grupo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_permissao_acl_recurso_sistema1` FOREIGN KEY (`recurso_sistema_id`) REFERENCES `recurso_sistema` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `produto`
--
ALTER TABLE `produto`
  ADD CONSTRAINT `fk_produto_fornecedor1` FOREIGN KEY (`fornecedor_id`) REFERENCES `fornecedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `remessa_fornecedor`
--
ALTER TABLE `remessa_fornecedor`
  ADD CONSTRAINT `fk_produto_estoque_fornecedor1` FOREIGN KEY (`fornecedor_id`) REFERENCES `fornecedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_usuario_grupo` FOREIGN KEY (`grupo_id`) REFERENCES `grupo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
