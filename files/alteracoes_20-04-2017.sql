-- MySQL Workbench Synchronization
-- Generated: 2017-04-20 11:16
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Erico

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `odontomad01`.`cliente`
ADD COLUMN `tipo` VARCHAR(45) NULL DEFAULT 'PF' AFTER `telefone_celular`,
ADD COLUMN `CNPJ` VARCHAR(45) NULL DEFAULT NULL AFTER `tipo`,
ADD COLUMN `razao_social` VARCHAR(255) NULL DEFAULT NULL AFTER `CNPJ`,
ADD COLUMN `nome_fantasia` VARCHAR(255) NULL DEFAULT NULL AFTER `razao_social`,
ADD COLUMN `data_cadastro` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `nome_fantasia`,
ADD COLUMN `ativo` INT(11) NULL DEFAULT 1 AFTER `data_cadastro`;

ALTER TABLE `odontomad01`.`endereco_cliente`
ADD COLUMN `referencia` VARCHAR(250) NULL DEFAULT NULL AFTER `CEP`;

ALTER TABLE `odontomad01`.`fornecedor`
ADD COLUMN `data_cadastro` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `observacao`;

ALTER TABLE `odontomad01`.`endereco_fornecedor`
ADD COLUMN `referencia` VARCHAR(250) NULL DEFAULT NULL AFTER `CEP`;

ALTER TABLE `odontomad01`.`pedido`
ADD COLUMN `usuario_id` INT(11) NULL DEFAULT NULL AFTER `cliente_id`,
ADD INDEX `fk_pedido_usuario1_idx` (`usuario_id` ASC);
ALTER TABLE `odontomad01`.`pedido`
ADD CONSTRAINT `fk_pedido_usuario1`
  FOREIGN KEY (`usuario_id`)
  REFERENCES `odontomad01`.`usuario` (`id`)
  ON DELETE SET NULL
  ON UPDATE SET NULL

ALTER TABLE `odontomad01`.`remessa_fornecedor`
ADD COLUMN `usuario_id` INT(11) NULL DEFAULT NULL AFTER `fornecedor_id`,
ADD INDEX `fk_remessa_fornecedor_usuario1_idx` (`usuario_id` ASC);
ALTER TABLE `odontomad01`.`remessa_fornecedor`
ADD CONSTRAINT `fk_remessa_fornecedor_usuario1`
  FOREIGN KEY (`usuario_id`)
  REFERENCES `odontomad01`.`usuario` (`id`)
  ON DELETE SET NULL
  ON UPDATE SET NULL


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
